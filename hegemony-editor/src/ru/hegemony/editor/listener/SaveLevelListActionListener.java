package ru.hegemony.editor.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import ru.hegemony.common.helper.DataHelper;
import ru.hegemony.editor.data.EdgeType;
import ru.hegemony.editor.data.EntityType;
import ru.hegemony.editor.data.LevelData;
import ru.hegemony.editor.data.LevelHolder;
import ru.hegemony.editor.data.LevelListHolder;
import ru.hegemony.editor.helper.MessageHelper;
import ru.hegemony.editor.render.ProgressBarDialog;
import ru.hegemony.xml.data.CountryEntity;
import ru.hegemony.xml.data.ItemEntity;
import ru.hegemony.xml.data.LevelType;
import ru.hegemony.xml.data.ObjectFactory;

import com.sun.xml.bind.marshaller.NamespacePrefixMapper;

public class SaveLevelListActionListener extends ProgressBarDialog implements ActionListener {
	private static final String ITEMS_IMG_PATH = "res/img/items";
	private static final String COUNTRIES_IMG_PATH = "res/img/countries";
	private static final String IMG_EXT = ".png";
	
	private LevelListHolder levelListHolder;
	private JFrame parentFrame;
	
	private Marshaller marshaller;
	private Map<String, InputStream> zipMap;
	
	private String resultFilename;

	public SaveLevelListActionListener(LevelListHolder levelListHolder, JFrame parentFrame) {
		super(parentFrame, false);
		this.levelListHolder = levelListHolder;
		this.parentFrame = parentFrame;
		this.zipMap = new HashMap<String, InputStream>();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		JFileChooser fc = new JFileChooser();
		fc.setCurrentDirectory(new File("."));
		fc.setDialogTitle("Please choose a directory...");
		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		fc.setAcceptAllFileFilterUsed(false);
		
		if(fc.showSaveDialog(parentFrame) == JFileChooser.APPROVE_OPTION) {
			try {
				createMarshaller();
				resultFilename = fc.getSelectedFile().getAbsolutePath() + "/wc_level_list.wcl";
				executeTaskWithProgressBar();
				MessageHelper.showInfo(
						"���������� ������ �������", 
						"���� ������� ������� �������� �� ������ " + resultFilename, 
						parentFrame);
			} catch (Exception ex) {
				MessageHelper.showError("������ ���������� ������ �������", parentFrame, ex);
			}
		}
	}
	
	private void createMarshaller() throws JAXBException {
		marshaller = JAXBContext.newInstance("ru.hegemony.xml.data").createMarshaller();
		NamespacePrefixMapper mapper = new NamespacePrefixMapper() {
			public String getPreferredPrefix(String namespaceUri, String suggestion, boolean requirePrefix) {
				return "tns";
			}
		};
		marshaller.setProperty("com.sun.xml.bind.namespacePrefixMapper", mapper);
	}
	
	private void addToZipMap(String image, EntityType entityType) throws JAXBException, IOException {
		for(EdgeType edge : EdgeType.values()) {
			FileInputStream fis = null;
			if(entityType.equals(EntityType.ITEM)) {
				fis = new FileInputStream(ITEMS_IMG_PATH + "/" + image + edge.getSuffix() + IMG_EXT);
			} else if(entityType.equals(EntityType.COUNTRY)) {
				fis = new FileInputStream(COUNTRIES_IMG_PATH + "/" + image + edge.getSuffix() + IMG_EXT);
			}
			if(fis != null) {
				zipMap.put(image + edge.getSuffix() + IMG_EXT, fis);
			}
		}
	}
	
	private void addToZipMap(JAXBElement<?> element, String elementName) throws JAXBException, IOException {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		marshaller.marshal(element, bos);
		ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
		zipMap.put(elementName, bis);
		bos.close();
	}

	private JAXBElement<LevelType> createLevelShortInfo(LevelData value) {
		ObjectFactory objectFactory = new ObjectFactory();
		LevelType levelType = objectFactory.createLevelType();
		levelType.getCountries().addAll(value.getCountries());
		levelType.setEventDesc(value.getEventDesc());
		levelType.getItems().addAll(value.getItems());
		levelType.getCountriesTurns().addAll(value.getTurns());
		levelType.getItemsEffects().addAll(value.getItemsEffects());
		return objectFactory.createLevel(levelType);
	}

	@Override
	protected void doWork(Task worker) throws Exception {
		int size = 2;
		for(LevelHolder level : levelListHolder.getLevelTypeList()) {
			size = size
					+ 1
					+ level.getLevel().getCountries().size()
					+ level.getLevel().getItems().size();
		}
		
		double incValue = (double) ( (double) 100.0 / (double) (size) );
		double allProgressValue = 0.0;
		for(LevelHolder level : levelListHolder.getLevelTypeList()) {
			addToZipMap(createLevelShortInfo(level.getLevel()), level.getFilename());
			allProgressValue += incValue;
			worker.setTaskProgress(allProgressValue);
			
			for(CountryEntity country : level.getLevel().getCountries()) {
				addToZipMap(country.getImage(), EntityType.COUNTRY);
				allProgressValue += incValue;
				worker.setTaskProgress(allProgressValue);
			}
			for(ItemEntity item : level.getLevel().getItems()) {
				addToZipMap(item.getImage(), EntityType.ITEM);
				allProgressValue += incValue;
				worker.setTaskProgress(allProgressValue);
			}
		}
		ObjectFactory objectFactory = new ObjectFactory();
		addToZipMap(objectFactory.createLevelListRoot(levelListHolder.getLevelList()), "level_list.xml");
		allProgressValue += incValue;
		worker.setTaskProgress(allProgressValue);
		DataHelper.putToZip(zipMap, resultFilename);
		allProgressValue += incValue;
		worker.setTaskProgress(allProgressValue);
	}
}
