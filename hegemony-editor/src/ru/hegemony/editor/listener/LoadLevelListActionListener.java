package ru.hegemony.editor.listener;

import java.awt.FileDialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JList;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.io.FileUtils;

import ru.hegemony.common.helper.DataHelper;
import ru.hegemony.editor.GuiEditorCountriesWnd;
import ru.hegemony.editor.GuiEditorItemsWnd;
import ru.hegemony.editor.data.EdgeType;
import ru.hegemony.editor.data.LevelData;
import ru.hegemony.editor.data.LevelHolder;
import ru.hegemony.editor.data.LevelListHolder;
import ru.hegemony.editor.helper.Converter;
import ru.hegemony.editor.helper.MessageHelper;
import ru.hegemony.editor.render.ProgressBarDialog;
import ru.hegemony.xml.data.CountryEntity;
import ru.hegemony.xml.data.ItemEntity;
import ru.hegemony.xml.data.LevelListType;
import ru.hegemony.xml.data.LevelType;

public class LoadLevelListActionListener extends ProgressBarDialog implements ActionListener {
	private static final String ITEMS_IMG_PATH = "res/img/items";
	private static final String COUNTRIES_IMG_PATH = "res/img/countries";
	private static final String IMG_EXT = ".png";
	private static final String LEVEL_LIST_FILE = "level_list.xml";
	
	private LevelListHolder levelListHolder;
	private JFrame parentFrame;
	private JList list;
	private Set<String> addedCountrySet = new HashSet<String>();
	private Set<String> addedItemsSet = new HashSet<String>();
	
	private Unmarshaller unmarshaller;
	
	private Map<String, InputStream> zipMap;

	public LoadLevelListActionListener(LevelListHolder levelListHolder, JFrame parentFrame, JList list) {
		super(parentFrame, false);
		this.levelListHolder = levelListHolder;
		this.parentFrame = parentFrame;
		this.list = list;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		FileDialog fdlg = new FileDialog(parentFrame, "Open file", FileDialog.LOAD);
		fdlg.setVisible(true);
		
		String filename = fdlg.getFile();
		if(filename != null) {
			try {
				String path = fdlg.getDirectory() + "" + fdlg.getFile();
				zipMap = DataHelper.readZip(path);
				
				unmarshaller = JAXBContext.newInstance("ru.hegemony.xml.data").createUnmarshaller();
				JAXBElement<LevelListType> levelListElement = unmarshaller.unmarshal(new StreamSource(zipMap.get(LEVEL_LIST_FILE)), LevelListType.class);
				LevelListType levelList = levelListElement.getValue();
				
				executeTaskWithProgressBar();
				
				levelListHolder.setLevelList(levelList);
				levelListHolder.setLevelDir(FileUtils.getTempDirectoryPath());
				
				String[] levels = new String[levelList.getLevel().size()];
				for(int i = 0; i < levels.length; i++) {
					levels[i] = levelList.getLevel().get(i).getTitle();
				}
				
				list.setListData(levels);
			} catch (Exception ex) {
				MessageHelper.showError("������ �������� ������ �������", parentFrame, ex);
			}
		}
	}

	private File writeFileFromInputStream(String path, InputStream inputStream) throws IOException {
		OutputStream outputStream = null;
		File result = new File(path);

		try {
			outputStream = new FileOutputStream(result);
			int read = 0;
			byte[] bytes = new byte[1024];
			while ((read = inputStream.read(bytes)) != -1) {
				outputStream.write(bytes, 0, read);
			}
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

			}
		}
		return result;
	}

	@Override
	protected void doWork(Task worker) throws Exception {
		double incValue = (double) ( (double) 100.0 / (double) (zipMap.size() - 1) );
		double allProgressValue = 0.0;
		System.out.println("Start loading level list...");
		for(Entry<String, InputStream> entry : zipMap.entrySet()) {
			if(
				entry.getKey().toLowerCase().endsWith(".xml") 
				&& !entry.getKey().toLowerCase().equals(LEVEL_LIST_FILE)
				) 
			{
				long commonStart = System.currentTimeMillis();
				System.out.println("==> Loading level " + entry.getKey());
				JAXBElement<LevelType> levelElement = unmarshaller.unmarshal(new StreamSource(entry.getValue()), LevelType.class);
				LevelType level = levelElement.getValue();
				LevelHolder levelHolder = getLevelHolderByFilename(levelListHolder.getLevelTypeList(), entry.getKey());
				levelHolder.getLevel().setCountries(level.getCountries());
				levelHolder.getLevel().setEventDesc(level.getEventDesc());
				levelHolder.getLevel().setItems(level.getItems());
				levelHolder.getLevel().setTurns(level.getCountriesTurns());
				levelHolder.getLevel().setItemsEffects(level.getItemsEffects());
				allProgressValue += incValue;
				worker.setTaskProgress(allProgressValue);
				
				long resStart = System.currentTimeMillis();
				for(CountryEntity country : levelHolder.getLevel().getCountries()) {
					if(!addedCountrySet.contains(country.getImage() + IMG_EXT)) {
						for(EdgeType edge : EdgeType.values()) {
							writeFileFromInputStream(
									COUNTRIES_IMG_PATH + "/" + country.getImage() + edge.getSuffix() + IMG_EXT, 
									zipMap.get(country.getImage() + edge.getSuffix() + IMG_EXT));
							allProgressValue += incValue;
							worker.setTaskProgress(allProgressValue);
						}
						GuiEditorCountriesWnd.saveCountryToDict(Converter.createCountryShortData(country));
						addedCountrySet.add(country.getImage() + IMG_EXT);
					}
				}
				for(ItemEntity item : levelHolder.getLevel().getItems()) {
					if(!addedItemsSet.contains(item.getImage() + IMG_EXT)) {
						for(EdgeType edge : EdgeType.values()) {
							writeFileFromInputStream(
									ITEMS_IMG_PATH + "/" + item.getImage() + edge.getSuffix() + IMG_EXT, 
									zipMap.get(item.getImage() + edge.getSuffix() + IMG_EXT));
							allProgressValue += incValue;
							worker.setTaskProgress(allProgressValue);
						}
						GuiEditorItemsWnd.saveItemToDict(Converter.createItemShortData(item));
						addedItemsSet.add(item.getImage() + IMG_EXT);
					}
				}
				System.out.println("Copy resources time: " + (System.currentTimeMillis() - resStart) + " ms.");
				System.out.println("<== End loading level " + entry.getKey() + ". Elapsed time: " + (System.currentTimeMillis() - commonStart) + " ms.");
			}
		}
	}
	
	private LevelHolder getLevelHolderByFilename(List<LevelHolder> holderList, String filename) {
		LevelHolder currentHolder = null;
		for(LevelHolder level : levelListHolder.getLevelTypeList()) {
			if(level.getFilename().contains(filename)) {
				currentHolder = level;
			}
		}
		if(currentHolder == null) {
			currentHolder = new LevelHolder();
			if(filename.toLowerCase().endsWith(".xml")) {
				currentHolder.setFilename(filename);
			} else{
				currentHolder.setFilename(filename + ".xml");
			}
			currentHolder.setLevel(new LevelData());
			levelListHolder.getLevelTypeList().add(currentHolder);
		}
		return currentHolder;
	}
}
