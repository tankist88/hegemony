package ru.hegemony.editor;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

import ru.hegemony.editor.data.LevelListHolder;
import ru.hegemony.editor.helper.MessageHelper;
import ru.hegemony.editor.helper.VersionHelper;
import ru.hegemony.editor.listener.LoadLevelListActionListener;
import ru.hegemony.editor.listener.SaveLevelListActionListener;

public class GuiEditorMainWnd {

	private JFrame frame;
	
	private LevelListHolder levelListHolder;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GuiEditorMainWnd window = new GuiEditorMainWnd();
					window.frame.setVisible(true);
				} catch (Exception e) {
					MessageHelper.showError("������ ������� ����������", null, e);
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GuiEditorMainWnd() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		levelListHolder = new LevelListHolder();
		
		frame = new JFrame();
		frame.setTitle("�������� ������� ��������� - ������ " + VersionHelper.getVersion());
		frame.setBounds(200, 200, 470, 320);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new GridLayout(0, 2, 10, 10));
		
		JPanel panelMenu = new JPanel();
		frame.getContentPane().add(panelMenu);
		panelMenu.setLayout(new GridLayout(0, 1, 5, 5));
		panelMenu.setBorder(BorderFactory.createEmptyBorder(10, 10, 0, 0));
		
		JPanel panelView = new JPanel();
		frame.getContentPane().add(panelView);
		panelView.setLayout(new BorderLayout(0, 0));
		panelView.setBorder(BorderFactory.createEmptyBorder(10, 0, 10, 10));
		
		JLabel lblTitle = new JLabel("��������� - �������� �������");
		lblTitle.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblTitle.setHorizontalAlignment(SwingConstants.CENTER);
		
		final JList list = new JList();
		list.setVisibleRowCount(17);
		panelView.add(
				new JScrollPane(
						list,
						JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
						JScrollPane.HORIZONTAL_SCROLLBAR_NEVER
					), 
				BorderLayout.NORTH);
		
		JButton btnOpenDialog = new JButton("����� �������");
		btnOpenDialog.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new GuiEditorLevelDialog(frame, list, levelListHolder, true);
			}
		});
		
		JButton btnSave = new JButton("���������");
		btnSave.addActionListener(new SaveLevelListActionListener(levelListHolder, frame));
		
		JButton btnEdit = new JButton("�������������");
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(list.getSelectedIndex() >= 0) {
					new GuiEditorLevelDialog(frame, list, levelListHolder, false);
				}
			}
		});
		
		JButton btnLoad = new JButton("��������� ������ �������");
		btnLoad.addActionListener(new LoadLevelListActionListener(levelListHolder, frame, list));
		
		JButton opinionEdit = new JButton("���������� �������� ������");
		opinionEdit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				GuiEditorOpinionWnd wnd = new GuiEditorOpinionWnd();
				wnd.getFrame().setVisible(true);
			}
		});
		JButton itemEdit = new JButton("���������� ���������");
		itemEdit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				GuiEditorItemsWnd wnd = new GuiEditorItemsWnd();
				wnd.getFrame().setVisible(true);
			}
		});
		JButton countryEdit = new JButton("���������� �����");
		countryEdit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				GuiEditorCountriesWnd wnd = new GuiEditorCountriesWnd();
				wnd.getFrame().setVisible(true);
			}
		});
		
		panelMenu.add(lblTitle);
		panelMenu.add(btnOpenDialog);
		panelMenu.add(btnEdit);
		panelMenu.add(btnSave);
		panelMenu.add(btnLoad);
		panelMenu.add(opinionEdit);
		panelMenu.add(itemEdit);
		panelMenu.add(countryEdit);
	}
}
