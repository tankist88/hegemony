package ru.hegemony.editor;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.lang3.StringUtils;

import ru.hegemony.editor.data.CountryShortData;
import ru.hegemony.editor.data.EdgeType;
import ru.hegemony.editor.data.EntityType;
import ru.hegemony.editor.data.FlagImage;
import ru.hegemony.editor.render.JTextFieldLimit;
import ru.hegemony.editor.render.TextWithImageListRenderer;
import ru.hegemony.xml.data.Countries;
import ru.hegemony.xml.data.CountryEntity;
import ru.hegemony.xml.data.ObjectFactory;

public class GuiEditorCountriesWnd extends DictionaryWithImageEditorWnd {
	private static final int MAX_COUNTRY_NAME_SIZE = 20;
	private static final int MAX_COUNTRY_LANG_SIZE = 20;
	private static final int MAX_COUNTRY_SQUARE_SIZE = 20;
	private static final int MAX_COUNTRY_WEAKNESS_SIZE = 3;
	private static final int MAX_DESC_SIZE = 1000;
	
	private JTextField countryNameText;
	private JTextField countryLangText;
	private JTextField countrySquareText;
	private JTextField countryWeaknessText;
	private JComboBox countryComboBox;
	private JTextArea descTa;
	
	private DefaultComboBoxModel countryComboBoxModel = new DefaultComboBoxModel();

	public GuiEditorCountriesWnd() {
		super("���������� ����� � ����������", 450, 470);
	}
	
	protected GuiEditorCountriesWnd(String title, int width, int height) {
		super(title, width, height);
	}
	
	public static void main(String args[]) {
		DictionaryEditorWnd.launch(new GuiEditorCountriesWnd());
	}
	
	public static void saveCountryToDict(CountryShortData data) throws JAXBException {
		String path = DICT_PATH + EntityType.COUNTRY.getDictFileName();
		saveCountryToDict(new File(path), data);
	}
	
	public static void saveCountryToDict(File file, CountryShortData data) throws JAXBException {
		ObjectFactory objectFactory = new ObjectFactory();
		
		Countries countries = null;
		try {
			Unmarshaller unmarshaller = XML_CONTEXT.createUnmarshaller();
			JAXBElement<Countries> element = unmarshaller.unmarshal(new StreamSource(file), Countries.class);
			countries = element.getValue();
		} catch(Exception ex) {
			countries = objectFactory.createCountries();
		}
		
		CountryEntity foundCoutnry = null;
		for(CountryEntity country : countries.getCountry()) {
			if(country.getImage().equals(data.getImage())) {
				foundCoutnry = country;
				break;
			}
		}
		
		int nextCode = (foundCoutnry != null) ? foundCoutnry.getCode() : countries.getCountry().size() + 1;
		
		CountryEntity countryForAdd = objectFactory.createCountryEntity();
		countryForAdd.setCode(nextCode);
		countryForAdd.setImage(data.getImage());
		countryForAdd.setName(data.getName());
		countryForAdd.setLanguageTitle(data.getLang());
		countryForAdd.setSquare(data.getSquare());
		countryForAdd.setWeaknessPercent(data.getWeaknessPercent());
		countryForAdd.setDesc(data.getDesc());
		
		if(foundCoutnry != null) {
			countries.getCountry().remove(foundCoutnry);
		}
		countries.getCountry().add(countryForAdd);
		
		Marshaller marshaller = XML_CONTEXT.createMarshaller();
		marshaller.setProperty("com.sun.xml.bind.namespacePrefixMapper", getNamespacePrefixMapper());
		marshaller.marshal(objectFactory.createCountries(countries), file);
	}
	
	@Override
	protected void saveAction(File file) throws JAXBException {
		CountryShortData data = new CountryShortData();
		data.setImage(getSelectedImage());
		data.setLang(countryLangText.getText());
		data.setName(countryNameText.getText());
		data.setSquare(countrySquareText.getText());
		data.setWeaknessPercent(Integer.parseInt(countryWeaknessText.getText()));
		data.setDesc(descTa.getText());
		saveCountryToDict(file, data);
	}
	
	@Override
	protected void initView(JPanel parrentPanel) {
		super.initView(parrentPanel);
		JPanel countryNamePanel = new JPanel();
		countryNamePanel.setLayout(new BoxLayout(countryNamePanel, BoxLayout.X_AXIS));
		countryNameText = new JTextField();
		countryNameText.setPreferredSize(new Dimension(200, 30));
		countryNameText.setDocument(new JTextFieldLimit(MAX_COUNTRY_NAME_SIZE));
		JLabel countryNameLabel = new JLabel("�������� ������");
		countryNameLabel.setPreferredSize(new Dimension(130, 30));
		countryNamePanel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
		countryNamePanel.add(countryNameLabel);
		countryNamePanel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
		countryNamePanel.add(countryNameText);
		countryNamePanel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
		
		JPanel countryLangPanel = new JPanel();
		countryLangPanel.setLayout(new BoxLayout(countryLangPanel, BoxLayout.X_AXIS));
		countryLangText = new JTextField();
		countryLangText.setPreferredSize(new Dimension(200, 30));
		countryLangText.setDocument(new JTextFieldLimit(MAX_COUNTRY_LANG_SIZE));
		JLabel countryLangLabel = new JLabel("����");
		countryLangLabel.setPreferredSize(new Dimension(130, 30));
		countryLangPanel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
		countryLangPanel.add(countryLangLabel);
		countryLangPanel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
		countryLangPanel.add(countryLangText);
		countryLangPanel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
		
		JPanel countrySquarePanel = new JPanel();
		countrySquarePanel.setLayout(new BoxLayout(countrySquarePanel, BoxLayout.X_AXIS));
		countrySquareText = new JTextField();
		countrySquareText.setPreferredSize(new Dimension(200, 30));
		countrySquareText.setDocument(new JTextFieldLimit(MAX_COUNTRY_SQUARE_SIZE));
		JLabel countrySquareLabel = new JLabel("�������");
		countrySquareLabel.setPreferredSize(new Dimension(130, 30));
		countrySquarePanel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
		countrySquarePanel.add(countrySquareLabel);
		countrySquarePanel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
		countrySquarePanel.add(countrySquareText);
		countrySquarePanel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
		
		JPanel countryWeaknessPanel = new JPanel();
		countryWeaknessPanel.setLayout(new BoxLayout(countryWeaknessPanel, BoxLayout.X_AXIS));
		countryWeaknessText = new JTextField();
		countryWeaknessText.setPreferredSize(new Dimension(200, 30));
		countryWeaknessText.setDocument(new JTextFieldLimit(MAX_COUNTRY_WEAKNESS_SIZE));
		JLabel countryWeaknessLabel = new JLabel("����. �������� (%)");
		countryWeaknessLabel.setPreferredSize(new Dimension(130, 30));
		countryWeaknessPanel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
		countryWeaknessPanel.add(countryWeaknessLabel);
		countryWeaknessPanel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
		countryWeaknessPanel.add(countryWeaknessText);
		countryWeaknessPanel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
		
		JPanel flagPanel = new JPanel();
		flagPanel.setLayout(new BoxLayout(flagPanel, BoxLayout.X_AXIS));
		JLabel flagInfoLabel = new JLabel("��������� ����");
		flagInfoLabel.setPreferredSize(new Dimension(130, 30));
		JPanel flagCenterPanel = new JPanel();
		flagCenterPanel.setPreferredSize(new Dimension(200, 80));
		flagCenterPanel.add(getImageLabel());
		flagPanel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
		flagPanel.add(flagInfoLabel);
		flagPanel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
		flagPanel.add(flagCenterPanel);
		flagPanel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
		
		JPanel countryComboBoxPanel = new JPanel();
		countryComboBoxPanel.setLayout(new BoxLayout(countryComboBoxPanel, BoxLayout.X_AXIS));
		countryComboBox = new JComboBox();
		countryComboBox.setRenderer(new TextWithImageListRenderer(EntityType.FLAG));
		countryComboBox.setPreferredSize(new Dimension(80,40));
		countryComboBox.setBackground(Color.WHITE);
		countryComboBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				FlagImage selectedFlagImage = (FlagImage) countryComboBox.getSelectedItem();
				if(selectedFlagImage != null) {
					setImage(selectedFlagImage.getImage(), EntityType.FLAG);
				}
			}
		});
		initCountryComboBox();
		JButton addImageButton = new JButton();
		addImageButton.setText("��������� �� �����");
		addImageButton.setPreferredSize(new Dimension(190,40));
		addImageButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				loadImageFromFile(EntityType.FLAG);
				initCountryComboBox();
			}
		});
		
		JPanel countryComboBoxCenterPanel = new JPanel();
		countryComboBoxCenterPanel.setPreferredSize(new Dimension(200, 50));
		countryComboBoxCenterPanel.add(countryComboBox);
		countryComboBoxCenterPanel.add(addImageButton);
		
		JLabel countryFlagLabel = new JLabel("�������� ����");
		countryFlagLabel.setPreferredSize(new Dimension(130, 40));
		countryComboBoxPanel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
		countryComboBoxPanel.add(countryFlagLabel);
		countryComboBoxPanel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
		countryComboBoxPanel.add(countryComboBoxCenterPanel);
		countryComboBoxPanel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
		
		descTa = new JTextArea(3, 37);
		descTa.setEditable(true);
		descTa.setLineWrap(true);
		descTa.setDocument(new JTextFieldLimit(MAX_DESC_SIZE));
		
		JPanel descPanel = new JPanel();
		JPanel descLabelPanel = new JPanel();
		descLabelPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
		descLabelPanel.add(new JLabel("��������"));
		descPanel.add(descLabelPanel);
		descPanel.setPreferredSize(new Dimension(300, 100));
		descPanel.add(
				new JScrollPane(
					descTa,
					JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
					JScrollPane.HORIZONTAL_SCROLLBAR_NEVER
				)
			);
		
		parrentPanel.add(countryNamePanel);
		parrentPanel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
		parrentPanel.add(countryLangPanel);
		parrentPanel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
		parrentPanel.add(countrySquarePanel);
		parrentPanel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
		parrentPanel.add(countryWeaknessPanel);
		parrentPanel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
		parrentPanel.add(flagPanel);
		parrentPanel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
		parrentPanel.add(countryComboBoxPanel);
		parrentPanel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
		parrentPanel.add(descPanel);
		parrentPanel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
	}
	
	private void initCountryComboBox() {
		countryComboBoxModel = new DefaultComboBoxModel();
		File folder = new File(IMG_PATH + EntityType.FLAG.getResSubDir());
		File[] listOfFiles = folder.listFiles();
		
		for(File file : listOfFiles) {
			if(file.isFile() && !EdgeType.fileHasDpiSuffix(file.getName())) {
				String[] filename = file.getName().split("\\.");
				countryComboBoxModel.addElement(new FlagImage(filename[0]));
			}
		}
		countryComboBox.setModel(countryComboBoxModel);
	}
	
	@Override
	protected void clearFields() {
		countryNameText.setText(null);
		countryLangText.setText(null);
		countrySquareText.setText(null);
		countryWeaknessText.setText(null);
		descTa.setText(null);
		clearImage(EntityType.FLAG);
	}
	
	@Override
	protected boolean allRequiredFieldsSetted() {
		String countryName = countryNameText.getText();
		String countryLang = countryLangText.getText();
		String countrySquare = countrySquareText.getText();
		String countryWeakness = countryWeaknessText.getText();
		String desc = descTa.getText();
		return 
				!StringUtils.isEmpty(countryName) && 
				!StringUtils.isEmpty(countryLang) && 
				!StringUtils.isEmpty(countrySquare) &&
				!StringUtils.isEmpty(countryWeakness) &&
				!StringUtils.isEmpty(desc) &&
				!StringUtils.isEmpty(getSelectedImage()) && !getSelectedImage().equalsIgnoreCase(EMPTY_IMAGE);
	}

	@Override
	protected EntityType getEntityType() {
		return EntityType.COUNTRY;
	}
}
