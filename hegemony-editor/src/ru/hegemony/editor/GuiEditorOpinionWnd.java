package ru.hegemony.editor;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.lang3.StringUtils;

import ru.hegemony.editor.data.EntityType;
import ru.hegemony.editor.data.OpinionShortData;
import ru.hegemony.xml.data.ObjectFactory;
import ru.hegemony.xml.data.OpinionTemplateEntity;
import ru.hegemony.xml.data.PatternType;

import com.inet.jortho.SpellChecker;

public class GuiEditorOpinionWnd extends DictionaryEditorWnd {
	public static final String[] SCALE_ITEM_COUNT = {
		"���� �������",
		"��� ��������",
		"��� ��������",
		"������ ��������",
		"���� ���������",
	};
	
	public static final String[] SCALE_OPINION = {
		"������ ������",
		"�������������",
		"������",
		"��������� �� ���������",
		"�� ���������",
		"���������� �����",
		"����� �����",
		"�����",
		"�� ������",
		"�� �� ��� �� ��������",
		"�����������",
		"������ �� ����� ��� �����",
		"����� ���� � �����",
		"��������� ���������",
		"����� �����������",
		"������",
		"����� ������",
		"������������",
		"�����������",
		"������ ��������",
	};
	
	public static final String[] FOR_BEST_ITEM_SET_BAD = {
		"������� �����",
		"�������� ���� ��",
		"��������",
		"�������� �������� �� ����� �������:",
		"���� �����������:"
	};
	
	public static final String[] FOR_BEST_ITEM_SET_GOOD = {
		"�� �� �������������� ������ ��������:",
		"��� ������� ������� ����� ����������������:", 
		"�� �� ����� ���������� �� ����� ��������:"
	};
	
	private JComboBox comboBoxOpinion;
	private JComboBox comboBoxItems;
	private JEditorPane text;
	
	public GuiEditorOpinionWnd() {
		super("���������� ������ � ����������", 670, 400);
	}

	protected GuiEditorOpinionWnd(String title, int width, int height) {
		super(title, width, height);
	}
	
	public static void main(String args[]) {
		DictionaryEditorWnd.launch(new GuiEditorOpinionWnd());
	}
	
	public static void saveOpinionToDict(List<OpinionShortData> dataList) throws JAXBException {
		String path = DICT_PATH + EntityType.OPINION.getDictFileName();
		saveOpinionToDict(new File(path), dataList);
	}
	
	public static void saveOpinionToDict(File file, OpinionShortData data) throws JAXBException {
		if(data != null) {
			List<OpinionShortData> dataList = new ArrayList<OpinionShortData>();
			dataList.add(data);
			saveOpinionToDict(file, dataList);
		}
	}
	
	public static void saveOpinionToDict(File file, List<OpinionShortData> dataList) throws JAXBException {
		if(dataList == null || dataList.size() == 0) {
			return;
		}
		
		ObjectFactory objectFactory = new ObjectFactory();
		
		PatternType patternType = null;
		try {
			Unmarshaller unmarshaller = XML_CONTEXT.createUnmarshaller();
			JAXBElement<PatternType> element = unmarshaller.unmarshal(new StreamSource(file), PatternType.class);
			patternType = element.getValue();
		} catch(Exception ex) {
			patternType = objectFactory.createPatternType();
		}
		for(OpinionShortData data : dataList) {
			addPattern(data.getItemCount(), data.getGrade(), patternType, data.getText());
		}
		savePatternToXML(patternType, file);
	}

	@Override
	protected void saveAction(File file) throws JAXBException {
		OpinionShortData data = new OpinionShortData();
		data.setItemCount(comboBoxItems.getSelectedIndex() + 1);
		data.setGrade(comboBoxOpinion.getSelectedIndex() + 1);
		data.setText(text.getText());
		saveOpinionToDict(file, data);
	}

	@Override
	protected void initMenu(JPanel parrentPanel) {
		super.initMenu(parrentPanel);
		
		JButton genAllButton = new JButton("������������� ������ �������������");
		genAllButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				generateAllOpinions();	
			}
		});
		
		parrentPanel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
		parrentPanel.add(genAllButton);
	}
	
	@Override
	protected void initView(JPanel parrentPanel) {
		JPanel comboBoxPanel = new JPanel();
		comboBoxPanel.setLayout(new BoxLayout(comboBoxPanel, BoxLayout.X_AXIS));
		
		String[] scaleWithNums = new String[SCALE_OPINION.length];
		for(int i = 0; i < scaleWithNums.length; i++) {
			scaleWithNums[i] = (i + 1) + " - " + SCALE_OPINION[i];
		}
		
		comboBoxOpinion = new JComboBox(scaleWithNums);
		comboBoxOpinion.setBackground(Color.WHITE);
		comboBoxItems = new JComboBox(SCALE_ITEM_COUNT);
		comboBoxItems.setBackground(Color.WHITE);
		
		comboBoxPanel.add(comboBoxOpinion);
		comboBoxPanel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
		comboBoxPanel.add(comboBoxItems);
		
		text = new JTextPane();
		text.setText("������������ �����������, <a1> <i1> � ������� �������� �� �������������. ������� ����� ::bis::.");
		SpellChecker.registerDictionaries(null, null);
		SpellChecker.register(text);
		JScrollPane scrollPane = new JScrollPane(
				text,
				JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER
			);
		scrollPane.setPreferredSize(new Dimension(300, 220));
		
		JPanel infoPanel = new JPanel();
		infoPanel.setLayout(new GridLayout(4, 1));
		JLabel infoLabel = new JLabel("  ������� ������ ������ � ��������, � ����� ��������� ��� ��������� � ����� �� ������ ���������� ��������� ������������.");
		infoLabel.setFont(new Font("SansSerif", Font.PLAIN, 10));
		JLabel infoItemLabel1 = new JLabel("  �������� � ����� ������ �������� ��������� � �������� ��� ����. ��� ����� � ����� ����� ������ ����� ��������:");
		infoItemLabel1.setFont(new Font("SansSerif", Font.PLAIN, 10));
		JLabel infoItemLabel2 = new JLabel("  <a1> � <i1>, ��� a (action) - ��������, i (item) - �������� ��������, � ����� �������� ���������� ����� �������� � �����������.");
		infoItemLabel2.setFont(new Font("SansSerif", Font.PLAIN, 10));
		JLabel infoItemLabel3 = new JLabel("  ::bis:: (Best Item Set) - � �������� ���� ����� ������� �� ������ ���������� �� ������� ������ ������ ��������� ��� ������.");
		infoItemLabel3.setFont(new Font("SansSerif", Font.PLAIN, 10));
		infoPanel.add(infoLabel);
		infoPanel.add(infoItemLabel1);
		infoPanel.add(infoItemLabel2);
		infoPanel.add(infoItemLabel3);
		
		parrentPanel.add(comboBoxPanel);
		parrentPanel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
		parrentPanel.add(infoPanel);
		parrentPanel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
		parrentPanel.add(scrollPane);
	}
	
	@Override
	protected boolean allRequiredFieldsSetted() {
		return !StringUtils.isEmpty(text.getText());
	}

	@Override
	protected void clearFields() {
		text.setText(null);
		comboBoxItems.setSelectedIndex(0);
		comboBoxOpinion.setSelectedIndex(0);
	}

	@Override
	protected EntityType getEntityType() {
		return EntityType.OPINION;
	}
	
	private static void addPattern(int itemCount, int grade, PatternType patternType, String text) {
		ObjectFactory objectFactory = new ObjectFactory();
		OpinionTemplateEntity opinionTemplate = objectFactory.createOpinionTemplateEntity();
		opinionTemplate.setGrade(grade);
		opinionTemplate.getPattern().add(text);
		
		switch(itemCount) {
			case 1: {
				// ���� ������� � �����������
				if(!opinionTextAlreadyExist(text, grade, patternType.getForOneItem())) {
					patternType.getForOneItem().add(opinionTemplate);
				}
			} break;
			case 2: {
				// ��� �������� � �����������
				if(!opinionTextAlreadyExist(text, grade, patternType.getForTwoItem())) {
					patternType.getForTwoItem().add(opinionTemplate);
				}
			} break;
			case 3: {
				// ��� �������� � �����������
				if(!opinionTextAlreadyExist(text, grade, patternType.getForThreeItem())) {
					patternType.getForThreeItem().add(opinionTemplate);
				}
			} break;
			case 4: {
				// ������ �������� � �����������
				if(!opinionTextAlreadyExist(text, grade, patternType.getForFourItem())) {
					patternType.getForFourItem().add(opinionTemplate);
				}
			} break;
			case 5: {
				// ���� ��������� � �����������
				if(!opinionTextAlreadyExist(text, grade, patternType.getForFiveItem())) {
					patternType.getForFiveItem().add(opinionTemplate);
				}
			} break;
		}
	}
	
	private static boolean opinionTextAlreadyExist(String text, int grade, List<OpinionTemplateEntity> opList) {
		for(OpinionTemplateEntity opinion : opList) {
			if(opinion.getGrade() == grade) {
				for(String opText : opinion.getPattern()) {
					String tmpOpText = new String(opText);
					String tmpText = new String(text);
					for(int i = 1; i <= SCALE_ITEM_COUNT.length; i++) {
						tmpOpText = tmpOpText.replaceAll("<i" + i + "> <a" + i + ">", "");
						tmpOpText = tmpOpText.replaceAll("<a" + i + "> <i" + i + ">", "");
						tmpText = tmpText.replaceAll("<i" + i + "> <a" + i + ">", "");
						tmpText = tmpText.replaceAll("<a" + i + "> <i" + i + ">", "");
					}
					if(tmpOpText.equalsIgnoreCase(tmpText)) {
						return true;
					}
				}
			}
		}
		return false;
	}
	
	private static void savePatternToXML(PatternType patternType, File file) throws JAXBException {
		Marshaller marshaller = XML_CONTEXT.createMarshaller();
		marshaller.setProperty("com.sun.xml.bind.namespacePrefixMapper", getNamespacePrefixMapper());
		ObjectFactory objectFactory = new ObjectFactory();
		marshaller.marshal(objectFactory.createOpinionPattern(patternType), file);
	}
	
	private void generateAllOpinions() {
		int dialogResult = JOptionPane.showConfirmDialog(
				getFrame(),
				"��� �������������� ��������� ������, ���������� ������ ����� �����������. ������ ����������?", 
				"��������!",
				JOptionPane.YES_NO_OPTION);
		if (dialogResult == JOptionPane.NO_OPTION) {
			return;
		}
		try {
			ObjectFactory objectFactory = new ObjectFactory();
			PatternType patternType = objectFactory.createPatternType();
			Random randomGen = new Random(System.currentTimeMillis());
			
			int opinionCounter = 0;
			for(int itemCount = 0; itemCount < SCALE_ITEM_COUNT.length; itemCount++) {
				for(int grade = 0; grade < SCALE_OPINION.length; grade++) {
					String currentItemSetText = "";
					for(int i = 0; i < itemCount + 1; i++) {
						currentItemSetText += "<a" + (i + 1) + "> <i" + (i + 1) + ">";
						if(i < itemCount) { 
							currentItemSetText += ", ";
						}
					}
					String opinionText = SCALE_OPINION[grade] + ", " + currentItemSetText + ".";
					if(grade < SCALE_OPINION.length - 4) {
						int randIndex = randomGen.nextInt(FOR_BEST_ITEM_SET_BAD.length - 1);
						opinionText += " " + FOR_BEST_ITEM_SET_BAD[randIndex] + " " + "::bis::.";
					} else if(grade < SCALE_OPINION.length - 1) {
						int randIndex = randomGen.nextInt(FOR_BEST_ITEM_SET_GOOD.length - 1);
						opinionText += " " + FOR_BEST_ITEM_SET_GOOD[randIndex] + " " + "::bis::.";
					}
					addPattern(itemCount + 1, grade + 1, patternType, opinionText);
					opinionCounter++;
				}
			}
			
			File file = getFileToSave();
			savePatternToXML(patternType, getFileToSave());
			showOkMessage(opinionCounter + " ������ ��������� � ���������� " + getEntityType().getSuffix() + " � ���� �� ������ " + file.getAbsolutePath());
		} catch (Exception e) {
			processSaveException(e);
		}
	}
}
