package ru.hegemony.editor.data;

import ru.hegemony.xml.data.DistributionType;

public class EffectValue {
	private Double value;
	private DistributionType dstType;
	private boolean valueMode;

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	public DistributionType getDstType() {
		return dstType;
	}

	public void setDstType(DistributionType dstType) {
		this.dstType = dstType;
	}

	public boolean isValueMode() {
		return valueMode;
	}

	public void setValueMode(boolean valueMode) {
		this.valueMode = valueMode;
	}

	@Override
	public String toString() {
		return "EffectValue [value=" + value + ", dstType=" + dstType + ", valueMode=" + valueMode + "]";
	}
}
