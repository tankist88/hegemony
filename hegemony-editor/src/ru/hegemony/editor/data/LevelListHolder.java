package ru.hegemony.editor.data;

import java.util.ArrayList;
import java.util.List;

import ru.hegemony.xml.data.LevelListType;

public class LevelListHolder {
	private LevelListType levelList;
	private String levelDir;
	private List<LevelHolder> levelTypeList;

	public LevelListType getLevelList() {
		return levelList;
	}

	public void setLevelList(LevelListType levelList) {
		this.levelList = levelList;
	}

	public List<LevelHolder> getLevelTypeList() {
		if(levelTypeList == null) {
			levelTypeList = new ArrayList<LevelHolder>();
		}
		return levelTypeList;
	}

	public void setLevelTypeList(List<LevelHolder> levelTypeList) {
		this.levelTypeList = levelTypeList;
	}

	public String getLevelDir() {
		return levelDir;
	}

	public void setLevelDir(String levelDir) {
		this.levelDir = levelDir;
	}
}
