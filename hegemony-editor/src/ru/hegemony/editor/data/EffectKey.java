package ru.hegemony.editor.data;


public class EffectKey {
	private int countryCode;
	private int resourceCode;
	private int actionCode;
	private String itemId;
	
	public int getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(int countryCode) {
		this.countryCode = countryCode;
	}
	public int getResourceCode() {
		return resourceCode;
	}
	public void setResourceCode(int resourceCode) {
		this.resourceCode = resourceCode;
	}
	public int getActionCode() {
		return actionCode;
	}
	public void setActionCode(int actionCode) {
		this.actionCode = actionCode;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + actionCode;
		result = prime * result + countryCode;
		result = prime * result + ((itemId == null) ? 0 : itemId.hashCode());
		result = prime * result + resourceCode;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EffectKey other = (EffectKey) obj;
		if (actionCode != other.actionCode)
			return false;
		if (countryCode != other.countryCode)
			return false;
		if (itemId == null) {
			if (other.itemId != null)
				return false;
		} else if (!itemId.equals(other.itemId))
			return false;
		if (resourceCode != other.resourceCode)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "EffectKey [countryCode=" + countryCode + ", resourceCode="
				+ resourceCode + ", actionCode=" + actionCode + ", itemId="
				+ itemId + "]";
	}
}
