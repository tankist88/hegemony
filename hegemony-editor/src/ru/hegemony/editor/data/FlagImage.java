package ru.hegemony.editor.data;

public class FlagImage {
	private String image;
	private String name;
	
	public FlagImage(String image) {
		this.image = image;
	}
	
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
