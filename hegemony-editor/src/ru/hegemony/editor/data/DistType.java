package ru.hegemony.editor.data;

public enum DistType {
	BETA ("����", "beta", "nextBeta", new String[] {"alpha", "beta", "return"}, new String[] {"double", "double", "double"}), 
	EXPONENTIAL ("����������������", "exp", "nextExponential", new String[] {"mean", "return"}, new String[] {"double", "double"}),
	CAUCHY ("����", "cauchy", "nextCauchy", new String[] {"median", "scale", "return"}, new String[] {"double", "double", "double"}),
	CHI_SQUARE ("�� �������", "chi_square", "nextChiSquare", new String[] {"df", "return"}, new String[] {"double", "double"}),
	GAMMA ("�����", "gamma", "nextGamma", new String[] {"shape", "scale", "return"}, new String[] {"double", "double", "double"}), 
	NORMAL ("����������", "normal", "nextGaussian", new String[] {"mu", "sigma", "return"}, new String[] {"double", "double", "double"}), 
	UNIFORM ("�����������", "unifarm", "nextUniform", new String[] {"lower", "upper", "return"}, new String[] {"double", "double", "double"}), 
	WEIBULL ("�������", "weibull", "nextWeibull", new String[] {"shape", "scale", "return"}, new String[] {"double", "double", "double"});
	
	final private String name;
	final private String code;
	final private String methodName;
	final private String[] paramNames;
	final private String[] paramTypes;
	
	private DistType(String name, String code, String methodName, String[] paramNames, String[] paramTypes) {
		this.name = name;
		this.code = code;
		this.methodName = methodName;
		this.paramNames = paramNames;
		this.paramTypes = paramTypes;
	}

	public String getName() {
		return name;
	}

	public String getCode() {
		return code;
	}

	public String getMethodName() {
		return methodName;
	}

	public String[] getParamNames() {
		return paramNames;
	}

	public String[] getParamTypes() {
		return paramTypes;
	}

	public static DistType findByCode(String code) {
		for(DistType dst : values()) {
			if(dst.getCode().equalsIgnoreCase(code)) {
				return dst;
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return name;
	}
}
