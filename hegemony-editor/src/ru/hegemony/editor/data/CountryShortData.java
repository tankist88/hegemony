package ru.hegemony.editor.data;

public class CountryShortData {
	private String name;
	private String lang;
	private String square;
	private String image;
	private int weaknessPercent;
	private String desc;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLang() {
		return lang;
	}
	public void setLang(String lang) {
		this.lang = lang;
	}
	public String getSquare() {
		return square;
	}
	public void setSquare(String square) {
		this.square = square;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public int getWeaknessPercent() {
		return weaknessPercent;
	}
	public void setWeaknessPercent(int weaknessPercent) {
		this.weaknessPercent = weaknessPercent;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
}
