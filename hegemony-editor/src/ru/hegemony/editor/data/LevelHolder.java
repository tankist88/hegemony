package ru.hegemony.editor.data;

public class LevelHolder {
	private String filename;
	private LevelData level;
	
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public LevelData getLevel() {
		return level;
	}
	public void setLevel(LevelData level) {
		this.level = level;
	}
}
