package ru.hegemony.editor.data;


public enum EntityType {
	/** ������ */
	COUNTRY(1, "�����", "countries", "countries.xml"),
	/** ������� */
	ITEM(2, "���������", "items", "item.xml"),
	/** ���� */
	FLAG(3, "������", "countries", null),
	/** ������ ������ */
	OPINION(4, "������", null, "opinion_patterns.xml");
	
	private final int code;
	private final String suffix;
	private final String resSubDir;
	private final String dictFileName;
	
	private EntityType(int code, String suffix, String resSubDir, String dictFileName) {
		this.code = code;
		this.suffix = suffix;
		this.resSubDir = resSubDir;
		this.dictFileName = dictFileName;
	}

	public int getCode() {
		return code;
	}

	public String getSuffix() {
		return suffix;
	}

	public String getResSubDir() {
		return resSubDir;
	}

	public String getDictFileName() {
		return dictFileName;
	}
}
