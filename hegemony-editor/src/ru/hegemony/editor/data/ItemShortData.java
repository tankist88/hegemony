package ru.hegemony.editor.data;

import java.util.ArrayList;
import java.util.List;

public class ItemShortData {
	private String name;
	private String image;
	private String desc;
	private List<String> actions;
	private List<String> properties;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public List<String> getActions() {
		if(actions == null) {
			actions = new ArrayList<String>();
		}
		return actions;
	}
	public void setActions(List<String> actions) {
		this.actions = actions;
	}
	public List<String> getProperties() {
		if(properties == null) {
			properties = new ArrayList<String>();
		}
		return properties;
	}
	public void setProperties(List<String> properties) {
		this.properties = properties;
	}
}
