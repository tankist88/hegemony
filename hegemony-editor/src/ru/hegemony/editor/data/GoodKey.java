package ru.hegemony.editor.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GoodKey {
	List<Integer> itemCodes;
	Map<Integer, Integer> actionCodes;
	
	public GoodKey(List<Integer> itemCodes, Map<Integer, Integer> actionCodes) {
		this.itemCodes = itemCodes;
		this.actionCodes = actionCodes;
	}
	public List<Integer> getItemCodes() {
		if(itemCodes == null) {
			itemCodes = new ArrayList<Integer>();
		}
		return itemCodes;
	}
	public Map<Integer, Integer> getActionCodes() {
		if(actionCodes == null) {
			actionCodes = new HashMap<Integer, Integer>();
		}
		return actionCodes;
	}
}
