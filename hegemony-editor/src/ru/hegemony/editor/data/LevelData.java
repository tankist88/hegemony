package ru.hegemony.editor.data;

import java.util.ArrayList;
import java.util.List;

import ru.hegemony.xml.data.AllowedItemSetsType;
import ru.hegemony.xml.data.CountryEntity;
import ru.hegemony.xml.data.EffectsType;
import ru.hegemony.xml.data.EventDescType;
import ru.hegemony.xml.data.ItemEntity;
import ru.hegemony.xml.data.ItemValueType;
import ru.hegemony.xml.data.TurnEntity;

public class LevelData {
	private EventDescType eventDesc;
	private List<CountryEntity> countries;
	private List<ItemEntity> items;
	private List<EffectsType> effects;
	private List<AllowedItemSetsType> itemSets;
	private List<TurnEntity> turns;
	private List<ItemValueType> itemsEffects;
	
	public EventDescType getEventDesc() {
		return eventDesc;
	}
	public void setEventDesc(EventDescType eventDesc) {
		this.eventDesc = eventDesc;
	}
	public List<CountryEntity> getCountries() {
		if(countries == null) {
			countries = new ArrayList<CountryEntity>();
		}
		return countries;
	}
	public void setCountries(List<CountryEntity> countries) {
		this.countries = countries;
	}
	public List<ItemEntity> getItems() {
		if(items == null) {
			items = new ArrayList<ItemEntity>();
		}
		return items;
	}
	public void setItems(List<ItemEntity> items) {
		this.items = items;
	}
	public List<EffectsType> getEffects() {
		if(effects == null) {
			effects = new ArrayList<EffectsType>();
		}
		return effects;
	}
	public void setEffects(List<EffectsType> effects) {
		this.effects = effects;
	}
	public List<AllowedItemSetsType> getItemSets() {
		if(itemSets == null) {
			itemSets = new ArrayList<AllowedItemSetsType>();
		}
		return itemSets;
	}
	public void setItemSets(List<AllowedItemSetsType> itemSets) {
		this.itemSets = itemSets;
	}
	public List<TurnEntity> getTurns() {
		if(turns == null) {
			turns = new ArrayList<TurnEntity>();
		}
		return turns;
	}
	public void setTurns(List<TurnEntity> turns) {
		this.turns = turns;
	}
	public List<ItemValueType> getItemsEffects() {
		if(itemsEffects == null) {
			itemsEffects = new ArrayList<ItemValueType>();
		}
		return itemsEffects;
	}
	public void setItemsEffects(List<ItemValueType> itemsEffects) {
		this.itemsEffects = itemsEffects;
	}
}
