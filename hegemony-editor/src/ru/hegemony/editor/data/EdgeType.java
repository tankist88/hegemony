package ru.hegemony.editor.data;

public enum EdgeType {
	DEFAULT("", 48),
	HDPI("_hdpi", 72), 
	MDPI("_mdpi", 48), 
	XHDPI("_xhdpi", 96);
	
	private final String suffix;
	private final int size;
	
	private EdgeType(String suffix, int size) {
		this.size = size;
		this.suffix = suffix;
	}

	public String getSuffix() {
		return suffix;
	}

	public int getSize() {
		return size;
	}
	
	public static boolean fileHasDpiSuffix(String filename) {
		for(EdgeType edge : values()) {
			if(!edge.equals(DEFAULT) && filename.contains(edge.getSuffix())) {
				return true;
			}
		}
		return false;
	}
}
