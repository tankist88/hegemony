package ru.hegemony.editor.render;

import java.awt.Color;
import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.border.EmptyBorder;

import ru.hegemony.editor.data.EntityType;
import ru.hegemony.editor.data.FlagImage;
import ru.hegemony.editor.helper.ImageHelper;
import ru.hegemony.xml.data.CountryEntity;
import ru.hegemony.xml.data.ItemEntity;

public class TextWithImageListRenderer extends DefaultListCellRenderer {
	private static final long serialVersionUID = 4482294984723669209L;

	private EntityType entityType;
	
	private JLabel label;
	private Color textSelectionColor = Color.BLACK;
	private Color backgroundSelectionColor = Color.CYAN;
	private Color textNonSelectionColor = Color.BLACK;
	private Color backgroundNonSelectionColor = Color.WHITE;

	public TextWithImageListRenderer(EntityType entityType) {
		this.label = new JLabel();
		this.label.setBorder(new EmptyBorder(7, 7, 7, 7));
		this.label.setOpaque(true);
		this.entityType = entityType;
	}

	@Override
	public Component getListCellRendererComponent(JList list, Object value,
			int index, boolean selected, boolean expanded) {
		
		if(value != null) {
			if(entityType.equals(EntityType.COUNTRY)) {
				CountryEntity country = (CountryEntity) value;
				label.setIcon(ImageHelper.createImageIcon(EntityType.COUNTRY.getResSubDir(), country.getImage()));
				label.setText(country.getName());
			} else if(entityType.equals(EntityType.ITEM)) {
				ItemEntity item = (ItemEntity) value;
				label.setIcon(ImageHelper.createImageIcon(EntityType.ITEM.getResSubDir(), item.getImage()));
				label.setText(item.getName());
			} else if(entityType.equals(EntityType.FLAG)) {
				FlagImage flag = (FlagImage) value;
				label.setIcon(ImageHelper.createImageIcon(EntityType.FLAG.getResSubDir(), flag.getImage()));
				label.setText("");
			}
		}

		if (selected) {
			label.setBackground(backgroundSelectionColor);
			label.setForeground(textSelectionColor);
		} else {
			label.setBackground(backgroundNonSelectionColor);
			label.setForeground(textNonSelectionColor);
		}

		return label;
	}
}
