package ru.hegemony.editor.render;

import java.awt.BorderLayout;
import java.awt.Dialog.ModalityType;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;

import ru.hegemony.common.helper.DataHelper;
import ru.hegemony.editor.helper.MessageHelper;

public abstract class ProgressBarDialog implements PropertyChangeListener {
	private JProgressBar progressBar;
	private Task task;
	private JFrame parentFrame;
	private boolean indeterminate;
	
	private JDialog dialog;
	
	public void executeTaskWithProgressBar() {
		progressBar = new JProgressBar(0, 100);
        progressBar.setValue(0);
        progressBar.setIndeterminate(indeterminate);
        progressBar.setStringPainted(!indeterminate);
        
		dialog = new JDialog(parentFrame, "���������", ModalityType.APPLICATION_MODAL);
		startTast();
		JPanel panel = new JPanel(new BorderLayout());
		panel.add(progressBar, BorderLayout.CENTER);
		panel.add(new JLabel("����������......."), BorderLayout.PAGE_START);
		panel.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
		dialog.add(panel);
		dialog.setSize(300, 120);
		dialog.setPreferredSize(new Dimension(300, 120));
		dialog.pack();
		dialog.setLocationRelativeTo(parentFrame);
		dialog.setVisible(true);
	}
	
	public ProgressBarDialog(JFrame parentFrame, boolean indeterminate) {
		this.parentFrame = parentFrame;
		this.indeterminate = indeterminate;
	}
	
	private void startTast() {
        task = new Task();
        task.addPropertyChangeListener(this);
        task.execute();
	}
	
	public class Task extends SwingWorker<Void, Void> {
        /* Main task. Executed in background thread */
        @Override
        public Void doInBackground() {
        	try {
        		this.setTaskProgress(0);
        		progressBar.setValue(0);
				doWork(this);
			} catch (Exception ex) {
				MessageHelper.showError("������ �������� ������ �������", parentFrame, ex);
			}
            return null;
        }

        /* Executed in event dispatching thread */
        @Override
        public void done() {
            Toolkit.getDefaultToolkit().beep();
            dialog.setVisible(false);
        }
        
        public void setTaskProgress(int value) {
        	setProgress(value);
        }
        
        public void setTaskProgress(double value) {
        	setProgress((int) DataHelper.round(value, 2));
        }
        
        public int getTaskProgress() {
        	return getProgress();
        }
    }

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if ("progress" == evt.getPropertyName()) {
			int progress = (Integer) evt.getNewValue();
			progressBar.setValue(progress);
		}
	}

	public JFrame getParentFrame() {
		return parentFrame;
	}

	protected abstract void doWork(Task worker) throws Exception;
}
