package ru.hegemony.editor;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FileDialog;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import ru.hegemony.editor.data.EdgeType;
import ru.hegemony.editor.data.EntityType;
import ru.hegemony.editor.helper.ImageHelper;
import ru.hegemony.editor.helper.MessageHelper;

import com.mortennobel.imagescaling.ResampleOp;

public abstract class DictionaryWithImageEditorWnd extends DictionaryEditorWnd {
	public static final String IMG_PATH = "res/img/";
	
	public static final String EMPTY_IMAGE = "no";
	
	private JLabel imageLabel;
	private String selectedImage;
	
	protected DictionaryWithImageEditorWnd(String title, int width, int height) {
		super(title, width, height);
	}
	
	@Override
	protected void initView(JPanel parrentPanel) {
		this.selectedImage = EMPTY_IMAGE;
		this.imageLabel = new JLabel(ImageHelper.createImageIcon(getEntityType().getResSubDir(), this.selectedImage));
		this.imageLabel.setPreferredSize(new Dimension(EdgeType.DEFAULT.getSize(), EdgeType.DEFAULT.getSize()));
		this.imageLabel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
	}

	protected void loadImageFromFile(EntityType entityType) {
		FileDialog fdlg = new FileDialog(getFrame(), "Open file", FileDialog.LOAD);
		fdlg.setVisible(true);
		
		String filename = fdlg.getFile();
		if(filename != null) {
			try {
				String path = fdlg.getDirectory() + fdlg.getFile();
				File srcImageFile = new File(path);
				
				String[] filenamePart = filename.split("\\.");
				String filenameWithOutExt = filenamePart[0].toLowerCase().replaceAll("-", "_");
				
				BufferedImage buffImage = ImageIO.read(srcImageFile);
				
				for(EdgeType edge : EdgeType.values()) {
					saveImage(buffImage, entityType, filenameWithOutExt, edge);
				}
				
				setImage(filenameWithOutExt, entityType);
			} catch (IOException ioe) {
				MessageHelper.showError("������ �����/������", getFrame(), ioe);
			} catch (Exception ex) {
				MessageHelper.showError("������ �������� �����������", getFrame(), ex);
			}
		}
	}
	
	private void saveImage(final BufferedImage buffImage, final EntityType entityType, String filenameWithOutExt, final EdgeType edge) throws IOException {
		int width = 0;
		int height = 0;
		if(buffImage.getWidth() > buffImage.getHeight()) {
			double div = ((double) buffImage.getWidth()) / ((double) buffImage.getHeight());
			width = edge.getSize();
			height = (int) (width / div);
		} else {
			double div = ((double) buffImage.getHeight()) / ((double) buffImage.getWidth());
			height = edge.getSize();
			width = (int) ((double) height / (double) div);
		}
		
		ResampleOp  resampleOp = new ResampleOp (width, height);
		BufferedImage rescaledImage = resampleOp.filter(buffImage, null);
		
		if(filenameWithOutExt.length() > 10) {
			filenameWithOutExt = filenameWithOutExt.substring(0, 10);
		}
		File destImageFile = new File(IMG_PATH + entityType.getResSubDir() + "/" + filenameWithOutExt + edge.getSuffix() + ".png");
		if(destImageFile.exists() && !destImageFile.isDirectory()) {
			int dialogResult = JOptionPane.showConfirmDialog(
					getFrame(),
					"� ����������� ����� ��� ���������� ���� � ������ "
							+ filenameWithOutExt + edge.getSuffix()
							+ ".png. �������� ����?", 
					"��������!",
					JOptionPane.YES_NO_OPTION);
			if (dialogResult == JOptionPane.NO_OPTION) {
				return;
			}
		}
		ImageIO.write(rescaledImage, "png", destImageFile);
	}
	
	protected void clearImage(EntityType entityType) {
		selectedImage = EMPTY_IMAGE;
		if(imageLabel != null) {
			imageLabel.setIcon(ImageHelper.createImageIcon(entityType.getResSubDir(), selectedImage));
		}
	}
	
	protected void setImage(String filenameWithOutExt, EntityType entityType) {
		if(imageLabel != null) {
			imageLabel.setIcon(ImageHelper.createImageIcon(entityType.getResSubDir(), filenameWithOutExt));
		}
		selectedImage = filenameWithOutExt;
	}

	protected JLabel getImageLabel() {
		return imageLabel;
	}

	protected String getSelectedImage() {
		return selectedImage;
	}
}
