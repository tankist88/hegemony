package ru.hegemony.editor;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import ru.hegemony.editor.data.EntityType;
import ru.hegemony.editor.helper.MessageHelper;

import com.sun.xml.bind.marshaller.NamespacePrefixMapper;

public abstract class DictionaryEditorWnd {
	public static final int PADDING = 10;
	/** ���� � ������������ */
	public static final String DICT_PATH = "res/data/";
	
	public static final JAXBContext XML_CONTEXT = initJAXBContext();
	
	private JDialog frame;
	
	private boolean saveFileToDefaultPath;

	private static JAXBContext initJAXBContext() {
		JAXBContext context = null;
		try {
			context = JAXBContext.newInstance("ru.hegemony.xml.data");
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return context;
	}
	
	protected DictionaryEditorWnd(String title, int width, int height) {
		initialize(null, title, width, height, true);
	}
	
	public static void launch(final DictionaryEditorWnd window) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					window.frame.setVisible(true);
				} catch (Exception e) {
					MessageHelper.showError("������ ������� ����������", null, e);
				}
			}
		});
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(JFrame ownerFrame, String title, int width, int height, boolean saveFileToDefaultPath) {
		frame = new JDialog(ownerFrame, true);
		frame.setBounds(300, 300, width, height);
		frame.setTitle(title);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));

		this.saveFileToDefaultPath = saveFileToDefaultPath;
		
		JPanel panelMenu = new JPanel();
		panelMenu.setLayout(new BoxLayout(panelMenu, BoxLayout.X_AXIS));
		initMenu(panelMenu);

		JPanel panelView = new JPanel();
		panelView.setLayout(new BoxLayout(panelView, BoxLayout.Y_AXIS));
		initView(panelView);
		
		frame.getContentPane().add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
		frame.getContentPane().add(panelView);
		frame.getContentPane().add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
		frame.getContentPane().add(panelMenu);
		frame.getContentPane().add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
	}

	protected void initMenu(JPanel parrentPanel) {
		JButton saveButton = new JButton();
		saveButton.setText("���������");
		saveButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				saveResult(getEntityType());
			}
		});

		JButton exitButton = new JButton();
		exitButton.setText("����� ��� ����������");
		exitButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				getFrame().dispose();
			}
		});
		
		parrentPanel.add(saveButton);
		parrentPanel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
		parrentPanel.add(exitButton);
	}
	
	protected abstract void saveAction(File file) throws JAXBException;
	protected abstract void initView(JPanel parrentPanel);
	protected abstract void clearFields();
	protected abstract boolean allRequiredFieldsSetted();
	protected abstract EntityType getEntityType();
	
	protected File getFileToSave() {
		String path = null;
		if(saveFileToDefaultPath) {
			path = DICT_PATH + getEntityType().getDictFileName();
		} else {
			JFileChooser fc = new JFileChooser();
			fc.setCurrentDirectory(new File("."));
			fc.setDialogTitle("Please choose a directory...");
			fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			fc.setAcceptAllFileFilterUsed(false);
			if(fc.showSaveDialog(getFrame()) != JFileChooser.APPROVE_OPTION) {
				return null;
			}
			path = fc.getSelectedFile().getAbsolutePath() + "/" + getEntityType().getDictFileName();
		}
		return new File(path);
	}
	
	private void saveResult(EntityType entityType) {
		if(!allRequiredFieldsSetted()) {
			MessageHelper.showCustomError(
					"������ ���������� � ���������� " + entityType.getSuffix(), 
					"��� ���� �������� �������������!",  
					getFrame());
			return;
		}
		
		try {
			File file = getFileToSave();
			saveAction(file);
			showOkMessage("��������� � ���������� " + entityType.getSuffix() + " � ���� �� ������ " + file.getAbsolutePath());
			clearFields();
		} catch (Exception ex) {
			processSaveException(ex);
		}
	}
	
	protected void showOkMessage(String text) {
		MessageHelper.showInfo("���������� �����", text, getFrame());
	}
	
	protected void processSaveException(Throwable ex) {
		MessageHelper.showError("������ ����������", getFrame(), ex);
	}
	
	protected static NamespacePrefixMapper getNamespacePrefixMapper() {
		NamespacePrefixMapper mapper = new NamespacePrefixMapper() {
			public String getPreferredPrefix(String namespaceUri, String suggestion, boolean requirePrefix) {
				return "tns";
			}
		};
		return mapper;
	}

	public JDialog getFrame() {
		return frame;
	}
}
