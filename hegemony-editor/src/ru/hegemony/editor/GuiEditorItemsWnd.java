package ru.hegemony.editor;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.lang3.StringUtils;

import ru.hegemony.editor.data.EntityType;
import ru.hegemony.editor.data.ItemShortData;
import ru.hegemony.editor.render.JTextFieldLimit;
import ru.hegemony.xml.data.ItemActionEntity;
import ru.hegemony.xml.data.ItemEntity;
import ru.hegemony.xml.data.ItemPropertyEntity;
import ru.hegemony.xml.data.Items;
import ru.hegemony.xml.data.ObjectFactory;

public class GuiEditorItemsWnd extends DictionaryWithImageEditorWnd {
	private static final int MAX_ITEM_NAME_SIZE = 20;
	private static final int MAX_ITEM_ACTION_SIZE = 20;
	private static final int MAX_DESC_SIZE = 1000;
	
	private JTextField itemNameText;
	private JTextArea descTa;
	private DefaultListModel actionListModel;
	private DefaultListModel propertyListModel;
	
	public GuiEditorItemsWnd() {
		super("���������� ��������� � ����������", 450, 450);
	}
	
	protected GuiEditorItemsWnd(String title, int width, int height) {
		super(title, width, height);
	}
	
	public static void main(String args[]) {
		DictionaryEditorWnd.launch(new GuiEditorItemsWnd());
	}

	public static void saveItemToDict(ItemShortData item) throws JAXBException {
		String path = DICT_PATH + EntityType.ITEM.getDictFileName();
		saveItemToDict(new File(path), item);
	}
	
	public static void saveItemToDict(File file, ItemShortData item) throws JAXBException {
		ObjectFactory objectFactory = new ObjectFactory();
		
		Items items = null;
		try {
			Unmarshaller unmarshaller = XML_CONTEXT.createUnmarshaller();
			JAXBElement<Items> element = unmarshaller.unmarshal(new StreamSource(file), Items.class);
			items = element.getValue();
		} catch(Exception ex) {
			items = objectFactory.createItems();
		}
		
		ItemEntity foundItem = null;
		for(ItemEntity existItem : items.getItem()) {
			if(existItem.getImage().equals(item.getImage())) {
				foundItem = existItem;
			}
		}
		
		List<ItemActionEntity> actionList = new ArrayList<ItemActionEntity>();
		for(int i = 0; i < item.getActions().size(); i++) {
			String actionName = item.getActions().get(i);
			ItemActionEntity action = objectFactory.createItemActionEntity();
			action.setCode(getActionCode(items, actionName, actionList));
			action.setName(actionName);
			actionList.add(action);
		}
		
		List<ItemPropertyEntity> propList = new ArrayList<ItemPropertyEntity>();
		for(int i = 0; i < item.getProperties().size(); i++) {
			String propName = item.getProperties().get(i);
			ItemPropertyEntity prop = objectFactory.createItemPropertyEntity();
			prop.setCode(getPropertyCode(items, propName, propList));
			prop.setValue(0);
			prop.setName(propName);
			propList.add(prop);
		}
		
		ItemEntity itemForAdd = objectFactory.createItemEntity();
		if(foundItem != null) {
			itemForAdd.setCode(foundItem.getCode());
		} else {
			itemForAdd.setCode(getNextItemCode(items.getItem()));
		}
		itemForAdd.setName(item.getName());
		itemForAdd.setImage(item.getImage());
		itemForAdd.setDesc(item.getDesc());
		itemForAdd.getActions().addAll(actionList);
		itemForAdd.getProperties().addAll(propList);
		
		if(foundItem != null) {
			items.getItem().remove(foundItem);
		}
		items.getItem().add(itemForAdd);
		
		Marshaller marshaller = XML_CONTEXT.createMarshaller();
		marshaller.setProperty("com.sun.xml.bind.namespacePrefixMapper", getNamespacePrefixMapper());
		marshaller.marshal(objectFactory.createItems(items), file);
	}
	
	@Override
	protected void saveAction(File file) throws JAXBException {
		ItemShortData item = new ItemShortData();
		item.setName(itemNameText.getText());
		item.setDesc(descTa.getText());
		item.setImage(getSelectedImage());
		for(int i = 0; i < actionListModel.size(); i++) {
			String actionName = (String) actionListModel.get(i);
			item.getActions().add(actionName);
		}
		for(int i = 0; i < propertyListModel.size(); i++) {
			String propName = (String) propertyListModel.get(i);
			item.getProperties().add(propName);
		}
		saveItemToDict(file, item);
	}
	
	private static int getNextItemCode(List<ItemEntity> itemList) {
		int maxCode = 0;
		for(ItemEntity item : itemList) {
			if(item.getCode() > maxCode) {
				maxCode = item.getCode();
			}
		}
		return maxCode + 1;
	}
	
	private static int getActionCode(final Items items, final String actionName, final List<ItemActionEntity> actionList) {
		Set<Integer> uniqueActions = new HashSet<Integer>();
		for(ItemActionEntity action : actionList) {
			uniqueActions.add(action.getCode());
		}
		for(ItemEntity item : items.getItem()) {
			for(ItemActionEntity action : item.getActions()) {
				if(StringUtils.equalsIgnoreCase(action.getName(), actionName)) {
					return action.getCode();
				} else {
					uniqueActions.add(action.getCode());
				}
			}
		}
		return uniqueActions.size() + 1;
	}
	
	private static int getPropertyCode(final Items items, final String propName, final List<ItemPropertyEntity> propList) {
		Set<Integer> uniqueProps = new HashSet<Integer>();
		for(ItemPropertyEntity prop : propList) {
			uniqueProps.add(prop.getCode());
		}
		for(ItemEntity item : items.getItem()) {
			for(ItemPropertyEntity prop : item.getProperties()) {
				if(StringUtils.equalsIgnoreCase(prop.getName(), propName)) {
					return prop.getCode();
				} else {
					uniqueProps.add(prop.getCode());
				}
			}
		}
		return uniqueProps.size() + 1;
	}

	@Override
	protected void initView(JPanel parrentPanel) {
		super.initView(parrentPanel);
		
		JPanel itemNamePanel = new JPanel();
		itemNamePanel.setLayout(new BoxLayout(itemNamePanel, BoxLayout.X_AXIS));
		itemNameText = new JTextField();
		itemNameText.setPreferredSize(new Dimension(200, 30));
		itemNameText.setDocument(new JTextFieldLimit(MAX_ITEM_NAME_SIZE));
		JLabel itemNameLabel = new JLabel("�������� ��������");
		itemNameLabel.setPreferredSize(new Dimension(130, 30));
		itemNamePanel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
		itemNamePanel.add(itemNameLabel);
		itemNamePanel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
		itemNamePanel.add(itemNameText);
		itemNamePanel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
		
		JPanel imagePanel = new JPanel();
		imagePanel.setLayout(new BoxLayout(imagePanel, BoxLayout.X_AXIS));
		imagePanel.setPreferredSize(new Dimension(350, 100));
		JLabel itemInfoLabel = new JLabel("�����������");
		itemInfoLabel.setPreferredSize(new Dimension(130, 30));
		JPanel itemCenterPanel = new JPanel();
		itemCenterPanel.setPreferredSize(new Dimension(200, 70));
		itemCenterPanel.add(getImageLabel());
		
		JButton addImageButton = new JButton();
		addImageButton.setText("��������� �� �����");
		addImageButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				loadImageFromFile(getEntityType());
			}
		});
		
		imagePanel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
		imagePanel.add(itemInfoLabel);
		imagePanel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
		imagePanel.add(itemCenterPanel);
		imagePanel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
		imagePanel.add(addImageButton);
		imagePanel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
		
		actionListModel = new DefaultListModel();
		JPanel actionsPanel = getActionOrPropPanel("��������", actionListModel);
		
		propertyListModel = new DefaultListModel();
		JPanel propPanel = getActionOrPropPanel("��������", propertyListModel);
		
		descTa = new JTextArea(3, 37);
		descTa.setEditable(true);
		descTa.setLineWrap(true);
		descTa.setDocument(new JTextFieldLimit(MAX_DESC_SIZE));
		
		JPanel descPanel = new JPanel();
		descPanel.add(new JLabel("��������"));
		descPanel.setPreferredSize(new Dimension(300, 100));
		descPanel.add(
				new JScrollPane(
					descTa,
					JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
					JScrollPane.HORIZONTAL_SCROLLBAR_NEVER
				)
			);
		
		clearFields();
		
		parrentPanel.add(itemNamePanel);
		parrentPanel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
		parrentPanel.add(imagePanel);
		parrentPanel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
		parrentPanel.add(descPanel);
		parrentPanel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
		parrentPanel.add(actionsPanel);
		parrentPanel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
		parrentPanel.add(propPanel);
		parrentPanel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
	}
	
	private JPanel getActionOrPropPanel(String title, final DefaultListModel listModel) {
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
		panel.setPreferredSize(new Dimension(300, 70));
		
		JPanel currentPanel = new JPanel();
		JLabel infoLabel = new JLabel(title);
		final JTextField currentText = new JTextField();
		currentText.setPreferredSize(new Dimension(180, 20));
		currentText.setDocument(new JTextFieldLimit(MAX_ITEM_ACTION_SIZE));
		currentPanel.add(infoLabel);
		currentPanel.add(currentText);
		
		final JList list = new JList();
		list.setModel(listModel);
		list.setVisibleRowCount(4);
		list.setCellRenderer(new DefaultListCellRenderer());
		
		JPanel listPanel = new JPanel();
		listPanel.setLayout(new BorderLayout());
		listPanel.setPreferredSize(new Dimension(250, 200));
		listPanel.add(
				new JScrollPane(
					list,
					JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
					JScrollPane.HORIZONTAL_SCROLLBAR_NEVER
				), BorderLayout.CENTER
			);
		
		JButton addButton = new JButton();
		addButton.setText(">");
		addButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(!StringUtils.isEmpty(currentText.getText())) {
					listModel.addElement(currentText.getText());
					list.setModel(listModel);
					currentText.setText(null);
				}
			}
		});
		
		panel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
		panel.add(currentPanel);
		panel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
		panel.add(addButton);
		panel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
		panel.add(listPanel);
		panel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
		
		return panel;
	}
	
	@Override
	protected boolean allRequiredFieldsSetted() {
		return 
				!StringUtils.isEmpty(itemNameText.getText()) && 
				!StringUtils.isEmpty(descTa.getText()) && 
				actionListModel.size() > 0 && 
				propertyListModel.size() > 0 && 
				!StringUtils.isEmpty(getSelectedImage()) && !getSelectedImage().equalsIgnoreCase(EMPTY_IMAGE);
	}

	@Override
	protected void clearFields() {
		itemNameText.setText(null);
		descTa.setText(null);
		actionListModel.clear();
		propertyListModel.clear();
		propertyListModel.addElement("����������");
		clearImage(getEntityType());
	}

	@Override
	protected EntityType getEntityType() {
		return EntityType.ITEM;
	}
}
