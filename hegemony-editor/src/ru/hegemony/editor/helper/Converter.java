package ru.hegemony.editor.helper;

import ru.hegemony.editor.data.CountryShortData;
import ru.hegemony.editor.data.ItemShortData;
import ru.hegemony.xml.data.CountryEntity;
import ru.hegemony.xml.data.ItemActionEntity;
import ru.hegemony.xml.data.ItemEntity;
import ru.hegemony.xml.data.ItemPropertyEntity;

public class Converter {
	public static CountryShortData createCountryShortData(CountryEntity country) {
		CountryShortData data = new CountryShortData();
		data.setImage(country.getImage());
		data.setLang(country.getLanguageTitle());
		data.setName(country.getName());
		data.setSquare(country.getSquare());
		data.setDesc(country.getDesc());
		data.setWeaknessPercent(country.getWeaknessPercent());
		return data;
	}
	
	public static ItemShortData createItemShortData(ItemEntity item) {
		ItemShortData data = new ItemShortData();
		data.setName(item.getName());
		data.setImage(item.getImage());
		data.setDesc(item.getDesc());
		for(ItemActionEntity action : item.getActions()) {
			data.getActions().add(action.getName());
		}
		for(ItemPropertyEntity prop : item.getProperties()) {
			data.getProperties().add(prop.getName());
		}
		return data;
	}
}
