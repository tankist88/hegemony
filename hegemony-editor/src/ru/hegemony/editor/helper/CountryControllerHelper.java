package ru.hegemony.editor.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import ru.hegemony.common.helper.ItemSetCompareHelper;
import ru.hegemony.xml.data.AllowedItemSetsType;
import ru.hegemony.xml.data.CountryEntity;
import ru.hegemony.xml.data.EffectsType;
import ru.hegemony.xml.data.ResourceDeltaEntity;
import ru.hegemony.xml.data.ResourcesEntity;

public class CountryControllerHelper {
	public final static double ZERO = 0.001;
	
	public static TreeMap<Integer, Double> getCountryEffectMap(final List<CountryEntity> countries, List<EffectsType> effects) {
		Map<Integer, Double> countryEffectMap = new HashMap<Integer, Double>();
		ValueComparatorAsc bvcAsc = new ValueComparatorAsc(countryEffectMap);
		TreeMap<Integer, Double> sortedMap = new TreeMap<Integer, Double>(bvcAsc);
		for(EffectsType effect : effects) {
			double val = 0.0;
			for(CountryEntity country : countries) {
				val += CountryControllerHelper.getResDeltaKoef(country, effect.getValues());
			}
			countryEffectMap.put(effect.getKey().getItemSetCode(), val);
		}
		sortedMap.putAll(countryEffectMap);
		return sortedMap;
	}
	
	public static TreeMap<Integer, Double> getSortedDescTurnMap(CountryEntity country, List<EffectsType> effects) {
		Map<Integer, Double> countryEffectMap = new HashMap<Integer, Double>();
		ValueComparatorDesc bvcDesc = new ValueComparatorDesc(countryEffectMap);
		TreeMap<Integer, Double> sortedMap = new TreeMap<Integer, Double>(bvcDesc);
		for(EffectsType effect : effects) {
			if(effect.getKey().getCountryCode() == country.getCode()) {
				countryEffectMap.put(effect.getKey().getItemSetCode(), CountryControllerHelper.getResDeltaKoef(country, effect.getValues()));
			}
		}
		sortedMap.putAll(countryEffectMap);
		return sortedMap;
	}
	
	public static List<String> getTurnKeys(CountryEntity country, TreeMap<Integer, Double> sortedMap, Map<Integer, AllowedItemSetsType> itemSets) {
		List<String> result = new ArrayList<String>();
		Entry<Integer, Double> bestEntry = sortedMap.firstEntry();
		ItemSetCompareHelper cHelper = new ItemSetCompareHelper(country.getWeaknessPercent(), bestEntry.getValue().doubleValue());
		for(Entry<Integer, Double> entry : sortedMap.entrySet()) {
			if(cHelper.compareWith(entry.getValue().doubleValue())) {
				result.add(itemSets.get(entry.getKey()).getKeyStr());
			}
		}
		return result;
	}
	
	/**
	 * ������ ���������� ������������ ����������� �� delta �������� ��������
	 * @param country - ������ ��� ������� ���� ������
	 * @param itemSetList - delta �������� ������
	 * @return ��������� �����������
	 */
	public static double getResDeltaKoef(CountryEntity country, final List<ResourceDeltaEntity> itemSetList) {
		Map<Integer, Double> resWeightMap = new HashMap<Integer, Double>();
		double resWeightSum = 0.0;
		for(ResourcesEntity res : country.getResources()) {
			resWeightMap.put(res.getCode(), res.getWeight());
			resWeightSum += res.getWeight();
		}
		
		if(Math.abs(resWeightSum - 1) > ZERO) {
			throw new IllegalStateException("������ ������� resWeightSum! ����� resWeightSum (" + resWeightSum + ") �� ����� 1.0");
		}
		
		return getKoef(resWeightMap, itemSetList);
	}
	
	private static double getKoef(Map<Integer, Double> resWeightMap, final List<ResourceDeltaEntity> itemSetList) {
		double koef = 0.0;
		for(ResourceDeltaEntity resDelta : itemSetList) {
			koef += resDelta.getDelta() * resWeightMap.get(resDelta.getResourceCode());
		}
		return koef;
	}
}
