package ru.hegemony.editor.helper;

import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import org.apache.commons.lang3.StringUtils;

public class VersionHelper {
	public static String getVersion() {
		String version = "<unknown>";
		URLClassLoader cl = (URLClassLoader) VersionHelper.class.getClassLoader();
		try {
			URL url = cl.findResource("META-INF/MANIFEST.MF");
			Manifest manifest = new Manifest(url.openStream());
			Attributes attr = manifest.getMainAttributes();
			String impVersAttr = attr.getValue("Implementation-Version");
			if(!StringUtils.isEmpty(impVersAttr)) {
				version = impVersAttr;
			}
		} catch (IOException ex) {
			MessageHelper.showError("������ �����/������", null, ex);
		}
		return version;
	}
}
