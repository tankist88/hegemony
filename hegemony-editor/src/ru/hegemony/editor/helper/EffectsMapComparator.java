package ru.hegemony.editor.helper;

import java.util.Comparator;
import java.util.List;
import java.util.Map;

import ru.hegemony.xml.data.CountryEntity;
import ru.hegemony.xml.data.ResourceDeltaEntity;

public class EffectsMapComparator implements Comparator<Integer> {
	Map<Integer, List<ResourceDeltaEntity>> base;
	CountryEntity country;

	public EffectsMapComparator(Map<Integer, List<ResourceDeltaEntity>> base, CountryEntity country) {
		this.base = base;
		this.country = country;
	}
	public int compare(Integer a, Integer b) {
		double aVal = CountryControllerHelper.getResDeltaKoef(country, base.get(a));
		double bVal = CountryControllerHelper.getResDeltaKoef(country, base.get(b));
		if (aVal < bVal) {
	        return 1;
	    } else if (aVal == bVal) {
	    	if(a.intValue() < b.intValue()) {
	    		return 1;
	    	} else if(a.intValue() == b.intValue()) {
	    		return 0;
	    	} else {
	    		return -1;
	    	}
	    } else {
	        return -1;
	    }
	}
}

