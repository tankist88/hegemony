package ru.hegemony.editor.helper;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ru.hegemony.xml.data.AllowedItemSets;
import ru.hegemony.xml.data.AllowedItemSetsType;
import ru.hegemony.xml.data.ItemActionEntity;
import ru.hegemony.xml.data.ItemEntity;

public class ItemHelper {
	private final static DecimalFormat itemCodeFormat = new DecimalFormat("00");
	private final static DecimalFormat actionCodeFormat = new DecimalFormat("00");
	
	public static ItemActionEntity getItemActionByCode(int code, List<ItemActionEntity> actionList) {
		for(ItemActionEntity action : actionList) {
			if(action.getCode() == code) {
				return action;
			}
		}
		return null;
	}
	
	public static List<ItemEntity> getAllItems(AllowedItemSets itemSetsRoot) {
		Set<ItemEntity> itemSet = new HashSet<ItemEntity>();
		
		List<ItemEntity> allItemsList = new ArrayList<ItemEntity>();
		for(AllowedItemSetsType ais : itemSetsRoot.getAllowedItemSet()) {
			allItemsList.addAll(ais.getValues());
		}
		
		for(ItemEntity item : allItemsList) {
			if(!itemAlreadyAddedInSet(itemSet, item)) {
				itemSet.add(item);
			}
		}
		
		List<ItemEntity> result = new ArrayList<ItemEntity>();
		for(ItemEntity item : itemSet) {
			result.add(item);
		}
		
		return result;
	}
	
	public static boolean itemAlreadyAddedInList(List<ItemEntity> list, ItemEntity itemForCheck) {
		if(list == null || itemForCheck == null) {
			return false;
		}
		for(ItemEntity item : list) {
			if(item.getImage().equalsIgnoreCase(itemForCheck.getImage())) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean itemAlreadyAddedInSet(Set<ItemEntity> set, ItemEntity itemForCheck) {
		if(set == null || itemForCheck == null) {
			return false;
		}
		for(ItemEntity item : set) {
			if(item.getImage().equalsIgnoreCase(itemForCheck.getImage())) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean itemListEquals(List<ItemEntity> listOne, List<ItemEntity> listTwo) {
		for(ItemEntity itemOne : listOne) {
			boolean itemFoundInListTwo = false;
			for(ItemEntity itemTwo : listTwo) {
				if (itemOne.getImage().equalsIgnoreCase(itemTwo.getImage())
						&& itemOne.getName().equalsIgnoreCase(itemTwo.getName())
						&& itemOne.getSa() == itemTwo.getSa()) 
				{
					itemFoundInListTwo = true;
					break;
				}
			}
			if(!itemFoundInListTwo) {
				return false;
			}
		}
		return listOne.size() == listTwo.size();
	}
	
	public static ItemEntity getItemByImage(String imageCode, List<ItemEntity> itemList) {
		for(ItemEntity item : itemList) {
			if(item.getImage().equalsIgnoreCase(imageCode)) {
				return item;
			}
		}
		return null;
	}
	
	public static ItemEntity getItemByCode(int code, List<ItemEntity> itemList) {
		for(ItemEntity item : itemList) {
			if(item.getCode() == code) {
				return item;
			}
		}
		return null;
	}
	
	public static int getItemHashCode(ItemEntity item) {
		final int prime = 31;
		int result = 1;
		result = prime * result + item.getCode();
		result = prime * result + ((item.getImage() == null) ? 0 : item.getImage().hashCode());
		result = prime * result + ((item.getName() == null) ? 0 : item.getName().hashCode());
		result = prime * result + item.getSa();
		return result;
	}
	
	public static String getItemSetStringCode(List<ItemEntity> itemSet) {
		String itemSetCode = "";
		for(ItemEntity item : itemSet) {
			itemSetCode += itemCodeFormat.format(item.getCode()) + actionCodeFormat.format(item.getSa());
		}
		return itemSetCode;
	}
}
