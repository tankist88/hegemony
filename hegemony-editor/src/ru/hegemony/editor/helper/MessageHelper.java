package ru.hegemony.editor.helper;

import java.awt.Component;

import javax.swing.JOptionPane;
import javax.xml.bind.JAXBException;

import org.apache.commons.lang3.StringUtils;
import org.apache.xerces.impl.io.MalformedByteSequenceException;

public class MessageHelper {
	private static final String PACKAGE_NAME = "ru.worldcrisis";
	
	public static void showError(String title, Component parrent, Throwable ex) {
		ex.printStackTrace();
		String errorMsg = null;
		if(ex instanceof JAXBException) {
			JAXBException jex = (JAXBException) ex;
			if(jex.getLinkedException() != null) {
				if(jex.getLinkedException() instanceof MalformedByteSequenceException) {
					errorMsg = "������ ����� �� ������������� XML ����� � �������� ����� LevelListRoot";
				} else {
					errorMsg = jex.getLinkedException().getMessage();
				}
			} else {
				errorMsg = jex.getMessage();
			}
		} else {
			errorMsg = ex.getMessage();
		}
		if(StringUtils.isEmpty(errorMsg)) {
			StackTraceElement actualElement = null;
			for(StackTraceElement element : ex.getStackTrace()) {
				if(element.getClassName().contains(PACKAGE_NAME)) {
					actualElement = element;
					break;
				}
			}
			
			errorMsg = "����������� ������. �����: " + ex.getClass();
			
			if(actualElement != null) {
				String errorLine = "\nat " + actualElement.getClassName() + "." + actualElement.getMethodName() + "() : " + actualElement.getLineNumber();
				errorMsg += errorLine;
			}
		}
		JOptionPane.showMessageDialog(parrent, errorMsg, title, JOptionPane.ERROR_MESSAGE);
	}
	
	public static void showCustomError(String title, String msg, Component parrent) {
		JOptionPane.showMessageDialog(parrent, msg, title, JOptionPane.ERROR_MESSAGE);
	}
	
	public static void showInfo(String title, String msg, Component parrent) {
		JOptionPane.showMessageDialog(parrent, msg, title, JOptionPane.INFORMATION_MESSAGE);
	}
}
