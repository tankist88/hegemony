package ru.hegemony.editor.helper;

import java.util.ArrayList;
import java.util.List;

import ru.hegemony.common.data.MainResources;
import ru.hegemony.editor.data.Resource;

public class ResourceHelper {
	public static Resource createResource(MainResources mainResource) {
		return new Resource(mainResource.getCode(), mainResource.getDescription());
	}
	
	public static Resource[] createResourceArray(MainResources[] mainResources) {
		Resource[] resArr = new Resource[mainResources.length];
		for(int i = 0; i < resArr.length; i++) {
			resArr[i] = createResource(mainResources[i]);
		}
		return resArr;
	}
	
	public static List<Resource> createResourceList() {
		return createResourceList(MainResources.values());
	}
	
	public static List<Resource> createResourceList(MainResources[] mainResources) {
		List<Resource> resList = new ArrayList<Resource>();
		for(int i = 0; i < mainResources.length; i++) {
			resList.add(createResource(mainResources[i]));
		}
		return resList;
	}
	
	public static Resource[] createResourceArray(List<Resource> resList) {
		if(resList != null) {
			return resList.toArray(new Resource[resList.size()]);
		} else {
			return null;
		}
	}
}
