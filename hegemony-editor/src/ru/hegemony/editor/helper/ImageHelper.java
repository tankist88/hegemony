package ru.hegemony.editor.helper;

import javax.swing.ImageIcon;

public class ImageHelper {
	private static final String IMAGE_RES_DIR = "res/img/";
	
	public static ImageIcon createImageIcon(String subDir, String filename) {
		if(filename != null) {
			return new ImageIcon(IMAGE_RES_DIR + subDir + "/" + filename + ".png");
		} else {
			return null;
		}
	}
}
