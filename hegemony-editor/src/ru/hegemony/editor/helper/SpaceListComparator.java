package ru.hegemony.editor.helper;

import java.util.Comparator;

import org.apache.commons.lang3.StringUtils;

import ru.hegemony.xml.data.ItemEntity;

public class SpaceListComparator implements Comparator<ItemEntity> {
	public int compare(ItemEntity a, ItemEntity b) {
		if (StringUtils.countMatches(a.getName(), " ") > StringUtils.countMatches(b.getName(), " ")) {
	        return -1;
	    } else if (StringUtils.countMatches(a.getName(), " ") == StringUtils.countMatches(b.getName(), " ")) {
	        return a.getName().compareTo(b.getName());
	    } else {
	        return 1;
	    }
	}
}
