package ru.hegemony.editor.helper;

import java.util.Comparator;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

public class SpaceMapComparator implements Comparator<Integer> {
	Map<Integer, String> base;

	public SpaceMapComparator(Map<Integer, String> base) {
		this.base = base;
	}
	public int compare(Integer a, Integer b) {
		if (StringUtils.countMatches(base.get(a), " ") > StringUtils.countMatches(base.get(b), " ")) {
	        return -1;
	    } else if (StringUtils.countMatches(base.get(a), " ") == StringUtils.countMatches(base.get(b), " ")) {
	        return a.compareTo(b);
	    } else {
	        return 1;
	    }
	}
}
