package ru.hegemony.editor.helper;

import java.util.Comparator;
import java.util.Map;

public class ValueComparatorAsc implements Comparator<Integer> {
	Map<Integer, Double> base;

	public ValueComparatorAsc(Map<Integer, Double> base) {
		this.base = base;
	}
	public int compare(Integer a, Integer b) {
		if (base.get(a).doubleValue() < base.get(b).doubleValue()) {
	        return -1;
	    } else if (base.get(a).doubleValue() == base.get(b).doubleValue()) {
	        return a.compareTo(b);
	    } else {
	        return 1;
	    }
	}
}
