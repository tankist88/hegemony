package ru.hegemony.editor.exception;

public class BusinessException extends Exception {
	private static final long serialVersionUID = 1193427109823731190L;

	public BusinessException(String msg) {
		super(msg);
	}
}
