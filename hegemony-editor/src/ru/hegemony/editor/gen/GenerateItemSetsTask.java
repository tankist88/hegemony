package ru.hegemony.editor.gen;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.math3.random.RandomDataGenerator;

import ru.hegemony.common.helper.DataHelper;
import ru.hegemony.editor.data.DistType;
import ru.hegemony.editor.data.EffectKey;
import ru.hegemony.editor.data.EffectValue;
import ru.hegemony.editor.data.LevelHolder;
import ru.hegemony.editor.data.Resource;
import ru.hegemony.editor.exception.BusinessException;
import ru.hegemony.editor.helper.CountryControllerHelper;
import ru.hegemony.editor.helper.ItemHelper;
import ru.hegemony.editor.helper.MessageHelper;
import ru.hegemony.editor.helper.ResourceHelper;
import ru.hegemony.editor.render.ProgressBarDialog;
import ru.hegemony.editor.wizard.BaseWizardPage;
import ru.hegemony.xml.data.AllowedItemSetsType;
import ru.hegemony.xml.data.CountryEntity;
import ru.hegemony.xml.data.DistributionType;
import ru.hegemony.xml.data.DstParameterType;
import ru.hegemony.xml.data.EffectsType;
import ru.hegemony.xml.data.ItemEntity;
import ru.hegemony.xml.data.ItemSetKey;
import ru.hegemony.xml.data.ItemValueType;
import ru.hegemony.xml.data.ObjectFactory;
import ru.hegemony.xml.data.TurnEntity;
import ru.hegemony.xml.data.ValueCode;

public class GenerateItemSetsTask extends ProgressBarDialog {
	private List<ItemEntity> allowedItemList;
	private LevelHolder levelHolder;
	private Map<EffectKey, EffectValue> itemInfluenceMap;
	
	public GenerateItemSetsTask(
			List<ItemEntity> allowedItemList, 
			Map<EffectKey, EffectValue> existItemInfluenceMap, 
			LevelHolder levelHolder) 
	{
		super(null, true);
		this.allowedItemList = allowedItemList;
		this.itemInfluenceMap = existItemInfluenceMap;
		this.levelHolder = levelHolder;
	}

	@Override
	protected void doWork(Task worker) throws Exception {
		clearLevelData();
		Map<Integer, AllowedItemSetsType> itemSets = generateUniqueItemSets(allowedItemList, worker);
		this.levelHolder.getLevel().getItemSets().addAll(itemSets.values());
		
		if(!fillItemInfluenceMap()) {
			throw new IllegalStateException("������� itemInfluenceMap ��������� ������������!");
		}
		
		fillItemsEffects();
		fillEffects();
		fillTurns(itemSets);
	}
	
	private void clearLevelData() {
		this.levelHolder.getLevel().getItemSets().clear();
		this.levelHolder.getLevel().getEffects().clear();
		this.levelHolder.getLevel().getItemsEffects().clear();
		this.levelHolder.getLevel().getTurns().clear();
	}
	
	private void fillTurns(Map<Integer, AllowedItemSetsType> itemSets) throws BusinessException {
		List<EffectsType> effects = levelHolder.getLevel().getEffects();
		List<CountryEntity> countries = levelHolder.getLevel().getCountries();		
		TreeMap<Integer, Double> cMap = CountryControllerHelper.getCountryEffectMap(countries, effects);
		levelHolder.getLevel().getEventDesc().setWorstItemSet(itemSets.get(cMap.firstKey()).getKeyStr());
		ObjectFactory objectFactory = new ObjectFactory();
		for(CountryEntity country : countries) {
			TreeMap<Integer, Double> sortedMap = CountryControllerHelper.getSortedDescTurnMap(country, effects);
			List<String> turnKeys = CountryControllerHelper.getTurnKeys(country, sortedMap, itemSets);
			if(turnKeys.size() < 3) {
				throw new BusinessException("��� ������ \"" + country.getName() + "\" ��� ��� ������� ���� ����������� ������� ���������");
			}
			country.setWorstItemSet(itemSets.get(sortedMap.lastKey()).getKeyStr());
			TurnEntity turnEntity = objectFactory.createTurnEntity();
			turnEntity.setCountryCode(country.getCode());
			turnEntity.setGkSize(turnKeys.size());
			turnEntity.getGk().addAll(turnKeys);	
			levelHolder.getLevel().getTurns().add(turnEntity);
		}
	}
	
	private void fillEffects() {
		List<Resource> allResources = ResourceHelper.createResourceList();
		ObjectFactory objectFactory = new ObjectFactory();
		for(AllowedItemSetsType itemSet : this.levelHolder.getLevel().getItemSets()) {
			for(CountryEntity country : this.levelHolder.getLevel().getCountries()) {
				ItemSetKey key = createItemSetKey(itemSet, country);		
				EffectsType effect = objectFactory.createEffectsType();
				effect.setKey(key);
				effect.getValues().addAll(ItemSetAttributeGenerator.createResourceDeltaList(this.itemInfluenceMap, itemSet, country, allResources));
				this.levelHolder.getLevel().getEffects().add(effect);
			}
		}
	}
	
	private void fillItemsEffects() {
		ObjectFactory objectFactory = new ObjectFactory();
		
		for(Entry<EffectKey, EffectValue> entry : this.itemInfluenceMap.entrySet()) {
			ItemValueType v = objectFactory.createItemValueType();
			ValueCode vc = objectFactory.createValueCode();
			vc.setActionCode(entry.getKey().getActionCode());
			vc.setCountryCode(entry.getKey().getCountryCode());
			ItemEntity item = ItemHelper.getItemByImage(entry.getKey().getItemId(), this.levelHolder.getLevel().getItems());
			vc.setItemCode(item.getCode());
			vc.setResourceCode(entry.getKey().getResourceCode());
			v.setValueCode(vc);
			v.setValue(entry.getValue().getValue());
			this.levelHolder.getLevel().getItemsEffects().add(v);
		}
	}
	
	/**
	 * ���������� �� ������������� ������ ��������� �� ������ ���������� ���������
	 * @param allowedItemList - ������ ���������� ���������
	 * @param setLength - ����� ������������ ������������������ ���������
	 * @return
	 */
	protected Map<Integer, AllowedItemSetsType> generateUniqueItemSets(final List<ItemEntity> allowedItemList, Task worker) {
		int setLength = allowedItemList.size() > BaseWizardPage.MAX_ITEMS_IN_SET ? BaseWizardPage.MAX_ITEMS_IN_SET : allowedItemList.size();
		TreeMap<Integer, AllowedItemSetsType> itemSets = new TreeMap<Integer, AllowedItemSetsType>(new IntKeysComparator());
		int[] indexCounters = new int[setLength];
		for(int i = 0; i < indexCounters.length; i++) {
			if(i == indexCounters.length - 1) {
				indexCounters[i] = 0;
			} else {
				indexCounters[i] = -1;
			}
		}
		Map<Integer, Boolean> itemSetsIndex = new HashMap<Integer, Boolean>(3000, (float) 1.0);
		
		ObjectFactory objectFactory = new ObjectFactory();
		
		boolean needContinue = true;
		while (needContinue) {
			int itemSetHashCode = 0;
			List<ItemEntity> itemList = new ArrayList<ItemEntity>();
			for(int i = 0; i < indexCounters.length; i++) {
				if(indexCounters[i] >= 0) {
					ItemEntity item = allowedItemList.get(indexCounters[i]);
					if(!ItemHelper.itemAlreadyAddedInList(itemList, item)) {
						itemSetHashCode += ItemHelper.getItemHashCode(item);
						itemList.add(item);
					}
				}
			}
			if(correctItemList(itemList) && !setAlreadyAdded(itemSetsIndex, itemSetHashCode)) {
				AllowedItemSetsType itemSet = objectFactory.createAllowedItemSetsType();
				itemSet.setKey(itemSetHashCode);
				itemSet.getValues().addAll(itemList);
				itemSet.setKeyStr(ItemHelper.getItemSetStringCode(itemList));
				itemSets.put(itemSetHashCode, itemSet);
				itemSetsIndex.put(itemSetHashCode, true);
			}
			needContinue = DataHelper.refreshCountersWithoutDuplicate(indexCounters, setLength, allowedItemList.size());
		}	
		return itemSets;
	}
	
	private class IntKeysComparator implements Comparator<Integer> {
		public int compare(Integer a, Integer b) {
			if(a != null && b != null) {
				if(a.intValue() > b.intValue()) {
					return 1;
				} else if(a.intValue() < b.intValue()) {
					return -1;
				} else {
					return 0;
				}
			} else if(a != null && b == null) {
				return 1;
			} else if(a == null && b != null) {
				return -1;
			} else {
				return 0;
			}
		}
	}
	
	private boolean setAlreadyAdded(Map<Integer, Boolean> itemSetsIndex, int itemSetHashCode) {
		return itemSetsIndex.get(itemSetHashCode) != null && itemSetsIndex.get(itemSetHashCode).booleanValue();
	}
	
	private boolean correctItemList(List<ItemEntity> itemList) {
		return itemList != null && itemList.size() > 0 && itemList.size() <= BaseWizardPage.MAX_ITEMS_IN_SET;
	}

	private boolean fillItemInfluenceMap() throws Exception {
		int oneItemSetCount = 0;
		for(AllowedItemSetsType itemSet : this.levelHolder.getLevel().getItemSets()) {
			if(itemSet.getValues().size() == 1) {
				oneItemSetCount++;
			}
		}
		List<CountryEntity> countries = this.levelHolder.getLevel().getCountries();
		int resourcesCount = ResourceHelper.createResourceList().size();
		int controlSize = oneItemSetCount * countries.size() * resourcesCount;
		if(controlSize != this.itemInfluenceMap.size()) {
			MessageHelper
					.showCustomError(
							"������ �������� ����������",
							"�������� ��������� �������� �� ��������� ��� ���� ������������ ���������.\n��������� ���������� ��������� � ������ ���������.",
							getParentFrame());
			return false;
		}
		try {
			for(EffectKey key : this.itemInfluenceMap.keySet()) {
				getAndSaveItemValue(key);
			}
		} catch(BusinessException bex) {
			MessageHelper.showCustomError("������ �������� ����������", bex.getMessage(), getParentFrame());
			return false;
		}
		return true;
	}
	
	private double getAndSaveItemValue(EffectKey key) throws Exception {
		return getAndSaveItemValue(key, false);
	}
	private double getAndSaveItemValue(EffectKey key, boolean ownKey) throws Exception {
		EffectValue effectValue = this.itemInfluenceMap.get(key);
		if(effectValue == null) {
			throw new BusinessException("�� ������� �������� �������� ��� ����� [" + createEffectKeyStr(key) + "]. � ������� �� ��������� ��� ��������� �������.");
		}
		if(!effectValue.isValueMode() && effectValue.getValue() == null) {
			if(effectValue.getDstType().getFunction() != null && !effectValue.getDstType().getFunction().isEmpty()) {
				// TODO ����������� ������������� �� � ��������� �������������� � �������������� Apache Commons Math
				throw new NotImplementedException("������ � ����������������� ��������� ������������� �� ����������� � ������ ������ ��");
			} else {
				effectValue.setValue(getRndVal(effectValue.getDstType(), key, ownKey));
			}
		}
		return effectValue.getValue().doubleValue();
	}
	
	private double getRndVal(DistributionType dst, EffectKey currentKey, boolean ownKey) throws Exception {
		RandomDataGenerator randomData = new RandomDataGenerator();
		DistType dstType = DistType.findByCode(dst.getStdType());
		if(!ownKey) {
			DstParameterType returnParam = null;
			for(DstParameterType dstParam : dst.getParameterList()) {
				if(dstParam.getName().equals("return")) {
					if(StringUtils.isEmpty(dstParam.getExprValue())) {
						dstParam.setExprValue("[" + createEffectKeyStr(currentKey) + "]");
					}
					returnParam = dstParam;
					break;
				}
			}
			if(returnParam == null) {
				ObjectFactory objectFactory = new ObjectFactory();
				returnParam = objectFactory.createDstParameterType();
				returnParam.setName("return");
				returnParam.setExprValue("[" + createEffectKeyStr(currentKey) + "]");
			}
			return (Double) getParamForMethod(returnParam, dstType, currentKey);
		} else {
			for(Method m : randomData.getClass().getMethods()) {
				if(m.getName().equals(dstType.getMethodName())) {
					int realElementsCount = 0;
					for(DstParameterType dstParam : dst.getParameterList()) {
						if(!dstParam.getName().equals("return")) {
							realElementsCount++;
						}
					}
					Object[] params = new Object[realElementsCount];
					int i = 0;
					for(DstParameterType dstParam : dst.getParameterList()) {
						if(!dstParam.getName().equals("return")) {
							params[i] = getParamForMethod(dstParam, dstType, currentKey);
							i++;
						}
					}
					return DataHelper.round(((Double) m.invoke(randomData, params)).doubleValue(), 3);
				}
			}
		}
		return 0.0;
	}
	
	private List<String> getRelationKeysStr(String s) {
		List<String> result = new ArrayList<String>();
		Pattern p = Pattern.compile("\\[.*?\\]");
		Matcher m = p.matcher(s);
		while (m.find()) {
			String b = m.group();
			result.add(b.substring(1, b.length() - 1));
		}
		return result;
	}
	
	private EffectKey createEffectKey(String strKey) {
		// ������ strKey: c1r2a3i4
		EffectKey key = new EffectKey();
		key.setActionCode(getElementFromKeyString("a", strKey));
		key.setCountryCode(getElementFromKeyString("c", strKey));
		key.setResourceCode(getElementFromKeyString("r", strKey));
		int itemCode = getElementFromKeyString("i", strKey);
		ItemEntity item = ItemHelper.getItemByCode(itemCode, levelHolder.getLevel().getItems());
		key.setItemId(item.getImage());
		return key;
	}
	
	private String createEffectKeyStr(EffectKey key) {
		return createEffectKeyStr(key, levelHolder.getLevel().getItems());
	}
	
	public static String createEffectKeyStr(EffectKey key, List<ItemEntity> items) {
		// ������ strKey: c1r2a3i4
		ItemEntity item = ItemHelper.getItemByImage(key.getItemId(), items);
		return 
				"c" + key.getCountryCode() +
				"r" + key.getResourceCode() + 
				"a" + key.getActionCode() + 
				"i" + item.getCode();
	}
	
	private int getElementFromKeyString(String element, String strKey) {
		Pattern p = Pattern.compile(element + "[0-9]*");
		Matcher m = p.matcher(strKey);
		while (m.find()) {
			String b = m.group();
			return Integer.parseInt(b.substring(1));
		}
		return -1;
	}
	
	private Object getParamForMethod(DstParameterType dstParam, DistType dstType, EffectKey currentKey) throws Exception {
		int i = 0;
		for(String name : dstType.getParamNames()) {
			if(name.equalsIgnoreCase(dstParam.getName())) {
				double paramValue = 0.0;
				if(dstParam.getExprValue() != null && !dstParam.getExprValue().isEmpty()) {
					List<String> exprKeyList = getRelationKeysStr(dstParam.getExprValue());
					String expr = dstParam.getExprValue();
					for(String exprKey : exprKeyList) {
						double actVaclue = 0.0;
						if(createEffectKeyStr(currentKey).equals(exprKey)) {
							actVaclue = getAndSaveItemValue(createEffectKey(exprKey), true);
						} else {
							actVaclue = getAndSaveItemValue(createEffectKey(exprKey), false);
						}
						expr = expr.replaceAll("\\[" + exprKey + "\\]", Double.toString(actVaclue));
					}
					
					Expression e = new ExpressionBuilder(expr).build();
	                paramValue = e.evaluate();
				} else {
					paramValue = dstParam.getValue();
				}
				String paramType = dstType.getParamTypes()[i];
				if(paramType.equalsIgnoreCase("double")) {
					return (double) paramValue;
				} else if(paramType.equalsIgnoreCase("int")) {
					return (int) paramValue;
				} else if(paramType.equalsIgnoreCase("short")) {
					return (short) paramValue;
				} else if(paramType.equalsIgnoreCase("long")) {
					return (long) paramValue;
				}
			}
			i++;
		}
		return null;
	}
	
	private ItemSetKey createItemSetKey(AllowedItemSetsType itemSet, CountryEntity country) {
		ObjectFactory objectFactory = new ObjectFactory();
		ItemSetKey key = objectFactory.createItemSetKey();
		key.setCountryCode(country.getCode());
		key.setItemSetCode(itemSet.getKey());
		return key;
	}
}
