package ru.hegemony.editor.gen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import ru.hegemony.common.helper.DataHelper;
import ru.hegemony.editor.data.EffectKey;
import ru.hegemony.editor.data.EffectValue;
import ru.hegemony.editor.data.GoodKey;
import ru.hegemony.editor.data.Resource;
import ru.hegemony.editor.helper.ResourceHelper;
import ru.hegemony.xml.data.AllowedItemSetsType;
import ru.hegemony.xml.data.CountryEntity;
import ru.hegemony.xml.data.ItemActionEntity;
import ru.hegemony.xml.data.ItemEntity;
import ru.hegemony.xml.data.ObjectFactory;
import ru.hegemony.xml.data.ResourceDeltaEntity;

/**
 * ����� ��� ��������� ��������� ������ ���������
 * ��������:
 * 1) ��������� �������� � ������ �������� ������ ���������
 * 2) ����� ������ ������ �� ������ ������ ���������
 */
public class ItemSetAttributeGenerator {
	private static final Random RANDOM_GENERATOR = new Random(System.currentTimeMillis());
	
	/** ���������� ��������� ������� ����� ��������� ������������� ������� �� ��� ������ */
	private static final double GOOD_ITEM_PERSENT = 10.0/100.0;
	/** ���������� ��������� ������� ����� ��������� ������������� ������� �� ���������� ������ */
	private static final double COUNTRY_GOOD_ITEM_PERSENT = 60.0/100.0;
	
	public static final double ALL_GOOD_MAX_DELTA = 60.0;
	public static final double ALL_GOOD_MIN_DELTA = 50.0;
	
	private static final double COUNTRY_GOOD_MAX_DELTA = 70.0;
	private static final double COUNTRY_GOOD_MIN_DELTA = 60.0;
	
	private static final double COUNTRY_BAD_MAX_DELTA = -70.0;
	private static final double COUNTRY_BAD_MIN_DELTA = -99.0;
	
	public static final double DEFAULT_MAX_RND_DELTA = 50.0;
	public static final double DEFAULT_MIN_RND_DELTA = -50.0;
	
	public static double getRandomResourceDelta(double begin, double end) {
		double randValue = RANDOM_GENERATOR.nextDouble();
		return DataHelper.round(begin + (randValue * (end - begin)), 3);
	}
	
	private static List<Double> getItemSetDeltaByRes(
			Resource res, 
			AllowedItemSetsType itemSet,
			Map<EffectKey, EffectValue> itemInfluenceMap, 
			CountryEntity country) 
	{
		List<Double> deltaList = new ArrayList<Double>();
		for(ItemEntity item : itemSet.getValues()) {
			EffectKey key = new EffectKey();
			key.setActionCode(item.getSa());
			key.setCountryCode(country.getCode());
			key.setResourceCode(res.getCode());
			key.setItemId(item.getImage());
			EffectValue value = itemInfluenceMap.get(key);
			deltaList.add(value.getValue());
		}
		return deltaList;
	}
	
	public static List<ResourceDeltaEntity> createResourceDeltaList(
			Map<EffectKey, EffectValue> itemInfluenceMap, 
			AllowedItemSetsType itemSet, 
			CountryEntity country, 
			List<Resource> resList) 
	{
		List<ResourceDeltaEntity> resDeltaList = new ArrayList<ResourceDeltaEntity>();
		ObjectFactory objectFactory = new ObjectFactory();
		
		for(Resource res : resList) {
			ResourceDeltaEntity delta = objectFactory.createResourceDeltaEntity();
			delta.setResourceCode(res.getCode());
			List<Double> deltaList = getItemSetDeltaByRes(res, itemSet, itemInfluenceMap, country);
			double mean = getMean(deltaList);
			double roundedMean = DataHelper.round(mean, 3);
			delta.setDelta(roundedMean);
			resDeltaList.add(delta);
		}
		
		return resDeltaList;
	}
	
	private static double getMean(List<Double> values) {
		double sum = 0.0;
		for(Double value : values) {
			sum += value.doubleValue();
		}
		return sum/values.size();
	}
	
	public static Map<EffectKey, EffectValue> generateItemEffectMap(final List<ItemEntity> allowedItemList, List<CountryEntity> countryList) {
		Map<EffectKey, EffectValue> result = new HashMap<EffectKey, EffectValue>();
		List<Resource> resourceList = ResourceHelper.createResourceList();
		
		Random generator = new Random(System.currentTimeMillis());
		
		int goodItemCount = (int) (GOOD_ITEM_PERSENT * allowedItemList.size());
		if(goodItemCount <= 0) {
			goodItemCount = 1;
		}
		
		int countryGoodItemCount = (int) ((COUNTRY_GOOD_ITEM_PERSENT * allowedItemList.size()) / countryList.size());
		if(countryGoodItemCount <= 0) {
			countryGoodItemCount = 1;
		}
		
		GoodKey allGk = getGoodKeys(goodItemCount, allowedItemList, generator);
		
		List<ItemEntity> remainingItems = new ArrayList<ItemEntity>();
		for(ItemEntity item : allowedItemList) {
			if(!allGk.getItemCodes().contains(item.getCode())) {
				remainingItems.add(item);
			}
		}
		
		Map<Integer, GoodKey> countryGk = new HashMap<Integer, GoodKey>();
		for(CountryEntity country : countryList) {
			countryGk.put(country.getCode(), getGoodKeys(countryGoodItemCount, remainingItems, generator, countryGk, country.getCode()));
		}
		
		for(ItemEntity item : allowedItemList) {
			for(ItemActionEntity action : item.getActions()) {
				for(CountryEntity country : countryList) {
					for(Resource resource : resourceList) {
						EffectKey key = new EffectKey();
						key.setActionCode(action.getCode());
						key.setCountryCode(country.getCode());
						key.setResourceCode(resource.getCode());
						key.setItemId(item.getImage());
						GoodKey cgk = countryGk.get(country.getCode());
						Integer rndAction = cgk.getActionCodes().get(item.getCode());
						Integer allRndAction = allGk.getActionCodes().get(item.getCode());
						double genDelta = 0.0;
						if(allGk.getItemCodes().contains(item.getCode()) && allRndAction != null && allRndAction.equals(action.getCode())) {
							genDelta = ItemSetAttributeGenerator.getRandomResourceDelta(ALL_GOOD_MIN_DELTA, ALL_GOOD_MAX_DELTA);
						} else if(itemContainsForOtherCountries(countryGk, item.getCode(), action.getCode(), country.getCode())) {
							genDelta = ItemSetAttributeGenerator.getRandomResourceDelta(COUNTRY_BAD_MIN_DELTA, COUNTRY_BAD_MAX_DELTA);
						} else if(cgk.getItemCodes().contains(item.getCode()) && rndAction != null && rndAction.equals(action.getCode())) {
							genDelta = ItemSetAttributeGenerator.getRandomResourceDelta(COUNTRY_GOOD_MIN_DELTA, COUNTRY_GOOD_MAX_DELTA);
						} else {
							genDelta = ItemSetAttributeGenerator.getRandomResourceDelta(DEFAULT_MIN_RND_DELTA, DEFAULT_MAX_RND_DELTA);
						}
						EffectValue value = new EffectValue();
						value.setValue(genDelta);
						result.put(key, value);
					}
				}
			}
		}
		
		return result;
	}
	
	private static boolean itemContainsForOtherCountries(Map<Integer, GoodKey> countryGk, int itemCode, int actionCode, int currentCountryCode) {
		for(Entry<Integer, GoodKey> entry : countryGk.entrySet()) {
			if(!entry.getKey().equals(currentCountryCode)) {
				GoodKey gk = entry.getValue();
				Integer action = gk.getActionCodes().get(itemCode);
				if(gk.getItemCodes().contains(itemCode) && action.equals(actionCode)) {
					return true;
				}
			}
		}
		return false;
	}
	
	private static GoodKey getGoodKeys(int count, final List<ItemEntity> allowedItemList, Random generator) {
		return getGoodKeys(count, allowedItemList, generator, null, null);
	}
	
	private static GoodKey getGoodKeys(int count, final List<ItemEntity> allowedItemList, Random generator, Map<Integer, GoodKey> countryGk, Integer currentCountryCode) {
		List<Integer> goodItemCodes = new ArrayList<Integer>();
		Map<Integer, Integer> goodActionCodes = new HashMap<Integer, Integer>();
		int realGoodItemCount = allowedItemList.size() > count ? count : allowedItemList.size();
		int currentCount = 0;
		for(ItemEntity item : allowedItemList) {
			if(currentCount >= realGoodItemCount) {
				break;
			}
			ItemActionEntity selectedAction = null;
			for(ItemActionEntity action : item.getActions()) {
				if(countryGk != null && currentCountryCode != null && itemContainsForOtherCountries(countryGk, item.getCode(), action.getCode(), currentCountryCode)) { 
					continue;
				} else {
					selectedAction = action;
					break;
				}
			}
			if(selectedAction == null) {
				continue;
			}
			goodItemCodes.add(item.getCode());
			goodActionCodes.put(item.getCode(), selectedAction.getCode());
			currentCount++;
		}
		return new GoodKey(goodItemCodes, goodActionCodes);
	}
}
