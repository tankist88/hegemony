package ru.hegemony.editor.wizard;

import java.awt.Dimension;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.swing.JList;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.ciscavate.cjwizard.WizardSettings;

import ru.hegemony.editor.data.DefaultResourceData;
import ru.hegemony.editor.data.EntityType;
import ru.hegemony.editor.data.LevelHolder;
import ru.hegemony.editor.helper.MessageHelper;
import ru.hegemony.xml.data.Countries;
import ru.hegemony.xml.data.CountryEntity;
import ru.hegemony.xml.data.ObjectFactory;
import ru.hegemony.xml.data.ResourcesEntity;

public class CountryListWizardPage extends ListTransferWizardPage {
	private static final long serialVersionUID = -1910303740736387044L;
	
	/** ���� � ����������� ����� */
	private static final String COUNTRY_CATALOG_PATH = "res/data/countries.xml";

	public CountryListWizardPage(LevelHolder levelFullInfo, Dimension dialogSize, boolean isLastPage) {
		super(levelFullInfo, "������ ����� ��������", "������ ����� �������� ��� ������", dialogSize, isLastPage);
	}
	
	@Override
	public void updateSettings(WizardSettings settings) {
		super.updateSettings(settings);
		getLevelFullInfo().getLevel().getCountries().clear();
		if(getSelectedListModel().getSize() > 0) {
			for(int i = 0; i < getSelectedListModel().getSize(); i++) {
				getLevelFullInfo().getLevel().getCountries().add((CountryEntity) getSelectedListModel().getElementAt(i));
			}
		}
	}

	@Override
	protected EntityType getListEntityType() {
		return EntityType.COUNTRY;
	}

	@Override
	protected void rightListToLeftAction(JList selectedList) {
		CountryEntity selectedCountry = (CountryEntity) selectedList.getSelectedValue();
		if(selectedCountry != null) {
			getSelectedListModel().removeElement(selectedCountry);
		}
	}

	@Override
	protected void leftListToRightAction(JList allowedList) {
		CountryEntity selectedCountry = (CountryEntity) allowedList.getSelectedValue();
		if(selectedCountry != null) {
			boolean countryExist = false;
			for(int i = 0; i < getSelectedListModel().getSize(); i++) {
				CountryEntity existsCountry = (CountryEntity) getSelectedListModel().getElementAt(i);
				if(existsCountry.getCode() == selectedCountry.getCode()) {
					countryExist = true;
				}
			}
			if(!countryExist) {
				DefaultResourceValuesDialog resDefaultsDialog = new DefaultResourceValuesDialog(null, selectedCountry.getWeaknessPercent());
				Set<DefaultResourceData> defDataSet = resDefaultsDialog.getDefaultResourceSet();
				if(defDataSet != null && !defDataSet.isEmpty()) {
					List<ResourcesEntity> resList = new ArrayList<ResourcesEntity>();
					ObjectFactory objectFactory = new ObjectFactory();
					for(DefaultResourceData defData : defDataSet) {
						ResourcesEntity res = objectFactory.createResourcesEntity();
						res.setCode(defData.getResource().getCode());
						res.setName(defData.getResource().getDescription());
						res.setValue(defData.getValue());
						res.setWeight(defData.getWeight());
						resList.add(res);
					}
					selectedCountry.getResources().clear();
					selectedCountry.getResources().addAll(resList);
					selectedCountry.setWeaknessPercent(resDefaultsDialog.getWeaknessKoef());
					getSelectedListModel().addElement(selectedCountry);
				}
			}
		}
	}

	@Override
	protected void initSelected() {
		super.initSelected();
		try {
			if (getLevelFullInfo().getLevel() != null
					&& getLevelFullInfo().getLevel().getCountries() != null) 
			{
				for(CountryEntity country : getLevelFullInfo().getLevel().getCountries()) {
					getSelectedListModel().addElement(country);
				}
			}
		} catch (Exception e) {
			MessageHelper.showError("������ �������� ������", this, e);
		}
	}

	@Override
	protected void initAllowed() {
		super.initAllowed();
		try {
			Unmarshaller unmarshaller = JAXBContext.newInstance("ru.hegemony.xml.data").createUnmarshaller();
			JAXBElement<Countries> element = unmarshaller.unmarshal(new StreamSource(new FileInputStream(COUNTRY_CATALOG_PATH)), Countries.class);
			List<CountryEntity> allowedCountryList = element.getValue().getCountry();
			
			for(CountryEntity country : allowedCountryList) {
				getAllowedListModel().addElement(country);
			}
		} catch (FileNotFoundException fex) {
			System.err.println(fex.getMessage());
		} catch (Exception e) {
			MessageHelper.showError("������ �������� ������", this, e);
		}
	}

	@Override
	protected void editAction(JList selectedList) {
		// Stub
	}

	@Override
	protected void generateRandomElementChar(JList allowedList) {
		// Stub
	}
}
