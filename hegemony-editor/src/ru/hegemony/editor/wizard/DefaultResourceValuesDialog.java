package ru.hegemony.editor.wizard;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import ru.hegemony.editor.data.DefaultResourceData;
import ru.hegemony.editor.data.Resource;
import ru.hegemony.editor.helper.CountryControllerHelper;
import ru.hegemony.editor.helper.MessageHelper;
import ru.hegemony.editor.helper.ResourceHelper;

public class DefaultResourceValuesDialog extends JDialog {
	private static final long serialVersionUID = 6553111779599686772L;
	
	private Set<DefaultResourceData> defaultResourceSet;
	private int weaknessKoef;
	
	public DefaultResourceValuesDialog(JFrame ownerFrame, int weaknessPercent) {
		super(ownerFrame, true);
		this.defaultResourceSet = new HashSet<DefaultResourceData>();
		this.weaknessKoef = weaknessPercent;
		initComponets();
	}
	
	private void initComponets() {
		this.getContentPane().setLayout(new BoxLayout(this.getContentPane(), BoxLayout.X_AXIS));
		
		final List<Resource> allResources = ResourceHelper.createResourceList();
		
		final JTextField defValueText = new JTextField();
		defValueText.setFont(new Font("SansSerif", Font.PLAIN, 18));
		
		final JTextField weightText = new JTextField();
		weightText.setFont(new Font("SansSerif", Font.PLAIN, 18));
		if(allResources.size() <= 1) {
			weightText.setText("1");
			weightText.setEnabled(false);
		}
		
		final JTextField weaknessText = new JTextField();
		weaknessText.setFont(new Font("SansSerif", Font.PLAIN, 18));
		weaknessText.setText(Integer.toString(weaknessKoef));
		
		final JComboBox resourceComboBox = new JComboBox(ResourceHelper.createResourceArray(allResources));
		resourceComboBox.setBackground(Color.WHITE);
		resourceComboBox.setFont(new Font("SansSerif", Font.PLAIN, 18));
		if(allResources.size() <= 1) {
			resourceComboBox.setEnabled(false);
		}
		resourceComboBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				defValueText.setText(null);
				weightText.setText(null);
			}
		});
		
		defValueText.addActionListener(new AddResourceDataActionListener(allResources, defValueText, weaknessText, weightText, resourceComboBox));
		
		JButton saveButton = new JButton();
		saveButton.setText("��������");
		saveButton.addActionListener(new AddResourceDataActionListener(allResources, defValueText, weaknessText, weightText, resourceComboBox));
		
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		
		JPanel resCbPanel = new JPanel();
		resCbPanel.setLayout(new BorderLayout());
		resCbPanel.add(resourceComboBox, BorderLayout.CENTER);
		resCbPanel.setBorder(BorderFactory.createEmptyBorder(5, 0, 5, 0));
		
		JPanel defValuePanel = new JPanel();
		defValuePanel.setLayout(new BorderLayout());
		defValuePanel.add(defValueText, BorderLayout.CENTER);
		defValuePanel.setBorder(BorderFactory.createEmptyBorder(5, 0, 5, 0));
		
		JPanel weightPanel = new JPanel();
		weightPanel.setLayout(new BorderLayout());
		weightPanel.add(weightText, BorderLayout.CENTER);
		weightPanel.setBorder(BorderFactory.createEmptyBorder(5, 0, 5, 0));
		
		JPanel weaknessPanel = new JPanel();
		weaknessPanel.setLayout(new BorderLayout());
		weaknessPanel.add(weaknessText, BorderLayout.CENTER);
		weaknessPanel.setBorder(BorderFactory.createEmptyBorder(5, 0, 5, 0));
		
		JPanel saveButtonPanel = new JPanel();
		saveButtonPanel.setLayout(new BorderLayout());
		saveButtonPanel.add(saveButton, BorderLayout.CENTER);
		saveButtonPanel.setBorder(BorderFactory.createEmptyBorder(5, 0, 5, 0));
		
		JPanel resLabelPanel = new JPanel();
		resLabelPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
		resLabelPanel.add(new JLabel("������"));
		JPanel valueLabelPanel = new JPanel();
		valueLabelPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
		valueLabelPanel.add(new JLabel("��������� ��������"));
		JPanel weightLabelPanel = new JPanel();
		weightLabelPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
		weightLabelPanel.add(new JLabel("���"));
		JPanel weaknessLabelPanel = new JPanel();
		weaknessLabelPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
		weaknessLabelPanel.add(new JLabel("����. �������� (%)"));
		
		panel.add(resLabelPanel);
		panel.add(resCbPanel);
		panel.add(valueLabelPanel);
		panel.add(defValuePanel);
		panel.add(weightLabelPanel);
		panel.add(weightPanel);
		panel.add(weaknessLabelPanel);
		panel.add(weaknessPanel);
		panel.add(saveButtonPanel);
		
		this.getContentPane().add(panel);
		
		Dimension dialogSize = new Dimension(260, 300);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setTitle("�������� � ��� ������� �� ���������");
		this.setSize(dialogSize);
		this.setResizable(false);
		this.setPreferredSize(dialogSize);
		this.pack();
		this.setVisible(true);
	}
	
	private Double getWeightFromString(String text) {
		Double value = null;
		try {
			value = Double.valueOf(text);
			if(value.doubleValue() < 0.0 || value.doubleValue() > 1.0) {
				MessageHelper.showCustomError(
						"������ � ��������", 
						"�������� ��� ��� ������� ����� ������ � �������� �� 0.0 �� 1.0 ������������", 
						this);
				value = null;
			}
		} catch (NumberFormatException nex) {
			MessageHelper.showCustomError(
					"������ � ��������", 
					"�������� ��� ��� ������� ������ ����� ������ ����� � ������", 
					this);
			value = null;
		}
		return value;
	}
	
	private Double getValueFromString(String text) {
		Double value = null;
		try {
			value = Double.valueOf(text);
		} catch (NumberFormatException nex) {
			MessageHelper.showCustomError(
					"������ � ��������", 
					"�������� ��� ��� ������� ������ ����� ������ ����� � ������", 
					this);
			value = null;
		}
		return value;
	}
	
	private Integer getWeaknessFromString(String text) {
		Integer value = null;
		try {
			value = Integer.valueOf(text);
		} catch (NumberFormatException nex) {
			MessageHelper.showCustomError(
					"������ � ��������", 
					"���������� �������� ������ ������ ����������� � ��������� ����� ������.", 
					this);
			value = null;
		}
		return value;
	}

	public Set<DefaultResourceData> getDefaultResourceSet() {
		return defaultResourceSet;
	}
	
	public int getWeaknessKoef() {
		return weaknessKoef;
	}

	private class AddResourceDataActionListener implements ActionListener {
		private List<Resource> allResources;
		private JTextField defValueText;
		private JTextField weightText;
		private JComboBox resourceComboBox;
		private JTextField weaknessText;
		
		protected AddResourceDataActionListener(List<Resource> allResources, JTextField defValueText, JTextField weaknessText, JTextField weightText, JComboBox resourceComboBox) {
			this.allResources = allResources;
			this.defValueText = defValueText;
			this.weaknessText = weaknessText;
			this.weightText = weightText;
			this.resourceComboBox = resourceComboBox;
		}
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			DefaultResourceData resDefData = new DefaultResourceData();
			Double value = getValueFromString(defValueText.getText());
			Double weight = getWeightFromString(weightText.getText());
			Integer weakness = getWeaknessFromString(weaknessText.getText());
			Resource res = (Resource) resourceComboBox.getSelectedItem();
			
			if(value != null && weight != null && res != null) {
				resDefData.setValue(value);
				resDefData.setWeight(weight);
				resDefData.setResource(res);
				defaultResourceSet.remove(resDefData);
				defaultResourceSet.add(resDefData);
			}
			
			if(weakness != null) {
				weaknessKoef = weakness.intValue();
			}
			
			if(defaultResourceSet.size() == allResources.size()) {
				double resWeightSum = 0.0;
				for(DefaultResourceData resDef : defaultResourceSet) {
					resWeightSum += resDef.getWeight();
				}
				
				if(Math.abs(resWeightSum - 1) > CountryControllerHelper.ZERO) {
					MessageHelper.showCustomError(
							"������ � ��������", 
							"������ ������� ����� �����! ����� ����� (" + resWeightSum + ") �� ����� 1.0!", 
							DefaultResourceValuesDialog.this);
				} else {
					DefaultResourceValuesDialog.this.dispose();
				}
			}
		}
	}
}
