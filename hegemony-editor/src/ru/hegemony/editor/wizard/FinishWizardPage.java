package ru.hegemony.editor.wizard;

import java.awt.Dimension;

import javax.swing.JLabel;

public class FinishWizardPage extends BaseWizardPage {
	private static final long serialVersionUID = 5131573546132101776L;
	
	public FinishWizardPage(Dimension dialogSize, boolean isLastPage) {
		super("����� ����������", "����� ���������� �� ������", dialogSize, isLastPage);
		
		initComponents();
	}

	@Override
	protected void initComponents() {
		add(new JLabel("�����������! �������������� ������ ���������!"));
	}
}
