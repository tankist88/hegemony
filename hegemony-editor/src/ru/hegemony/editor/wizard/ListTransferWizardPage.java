package ru.hegemony.editor.wizard;

import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.ciscavate.cjwizard.WizardPage;
import org.ciscavate.cjwizard.WizardSettings;

import ru.hegemony.editor.data.EntityType;
import ru.hegemony.editor.data.LevelHolder;
import ru.hegemony.editor.render.TextWithImageListRenderer;

public abstract class ListTransferWizardPage extends BaseWizardPage {
	private static final long serialVersionUID = -3868430319744676038L;
	
	private DefaultListModel allowedListModel = new DefaultListModel();
	private DefaultListModel selectedListModel = new DefaultListModel();
	
	private LevelHolder levelFullInfo;

	public ListTransferWizardPage(LevelHolder levelFullInfo, String title, String desc, Dimension dialogSize, boolean isLastPage) {
		super(title, desc, dialogSize, isLastPage);
		this.levelFullInfo = levelFullInfo;
		initComponents();
	}
	
	@Override
	public void rendering(List<WizardPage> path, WizardSettings settings) {
		super.rendering(path, settings);
		initAllowed();
		initSelected();
	}
	
	protected void initComponents() {
		initAllowed();
		initSelected();
		initUIComponents();
	}
	
	protected void initUIComponents() {
		this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		
		final JList allowedList = new JList();
		allowedList.setModel(getAllowedListModel());
		allowedList.setCellRenderer(new TextWithImageListRenderer(getListEntityType()));
		allowedList.setVisibleRowCount(4);
        
        JPanel allowedPanel = new JPanel();
        allowedPanel.setLayout(new BoxLayout(allowedPanel, BoxLayout.Y_AXIS));
        allowedPanel.add(new JLabel("������ ���������� " + getListEntityType().getSuffix()));
        allowedPanel.add(
				new JScrollPane(
					allowedList,
					JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
					JScrollPane.HORIZONTAL_SCROLLBAR_NEVER
				)
			);
        allowedPanel.setPreferredSize(new Dimension(350, 300));
		
		final JList selectedList = new JList();
		selectedList.setName("selectedList");
		selectedList.setModel(getSelectedListModel());
		selectedList.setCellRenderer(new TextWithImageListRenderer(getListEntityType()));
		selectedList.setVisibleRowCount(4);
        
        JPanel selectedPanel = new JPanel();
        selectedPanel.setLayout(new BoxLayout(selectedPanel, BoxLayout.Y_AXIS));
        selectedPanel.add(new JLabel("������ ��������� " + getListEntityType().getSuffix()));
        selectedPanel.add(
				new JScrollPane(
					selectedList,
					JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
					JScrollPane.HORIZONTAL_SCROLLBAR_NEVER
				)
			);
        selectedPanel.setPreferredSize(new Dimension(350, 300));
        
        JPanel controlPanel = new JPanel();
        controlPanel.setLayout(new BoxLayout(controlPanel, BoxLayout.Y_AXIS));
		
        JButton selectButton = new JButton();
        selectButton.setText(">>");
        selectButton.setMargin(new Insets(5, 9, 5, 8));
        selectButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				leftListToRightAction(allowedList);
			}
		});
        
        JButton delButton = new JButton();
        delButton.setText("<<");
        delButton.setMargin(new Insets(5, 9, 5, 8));
        delButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				rightListToLeftAction(selectedList);
			}
		});
        
        JButton editButton = new JButton();
        editButton.setText("Edit");
        editButton.setMargin(new Insets(5, 5, 5, 5));
        editButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				editAction(selectedList);
			}
		});
        
        JButton rndButton = new JButton();
        rndButton.setText("Rnd");
        rndButton.setMargin(new Insets(5, 5, 5, 4));
        rndButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				generateRandomElementChar(allowedList);
			}
		});
        
        controlPanel.add(selectButton);
        controlPanel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
        controlPanel.add(delButton);
        if(getListEntityType().equals(EntityType.ITEM)) {
	        controlPanel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
	        controlPanel.add(editButton);
	        controlPanel.add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
	        controlPanel.add(rndButton);
        }
        
        add(allowedPanel);
        add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
        add(controlPanel);
        add(Box.createRigidArea(new Dimension(PADDING,PADDING)));
		add(selectedPanel);
	}
	
	protected DefaultListModel getAllowedListModel() {
		return allowedListModel;
	}

	protected DefaultListModel getSelectedListModel() {
		return selectedListModel;
	}

	protected LevelHolder getLevelFullInfo() {
		return levelFullInfo;
	}
	
	/**
	 * ������������� �������������� ���������� �������� �������������
	 */
	protected abstract void generateRandomElementChar(final JList allowedList);
	/**
	 * �������������� �������� �� ���������� ������
	 */
	protected abstract void editAction(final JList selectedList);
	/**
	 * ��������� ���� �������� ������
	 */
	protected abstract EntityType getListEntityType();
	/**
	 * ����� ������������ � ������ ������� ������ ����������� �������� ������ �� ������� ������ � �����
	 */
	protected abstract void rightListToLeftAction(final JList selectedList);
	/**
	 * ����� ������������ � ������ ������� ������ ����������� �������� ������ �� ������ ������ � ������
	 */
	protected abstract void leftListToRightAction(final JList allowedList);
	/**
	 * ������������� ������ ��������� ���������
	 */
	protected void initSelected() {
		if(selectedListModel == null) {
			selectedListModel = new DefaultListModel();
		} else {
			selectedListModel.clear();
		}
	}
	/**
	 * ������������� ������ ��������� ���������
	 */
	protected void initAllowed() {
		if(allowedListModel == null) {
			allowedListModel = new DefaultListModel();
		} else {
			allowedListModel.clear();
		}
	}
}
