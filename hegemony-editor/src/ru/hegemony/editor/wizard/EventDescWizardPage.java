package ru.hegemony.editor.wizard;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.apache.commons.lang3.StringUtils;
import org.ciscavate.cjwizard.WizardPage;
import org.ciscavate.cjwizard.WizardSettings;

import ru.hegemony.editor.data.LevelData;
import ru.hegemony.editor.data.LevelHolder;
import ru.hegemony.editor.helper.MessageHelper;
import ru.hegemony.editor.render.JTextFieldLimit;
import ru.hegemony.xml.data.ObjectFactory;

public class EventDescWizardPage extends BaseWizardPage {
	private static final long serialVersionUID = 4486596650077333613L;
	
	private static final int MAX_FILENAME_SIZE = 16;
	private static final int MAX_TITLE_SIZE = 20;
	private static final int MAX_DESC_SIZE = 1000;
	private static final int MAX_TURNS_SIZE = 3;
	
	private LevelHolder levelFullInfo;
	
	private JTextField fNameTf;

	public EventDescWizardPage(LevelHolder levelFullInfo, Dimension dialogSize, boolean isLastPage) {
		super("�������� ������", "���� ������ � �������� ������", dialogSize, isLastPage);

		this.levelFullInfo = levelFullInfo;
		
		initComponents();
	}
	
	@Override
	protected void initComponents() {
		this.setLayout(new FlowLayout());
		
		fNameTf = new JTextField();
		fNameTf.setName("filename");
		fNameTf.setPreferredSize(new Dimension(430, 20));
		fNameTf.setDocument(new JTextFieldLimit(MAX_FILENAME_SIZE));
		if(levelFullInfo != null && levelFullInfo.getFilename() != null) {
			fNameTf.setText(levelFullInfo.getFilename());
		}
		
		JTextField titleTf = new JTextField();
		titleTf.setName("levelTitle");
		titleTf.setPreferredSize(new Dimension(430, 20));
		titleTf.setDocument(new JTextFieldLimit(MAX_TITLE_SIZE));
		if (levelFullInfo != null
				&& levelFullInfo.getLevel() != null
				&& levelFullInfo.getLevel().getEventDesc() != null
				&& levelFullInfo.getLevel().getEventDesc().getTitle() != null) 
		{
			titleTf.setText(levelFullInfo.getLevel().getEventDesc().getTitle());
		}
		
		JTextArea descTa = new JTextArea(9, 37);
		descTa.setName("levelDesc");
		descTa.setEditable(true);
		descTa.setLineWrap(true);
		descTa.setDocument(new JTextFieldLimit(MAX_DESC_SIZE));
		if (levelFullInfo != null
				&& levelFullInfo.getLevel() != null
				&& levelFullInfo.getLevel().getEventDesc() != null
				&& levelFullInfo.getLevel().getEventDesc().getDesc() != null) 
		{
			descTa.append(levelFullInfo.getLevel().getEventDesc().getDesc());
		}
		
		JTextField maxTurnsTf = new JTextField();
		maxTurnsTf.setName("maxTurns");
		maxTurnsTf.setPreferredSize(new Dimension(430, 20));
		maxTurnsTf.setDocument(new JTextFieldLimit(MAX_TURNS_SIZE));
		if (levelFullInfo != null
				&& levelFullInfo.getLevel() != null
				&& levelFullInfo.getLevel().getEventDesc() != null) 
		{
			maxTurnsTf.setText(Integer.toString(levelFullInfo.getLevel().getEventDesc().getTurnCount()));
		}
		
		JTextField bonusFormulaTf = new JTextField();
		bonusFormulaTf.setName("bonusFormula");
		bonusFormulaTf.setPreferredSize(new Dimension(430, 20));
		if (levelFullInfo != null
				&& levelFullInfo.getLevel() != null
				&& levelFullInfo.getLevel().getEventDesc() != null
				&& levelFullInfo.getLevel().getEventDesc().getBonusFormula() != null
				&& !levelFullInfo.getLevel().getEventDesc().getBonusFormula().isEmpty()) 
		{
			bonusFormulaTf.setText(levelFullInfo.getLevel().getEventDesc().getBonusFormula());
		} else {
			bonusFormulaTf.setText("(exp(-0.5*[t]))*50");
		}
		
		JPanel fNamePanel = new JPanel();
		fNamePanel.setLayout(new BoxLayout(fNamePanel, BoxLayout.Y_AXIS));
		JPanel fNameLabelPanel = new JPanel();
		fNameLabelPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
		fNameLabelPanel.add(new JLabel("��� �����"));
		fNamePanel.add(fNameLabelPanel);
		fNamePanel.add(fNameTf);
		
		JPanel titlePanel = new JPanel();
		titlePanel.setLayout(new BoxLayout(titlePanel, BoxLayout.Y_AXIS));
		JPanel titleLabelPanel = new JPanel();
		titleLabelPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
		titleLabelPanel.add(new JLabel("���������"));
		titlePanel.add(titleLabelPanel);
		titlePanel.add(titleTf);
		
		JPanel descPanel = new JPanel();
		descPanel.setLayout(new BoxLayout(descPanel, BoxLayout.Y_AXIS));
		JPanel descLabelPanel = new JPanel();
		descLabelPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
		descLabelPanel.add(new JLabel("��������"));
		descPanel.add(descLabelPanel);
		descPanel.add(
				new JScrollPane(
					descTa,
					JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
					JScrollPane.HORIZONTAL_SCROLLBAR_NEVER
				)
			);
		
		JPanel maxTurnsPanel = new JPanel();
		maxTurnsPanel.setLayout(new BoxLayout(maxTurnsPanel, BoxLayout.Y_AXIS));
		JPanel maxTurnsLabelPanel = new JPanel();
		maxTurnsLabelPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
		maxTurnsLabelPanel.add(new JLabel("������������ ���������� ����� �� ���������"));
		maxTurnsPanel.add(maxTurnsLabelPanel);
		maxTurnsPanel.add(maxTurnsTf);
		
		JPanel bonusFormulaPanel = new JPanel();
		bonusFormulaPanel.setLayout(new BoxLayout(bonusFormulaPanel, BoxLayout.Y_AXIS));
		JPanel bonusFormulaLabelPanel = new JPanel();
		bonusFormulaLabelPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
		bonusFormulaLabelPanel.add(new JLabel("������� ������� ������ ��� ���� ([t] - ����� ����)."));
		bonusFormulaPanel.add(bonusFormulaLabelPanel);
		bonusFormulaPanel.add(bonusFormulaTf);
		
		add(fNamePanel);
		add(titlePanel);
		add(descPanel);
		add(maxTurnsPanel);
		add(bonusFormulaPanel);
	}
	
	@Override
	public void rendering(List<WizardPage> path, WizardSettings settings) {
		super.rendering(path, settings);
		if(levelFullInfo != null && levelFullInfo.getFilename() != null && levelFullInfo.getFilename().length() > 0) {
			if(levelFullInfo.getFilename().toLowerCase().endsWith(".xml") && fNameTf != null) {
				int length = levelFullInfo.getFilename().length();
				fNameTf.setText(levelFullInfo.getFilename().substring(0, length - 4));
			}
		}
	}
	
	@Override
	public void updateSettings(WizardSettings settings) {
		super.updateSettings(settings);
		
		if(!validatePageFields(settings)) {
			throw new IllegalArgumentException("������ ��������� ����� �����!");
		}
		
		String filename = getDefaultValue((String) settings.get("filename"));
		if(!filename.endsWith(".xml")) {
			filename += ".xml";
		}
		filename = filename.replaceAll("-", "_");
		levelFullInfo.setFilename(filename);
		
		ObjectFactory objectFactory = new ObjectFactory();
		
		if(levelFullInfo.getLevel() == null) {
			levelFullInfo.setLevel(new LevelData());
		}
		if(levelFullInfo.getLevel().getEventDesc() == null) {
			levelFullInfo.getLevel().setEventDesc(objectFactory.createEventDescType());
		}
		
		levelFullInfo.getLevel().getEventDesc().setTitle(getDefaultValue((String) settings.get("levelTitle")));
		levelFullInfo.getLevel().getEventDesc().setDesc(getDefaultValue((String) settings.get("levelDesc")));
		levelFullInfo.getLevel().getEventDesc().setTurnCount(Integer.parseInt((String) settings.get("maxTurns")));
		levelFullInfo.getLevel().getEventDesc().setBonusFormula((String) settings.get("bonusFormula"));
	}
	
	private boolean validatePageFields(WizardSettings settings) {
		String filename = (String) settings.get("filename");
		String title = (String) settings.get("levelTitle");
		String desc = (String) settings.get("levelDesc");
		String maxTurns = (String) settings.get("maxTurns");
		String binusFormula = (String) settings.get("bonusFormula");
		if(
			StringUtils.isEmpty(filename)
			|| StringUtils.isEmpty(title)
			|| StringUtils.isEmpty(desc)
			|| StringUtils.isEmpty(maxTurns)
			|| StringUtils.isEmpty(binusFormula)) 
		{
			MessageHelper.showCustomError(
					"������ ���������", 
					"��� ���� �������� ������������� ��� ����������!", 
					this);
			return false;
		}
		if(
			filename.trim().length() > MAX_FILENAME_SIZE
			|| title.trim().length() > MAX_TITLE_SIZE) 
		{
			MessageHelper.showCustomError(
					"������ ���������", 
					"����� ����� \"��� �����\" � \"���������\" �� ������ ��������� " + MAX_FILENAME_SIZE + " � " + MAX_TITLE_SIZE + " �������� ��������������!", 
					this);
			return false;
		}
		if(!filename.matches("\\w+")) {
			MessageHelper.showCustomError(
					"������ ���������", 
					"� ���� \"��� �����\" ����� ����������� ������ ��������� �������, ����� � ������ \"_\".", 
					this);
			return false;
		}
		int maxTurnsInt = Integer.parseInt(maxTurns);
		if(maxTurnsInt < 1 || maxTurnsInt > 100) {
			MessageHelper.showCustomError(
					"������ ���������", 
					"� ���� \"������������ ���������� �����\" ����� ����������� ������ ����� �� 1 �� 100", 
					this);
			return false;
		}
		if(!binusFormula.toLowerCase().contains("[t]")) {
			MessageHelper.showCustomError(
					"������ ���������", 
					"� ���� \"������� ��� ������� ������\" ������ ����������� ���������� \"������� ��� ([t])\"", 
					this);
			return false;
		}
		
		return true;
	}
	
	private String getDefaultValue(String value) {
		return StringUtils.isEmpty(value) ? "noname" : value;
	}
}
