package ru.hegemony.editor.wizard;

import java.awt.Dimension;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.JList;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.ciscavate.cjwizard.WizardPage;
import org.ciscavate.cjwizard.WizardSettings;

import ru.hegemony.editor.data.EffectKey;
import ru.hegemony.editor.data.EffectValue;
import ru.hegemony.editor.data.EntityType;
import ru.hegemony.editor.data.LevelHolder;
import ru.hegemony.editor.gen.GenerateItemSetsTask;
import ru.hegemony.editor.gen.ItemSetAttributeGenerator;
import ru.hegemony.editor.helper.ItemHelper;
import ru.hegemony.editor.helper.MessageHelper;
import ru.hegemony.xml.data.CountryEntity;
import ru.hegemony.xml.data.DstEffectType;
import ru.hegemony.xml.data.EffectDstType;
import ru.hegemony.xml.data.ItemActionEntity;
import ru.hegemony.xml.data.ItemEntity;
import ru.hegemony.xml.data.ItemValueType;
import ru.hegemony.xml.data.Items;
import ru.hegemony.xml.data.ObjectFactory;
import ru.hegemony.xml.data.ValueCode;

public class ItemSetsWizardPage extends ListTransferWizardPage {
	private static final long serialVersionUID = 2030990446899715230L;
	
	/** ���� � ����������� ��������� */
	private static final String ITEM_CATALOG_PATH = "res/data/item.xml";
	/** ���� � ����������� ������������� */
	private static final String DIST_CATALOG_PATH = "res/data/dist.xml";
	
	private Map<EffectKey, EffectValue> itemInfluenceMap;
	
	public ItemSetsWizardPage(LevelHolder levelFullInfo, Dimension dialogSize, boolean isLastPage) {
		super(levelFullInfo, "������ ���������", "���� ���������� ������� ���������", dialogSize, isLastPage);
	}
	
	@Override
	protected void initComponents() {
		super.initComponents();
	}
	
	@Override
	public void rendering(List<WizardPage> path, WizardSettings settings) {
		super.rendering(path, settings);
		if(	getItemInfluenceMap().isEmpty() 
			&& getLevelFullInfo().getLevel().getItemsEffects() != null 
			&& !getLevelFullInfo().getLevel().getItemsEffects().isEmpty()) 
		{
			for(ItemValueType value : getLevelFullInfo().getLevel().getItemsEffects()) {
				EffectKey key = new EffectKey();
				key.setActionCode(value.getValueCode().getActionCode());
				key.setCountryCode(value.getValueCode().getCountryCode());
				key.setItemId(
						ItemHelper.getItemByCode(
								value.getValueCode().getItemCode(), 
								getLevelFullInfo().getLevel().getItems()).getImage());
				key.setResourceCode(value.getValueCode().getResourceCode());
				EffectValue effectValue = new EffectValue();
				effectValue.setValue(value.getValue());
				effectValue.setValueMode(true);
				getItemInfluenceMap().put(key, effectValue);
			}
		}
	}
	
	@Override
	public void updateSettings(WizardSettings settings) {
		super.updateSettings(settings);
		getLevelFullInfo().getLevel().getItems().clear();
		final List<ItemEntity> allowedItemList = new ArrayList<ItemEntity>();
		for(int i = 0; i < getSelectedListModel().getSize(); i++) {
			ItemEntity item = (ItemEntity) getSelectedListModel().getElementAt(i);
			item.setSa(0);
			getLevelFullInfo().getLevel().getItems().add(item);
			allowedItemList.addAll(generateItemForEachAction(item));
		}
		
		GenerateItemSetsTask task = new GenerateItemSetsTask(allowedItemList, getItemInfluenceMap(), getLevelFullInfo());
		task.executeTaskWithProgressBar();
	}

	@Override
	protected EntityType getListEntityType() {
		return EntityType.ITEM;
	}

	@Override
	protected void rightListToLeftAction(JList selectedList) {
		ItemEntity selected = (ItemEntity) selectedList.getSelectedValue();
		if(selected != null) {
			getSelectedListModel().removeElement(selected);
			Set<EffectKey> keysForDeleteSet = new HashSet<EffectKey>();
			for(EffectKey key : getItemInfluenceMap().keySet()) {
				if(key.getItemId().equalsIgnoreCase(selected.getImage())) {
					keysForDeleteSet.add(key);
				}
			}
			for(EffectKey keyForDel : keysForDeleteSet) {
				getItemInfluenceMap().remove(keyForDel);
			}
		}
	}

	@Override
	protected void leftListToRightAction(JList allowedList) {
		ItemEntity selected = (ItemEntity) allowedList.getSelectedValue();
		if(selected != null) {
			if(!itemAlreadySelected(selected)) {
				fillItemInfluenceMapFromDst(selected);
				DefaultItemParamsDialog itemAddDialog = new DefaultItemParamsDialog(null, getItemInfluenceMap(), getLevelFullInfo(), selected);
				if(itemAddDialog.getItemInfluenceMap() != null && !itemAddDialog.getItemInfluenceMap().isEmpty()) {
					getItemInfluenceMap().putAll(itemAddDialog.getItemInfluenceMap());
					getSelectedListModel().addElement(selected);
					saveDistributionDataToFile();
				}
			}
		}
	}

	@Override
	protected void initSelected() {
		super.initSelected();
		try {
			if (getLevelFullInfo() != null 
					&& getLevelFullInfo().getLevel() != null
					&& getLevelFullInfo().getLevel().getItems() != null) 
			{
				List<ItemEntity> selectedItemList = getLevelFullInfo().getLevel().getItems();
				for(ItemEntity item : selectedItemList) {
					getSelectedListModel().addElement(item);
				}
			}
		} catch (Exception e) {
			MessageHelper.showError("������ �������� ������", this, e);
		}
	}

	@Override
	protected void initAllowed() {
		super.initAllowed();
		try {
			Unmarshaller unmarshaller = JAXBContext.newInstance("ru.hegemony.xml.data").createUnmarshaller();
			JAXBElement<Items> element = unmarshaller.unmarshal(new StreamSource(new FileInputStream(ITEM_CATALOG_PATH)), Items.class);
			List<ItemEntity> allowedItemList = element.getValue().getItem();
			for(ItemEntity item : allowedItemList) {
				getAllowedListModel().addElement(item);
			}
		} catch (FileNotFoundException fex) {
			System.err.println(fex.getMessage());
		} catch (Exception e) {
			MessageHelper.showError("������ �������� ������", this, e);
		}
	}

	@Override
	protected void editAction(JList selectedList) {
		ItemEntity selected = (ItemEntity) selectedList.getSelectedValue();
		if(selected != null) {
			fillItemInfluenceMapFromDst(selected);
			DefaultItemParamsDialog itemAddDialog = new DefaultItemParamsDialog(null, getItemInfluenceMap(), getLevelFullInfo(), selected);
			if(itemAddDialog.getItemInfluenceMap() != null && !itemAddDialog.getItemInfluenceMap().isEmpty()) {
				getItemInfluenceMap().putAll(itemAddDialog.getItemInfluenceMap());
				saveDistributionDataToFile();
			}
		}
	}

	@Override
	protected void generateRandomElementChar(JList allowedList) {
		Object[] selectedItems = allowedList.getSelectedValues();
		if (selectedItems != null && selectedItems.length > 0) {
			List<ItemEntity> allowedItemList = new ArrayList<ItemEntity>();
			for (Object obj : selectedItems) {
				allowedItemList.add((ItemEntity) obj);
			}
			Map<EffectKey, EffectValue> infMap = ItemSetAttributeGenerator.generateItemEffectMap(
					allowedItemList, 
					getLevelFullInfo().getLevel().getCountries());
			getItemInfluenceMap().putAll(infMap);
			for (ItemEntity item : allowedItemList) {
				if (!itemAlreadySelected(item)) {
					getSelectedListModel().addElement(item);
				}
			}
			MessageHelper.showInfo(
					"���������� ���������", 
					"������������� " + infMap.size() + " ������������� ��� " + allowedItemList.size() + " ���������", 
					this);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void fillItemInfluenceMapFromDst(ItemEntity selectedItem) {
		List<ItemEntity> itemList = (List<ItemEntity>) Collections.list(getAllowedListModel().elements());
		DstEffectType dst = loadDistributionDataFromFile();
		for(EffectDstType effect : dst.getDstEffect()) {
			if(effect.getValueCode().getItemCode() == selectedItem.getCode() && isCountryInLevel(effect.getValueCode().getCountryCode())) {
				EffectKey key = new EffectKey();
				key.setActionCode(effect.getValueCode().getActionCode());
				key.setCountryCode(effect.getValueCode().getCountryCode());
				key.setItemId(
						ItemHelper.getItemByCode(
								effect.getValueCode().getItemCode(), 
								itemList).getImage());
				key.setResourceCode(effect.getValueCode().getResourceCode());
				EffectValue effectValue = new EffectValue();
				effectValue.setDstType(effect.getDistribution());
				effectValue.setValueMode(false);
				getItemInfluenceMap().put(key, effectValue);
			}
		}
	}
	
	private boolean isCountryInLevel(int countryCode) {
		for(CountryEntity country : getLevelFullInfo().getLevel().getCountries()) {
			if(country.getCode() == countryCode) {
				return true;
			}
		}
		return false;
	}
	
	private DstEffectType loadDistributionDataFromFile() {
		try {
			Unmarshaller unmarshaller = JAXBContext.newInstance("ru.hegemony.xml.data").createUnmarshaller();
			JAXBElement<DstEffectType> element = unmarshaller.unmarshal(new StreamSource(new FileInputStream(DIST_CATALOG_PATH)), DstEffectType.class);
			DstEffectType res = element.getValue();
			if(!res.getDstEffect().isEmpty()) {
				return res;
			}
		} catch (FileNotFoundException fex) {
			System.err.println(fex.getMessage());
		} catch (Exception e) {
			MessageHelper.showError("������ �������� ����������� �������������", this, e);
		}
		ObjectFactory objectFactory = new ObjectFactory();
		DstEffectType dstEffectType = objectFactory.createDstEffectType();
		return dstEffectType;
	}
	
	@SuppressWarnings("unchecked")
	private void saveDistributionDataToFile() {
		try {
			List<ItemEntity> itemList = (List<ItemEntity>) Collections.list(getAllowedListModel().elements());
			ObjectFactory objectFactory = new ObjectFactory();
			DstEffectType dstEffectType = loadDistributionDataFromFile();
			for(Entry<EffectKey, EffectValue> entry : getItemInfluenceMap().entrySet()) {
				if(entry.getValue().getDstType() != null) {
					ValueCode valueCode = objectFactory.createValueCode();
					valueCode.setActionCode(entry.getKey().getActionCode());
					valueCode.setCountryCode(entry.getKey().getCountryCode());
					valueCode.setItemCode(ItemHelper.getItemByImage(entry.getKey().getItemId(), itemList).getCode());
					valueCode.setResourceCode(entry.getKey().getResourceCode());
					EffectDstType effectDst = null;
					for(EffectDstType effect : dstEffectType.getDstEffect()) {
						if(valueCodeEquals(effect.getValueCode(), valueCode)) {
							effectDst = effect;
							break;
						}
					}
					if(effectDst == null) {
						effectDst = objectFactory.createEffectDstType();
						dstEffectType.getDstEffect().add(effectDst);
					}
					effectDst.setValueCode(valueCode);
					effectDst.setDistribution(entry.getValue().getDstType());
				}
			}
			Marshaller marshaller = JAXBContext.newInstance("ru.hegemony.xml.data").createMarshaller();
			marshaller.marshal(objectFactory.createDstEffectDict(dstEffectType), new File(DIST_CATALOG_PATH));
		} catch (Exception e) {
			MessageHelper.showError("������ ���������� ����������� �������������", this, e);
		}
	}
	
	private boolean valueCodeEquals(ValueCode v1, ValueCode v2) {
		return 
				v1.getActionCode() == v2.getActionCode() 
				&& v1.getCountryCode() == v2.getCountryCode()
				&& v1.getResourceCode() == v2.getResourceCode()
				&& v1.getItemCode() == v2.getItemCode();
	}
	
	public Map<EffectKey, EffectValue> getItemInfluenceMap() {
		if(itemInfluenceMap == null) {
			itemInfluenceMap = new HashMap<EffectKey, EffectValue>();
		}
		return itemInfluenceMap;
	}
	
	private List<ItemEntity> generateItemForEachAction(ItemEntity item) {
		List<ItemEntity> result = new ArrayList<ItemEntity>();
		for(ItemActionEntity action : item.getActions()) {
			ObjectFactory objectFactory = new ObjectFactory();
			ItemEntity newItem = objectFactory.createItemEntity();
			newItem.setImage(item.getImage());
			newItem.setCode(item.getCode());
			newItem.setName(item.getName());
			newItem.setSa(action.getCode());
			newItem.getActions().addAll(item.getActions());
			newItem.getProperties().addAll(item.getProperties());
			result.add(newItem);
		}
		return result;
	}
	
	private boolean itemAlreadySelected(ItemEntity selected) {
		boolean exist = false;
		for(int i = 0; i < getSelectedListModel().getSize(); i++) {
			ItemEntity exists = (ItemEntity) getSelectedListModel().getElementAt(i);
			// ���������� image ��� ID, �.�. ������ ID ���
			if(exists.getImage().equalsIgnoreCase(selected.getImage())) {
				exist = true;
			}
		}
		return exist;
	}
}
