package ru.hegemony.editor.wizard;

import java.awt.Dimension;
import java.util.List;

import org.ciscavate.cjwizard.WizardPage;
import org.ciscavate.cjwizard.WizardSettings;

public abstract class BaseWizardPage extends WizardPage {
	private static final long serialVersionUID = 7774312047782562817L;
	
	/** ������������ ����� ��������� � ������ */
	public static final int MAX_ITEMS_IN_SET = 5;
	
	public static final int PADDING = 10;

	private boolean isLastPage;
	
	public BaseWizardPage(String title, String description, Dimension dialogSize, boolean isLastPage) {
		super(title, description);
		this.isLastPage = isLastPage;
		
		this.setSize(dialogSize);
		this.setPreferredSize(dialogSize);
	}
	
	protected abstract void initComponents();
	
	/** 
	 * ����������� �� ��������� ��������
	 * WizardSettings settings - ����� � ������ wizard ����������,
	 * ������� ����� ������������ ��� ����� ��� �������� ��������
	 */
	@Override
	public void rendering(List<WizardPage> path, WizardSettings settings) {
		if(isLastPage) {
			setFinishEnabled(true);
			setNextEnabled(false);
		}
	}
	
	/** 
	 * ����������� ����� ������� �� ������ "Next"
	 * WizardSettings settings - ����� � ������ wizard ����������,
	 * ������� ����� ������������ ��� ����� ��� �������� �������� 
	 */
	@Override
	public void updateSettings(WizardSettings settings) {
		super.updateSettings(settings);
	}
}
