package ru.hegemony.editor.wizard;

import com.rits.cloning.Cloner;

import org.apache.commons.lang3.StringUtils;
import org.jdesktop.swingx.prompt.PromptSupport;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import ru.hegemony.common.helper.DataHelper;
import ru.hegemony.editor.data.DistType;
import ru.hegemony.editor.data.EffectKey;
import ru.hegemony.editor.data.EffectValue;
import ru.hegemony.editor.data.LevelHolder;
import ru.hegemony.editor.data.Resource;
import ru.hegemony.editor.gen.GenerateItemSetsTask;
import ru.hegemony.editor.helper.MessageHelper;
import ru.hegemony.editor.helper.ResourceHelper;
import ru.hegemony.xml.data.CountryEntity;
import ru.hegemony.xml.data.DstParameterType;
import ru.hegemony.xml.data.ItemActionEntity;
import ru.hegemony.xml.data.ItemEntity;
import ru.hegemony.xml.data.ObjectFactory;

public class DefaultItemParamsDialog extends JDialog {
	private static final long serialVersionUID = 5813396912519567567L;
	
	private static final boolean USE_TEXT_INTERFACE = false;
	
	private final Map<EffectKey, EffectValue> srcItemInfluenceMap;
	private Map<EffectKey, EffectValue> innerItemInfluenceMap;
	private final LevelHolder levelFullInfo;
	private final ItemEntity item;
	private EffectKey currentKey;
	private boolean valueMode;
	
	private JButton saveButton;
	private DefaultTableModel paramTableModel;
	private JTextField valueText;
	private JComboBox dstComboBox;
	private JTextField manualFuncText;
	private JLabel effectKeyLabel;

	public DefaultItemParamsDialog(JFrame ownerFrame, LevelHolder levelFullInfo, ItemEntity item) {
		this(ownerFrame, new HashMap<EffectKey, EffectValue>(), levelFullInfo, item);
	}
	
	public DefaultItemParamsDialog(final JFrame ownerFrame, final Map<EffectKey, EffectValue> itemInfluenceMap, final LevelHolder levelFullInfo, final ItemEntity item) {
		super(ownerFrame, true);
		this.srcItemInfluenceMap = itemInfluenceMap;
		this.levelFullInfo = levelFullInfo;
		this.item = item;
		this.valueMode = true;
		initInfluenceMap();
		initComponents();
	}
	
	private void initInfluenceMap() {
		innerItemInfluenceMap = new HashMap<EffectKey, EffectValue>();
		List<Resource> resourceList = ResourceHelper.createResourceList();
		List<CountryEntity> countryList = levelFullInfo.getLevel().getCountries();
		
		// �������������� Map'� ������� ���������� ��� ���� ���������
		for(ItemActionEntity action : item.getActions()) {
			for(CountryEntity country : countryList) {
				for(Resource resource : resourceList) {
					EffectKey key = new EffectKey();
					key.setActionCode(action.getCode());
					key.setCountryCode(country.getCode());
					key.setResourceCode(resource.getCode());
					key.setItemId(item.getImage());
					innerItemInfluenceMap.put(key, null);
				}
			}
		}
		
		// ���� �������� Map'� ���� ���������, �� ����� �� ��� ����������� �������� ��� �������� ��������
		if(!srcItemInfluenceMap.isEmpty()) {
			Cloner c = new Cloner();
			for(EffectKey key : srcItemInfluenceMap.keySet()) {
				if(key.getItemId().equalsIgnoreCase(item.getImage()) && getCountryByCode(key.getCountryCode()) != null) {
					innerItemInfluenceMap.put(c.deepClone(key), c.deepClone(srcItemInfluenceMap.get(key)));
				}
			}
		}
	}
	
	private CountryEntity getCountryByCode(int code) {
		List<CountryEntity> countryList = levelFullInfo.getLevel().getCountries();
		for(CountryEntity country : countryList) {
			if(country.getCode() == code) {
				return country;
			}
		}
		return null;
	}
	
	private ItemActionEntity getActionByCode(int code) {
		for(ItemActionEntity action : item.getActions()) {
			if(action.getCode() == code) {
				return action;
			}
		}
		return null;
	}
	
	private Resource getResourceByCode(int code) {
		List<Resource> resourceList = ResourceHelper.createResourceList();
		for(Resource resource : resourceList) {
			if(resource.getCode() == code) {
				return resource;
			}
		}
		return null;
	}
	
	private void fillLabelFromKey(final EffectKey nextKey, final JLabel countryLabel, final JLabel actionLabel, final JLabel resourceLabel, final JLabel itemLabel) {
		countryLabel.setText(getCountryByCode(nextKey.getCountryCode()).getName() + " (" + nextKey.getCountryCode() + ")");
		actionLabel.setText(getActionByCode(nextKey.getActionCode()).getName() + " (" + nextKey.getActionCode() + ")");
		resourceLabel.setText(getResourceByCode(nextKey.getResourceCode()).getDescription() + " (" + nextKey.getResourceCode() + ")");
		itemLabel.setText(item.getName() + " (" + item.getCode() + ")");
	}
	
	private void fillLabelFromKey(final EffectKey nextKey, final JLabel commonLabel) {
		String commonText = "��������� ������� \""
				+ getResourceByCode(nextKey.getResourceCode()).getDescription()
				+ "\", � ������ �������� ������� \""
				+ getCountryByCode(nextKey.getCountryCode()).getName()
				+ "\" ����������� \""
				+ getActionByCode(nextKey.getActionCode()).getName().toLowerCase() + " " + item.getName().toLowerCase()
				+ "\"";
		commonLabel.setText("<html><body style='width: 200px'>" + commonText);
	}
	
	private boolean saveCurrentValue() throws NumberFormatException {
		if(valueMode) {
			Double dVal = getEffectFromString(valueText.getText());
			if(dVal != null) {
				EffectValue currentValue = new EffectValue();
				currentValue.setValue(dVal);
				currentValue.setValueMode(valueMode);
				innerItemInfluenceMap.put(currentKey, currentValue);
				refreshSaveButtonState();
				return true;
			} else {
				return false;
			}
		} else {
			List<DstParameterType> paramList = getDstParams(paramTableModel);
			if(paramList != null && !paramList.isEmpty()) {
				EffectValue currentValue = new EffectValue();
				currentValue.setValueMode(valueMode);
				ObjectFactory objectFactory = new ObjectFactory();
				currentValue.setDstType(objectFactory.createDistributionType());
				currentValue.getDstType().getParameterList().addAll(paramList);
				if(manualFuncText.getText() != null && !manualFuncText.getText().isEmpty()) {
					currentValue.getDstType().setFunction(manualFuncText.getText());
				} else {
					currentValue.getDstType().setFunction(null);
				}
				DistType stdType = (DistType) dstComboBox.getSelectedItem();
				currentValue.getDstType().setStdType(stdType.getCode());
				innerItemInfluenceMap.put(currentKey, currentValue);
				refreshSaveButtonState();
				return true;
			} else {
				return false;
			}
		}
	}
	
	private void showValue(final JLabel countryLabel, final JLabel actionLabel, final JLabel resourceLabel, final JLabel itemLabel) {
		fillLabelFromKey(currentKey, countryLabel, actionLabel, resourceLabel, itemLabel);
		fillValue();
	}
	
	private void fillValue() {
		EffectValue value = innerItemInfluenceMap.get(currentKey);
		if(value != null && value.getValue() != null) {
			valueText.setText(DataHelper.round(value.getValue(), 3) + "");
		} else {
			valueText.setText(null);
		}
		if(value != null && value.getDstType() != null) {
			dstComboBox.setSelectedItem(DistType.findByCode(value.getDstType().getStdType()));
			manualFuncText.setText(value.getDstType().getFunction());
			effectKeyLabel.setText("����: [" + GenerateItemSetsTask.createEffectKeyStr(currentKey, Arrays.asList(new ItemEntity[] {item})) + "]");
			fillParametersFromDist(value);
		}
	}
	
	private void fillParametersFromDist(EffectValue value) {
		if(value != null && value.getDstType() != null) {
			@SuppressWarnings("unchecked")
			Vector<Vector<Object>> rowVect = (Vector<Vector<Object>>) paramTableModel.getDataVector();
			for(DstParameterType param : value.getDstType().getParameterList()) {
				int rowNum = 0;
				for(Vector<Object> row : rowVect) {
					String paramName = (String) row.get(0);
					if(paramName.equalsIgnoreCase(param.getName())) {
						if(param.getExprValue() != null && !param.getExprValue().isEmpty()) {
							paramTableModel.setValueAt(param.getExprValue(), rowNum, 1);
						} else {
							paramTableModel.setValueAt(Double.toString(param.getValue()), rowNum, 1);
						}
					}
					rowNum++;
				}
			}
		}
	}
	
	private void showValue(final JLabel commonLabel) {
		fillLabelFromKey(currentKey, commonLabel);
		fillValue();
	}
	
	private Double getEffectFromString(String text) throws NumberFormatException {
		if(text == null || text.isEmpty()) {
			return null;
		}
		return Double.valueOf(text);
	}
	
	private boolean allValuesSetted() {
		for(EffectValue value : innerItemInfluenceMap.values()) {
			if(value == null) {
				return false;
			}
		}
		return true;
	}
	
	private void refreshSaveButtonState() {
		if(allValuesSetted()) {
			saveButton.setEnabled(true);
		} else {
			saveButton.setEnabled(false);
		}
	}
	
	private void initComponents() {
		this.getContentPane().setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
		
		LinkedList<EffectKey> keySet = new LinkedList<EffectKey>();
		Iterator<EffectKey> iterMap = innerItemInfluenceMap.keySet().iterator();
		while(iterMap.hasNext()) {
			keySet.add(iterMap.next());
		}
		
		final ListIterator<EffectKey> iter = keySet.listIterator();
		
		currentKey = iter.next();
		
		final JPanel valueTextPanel = new JPanel();
		valueTextPanel.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));
		valueTextPanel.setPreferredSize(new Dimension(290, 40));
		valueText = new JTextField();
		valueText.setPreferredSize(new Dimension(290, 30));
		valueText.setFont(new Font("SansSerif", Font.PLAIN, 18));
		valueTextPanel.add(valueText);
		
		// ---------------
		final JPanel dstPanel = new JPanel();
		dstPanel.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));
		dstPanel.setPreferredSize(new Dimension(290, 200));
		
		final JPanel paramListPanel = getJListPanel();
		
		dstComboBox = new JComboBox(DistType.values());
		dstComboBox.setBackground(Color.WHITE);
		dstComboBox.setPreferredSize(new Dimension(290, 30));
		dstComboBox.setFont(new Font("SansSerif", Font.PLAIN, 18));
		dstComboBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				setDistParams();
			}
		});
		clearParameters();
		
		manualFuncText = new JTextField();
		manualFuncText.setPreferredSize(new Dimension(290, 30));
		manualFuncText.setFont(new Font("SansSerif", Font.PLAIN, 11));
		manualFuncText.setEnabled(false);
		PromptSupport.setPrompt("���� �� ���������� � ������ ������ ��", manualFuncText);
		
		effectKeyLabel = new JLabel();
		effectKeyLabel.setText("����: [" + GenerateItemSetsTask.createEffectKeyStr(currentKey, Arrays.asList(new ItemEntity[] {item})) + "]");
		
		dstPanel.add(dstComboBox);
		dstPanel.add(manualFuncText);
		dstPanel.add(paramListPanel);
		dstPanel.add(effectKeyLabel);
		dstPanel.setVisible(false);
		// ---------------
		
		final JLabel commonLabel = new JLabel();
		final JLabel countryLabel = new JLabel();
		final JLabel actionLabel = new JLabel();
		final JLabel resourceLabel = new JLabel();
		final JLabel itemLabel = new JLabel();
		
		if(USE_TEXT_INTERFACE) {
			commonLabel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
			showValue(commonLabel);
		} else {
			countryLabel.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
			actionLabel.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
			resourceLabel.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
			itemLabel.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
			showValue(countryLabel, actionLabel, resourceLabel, itemLabel);
		}
		
		JPanel nextButtonPanel = new JPanel();
		nextButtonPanel.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 0));
		JButton nextButton = new JButton();
		nextButton.setText(">>");
		nextButton.setPreferredSize(new Dimension(50, 95));
		nextButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					if(iter.hasNext()) {
						saveCurrentValue();
						EffectKey tmpKey = iter.next();
						if(tmpKey.equals(currentKey)) {
							// ���� ��� ����� ��� ���������, �� ������������ �� ���, ��� ��� ���������
							// � ������ ��� ��� �����, ����� ������������� �� ���� ����� ����
							if(iter.hasNext()) {
								currentKey = iter.next();
							}
						} else {
							currentKey = tmpKey;
						}
						if(USE_TEXT_INTERFACE) {
							showValue(commonLabel);
						} else {
							showValue(countryLabel, actionLabel, resourceLabel, itemLabel);
						}
					}
				} catch (NumberFormatException bex) {
					MessageHelper.showCustomError(
							"������ � ��������", 
							"�������� ������� ������ ����� ������ ����� � ������, �������� 0.7", 
							DefaultItemParamsDialog.this);
				}
			}
		});
		nextButtonPanel.add(nextButton);
		
		JPanel prevButtonPanel = new JPanel();
		prevButtonPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 10));
		JButton prevButton = new JButton();
		prevButton.setText("<<");
		prevButton.setPreferredSize(new Dimension(50, 95));
		prevButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if(iter.hasPrevious()) {
						saveCurrentValue();
						EffectKey tmpKey = iter.previous();
						if(tmpKey.equals(currentKey)) {
							// ���� ��� ����� ��� ���������, �� ������������ �� ���, ��� ��� ���������
							// � ������ ��� ��� �����, ����� ������������� �� ���� ����� ���
							if(iter.hasPrevious()) {
								currentKey = iter.previous();
							}
						} else {
							currentKey = tmpKey;
						}
						if(USE_TEXT_INTERFACE) {
							showValue(commonLabel);
						} else {
							showValue(countryLabel, actionLabel, resourceLabel, itemLabel);
						}
					}
				} catch (NumberFormatException bex) {
					MessageHelper.showCustomError(
							"������ � ��������", 
							"�������� ������� ������ ����� ������ ����� � ������, �������� 0.7", 
							DefaultItemParamsDialog.this);
				}
			}
		});
		prevButtonPanel.add(prevButton);
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));
		saveButton = new JButton();
		saveButton.setText("���������");
		saveButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if(saveCurrentValue()) {
						DefaultItemParamsDialog.this.dispose();
					}
				} catch (NumberFormatException bex) {
					MessageHelper.showCustomError(
							"������ � ��������", 
							"�������� ������� ������ ����� ������ ����� � ������, �������� 0.7", 
							DefaultItemParamsDialog.this);
				}
			}
		});
		buttonPanel.add(saveButton);
		
		final JButton modeButton = new JButton();
		modeButton.setText("�������������");
		modeButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(dstPanel.isVisible()) {
					dstPanel.setVisible(false);
					valueTextPanel.setVisible(true);
					modeButton.setText("�������������");
					valueMode = true;
					
					Dimension dialogSize = new Dimension(310, 260);
					DefaultItemParamsDialog.this.setSize(dialogSize);
					DefaultItemParamsDialog.this.setResizable(false);
					DefaultItemParamsDialog.this.setPreferredSize(dialogSize);
					DefaultItemParamsDialog.this.pack();
				} else {
					dstPanel.setVisible(true);
					valueTextPanel.setVisible(false);
					modeButton.setText("��������");
					valueMode = false;
					
					Dimension dialogSize = new Dimension(310, 420);
					DefaultItemParamsDialog.this.setSize(dialogSize);
					DefaultItemParamsDialog.this.setResizable(false);
					DefaultItemParamsDialog.this.setPreferredSize(dialogSize);
					DefaultItemParamsDialog.this.pack();
				}
			}
		});
		buttonPanel.add(modeButton);
		
		refreshSaveButtonState();
		
		JPanel infoPanel = new JPanel();
		if(USE_TEXT_INTERFACE) {
			infoPanel.add(commonLabel);
		} else {
			infoPanel.setLayout(new BoxLayout(infoPanel, BoxLayout.Y_AXIS));
			infoPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
			infoPanel.setBackground(Color.WHITE);
			infoPanel.add(countryLabel);
			infoPanel.add(resourceLabel);
			infoPanel.add(actionLabel);
			infoPanel.add(itemLabel);
		}
		
		JPanel controlPanel = new JPanel();
		if(USE_TEXT_INTERFACE) {
			controlPanel.setLayout(new BoxLayout(controlPanel, BoxLayout.Y_AXIS));
		} else {
			controlPanel.setLayout(new BoxLayout(controlPanel, BoxLayout.X_AXIS));
		}
		controlPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		if(USE_TEXT_INTERFACE) {
			JPanel controlButtonsPanel = new JPanel();
			controlButtonsPanel.setLayout(new BoxLayout(controlButtonsPanel, BoxLayout.X_AXIS));
			controlButtonsPanel.add(prevButtonPanel);
			controlButtonsPanel.add(nextButtonPanel);
			controlPanel.add(infoPanel);
			controlPanel.add(controlButtonsPanel);
		} else {
			controlPanel.add(prevButtonPanel);
			controlPanel.add(infoPanel);
			controlPanel.add(nextButtonPanel);
		}
		
		this.getContentPane().add(controlPanel);
		this.getContentPane().add(valueTextPanel);
		this.getContentPane().add(dstPanel);
		this.getContentPane().add(buttonPanel);
		
		Dimension dialogSize = new Dimension(310, 260);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setTitle("�� ��������� - ������ ��������");
		this.setSize(dialogSize);
		this.setResizable(false);
		this.setPreferredSize(dialogSize);
		this.pack();
		this.setVisible(true);
	}
	
	private void setDistParams() {
		clearParameters();
		DistType selected = (DistType) dstComboBox.getSelectedItem();
		EffectValue value = innerItemInfluenceMap.get(currentKey);
		if(value != null && value.getDstType().getStdType().equals(selected.getCode())) {
			fillParametersFromDist(value);
		}
	}
	
	private void clearParameters() {
		DistType selected = (DistType) dstComboBox.getSelectedItem();
		int rowCount = paramTableModel.getRowCount();
		for(int i = 0; i < rowCount; i++) {
			paramTableModel.removeRow(0);
		}
		for(String param : selected.getParamNames()) {
			paramTableModel.addRow(new String[] {param, ""});
		}
	}
	
	private JPanel getJListPanel() {
		JPanel panel = new JPanel();
		panel.setPreferredSize(new Dimension(288, 115));
		paramTableModel = new DefaultTableModel();
		paramTableModel.addColumn("��������");
		paramTableModel.addColumn("��������");
		paramTableModel.addRow(new String[] {"", ""});
		JTable paramTable = new JTable(paramTableModel);
		paramTable.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {
			}
			@Override
			public void keyReleased(KeyEvent e) {
			}
			@Override
			public void keyPressed(KeyEvent e) {
				// ������� ����
				if(e.getKeyCode() == 40 && !emptyRowExists(paramTableModel) && !StringUtils.isEmpty(manualFuncText.getText())) {
					paramTableModel.addRow(new String[] {"", ""});
				}
			}
		});
		
		JScrollPane scrollPane = new JScrollPane(
				paramTable,
				JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER
			);
		scrollPane.setPreferredSize(new Dimension(288, 110));
		panel.add(scrollPane);
		return panel;
	}
	
	private boolean emptyRowExists(DefaultTableModel paramTableModel) {
		@SuppressWarnings("unchecked")
		Vector<Vector<Object>> rowVect = (Vector<Vector<Object>>) paramTableModel.getDataVector();
		for(Vector<Object> row : rowVect) {
			String value = (String) row.get(1);
			if(value == null || value.isEmpty()) {
				return true;
			}
		}
		return false;
	}
	
	private List<DstParameterType> getDstParams(DefaultTableModel paramTableModel) {
		List<DstParameterType> result = new ArrayList<DstParameterType>();
		ObjectFactory objectFactory = new ObjectFactory();
		@SuppressWarnings("unchecked")
		Vector<Vector<Object>> rowVect = (Vector<Vector<Object>>) paramTableModel.getDataVector();
		for(Vector<Object> row : rowVect) {
			String value = (String) row.get(1);
			if(value != null && !value.isEmpty()) {
				String name = (String) row.get(0);
				DstParameterType param = objectFactory.createDstParameterType();
				param.setName(name);
				try {
					param.setValue(Double.parseDouble(value));
				} catch (NumberFormatException nex) {
					param.setExprValue(value);
				}
				result.add(param);
			}
		}
		return result;
	}

	public Map<EffectKey, EffectValue> getItemInfluenceMap() {
		if(allValuesSetted()) {
			return innerItemInfluenceMap;
		} else {
			return new HashMap<EffectKey, EffectValue>();
		}
	}
}
