package ru.hegemony.editor;

import java.awt.Dimension;
import java.util.List;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JList;

import org.ciscavate.cjwizard.PageFactory;
import org.ciscavate.cjwizard.WizardContainer;
import org.ciscavate.cjwizard.WizardListener;
import org.ciscavate.cjwizard.WizardPage;
import org.ciscavate.cjwizard.WizardSettings;
import org.ciscavate.cjwizard.pagetemplates.TitledPageTemplate;

import ru.hegemony.editor.data.LevelHolder;
import ru.hegemony.editor.data.LevelListHolder;
import ru.hegemony.editor.exception.BusinessException;
import ru.hegemony.editor.helper.MessageHelper;
import ru.hegemony.editor.wizard.CountryListWizardPage;
import ru.hegemony.editor.wizard.EventDescWizardPage;
import ru.hegemony.editor.wizard.FinishWizardPage;
import ru.hegemony.editor.wizard.ItemSetsWizardPage;
import ru.hegemony.xml.data.LevelList;
import ru.hegemony.xml.data.ObjectFactory;

public class GuiEditorLevelDialog extends JDialog {
	private static final long serialVersionUID = 3955945271061723587L;
	
	private static final int DIALOG_WIDTH = 500;
	private static final int DIALOG_HEIGHT = 450;
	
	private JList list;
	
	private LevelListHolder levelListHolder;
//	TODO ��������, �������� ���� levelShortInfo �� �����
	private LevelList levelShortInfo;
	private LevelHolder levelFullInfo;
	
	private boolean newLevel;
	
	private Dimension dialogSize = new Dimension(DIALOG_WIDTH, DIALOG_HEIGHT);
	
	public GuiEditorLevelDialog(JFrame ownerFrame, JList list, LevelListHolder levelListHolder, boolean newLevel) {
		super(ownerFrame, true);
		this.levelListHolder = levelListHolder;
		this.newLevel = newLevel;
		this.list = list;
		try {
			initLevel();
			initComponents();
		} catch (Exception ex) {
			MessageHelper.showError("������ �������� ������", ownerFrame, ex);
		}
	}
	
	private void initComponents() {
		final WizardContainer wc = new WizardContainer(new EditorWizardFactory(), new TitledPageTemplate());
		wc.addWizardListener(new WizardListener() {
			@Override
			public void onCanceled(List<WizardPage> path, WizardSettings settings) {
				GuiEditorLevelDialog.this.dispose();
			}
			@Override
			public void onFinished(List<WizardPage> path, WizardSettings settings) {
				if(newLevel) {
					ObjectFactory objectFactory = new ObjectFactory();
					if(levelListHolder.getLevelList() == null) {
						levelListHolder.setLevelList(objectFactory.createLevelListType());
					}
					levelListHolder.getLevelTypeList().add(levelFullInfo);
					levelListHolder.getLevelList().getLevel().add(createLevelShortInfo(levelFullInfo));
				} else {
					for(LevelList level : levelListHolder.getLevelList().getLevel()) {
						if(level.getFilename().equalsIgnoreCase(levelShortInfo.getFilename())) {
							level.setFilename(levelFullInfo.getFilename());
							level.setTitle(levelFullInfo.getLevel().getEventDesc().getTitle());
							break;
						}
					}
					for(LevelHolder levelHolder : levelListHolder.getLevelTypeList()) {
						if(levelHolder.getFilename().equalsIgnoreCase(levelShortInfo.getFilename())) {
							levelHolder.setFilename(levelFullInfo.getFilename());
							levelHolder.setLevel(levelFullInfo.getLevel());
							break;
						}
					}
				}
				
				String[] levels = new String[levelListHolder.getLevelList().getLevel().size()];
				for(int i = 0; i < levels.length; i++) {
					levels[i] = levelListHolder.getLevelList().getLevel().get(i).getTitle();
				}
				
				list.setListData(levels);
				
				GuiEditorLevelDialog.this.dispose();
			}
			@Override
			public void onPageChanged(WizardPage newPage, List<WizardPage> path) {
				GuiEditorLevelDialog.this.setTitle(newPage.getDescription());
			}
		});
		
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.getContentPane().add(wc);
		this.setSize(dialogSize);
		this.setResizable(false);
		this.setPreferredSize(dialogSize);
		this.pack();
		this.setVisible(true);
	}
	
	private void initLevel() throws BusinessException {
		levelFullInfo = new LevelHolder();
		if(newLevel) {
			levelShortInfo = createLevelShortInfo(new LevelList());
		} else {
			int selectedIndex = list.getSelectedIndex();
			levelShortInfo = createLevelShortInfo(levelListHolder.getLevelList().getLevel().get(selectedIndex));
			levelFullInfo.setFilename(levelShortInfo.getFilename());
			for(LevelHolder level : levelListHolder.getLevelTypeList()) {
				if(level.getFilename().equalsIgnoreCase(levelShortInfo.getFilename())) {
					levelFullInfo.setLevel(level.getLevel());
					break;
				}
			}
			if(levelFullInfo.getLevel() == null) {
				// ���� ��������, ���� �������� "�������������", �� ��� �����, �� ������� ����� �������� �������
				throw new BusinessException("�� ������� �������� ���������� �� ������. ���� ������ ��� ��������� ��� ������������.");
			}
		}
	}
	
	private LevelList createLevelShortInfo(LevelList srcShortInfo) {
		ObjectFactory objectFactory = new ObjectFactory();
		LevelList newLevelShortInfo = objectFactory.createLevelList();
		newLevelShortInfo.setFilename(srcShortInfo.getFilename());
		newLevelShortInfo.setId(srcShortInfo.getId());
		newLevelShortInfo.setTitle(srcShortInfo.getTitle());
		return newLevelShortInfo;
	}
	
	private LevelList createLevelShortInfo(LevelHolder srcLevelHolder) {
		ObjectFactory objectFactory = new ObjectFactory();
		LevelList newLevelShortInfo = objectFactory.createLevelList();
		newLevelShortInfo.setFilename(srcLevelHolder.getFilename());
		newLevelShortInfo.setTitle(srcLevelHolder.getLevel().getEventDesc().getTitle());
		return newLevelShortInfo;
	}
	
	private class EditorWizardFactory implements PageFactory {
		private final WizardPage[] pages = {
				new EventDescWizardPage(levelFullInfo, dialogSize, false),
				new CountryListWizardPage(levelFullInfo, dialogSize, false),
				new ItemSetsWizardPage(levelFullInfo, dialogSize, false),
				new FinishWizardPage(dialogSize, true)
		};

		@Override
		public WizardPage createPage(List<WizardPage> path, WizardSettings settings) {
			WizardPage page = pages[path.size()];
			return page;
		}
	}
}
