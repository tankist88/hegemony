package ru.hegemony.editor.test;

import java.io.FileOutputStream;
import java.util.Random;
import java.util.zip.DeflaterOutputStream;

public class CompressionTest {
	private static final Random RANDOM_GENERATOR = new Random(System.currentTimeMillis());
	
	public static void main(String args[]) {
		try {
//			int[] bArr = new int[10000];
//			for(int i = 0; i < bArr.length; i++) {
//				int randValue = (RANDOM_GENERATOR.nextInt(70000000) + 60000000) - 60000000;
//				bArr[i] = randValue;
//			}
			
//			int[] bArr = new int[10000];
//			for (int k = 0; k < bArr.length; k += 1) {
//				bArr[k] = (RANDOM_GENERATOR.nextInt(99) + 1) - 1;
//				System.out.println("bArr[k]: " + bArr[k]);
//			}
			
			byte[] bArr = new byte[6000];
			RANDOM_GENERATOR.nextBytes(bArr);
			for (int k = 0; k < bArr.length; k += 1) {
				bArr[k] = (byte) Math.abs(bArr[k]);
				System.out.println("bArr[k]: " + bArr[k]);
			}
			
//			ByteArrayOutputStream bosNoComp = new ByteArrayOutputStream();
//			DataOutputStream dosNoComp = new DataOutputStream(bosNoComp);
//			for(int i = 0; i < bArr.length; i++) {
//				dosNoComp.writeInt(bArr[i]);
//			}
//			dosNoComp.close();
//			System.out.println("No comp size: " + bosNoComp.size());
			
//			DataOutputStream dosNoComp = new DataOutputStream(new FileOutputStream("test"));
//			for(int i = 0; i < bArr.length; i++) {
//				dosNoComp.writeInt(bArr[i]);
//			}
			
			FileOutputStream fos = new FileOutputStream("test");
			fos.write(bArr);
			fos.close();
			
//			ByteArrayOutputStream bosComp = new ByteArrayOutputStream();
//			DataOutputStream dosComp = new DataOutputStream(new DeflaterOutputStream(bosComp));
//			for(int i = 0; i < bArr.length; i++) {
//				dosComp.writeInt(bArr[i]);
//			}
//			dosComp.close();
//			System.out.println("Comp size: " + bosComp.size());
			
//			DataOutputStream dosComp = new DataOutputStream(new DeflaterOutputStream(new FileOutputStream("testDeflater")));
//			for(int i = 0; i < bArr.length; i++) {
//				dosComp.writeInt(bArr[i]);
//			}
//			dosComp.close();
			
			DeflaterOutputStream dOut = new DeflaterOutputStream(new FileOutputStream("testDeflater"));
			dOut.write(bArr);
			dOut.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
