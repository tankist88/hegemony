package ru.hegemony.editor.test;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;

import net.jpountz.lz4.LZ4Compressor;
import net.jpountz.lz4.LZ4Factory;
import net.jpountz.lz4.LZ4FastDecompressor;
import net.jpountz.lz4.LZ4SafeDecompressor;

public class Compression2Test {
	public static void main(String args[]) {
		try {
			LZ4Factory factory = LZ4Factory.fastestInstance();
	
			int[] dataInt = new int[10000];
			for(int i = 0; i < dataInt.length; i++) {
				dataInt[i] = i * i;
			}
			
			ByteBuffer byteBuffer = ByteBuffer.allocate(dataInt.length * 4);
			IntBuffer intBuffer = byteBuffer.asIntBuffer();
			intBuffer.put(dataInt);
			
			byte[] data = byteBuffer.array();
			
//			byte[] data = "12345345234572".getBytes("UTF-8");
			final int decompressedLength = data.length;
			System.out.println("data.length: " + data.length);
	
			// compress data
			LZ4Compressor compressor = factory.fastCompressor();
			int maxCompressedLength = compressor.maxCompressedLength(decompressedLength);
			byte[] compressed = new byte[maxCompressedLength];
			int compressedLength = compressor.compress(data, 0, decompressedLength, compressed, 0, maxCompressedLength);
			System.out.println("compressed: " + compressed.length);
			System.out.println("compressedLength: " + compressedLength);
	
			// decompress data
			// - method 1: when the decompressed length is known
			LZ4FastDecompressor decompressor = factory.fastDecompressor();
			byte[] restored = new byte[decompressedLength];
			int compressedLength2 = decompressor.decompress(compressed, 0, restored, 0, decompressedLength);
			System.out.println("compressedLength2: " + compressedLength2);
			// compressedLength == compressedLength2
	
			// - method 2: when the compressed length is known (a little slower)
			// the destination buffer needs to be over-sized
			LZ4SafeDecompressor decompressor2 = factory.safeDecompressor();
			int decompressedLength2 = decompressor2.decompress(compressed, 0, compressedLength, restored, 0);
			System.out.println("decompressedLength2: " + decompressedLength2);
			// decompressedLength == decompressedLength2
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}
}
