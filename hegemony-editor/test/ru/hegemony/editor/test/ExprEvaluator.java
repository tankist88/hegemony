package ru.hegemony.editor.test;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

public class ExprEvaluator
{
    public static void main(String[] args) throws Exception {
        System.out.println("Expr Evaluator v1.0");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            try {
                System.out.print(">");
                String line = br.readLine();
                if (line == null)
                    break;
                Expression e = new ExpressionBuilder(line).build();
                System.out.println(e.evaluate());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
