package ru.hegemony.editor.test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Random;

import org.apache.commons.math3.random.RandomDataGenerator;

public class DistributionTest {
	public static void main(String args[]) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
		RandomDataGenerator randomData = new RandomDataGenerator();
		for (int i = 0; i < 10; i++) {
			System.out.println(randomData.nextBinomial(1, 0.8));
		}
		System.out.println("-------");
		for (int i = 0; i < 10; i++) {
			System.out.println(nextBernoulli(0.8));
		}
		System.out.println("-------");
		System.out.println("REFLECT CALL");
		for(Method m : randomData.getClass().getMethods()) {
			if(m.getName().equals("nextBinomial")) {
				System.out.println(m.invoke(randomData, new Object[] {1, 0.8}));
			}
		}
		System.out.println("-------");
	}
	
	public static int nextBernoulli(double p) {
		double q = 1.0 - p;
		Random rnd = new Random(System.nanoTime());
		double uniformValue = rnd.nextDouble();
		return (uniformValue < q) ? 0 : 1;
	}
}
