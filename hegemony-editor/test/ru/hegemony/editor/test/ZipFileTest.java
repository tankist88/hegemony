package ru.hegemony.editor.test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

public class ZipFileTest {
	private static Map<String, InputStream> zipMap = new HashMap<String, InputStream>();
	public static void main(String[] args) {
		try {
			zipMap.put("file1.txt", new FileInputStream("C:\\setup.log"));
			zipMap.put("file2.txt", new FileInputStream("C:\\setup.log"));
			zipMap.put("file3.txt", new FileInputStream("C:\\setup.log"));
			
			putToZip(zipMap, "C:\\Users\\Asus\\Desktop\\MyFile.wcl");
			System.out.println("Done");
			readZip("C:\\Users\\Asus\\Desktop\\MyFile.wcl");
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
	
	private static Map<String, InputStream> readZip(final String filename) throws IOException {
		Map<String, InputStream> zipMap = new HashMap<String, InputStream>();
		ZipFile zipFile = new ZipFile(filename);
		Enumeration<? extends ZipEntry> entries = zipFile.entries();
		while (entries.hasMoreElements()) {
			ZipEntry entry = entries.nextElement();
			InputStream stream = zipFile.getInputStream(entry);
			zipMap.put(entry.getName(), stream);
		}
		return zipMap;
	}
	
	private static void putToZip(final Map<String, InputStream> zipMap, final String filename) throws IOException {
		FileOutputStream fos = new FileOutputStream(filename);
		ZipOutputStream zos = new ZipOutputStream(fos);		
		for(Entry<String, InputStream> entry : zipMap.entrySet()) {
			putEntryToZip(entry.getKey(), zos, entry.getValue());
			entry.getValue().close();
		}
		zos.close();
	}
	
	private static void putEntryToZip(final String entryName, final ZipOutputStream zos, final InputStream in) throws IOException {
		ZipEntry ze = new ZipEntry(entryName);
		zos.putNextEntry(ze);
		byte[] buffer = new byte[1024];
		int len;
		while ((len = in.read(buffer)) > 0) {
			zos.write(buffer, 0, len);
		}
		zos.closeEntry();
	}
}
