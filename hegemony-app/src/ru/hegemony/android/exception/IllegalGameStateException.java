package ru.hegemony.android.exception;

public class IllegalGameStateException extends Exception {
	private static final long serialVersionUID = 6699838171456735435L;

	public IllegalGameStateException(String msg) {
		super(msg);
	}
}
