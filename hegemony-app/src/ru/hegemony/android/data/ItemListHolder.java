package ru.hegemony.android.data;

import java.io.Serializable;
import java.util.List;

import ru.hegemony.xml.data.CountryEntity;
import ru.hegemony.xml.data.ItemEntity;

public class ItemListHolder implements Serializable {
	private static final long serialVersionUID = 6305834837027316989L;
	
	private List<ItemEntity> itemList;
	private CountryEntity selectedCountry;

	public List<ItemEntity> getItemList() {
		return itemList;
	}
	public void setItemList(List<ItemEntity> itemList) {
		this.itemList = itemList;
	}
	public CountryEntity getSelectedCountry() {
		return selectedCountry;
	}
	public void setSelectedCountry(CountryEntity selectedCountry) {
		this.selectedCountry = selectedCountry;
	}
}
