package ru.hegemony.android.data;

import java.io.Serializable;
import java.util.HashSet;

import ru.hegemony.xml.data.ItemEntity;

/**
 * ����� ������� ��� ���������� ����������� � ����������� ��������� ��� ��������� ��������� ���������� (Activity)
 * (������� ������ � �.�.)
 */
public class SelectedItemsContainer implements Serializable {
	private static final long serialVersionUID = -2268442803307893339L;
	
	private HashSet<ItemEntity> selectedItems;

	public SelectedItemsContainer(HashSet<ItemEntity> selectedItems) {
		this.selectedItems = selectedItems;
	}

	public HashSet<ItemEntity> getSelectedItems() {
		return selectedItems;
	}
}
