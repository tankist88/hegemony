package ru.hegemony.android.data;

public enum ResourceType {
	DRAWABLE("drawable"),
	RAW("raw");
	
	private final String name;
	
	private ResourceType(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
