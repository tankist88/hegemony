package ru.hegemony.android.helper;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import android.app.Activity;

public class ActivityManager {
	/**
	 * ���� ����������� (Activity stack)
	 */
	private static ArrayList<WeakReference<Activity>> activityStack = new ArrayList<WeakReference<Activity>>();

	/**
	 * ���������� ���������� (Activity) � ����
	 * @param act - ����������
	 */
	public static void addToActivityStack(Activity act) {
		WeakReference<Activity> ref = new WeakReference<Activity>(act);
		activityStack.add(ref);
	}

	/**
	 * ��������� ��� ���������� � �����, ����� ���������� �� ����
	 * @param act - ����������, ������� �� ����� ������������, null - ���������� ��� ���������� ����������
	 */
	public static void killAllExcept(Activity act) {
		for (WeakReference<Activity> ref : activityStack) {
			if (ref != null && ref.get() != null) {
				if (act != null && ref.get().equals(act)) {
					continue;// dont finish this up.
				}
				ref.get().finish();
			}
		}
		activityStack.clear();// but clear all the activity references
	}
	
	public static void killAllExcept() {
		ActivityManager.killAllExcept(null);
	}
}
