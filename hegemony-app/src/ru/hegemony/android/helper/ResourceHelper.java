package ru.hegemony.android.helper;

import ru.hegemony.android.data.ResourceType;
import android.content.res.Resources;

public class ResourceHelper {
	public static int getResourceId(ResourceType type, String name, Resources resources) {
		return resources.getIdentifier(
				"ru.hegemony.android.activity:"+ type.getName() +"/" + name, 
				null, 
				null
			);
	}
}
