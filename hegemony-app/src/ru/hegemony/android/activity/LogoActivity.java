package ru.hegemony.android.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

public class LogoActivity extends Activity {
	protected int getContentViewId() {
		return R.layout.activity_logo;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        setContentView(getContentViewId());
		
		TextView titleText = (TextView) findViewById(R.id.title_text_view);
		Typeface font = Typeface.createFromAsset(getAssets(), "fonts/logo.ttf");
		titleText.setTypeface(font);
		
		Handler mHideHandler = new Handler();
		Runnable mHideRunnable = new Runnable() {
			@Override
			public void run() {
				// ������� �� ��������� ��������
				Intent intent = new Intent(LogoActivity.this, SettingsActivity.class);
				startActivity(intent);
			}
		};
		
		mHideHandler.removeCallbacks(mHideRunnable);
		mHideHandler.postDelayed(mHideRunnable, 4000);
	}
}
