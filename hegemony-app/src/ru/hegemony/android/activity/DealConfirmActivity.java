package ru.hegemony.android.activity;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import ru.hegemony.android.activity.ui.DealListAdapter;
import ru.hegemony.android.exception.IllegalGameStateException;
import ru.hegemony.logic.data.UnionDealData;
import ru.hegemony.xml.data.CountryEntity;
import ru.hegemony.xml.data.PlayerEntity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

public class DealConfirmActivity extends BaseGameActivity {

	private ListView dealList;
	
	@Override
	protected int getContentViewId() {
		return R.layout.activity_deal;
	}
	
	@Override
	protected Class<? extends BaseGameActivity> getDefaultNextActivity() {
		return TurnResultActivity.class;
	}

	@Override
	protected void doBeforeNext() throws IllegalGameStateException, Exception {
		Map<Integer, Boolean> playerAnswers = ((DealListAdapter) dealList.getAdapter()).getPlayerAnswers();
		for(Entry<Integer, Boolean> entry : playerAnswers.entrySet()) {
			getGameController().setHumanPlayerAnswer(entry.getKey(), entry.getValue());
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		initActivity();
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initActivity();
	}
	
	private void initActivity() {
		if(dealList == null) {
			dealList = (ListView) findViewById(R.id.deal_list);
		}
		if(dealList.getAdapter() == null) {
			DealListAdapter adapter = new DealListAdapter(this);
			List<CountryEntity> countryListWithoutPlayer = adapter.getCountryListWithoutPlayer();
			for(CountryEntity initiator : countryListWithoutPlayer) {
				for(CountryEntity recievier : countryListWithoutPlayer) {
					if(recievier.getCode() != initiator.getCode()) {
						getGameController().breakTheUnionIfNeed(getUnionDealData(initiator, recievier));
						getGameController().suggestUnion(getUnionDealData(initiator, recievier), false);
					}
				}
			}
			dealList.setAdapter(adapter);
		}
	}
	
	private UnionDealData getUnionDealData(CountryEntity initiator, CountryEntity recievier) {
		List<PlayerEntity> players = getGameController().getLevel().getPlayers();
		return new UnionDealData(initiator.getCode(), recievier.getCode(), players);
	}
}
