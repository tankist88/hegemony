package ru.hegemony.android.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.Resources.NotFoundException;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import org.jibx.runtime.JiBXException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import ru.hegemony.android.activity.ui.helper.SpinnerHelper;
import ru.hegemony.android.data.ResourceType;
import ru.hegemony.android.exception.IllegalGameStateException;
import ru.hegemony.android.helper.ActivityManager;
import ru.hegemony.android.helper.ResourceHelper;
import ru.hegemony.common.data.MainResources;
import ru.hegemony.logic.action.GameControllerImpl;
import ru.hegemony.logic.data.DataSources;
import ru.hegemony.logic.data.LevelDifficulty;
import ru.hegemony.logic.data.LevelInitData;
import ru.hegemony.logic.helper.CountryHelper;
import ru.hegemony.logic.helper.LevelHelper;
import ru.hegemony.xml.data.CountryEntity;
import ru.hegemony.xml.data.LevelList;
import ru.hegemony.xml.data.LevelType;
import ru.hegemony.xml.data.PlayerEntity;
import ru.hegemony.xml.data.ResourcesEntity;

public class SettingsActivity extends BaseGameActivity {
	private Spinner countrySpinner = null;
	private Spinner dificultySpinner = null;
	private Spinner levelSpinner = null;
	private Button clearCacheButton = null;
	private ProgressBar gameProgress = null;
	private TextView progressText = null;
	
	private int selectedLevelResId;
	private String levelFilename;
	
	@Override
	protected int getContentViewId() {
		return R.layout.activity_settings;
	}
	
	@Override
	protected Class<? extends BaseGameActivity> getDefaultNextActivity() {
		return ScenarioInfoActivity.class;
	}

	@Override
	protected void doBeforeNext() throws IllegalGameStateException, Exception {
		selectEvent();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		setFieldsParams();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		progressText = (TextView) findViewById(R.id.progress_text);
		gameProgress = (ProgressBar) findViewById(R.id.game_progress);
		
		countrySpinner = (Spinner) findViewById(R.id.country_combo_box);
		dificultySpinner = (Spinner) findViewById(R.id.difficulty_combo_box);
		levelSpinner = (Spinner) findViewById(R.id.level_combo_box);
		clearCacheButton = (Button) findViewById(R.id.clear_cache_button);
		
		levelSpinner.setAdapter(SpinnerHelper.getSpinnerElements(this, LevelList.class, getLevelList()));
		countrySpinner.setAdapter(SpinnerHelper.getImageSpinnerElements(this, new ArrayList<CountryEntity>()));
		dificultySpinner.setAdapter(SpinnerHelper.getSpinnerElements(this, LevelDifficulty.class, LevelDifficulty.values()));
		
		clearCacheButton.setOnClickListener(new ClearCacheOnClickListener());
		
		setFieldsParams();
	}
	
	@Override
	public void onBackPressed() {
	    AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    builder.setMessage(getResources().getString(R.string.exit_question))
	           .setCancelable(false)
	           .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
	               public void onClick(DialogInterface dialog, int id) {
	            	   ActivityManager.killAllExcept();
                       finish();
                       moveTaskToBack(true);
	               }
	           })
	           .setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
	               public void onClick(DialogInterface dialog, int id) {
	                    dialog.cancel();
	               }
	           });
	    AlertDialog alert = builder.create();
	    alert.show();
	}
	
	private PlayerEntity getPlayerOrHegemon(boolean hegemon) {
		PlayerEntity player = null;
		try {
			if(getGameController() == null) {
				setGameController(new GameControllerImpl());
			}
			InputStream ds = createPlayersStatDs();
			if(ds != null) {
				if(hegemon) {
					player = getGameController().getHegemonFromCache(ds);
				} else {
					player = getGameController().getHumanPlayerFromCache(ds);
				}
			}
		} catch(Exception ex) {
			Log.e(getClass().toString(), ex.getMessage(), ex);
		}
		return player;
	}
	
	private void setFieldsParams() {
		PlayerEntity player = getPlayerOrHegemon(false);
		levelSpinner.setOnItemSelectedListener(new LevelOnItemSelectedListener(this, player));
		if(player != null) {
			countrySpinner.setEnabled(false);
			clearCacheButton.setEnabled(true);
			PlayerEntity hegemon = getPlayerOrHegemon(true);
			int infCode = MainResources.INFLUENCE.getCode();
			double playerInf = CountryHelper.getResourceByCode(infCode, player.getCountry()).getValue().doubleValue();
			double hegemonInf = CountryHelper.getResourceByCode(infCode, hegemon.getCountry()).getValue().doubleValue();
			// ����� ����� ��������� ����� ��������� ������� �������� �������� + 20%
			int max = (int)(hegemonInf + hegemonInf * 0.2);
			int progress = (int)(playerInf);
			progressText.setVisibility(View.VISIBLE);
			gameProgress.setVisibility(View.VISIBLE);
			if(progress >= max) {
				progressText.setText(getResources().getString(R.string.hegemon_text));
				setNextButtonEnabled(false);
			} else {
				progressText.setText(getResources().getString(R.string.hegemometr_text) + ": " + progress + "/" + max);
				setNextButtonEnabled(true);
			}
			gameProgress.setMax(max);
			gameProgress.setProgress(progress);
		} else {
			try {
				countrySpinner.setEnabled(true);
				countrySpinner.setSelection(0);
				clearCacheButton.setEnabled(false);
				setNextButtonEnabled(true);
				progressText.setVisibility(View.GONE);
				gameProgress.setVisibility(View.GONE);
				selectLevel(this, player);
			} catch (Exception e) {
				Log.e(this.getClass().toString(), e.getMessage(), e);
			}
		}
	}
	
	private void selectEvent() throws Exception {
		DataSources dataSources = new DataSources();
		dataSources.setLevelDs(getResources().openRawResource(selectedLevelResId));
		dataSources.setPlayersStatDs(createPlayersStatDs());
		dataSources.setOpinionPatternsDs(getResources().openRawResource(R.raw.opinion_patterns));
		
		int countryId = ((CountryEntity) countrySpinner.getSelectedItem()).getCode();
		LevelDifficulty difficulty = ((LevelDifficulty) dificultySpinner.getSelectedItem());
		
		LevelInitData initData = new LevelInitData(levelFilename, dataSources, countryId, difficulty, null);
		setGameController(new GameControllerImpl());
		getGameController().initLevel(initData);
	}
	
	private InputStream createPlayersStatDs() {
		InputStream result = null;
		String sdState = android.os.Environment.getExternalStorageState();
		if (sdState.equals(android.os.Environment.MEDIA_MOUNTED)) {
			try {
				File sdDir = android.os.Environment.getExternalStorageDirectory();
				File fileDir = new File(sdDir.getAbsolutePath(), LevelResultActivity.CACHE_DIR);
				File fileToRead = new File(fileDir, LevelResultActivity.CACHE_FILE);
				if (fileToRead.exists()) {
					return new FileInputStream(fileToRead);
				}
			} catch (Exception e) {
				Log.w(getClass().toString(), "Unable to read cache. " + e.getMessage());
			}
		}
		return result;
	}
	
	private class ClearCacheOnClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this);
		    builder.setMessage(getResources().getString(R.string.clear_cache_question))
		           .setCancelable(false)
		           .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
		               public void onClick(DialogInterface dialog, int id) {
		            	   clearCache();
		               }
		           })
		           .setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
		               public void onClick(DialogInterface dialog, int id) {
		                    dialog.cancel();
		               }
		           });
		    AlertDialog alert = builder.create();
		    alert.show();
		}
	}
	
	private void clearCache() {
		String sdState = android.os.Environment.getExternalStorageState();
		if (sdState.equals(android.os.Environment.MEDIA_MOUNTED)) {
			try {
				File sdDir = android.os.Environment.getExternalStorageDirectory();
				File fileDir = new File(sdDir.getAbsolutePath(), LevelResultActivity.CACHE_DIR);
				File fileToRead = new File(fileDir, LevelResultActivity.CACHE_FILE);
				if (fileToRead.exists()) {
					fileToRead.delete();						
					setFieldsParams();
				}
			} catch (Exception e) {
				Log.w(getClass().toString(), "Unable to delete cache. " + e.getMessage());
			}
		}
	}
	
	private class LevelOnItemSelectedListener implements OnItemSelectedListener {
		private Activity activity;
		private PlayerEntity player;
		
		public LevelOnItemSelectedListener(Activity activity, PlayerEntity player) {
			this.activity = activity;
			this.player = player;
		}
		
		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
			try {
				LevelList selectedLevel = (LevelList) parent.getSelectedItem();
				
				String[] arr = selectedLevel.getFilename().split("\\.");
				String resName = arr[0].trim();
				
				levelFilename = selectedLevel.getFilename();
				selectedLevelResId = ResourceHelper.getResourceId(ResourceType.RAW, resName, getResources());
				
				selectLevel(activity, player);
			} catch (Exception e) {
				Log.e(this.getClass().toString(), e.getMessage(), e);
			}
		}
		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// do nothing
		}
	}
	
	private void selectLevel(Activity activity, PlayerEntity player) throws NotFoundException, JiBXException, IOException {
		LevelType level = LevelHelper.getLevel(getResources().openRawResource(selectedLevelResId));
		
		List<CountryEntity> countries = new ArrayList<CountryEntity>();
		int pos = 0;
		if(player != null) {
			countries = level.getCountryList();
			for(int i = 0; i < countries.size(); i++) {
				if(countries.get(i).getCode() == player.getCountry().getCode()) {
					pos = i;
					break;
				}
			}
		} else {
			countries = level.getCountryList();
			double infVal = 0.0;
			int index = 0;
			for(int i = 0; i < countries.size(); i++) {
				CountryEntity c = countries.get(i);
				ResourcesEntity res = CountryHelper.getResourceByCode(MainResources.INFLUENCE.getCode(), c);
				if(res.getValue().doubleValue() > infVal) {
					infVal = res.getValue().doubleValue();
					index = i;
				}
			}
			countries.remove(index);
		}
		countrySpinner.setAdapter(SpinnerHelper.getImageSpinnerElements(activity, countries));
		countrySpinner.setSelection(pos);
	}
	
	private List<LevelList> getLevelList() {
		List<LevelList> levelList = null;
		try {
			int resId = ResourceHelper.getResourceId(ResourceType.RAW, "level_list", getResources());
			levelList = LevelHelper.getLevelList(getResources().openRawResource(resId));
		} catch (Exception e) {
			Log.e(this.getClass().toString(), e.getMessage(), e);
			levelList = new ArrayList<LevelList>();
		}
		return levelList;
	}
}
