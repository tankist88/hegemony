package ru.hegemony.android.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.lucasr.twowayview.TwoWayView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ru.hegemony.android.activity.popup.AddItemPopup;
import ru.hegemony.android.activity.popup.CountryForOpinionPopup;
import ru.hegemony.android.activity.ui.ImageGridAdapter;
import ru.hegemony.android.activity.ui.ResourceListAdapter;
import ru.hegemony.android.data.ItemListHolder;
import ru.hegemony.android.data.ResourceType;
import ru.hegemony.android.data.SelectedItemsContainer;
import ru.hegemony.android.exception.IllegalGameStateException;
import ru.hegemony.android.helper.ResourceHelper;
import ru.hegemony.common.helper.DataHelper;
import ru.hegemony.logic.helper.ItemHelper;
import ru.hegemony.xml.data.CountryEntity;
import ru.hegemony.xml.data.ItemEntity;
import ru.hegemony.xml.data.PlayerEntity;

public class SuggestionActivity extends BaseGameActivity {
	/**
	 * ������������ ����� ��������� � �����������
	 * MAX_SUGGESTION_ITEMS = 5
	 */
	public static final int MAX_SUGGESTION_ITEMS = 5;
	/** 
	 * ��� ���������, ������� ���������� � ���������� � ���������� ��������� �������� 
	 * SELECTED_ITEMS_TAG = "SelectedItemsTag"
	 */
	private static final String SELECTED_ITEMS_TAG = "SelectedItemsTag";
	
	// �������� �� ��������� ����
	private ImageView countryFlag;
	private TextView countryName;
	private TwoWayView countryResourceList;
	private TextView currentTurnText;
	private TextView sugValueText;
	private GridView allowedItems;
	private GridView suggestionItems;
	private Button opinionsButton;
	
	// ��������� ����
	private HashSet<ItemEntity> selectedItems;
	private List<ItemEntity> allowedItemList = new ArrayList<ItemEntity>();
	
	@Override
	protected int getContentViewId() {
		return R.layout.activity_suggestion;
	}
	
	@Override
	protected Class<? extends BaseGameActivity> getDefaultNextActivity() {
		return DealConfirmActivity.class;
	}

	@Override
	protected void doBeforeNext() throws IllegalGameStateException, Exception {
		if(selectedItems.size() > 0) {
			getGameController().setSelectedItems(selectedItems);
		} else {
			throw new IllegalGameStateException(getResources().getString(R.string.suggestion_is_empty_error));
		}
	}
	
	/**
	 * ������������� ��������������� �������� ������ � ���������� ���������� � � �������������
	 */
	private void setItemsGrids() {
		ItemListHolder holder = (ItemListHolder) getIntent().getSerializableExtra(OpinionActivity.CONFIRMED_ITEM_SET);
		if(holder != null && holder.getItemList() != null && holder.getItemList().size() > 0) {
			Set<ItemEntity> itemsFromOpinion = DataHelper.createSetFromList(holder.getItemList());
			int currentSugSize = (selectedItems != null) ? selectedItems.size() : 0;
			if(itemsFromOpinion.size() + currentSugSize <= MAX_SUGGESTION_ITEMS) {
				if(selectedItems == null) {
					selectedItems = new HashSet<ItemEntity>();
				}
				for(ItemEntity item : itemsFromOpinion) {
					if(!itemAlreadyAddedInSet(selectedItems, item)) {
						selectedItems.add(item);
					}
				}
			} else if(itemsFromOpinion.size() == MAX_SUGGESTION_ITEMS) {
				selectedItems = new HashSet<ItemEntity>();
				for(ItemEntity item : itemsFromOpinion) {
					if(!itemAlreadyAddedInSet(selectedItems, item)) {
						selectedItems.add(item);
					}
				}
			}
		}
		if(selectedItems == null || selectedItems.size() == 0) {
			selectedItems = (HashSet<ItemEntity>) getGameController().getSelectedItems();
		}
		if(selectedItems == null || selectedItems.size() == 0) {
			selectedItems = new HashSet<ItemEntity>();
		}
		allowedItemList = getGameController().getLevel().getHeaderInfo().getItemList();
		if(allowedItems != null) {
			// ������������� ��������� � ��������� ������� ���������. �������� ����� ���������� �������.
			allowedItems.setAdapter(new ImageGridAdapter<ItemEntity>(SuggestionActivity.this, allowedItemList));
			(new SetHintTask()).execute();
		}
		if(suggestionItems != null) {
			suggestionItems.setAdapter(new ImageGridAdapter<ItemEntity>(this, selectedItems));
		}
		updateItemSetEffect();
	}
	
	public void updateItemSetEffect() {
		if(selectedItems != null && !selectedItems.isEmpty()) {
	        CountryEntity playerCountry = getGameController().getLevel().getHumanPlayer().getCountry();
	        double avrResDelta = getGameController().getItemSetEffect(playerCountry, DataHelper.createListFromSet(selectedItems));
	        sugValueText.setText(Double.toString(avrResDelta));
	        if(avrResDelta >= 0.0) {
	        	sugValueText.setTextColor(getResources().getColor(R.color.deal_confirmed_color));
	        } else {
	        	sugValueText.setTextColor(getResources().getColor(R.color.deal_canceled_color));
	        }
		} else {
			sugValueText.setText("");
		}
	}
	
	private void updateCurrentTurn() {
		int currentTurn = getGameController().getLevel().getCurrentTurn() + 1;
		int maxTurn = getGameController().getLevel().getHeaderInfo().getEventDesc().getTurnCount();
		currentTurnText.setText(" ("
				+ getResources().getString(R.string.current_turn_text) + ": "
				+ currentTurn + " "
				+ getResources().getString(R.string.turn_from_text) + " "
				+ maxTurn + ")");
	}

	@Override
	protected void onResume() {
		super.onResume();
		setItemsGrids();
		updateCurrentTurn();
        CountryEntity playerCountry = getGameController().getLevel().getHumanPlayer().getCountry();
        countryResourceList.setAdapter(new ResourceListAdapter(this, playerCountry.getResourceList()));
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		if(savedInstanceState != null) {
			selectedItems = ((SelectedItemsContainer) savedInstanceState.getSerializable(SELECTED_ITEMS_TAG)).getSelectedItems();
        }
	
		countryFlag = (ImageView) findViewById(R.id.country_image);
		countryName = (TextView) findViewById(R.id.country_name);
		countryResourceList = (TwoWayView) findViewById(R.id.resource_list);
		currentTurnText = (TextView) findViewById(R.id.current_turn);
		allowedItems = (GridView) findViewById(R.id.allowed_items);
		suggestionItems = (GridView) findViewById(R.id.sug_items);
		opinionsButton = (Button) findViewById(R.id.opinions_button);
		sugValueText = (TextView) findViewById(R.id.sug_value);
		
		CountryEntity playerCountry = getGameController().getLevel().getHumanPlayer().getCountry();
		
		countryFlag.setImageResource(ResourceHelper.getResourceId(ResourceType.DRAWABLE, playerCountry.getImage(), getResources()));
		countryName.setText(playerCountry.getName());
		
		countryResourceList.setAdapter(new ResourceListAdapter(this, playerCountry.getResourceList()));
		
		allowedItems.setOnItemClickListener(new OnAllowedItemsClickListener());
		
		suggestionItems.setLongClickable(true);
		suggestionItems.setOnItemLongClickListener(new OnSuggestionItemsLongClickListener());
		suggestionItems.setOnItemClickListener(new OnSuggestionItemsClickListener());
		
		opinionsButton.setOnClickListener(new ShowOpinionsOnClickListener());
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putSerializable(SELECTED_ITEMS_TAG, new SelectedItemsContainer(selectedItems));
		getGameController().setSelectedItems(selectedItems);
	}
	
	private class ShowOpinionsOnClickListener implements View.OnClickListener {
		@Override
		public void onClick(View v) {
			CountryForOpinionPopup.getInstance().show(SuggestionActivity.this);
		}
	}
	
	private class OnSuggestionItemsLongClickListener implements OnItemLongClickListener {
		@Override
		public boolean onItemLongClick(AdapterView<?> parent, View v, final int position, long id) {
//			TODO ������� �������� �������� � ����� � ����������� ������ �������� ���� ��������� �� �����������, � SuggestionActivity
			final Object[] itemArray = selectedItems.toArray();
			if(itemArray.length <= position) {
				return false;
			}
			AlertDialog.Builder builder = new AlertDialog.Builder(SuggestionActivity.this);
			builder.setMessage(
					getResources().getString(R.string.item_del_question))
					.setCancelable(false)
					.setPositiveButton(getResources().getString(R.string.yes),
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int id) {
									ItemEntity selectedItem = (ItemEntity) (itemArray[position]);
									selectedItems.remove(selectedItem);
									if (suggestionItems != null) {
										suggestionItems.setAdapter(
												new ImageGridAdapter<ItemEntity>(SuggestionActivity.this, selectedItems)
											);
									}
									updateItemSetEffect();
								}
							})
					.setNegativeButton(getResources().getString(R.string.no),
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int id) {
									dialog.cancel();
								}
							});
			AlertDialog alert = builder.create();
			alert.show();
			return true;
		}
	}
	
	private class OnAllowedItemsClickListener implements OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
			Object[] itemArray = allowedItemList.toArray();
        	if(itemArray.length > position) {
        		if(selectedItems.size() > MAX_SUGGESTION_ITEMS - 1) {
        			String maxItemsErrorMsg = getResources().getString(R.string.max_suggestion_error);
        			Toast.makeText(
							SuggestionActivity.this,
							maxItemsErrorMsg + " " + MAX_SUGGESTION_ITEMS, 
							Toast.LENGTH_SHORT).show();
        		} else {
        			ItemEntity selectedItem = ((ItemEntity) (itemArray[position]));
        			if(!itemAlreadyAddedInSet(selectedItems, selectedItem)) {
        				AddItemPopup.getInstance().show(selectedItem, SuggestionActivity.this);
        			}
        		}
        	}
		}
	}
	
	private class OnSuggestionItemsClickListener implements OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
			Object[] itemArray = selectedItems.toArray();
        	if(itemArray.length > position) {
        		ItemEntity selectedItem = ((ItemEntity) (itemArray[position]));
    			Toast.makeText(
						SuggestionActivity.this,
						selectedItem.getName() + " (" + ItemHelper.getItemActionByCode(selectedItem.getSa(), selectedItem.getActionList()).getName() + ")", 
						Toast.LENGTH_SHORT).show();
        	}
		}
	}
	
	private boolean itemAlreadyAddedInSet(Set<ItemEntity> set, ItemEntity itemForCheck) {
		if(set == null || itemForCheck == null) {
			return false;
		}
		for(ItemEntity item : set) {
			if(item.getImage().equalsIgnoreCase(itemForCheck.getImage())) {
				return true;
			}
		}
		return false;
	}
	
	private class SetHintTask extends AsyncTask<Void, Void, List<ItemEntity>> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			opinionsButton.setEnabled(false);
			setNextButtonEnabled(false);
		}
		@Override
		protected List<ItemEntity> doInBackground(Void... params) {
			for(PlayerEntity player : getGameController().getLevel().getPlayers()) {
				getGameController().getItemSetForSendDeal(player.getCountry());
				Log.i(getClass().getName(), player.getCountry().getName() + " - turn init.");
			}
			CountryEntity playerCountry = getGameController().getLevel().getHumanPlayer().getCountry();
			return getGameController().getItemSetForSendDeal(playerCountry);
		}
		@Override
		protected void onPostExecute(List<ItemEntity> result) {
			super.onPostExecute(result);
			allowedItems.setAdapter(new ImageGridAdapter<ItemEntity>(SuggestionActivity.this, allowedItemList, result));
			opinionsButton.setEnabled(true);
			setNextButtonEnabled(true);
		}
	}

	public GridView getSuggestionItems() {
		return suggestionItems;
	}

	public HashSet<ItemEntity> getSelectedItems() {
		return selectedItems;
	}
	
	public void addItemToSelected(ItemEntity item) {
		if(selectedItems != null) {
			selectedItems.add(item);
		}
	}
}
