package ru.hegemony.android.activity;

import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;

import ru.hegemony.android.exception.IllegalGameStateException;

public class ScenarioInfoActivity extends BaseGameActivity {
	@Override
	protected int getContentViewId() {
		return R.layout.activity_scenario_info;
	}
	
	@Override
	protected Class<? extends BaseGameActivity> getDefaultNextActivity() {
		return SuggestionActivity.class;
	}

	@Override
	protected void doBeforeNext() throws IllegalGameStateException, Exception {
		// do nothing
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		TextView scenarioText = (TextView) findViewById(R.id.scenario_text);
		scenarioText.setMovementMethod(new ScrollingMovementMethod());
		String scenarioTextStr = getGameController().getLevel().getHeaderInfo().getEventDesc().getDesc();
		scenarioText.setText(scenarioTextStr);
	}
}
