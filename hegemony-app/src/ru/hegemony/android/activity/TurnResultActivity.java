package ru.hegemony.android.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ru.hegemony.android.activity.ui.ImageGridAdapter;
import ru.hegemony.android.activity.ui.ResultListAdapter;
import ru.hegemony.android.activity.ui.helper.SpinnerHelper;
import ru.hegemony.android.exception.IllegalGameStateException;
import ru.hegemony.common.helper.DataHelper;
import ru.hegemony.logic.data.LevelStatus;
import ru.hegemony.logic.data.TurnStatusForCountry;
import ru.hegemony.xml.data.CountryEntity;
import ru.hegemony.xml.data.ItemEntity;
import ru.hegemony.xml.data.PlayerEntity;

public class TurnResultActivity extends BaseGameActivity {
	private Spinner countrySpinner = null;
	private GridView suggestionItems = null;
	private ListView resultList = null;
	private TextView resultText = null;
	
	private Map<CountryEntity, List<TurnStatusForCountry>> resultMap = new HashMap<CountryEntity, List<TurnStatusForCountry>>();
	private Map<CountryEntity, List<ItemEntity>> itemSetMap = new HashMap<CountryEntity, List<ItemEntity>>();
	
	@Override
	protected int getContentViewId() {
		return R.layout.activity_result;
	}
	
	@Override
	protected Class<? extends BaseGameActivity> getDefaultNextActivity() {
		LevelStatus levelStatus = getGameController().getLevelStatus();		
		if(levelStatus.equals(LevelStatus.LEVEL_CONTINUES)) {
			return SuggestionActivity.class;
		} else {
			return LevelResultActivity.class;
		}
	}

	@Override
	protected void doBeforeNext() throws IllegalGameStateException, Exception {
		// do nothing
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		List<CountryEntity> countryList = getGameController().getLevel().getHeaderInfo().getCountryList();
		int playerCountPosition = -1;
		for(int i = 0; i < countryList.size(); i++) {
			if(countryList.get(i).getCode() == getGameController().getLevel().getHumanPlayer().getCountry().getCode()) {
				playerCountPosition = i;
				break;
			}
		}
		
		countrySpinner = (Spinner) findViewById(R.id.country_combo_box);
		countrySpinner.setAdapter(SpinnerHelper.getImageSpinnerElements(this, countryList));
		countrySpinner.setOnItemSelectedListener(new CountryOnItemSelectedListener());
		countrySpinner.setSelection(playerCountPosition);
		
		suggestionItems = (GridView) findViewById(R.id.sug_items);
		suggestionItems.setAdapter(new ImageGridAdapter<ItemEntity>(this, new ArrayList<ItemEntity>()));
		
		resultList = (ListView) findViewById(R.id.result_list);
		resultList.setAdapter(new ResultListAdapter(this, new ArrayList<TurnStatusForCountry>()));
		
		for(PlayerEntity player : getGameController().getLevel().getPlayers()) {
			List<ItemEntity> itemList = null;
			if(!player.isArtIntSign()) {
				itemList = DataHelper.createListFromSet(getGameController().getSelectedItems());
			} else {
				itemList = getGameController().getItemSetForSendDeal(player.getCountry());
			}
			itemSetMap.put(player.getCountry(), itemList);
			List<TurnStatusForCountry> turnStat = null;
			try {
				turnStat = getGameController().sendDealToAll(player.getCountry(), itemList);
			} catch (Exception e) {
				throw new IllegalStateException(e);
			}
			resultMap.put(player.getCountry(), turnStat);
		}
		
		LevelStatus levelStatus = getGameController().getLevelStatus();
		
		resultText = (TextView) findViewById(R.id.result_text);
		
		if(levelStatus.equals(LevelStatus.LEVEL_CONTINUES)) {
			setNextButtonText(R.string.result_next_button_text);
			resultText.setText(R.string.result_no);
			resultText.setTextColor(getResources().getColor(R.color.deal_canceled_color));
		} else if(levelStatus.equals(LevelStatus.LEVEL_END_LOST)) {
			setNextButtonText(R.string.end_next_button_text);
			resultText.setText(R.string.result_no);
			resultText.setTextColor(getResources().getColor(R.color.deal_canceled_color));
		} else {
			setNextButtonText(R.string.end_next_button_text);
			resultText.setText(R.string.result_yes);
			resultText.setTextColor(getResources().getColor(R.color.deal_confirmed_color));
		}
	}

	private class CountryOnItemSelectedListener implements OnItemSelectedListener {
		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
			try {
				CountryEntity selectedCountry = (CountryEntity) parent.getSelectedItem();
				
				List<ItemEntity> itemSetList = itemSetMap.get(selectedCountry);
				
				suggestionItems.setAdapter(new ImageGridAdapter<ItemEntity>(TurnResultActivity.this, itemSetList));
				resultList.setAdapter(new ResultListAdapter(TurnResultActivity.this, resultMap.get(selectedCountry)));
			} catch (Exception e) {
				Log.e(this.getClass().toString(), e.getMessage(), e);
			}
		}
		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// do nothing
		}
	}
}
