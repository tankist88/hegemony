package ru.hegemony.android.activity.popup;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupWindow;

import ru.hegemony.android.activity.OpinionActivity;
import ru.hegemony.android.activity.R;
import ru.hegemony.xml.data.CountryEntity;

public class SpyAmountPopup {
    private static final double FIRST_AMOUNT = 50.0;
    private static final double SECOND_AMOUNT = 100.0;
    private static final double THIRD_AMOUNT = 150.0;

	private SpyAmountPopup() {
	}
	
	public static SpyAmountPopup getInstance() {
		return new SpyAmountPopup();
	}
	
	public void show(final CountryEntity destCountry, final OpinionActivity activity) {
		LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View layout = inflater.inflate(R.layout.popup_spy_amount, (ViewGroup) activity.findViewById(R.id.spy_amount_element));
		final PopupWindow popupWnd = new PopupWindow(layout, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
		popupWnd.showAtLocation(layout, Gravity.CENTER, 0, 0);

        final CountryEntity initiatorCountry = activity.getGameController().getLevel().getHumanPlayer().getCountry();

		Button cancelBtn = (Button) layout.findViewById(R.id.back_button);
		cancelBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
                activity.refreshSpyState(false);
                popupWnd.dismiss();
			}
		});

        Button firstBtn = (Button) layout.findViewById(R.id.first_amount_button);
        firstBtn.setText(Double.toString(FIRST_AMOUNT));
        firstBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.getGameController().spySuccess(initiatorCountry, destCountry, FIRST_AMOUNT);
                activity.refreshSpyState(true);
                popupWnd.dismiss();
            }
        });

        Button secondBtn = (Button) layout.findViewById(R.id.second_amount_button);
        secondBtn.setText(Double.toString(SECOND_AMOUNT));
        secondBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.getGameController().spySuccess(initiatorCountry, destCountry, SECOND_AMOUNT);
                activity.refreshSpyState(true);
                popupWnd.dismiss();
            }
        });

        Button thirdBtn = (Button) layout.findViewById(R.id.third_amount_button);
        thirdBtn.setText(Double.toString(THIRD_AMOUNT));
        thirdBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.getGameController().spySuccess(initiatorCountry, destCountry, THIRD_AMOUNT);
                activity.refreshSpyState(true);
                popupWnd.dismiss();
            }
        });
	}
}
