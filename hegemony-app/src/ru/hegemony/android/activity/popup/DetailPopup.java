package ru.hegemony.android.activity.popup;

import android.content.Context;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.TextView;

import ru.hegemony.android.activity.BaseGameActivity;
import ru.hegemony.android.activity.R;
import ru.hegemony.android.activity.popup.data.BaseData;
import ru.hegemony.android.activity.popup.data.PopupType;

public abstract class DetailPopup {
	private BaseGameActivity activity;
	private String title;
	
	protected DetailPopup(BaseData data) {
		this.activity = data.getActivity();
		this.title = data.getTitle();
	}
	
	public static DetailPopup getInstance(PopupType popupType, BaseData data) {
		if(popupType.equals(PopupType.DETAIL_INFO)) {
			return new DealDetailPopup(data);
		} else if(popupType.equals(PopupType.UNION_SUGGEST)) {
			return new CountryMsgPopup(data);
		} else if(popupType.equals(PopupType.UNION_ANSWER)) {
			return new UnionPopup(data);
		} else {
			throw new IllegalArgumentException("Illegal type of popup windows. Supports only " + PopupType.DETAIL_INFO + " or " + PopupType.UNION_SUGGEST);
		}
	}
	
	protected abstract String getText();
	protected abstract void doConfirm();
	protected abstract void doCancel();
	protected abstract boolean isTwoButtons();
	protected abstract void updateParrent();
	
	protected BaseGameActivity getActivity() {
		return activity;
	}
	
	public void initPopup() {
		try {
			LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View layout = inflater.inflate(R.layout.popup_deal_info, (ViewGroup) activity.findViewById(R.id.popup_deal_info_element));
			// TODO ����� ����������� ������������ �������� popup ����
			final PopupWindow popup = new PopupWindow(layout, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
			popup.showAtLocation(layout, Gravity.CENTER, 0, 0);
			
			TextView titleText = (TextView) layout.findViewById(R.id.detail_popup_title);
			titleText.setText(title);
			
			TextView itemSetText = (TextView) layout.findViewById(R.id.popup_itemset_detail_text);
			itemSetText.setMovementMethod(new ScrollingMovementMethod());
			itemSetText.setText(getText());
			
			Button dealConfirmButton = (Button) layout.findViewById(R.id.deal_confirm_button);
			Button dealCancelButton = (Button) layout.findViewById(R.id.deal_cancel_button);
			if(isTwoButtons()) {
				dealConfirmButton.setText(R.string.deal_detail_confirm);
				dealCancelButton.setText(R.string.deal_cancel_button_text);
				dealCancelButton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						popup.dismiss();
						doCancel();
						updateParrent();
					}
				});
			} else {
				dealConfirmButton.setText(R.string.deal_ok_button_text);
				dealCancelButton.setVisibility(View.GONE);
			}
			dealConfirmButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					popup.dismiss();
					doConfirm();
					updateParrent();
				}
			});
		} catch (Exception ex) {
			Log.e(this.getClass().getName(), ex.getMessage(), ex);
		}
	}
}
