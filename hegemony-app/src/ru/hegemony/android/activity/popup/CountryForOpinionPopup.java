package ru.hegemony.android.activity.popup;

import java.util.ArrayList;
import java.util.List;

import ru.hegemony.android.activity.OpinionActivity;
import ru.hegemony.android.activity.R;
import ru.hegemony.android.activity.SuggestionActivity;
import ru.hegemony.android.activity.ui.helper.SpinnerHelper;
import ru.hegemony.android.data.ItemListHolder;
import ru.hegemony.common.helper.DataHelper;
import ru.hegemony.xml.data.CountryEntity;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.Toast;

public class CountryForOpinionPopup {
	private CountryForOpinionPopup() {
	}
	
	public static CountryForOpinionPopup getInstance() {
		return new CountryForOpinionPopup();
	}
	
	/**
	 * ����� ������������ ���� ��� ������ ������, ����� �������� ����� � ������
	 */
	public void show(final SuggestionActivity activity) {
		LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View layout = inflater.inflate(R.layout.popup_country_select, (ViewGroup) activity.findViewById(R.id.country_select_popup_element));
		final PopupWindow popupWnd = new PopupWindow(layout, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
		popupWnd.showAtLocation(layout, Gravity.CENTER, 0, 0);
		
		final Spinner chooseCountrySpinner = (Spinner) layout.findViewById(R.id.popup_country);
		List<CountryEntity> countryList = new ArrayList<CountryEntity>();
		for(CountryEntity c : activity.getGameController().getLevel().getHeaderInfo().getCountryList()) {
			if(c.getCode() != activity.getGameController().getLevel().getHumanPlayer().getCountry().getCode()) {
				countryList.add(c);
			}
		}
		chooseCountrySpinner.setAdapter(SpinnerHelper.getImageSpinnerElements(activity, countryList));
		
		Button chooseCountryBtn = (Button) layout.findViewById(R.id.country_confirm_button);
		chooseCountryBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// ��������� �� ���������� (Activity) ��������� ������ ������
				popupWnd.dismiss();
				activity.getGameController().setSelectedItems(activity.getSelectedItems());
				if(activity.getSelectedItems().size() > 0) {
					CountryEntity selectedCountry = (CountryEntity) chooseCountrySpinner.getSelectedItem();
					ItemListHolder holder = new ItemListHolder();
					holder.setSelectedCountry(selectedCountry);
					holder.setItemList(DataHelper.createListFromSet(activity.getSelectedItems()));
					activity.goToActivity(OpinionActivity.class, OpinionActivity.ITEM_LIST_HOLDER, holder);
				} else {
					Toast.makeText(
						activity,
						"����������� �� �������. ����� ������ ����������.",
						Toast.LENGTH_SHORT).show();
				}
			}
		});
		
		Button cancelBtn = (Button) layout.findViewById(R.id.cancel_button);
		cancelBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// ������ ��������� ����������� ����
				popupWnd.dismiss();
			}
		});
	}
}
