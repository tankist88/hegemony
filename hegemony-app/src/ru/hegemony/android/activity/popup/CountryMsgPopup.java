package ru.hegemony.android.activity.popup;

import java.util.List;

import ru.hegemony.android.activity.R;
import ru.hegemony.android.activity.popup.data.BaseData;
import ru.hegemony.android.activity.popup.data.MsgData;
import ru.hegemony.logic.data.UnionDealData;
import ru.hegemony.xml.data.CountryEntity;
import ru.hegemony.xml.data.PlayerEntity;

public class CountryMsgPopup extends DetailPopup {
	private MsgData msgData;
	
	protected CountryMsgPopup(BaseData data) throws ClassCastException {
		super(data);
		if(data instanceof MsgData) {
			this.msgData = (MsgData) data;
		} else {
			throw new ClassCastException("Illegal type of input data for class " + CountryMsgPopup.class + ". Data must be only " + MsgData.class + " type");
		}
	}
	@Override
	protected String getText() {
		if(msgData.isUnionBreak()) {
			return getUnionCountry().getName() + " " + getActivity().getResources().getString(R.string.break_union_text);
		} else if(isUnionPosible()) {
			return getUnionCountry().getName() + " " + getActivity().getResources().getString(R.string.union_suggest_text);
		} else{
			return getActivity().getResources().getString(R.string.deal_no_msg_text);
		}
	}
	@Override
	protected void doConfirm() {
		if(isUnionPosible()) {
			getActivity().getGameController().suggestUnion(getUnionDealData(), true);
		}
	}
	@Override
	protected void doCancel() {
		// do nothing
	}
	@Override
	protected boolean isTwoButtons() {
		return isUnionPosible();
	}
	private CountryEntity getUnionCountry() {
		CountryEntity country = null;
		for(PlayerEntity player : getActivity().getGameController().getLevel().getPlayers()) {
			if(player.getCountry().getCode() == msgData.getUnionCountryCode()) {
				country = player.getCountry();
				break;
			}
		}
		return country;
	}
	private UnionDealData getUnionDealData() {
		List<PlayerEntity> players = getActivity().getGameController().getLevel().getPlayers();
		return new UnionDealData(msgData.getUnionCountryCode(), msgData.getCurrentCountryCode(), players);
	}
	private boolean isUnionPosible() {
		return getActivity().getGameController().checkSuggestUnion(getUnionDealData());
	}
	@Override
	protected void updateParrent() {
		msgData.getAdapter().notifyDataSetChanged();
	}
}
