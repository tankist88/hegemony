package ru.hegemony.android.activity.popup;

import java.util.List;

import ru.hegemony.android.activity.R;
import ru.hegemony.android.activity.popup.data.BaseData;
import ru.hegemony.android.activity.popup.data.MsgData;
import ru.hegemony.logic.data.UnionDealData;
import ru.hegemony.xml.data.PlayerEntity;

public class UnionPopup extends DetailPopup {
	private MsgData unionData;
	
	protected UnionPopup(BaseData data) throws ClassCastException {
		super(data);
		if(data instanceof MsgData) {
			this.unionData = (MsgData) data;
		} else {
			throw new ClassCastException("Illegal type of input data for class " + UnionPopup.class + ". Data must be only " + MsgData.class + " type");
		}
	}
	@Override
	protected String getText() {
		if(unionData.isUnionBreak()) {
			return getActivity().getResources().getString(R.string.player_break_union_text);
		} else {
			boolean unionConfirmed = getActivity().getGameController().suggestUnion(getUnionDealData(), false);
			if(unionConfirmed) {
				return getActivity().getResources().getString(R.string.union_confirmed_text);
			} else {
				return getActivity().getResources().getString(R.string.union_not_confirmed_text);
			}
		}
	}
	@Override
	protected void doConfirm() {
		// do nothing
	}
	@Override
	protected void doCancel() {
		// do nothing
	}
	@Override
	protected boolean isTwoButtons() {
		return false;
	}
	private UnionDealData getUnionDealData() {
		List<PlayerEntity> players = getActivity().getGameController().getLevel().getPlayers();
		return new UnionDealData(unionData.getCurrentCountryCode(), unionData.getUnionCountryCode(), players);
	}
	@Override
	protected void updateParrent() {
		unionData.getAdapter().notifyDataSetChanged();
	}
}
