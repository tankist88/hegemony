package ru.hegemony.android.activity.popup;

import android.content.Context;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import ru.hegemony.android.activity.R;
import ru.hegemony.android.activity.SuggestionActivity;
import ru.hegemony.android.activity.ui.ImageGridAdapter;
import ru.hegemony.android.activity.ui.helper.SpinnerHelper;
import ru.hegemony.logic.helper.ItemHelper;
import ru.hegemony.xml.data.ItemActionEntity;
import ru.hegemony.xml.data.ItemEntity;

public class AddItemPopup {
	private AddItemPopup() {
	}
	
	public static AddItemPopup getInstance() {
		return new AddItemPopup();
	}
	
	/**
	 * ����� ������������ ���� ��� ������ ������� �������� ��� ��� ���������� � �����������
	 * @param selectedItem - ��������� �������
	 */
	public void show(final ItemEntity selectedItem, final SuggestionActivity activity) {
		LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View layout = inflater.inflate(R.layout.popup_item_select, (ViewGroup) activity.findViewById(R.id.item_popup_element));
		final PopupWindow popupWnd = new PopupWindow(layout, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
		popupWnd.showAtLocation(layout, Gravity.CENTER, 0, 0);
		
		TextView itemName = (TextView) layout.findViewById(R.id.popup_item_name);
		itemName.setText(selectedItem.getName());
		
		Spinner actionSpinner = (Spinner) layout.findViewById(R.id.popup_item_action);
		actionSpinner.setAdapter(SpinnerHelper.getItemActionSpinnerAdapter(activity, selectedItem));
		actionSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				try {
					ItemActionEntity selectedAction = (ItemActionEntity) parent.getSelectedItem();
					selectedItem.setSa(selectedAction.getCode());
				} catch (Exception e) {
					Log.e(this.getClass().toString(), e.getMessage(), e);
				}
			}
			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// do nothing
			}
		});
		
		Button descBtn = (Button) layout.findViewById(R.id.desc_button);
		descBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				popupWnd.dismiss();
				showDesc(selectedItem, activity);
			}
		});
		
		Button addItemBtn = (Button) layout.findViewById(R.id.confirm_button);
		addItemBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				popupWnd.dismiss();
				activity.addItemToSelected(selectedItem);
				activity.getSuggestionItems().setAdapter(new ImageGridAdapter<ItemEntity>(activity, activity.getSelectedItems()));
	            activity.updateItemSetEffect();
				Toast.makeText(
						activity,
						"������� \"" + selectedItem.getName() + " ("
								+ ItemHelper.getItemActionByCode(selectedItem.getSa(), selectedItem.getActionList()).getName()
								+ ") " + "\" ��������", Toast.LENGTH_SHORT).show();
			}
		});
		
		Button cancelBtn = (Button) layout.findViewById(R.id.cancel_button);
		cancelBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// ������ ��������� ����������� ����
				popupWnd.dismiss();
			}
		});
	}
	
	private void showDesc(final ItemEntity selectedItem, final SuggestionActivity activity) {
		LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View layout = inflater.inflate(R.layout.popup_item_desc, (ViewGroup) activity.findViewById(R.id.item_desc_element));
		final PopupWindow popupWnd = new PopupWindow(layout, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
		popupWnd.showAtLocation(layout, Gravity.CENTER, 0, 0);
		
		TextView descText = (TextView) layout.findViewById(R.id.desc_text);
		descText.setMovementMethod(new ScrollingMovementMethod());
		descText.setText(selectedItem.getDesc());
		
		Button cancelBtn = (Button) layout.findViewById(R.id.back_button);
		cancelBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				popupWnd.dismiss();
				show(selectedItem, activity);
			}
		});
	}
}
