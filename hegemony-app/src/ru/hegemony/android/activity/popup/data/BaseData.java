package ru.hegemony.android.activity.popup.data;

import ru.hegemony.android.activity.BaseGameActivity;

public class BaseData {
	private BaseGameActivity activity;
	private String title;

	public BaseData(BaseGameActivity activity, String title) {
		this.activity = activity;
		this.title = title;
	}

	public BaseGameActivity getActivity() {
		return activity;
	}

	public void setActivity(BaseGameActivity activity) {
		this.activity = activity;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
}
