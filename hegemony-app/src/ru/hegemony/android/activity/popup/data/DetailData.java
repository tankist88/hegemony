package ru.hegemony.android.activity.popup.data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ru.hegemony.android.activity.BaseGameActivity;
import ru.hegemony.android.activity.ui.DealListAdapter;
import ru.hegemony.xml.data.ItemEntity;
import android.annotation.SuppressLint;

public class DetailData extends DealAdapterData {
	private List<ItemEntity> itemSet;
	@SuppressLint("UseSparseArrays")
	private Map<Integer, Boolean> playerAnswers = new HashMap<Integer, Boolean>();
	
	public DetailData(BaseGameActivity activity, String title, DealListAdapter adapter, List<ItemEntity> itemSet, Map<Integer, Boolean> playerAnswers) {
		super(activity, title, adapter);
		this.itemSet = itemSet;
		this.playerAnswers = playerAnswers;
	}
	public List<ItemEntity> getItemSet() {
		return itemSet;
	}
	public void setItemSet(List<ItemEntity> itemSet) {
		this.itemSet = itemSet;
	}
	public Map<Integer, Boolean> getPlayerAnswers() {
		return playerAnswers;
	}
	public void setPlayerAnswers(Map<Integer, Boolean> playerAnswers) {
		this.playerAnswers = playerAnswers;
	}
}
