package ru.hegemony.android.activity.popup.data;

public enum PopupType {
	DETAIL_INFO,
	UNION_SUGGEST,
	UNION_ANSWER;
}
