package ru.hegemony.android.activity.popup.data;

import ru.hegemony.android.activity.BaseGameActivity;
import ru.hegemony.android.activity.ui.DealListAdapter;

public class MsgData extends DealAdapterData {
	/** ������ ��, ������������ ���� */
	private int unionCountryCode;
	/** ������ ������ ��������*/
	private int currentCountryCode;
	/** ��� �� ���� ���������� */
	private boolean isUnionBreak;
	
	public MsgData(BaseGameActivity activity, String title, DealListAdapter adapter, int unionCountryCode, int currentCountryCode, boolean isUnionBreak) {
		super(activity, title, adapter);
		this.unionCountryCode = unionCountryCode;
		this.currentCountryCode = currentCountryCode;
		this.isUnionBreak = isUnionBreak;
	}
	public int getUnionCountryCode() {
		return unionCountryCode;
	}
	public void setUnionCountryCode(int unionCountryCode) {
		this.unionCountryCode = unionCountryCode;
	}
	public int getCurrentCountryCode() {
		return currentCountryCode;
	}
	public void setCurrentCountryCode(int currentCountryCode) {
		this.currentCountryCode = currentCountryCode;
	}
	public boolean isUnionBreak() {
		return isUnionBreak;
	}
	public void setUnionBreak(boolean isUnionBreak) {
		this.isUnionBreak = isUnionBreak;
	}
}
