package ru.hegemony.android.activity.popup.data;

import ru.hegemony.android.activity.BaseGameActivity;
import ru.hegemony.android.activity.ui.DealListAdapter;

public class DealAdapterData extends BaseData {
	private DealListAdapter adapter;

	public DealAdapterData(BaseGameActivity activity, String title, DealListAdapter adapter) {
		super(activity, title);
		this.adapter = adapter;
	}

	public DealListAdapter getAdapter() {
		return adapter;
	}

	public void setAdapter(DealListAdapter adapter) {
		this.adapter = adapter;
	}
}
