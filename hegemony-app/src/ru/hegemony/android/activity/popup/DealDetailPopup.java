package ru.hegemony.android.activity.popup;

import java.util.List;

import ru.hegemony.android.activity.popup.data.BaseData;
import ru.hegemony.android.activity.popup.data.DetailData;
import ru.hegemony.logic.helper.ItemHelper;
import ru.hegemony.xml.data.ItemActionEntity;
import ru.hegemony.xml.data.ItemEntity;

public class DealDetailPopup extends DetailPopup {
	private DetailData detailData;
	
	protected DealDetailPopup(BaseData data) throws ClassCastException {
		super(data);
		if(data instanceof DetailData) {
			this.detailData = (DetailData) data;
		} else {
			throw new ClassCastException("Illegal type of input data for class " + DealDetailPopup.class + ". Data must be only " + DetailData.class + " type");
		}
	}
	@Override
	protected String getText() {
		List<ItemEntity> itemList = detailData.getItemSet();
		StringBuffer text = new StringBuffer();
		for(ItemEntity item : itemList) {
			ItemActionEntity action = ItemHelper.getItemActionByCode(item.getSa(), item.getActionList());
			text.append(action.getName() + " " + item.getName());
			text.append(", ");
		}
		text.delete(text.length() - 2, text.length() - 1);
		return text.toString();
	}
	@Override
	protected void doConfirm() {
		detailData.getPlayerAnswers().put(ItemHelper.getItemSetHashCode(detailData.getItemSet()), true);
	}
	@Override
	protected void doCancel() {
		detailData.getPlayerAnswers().put(ItemHelper.getItemSetHashCode(detailData.getItemSet()), false);
	}
	@Override
	protected boolean isTwoButtons() {
		return true;
	}
	@Override
	protected void updateParrent() {
		detailData.getAdapter().notifyDataSetChanged();
	}
}
