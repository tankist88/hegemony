package ru.hegemony.android.activity;

import java.io.Serializable;

import ru.hegemony.android.exception.IllegalGameStateException;
import ru.hegemony.android.info.GameActivityInfo;
import ru.hegemony.logic.action.GameController;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public abstract class BaseGameActivity extends Activity implements GameActivityInfo {
	private static final String GAME_CONTROLLER_TAG = "GameControllerTag";
	
	/** �������� ������ ������� ��������� */
	private GameController gameController;
	/** ������ �������� �� ��������� �������� */
	private Button nextButton = null;
	private TextView titleText;

	@Override
	public GameController getGameController() {
		return gameController;
	}

	@Override
	public void setGameController(GameController gameController) {
		this.gameController = gameController;
	}
	
	private void initGameControllerFromIntent() {
		gameController = (GameController) getIntent().getSerializableExtra(GAME_CONTROLLER_TAG);
		if(gameController == null && !(this instanceof SettingsActivity)) {
    		// ���� �� ����� ���������� ����, �� ����� ��� ������ ����������������
    		Log.w(getClass().toString(), "Unable to find game controller. Start SettingsActivity to set it.");
    		goToActivity(SettingsActivity.class);
    	}
	}
	
	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		initGameControllerFromIntent();
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        setContentView(getContentViewId());

		initGameControllerFromIntent();

		nextButton = (Button) findViewById(R.id.next_button);
		if (nextButton != null) {
			nextButton.setOnClickListener(new NextButtonOnclickListener());
		}
		
		titleText = (TextView) findViewById(R.id.title_text_view);
	}
	
	protected void setNextButtonText(int stringId) {
		nextButton.setText(getResources().getString(stringId));
	}
	
	protected void setNextButtonEnabled(boolean enabled) {
		nextButton.setEnabled(enabled);
	}
	
	protected TextView getTitleTextView() {
		return titleText;
	}
	
	@Override
    protected void onSaveInstanceState(Bundle outState) {
    	super.onSaveInstanceState(outState);
    	outState.putSerializable(GAME_CONTROLLER_TAG, gameController);
    }
	
	@Override
	public void onBackPressed() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    builder.setMessage(getResources().getString(R.string.back_question))
	           .setCancelable(false)
	           .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
	               public void onClick(DialogInterface dialog, int id) {
						Log.w(getClass().toString(), "Back pressed. Start SettingsActivity.");
						goToActivity(SettingsActivity.class);
	               }
	           })
	           .setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
	               public void onClick(DialogInterface dialog, int id) {
	                    dialog.cancel();
	               }
	           });
	    AlertDialog alert = builder.create();
	    alert.show();
	}
	
	/**
	 * ������� �� �������� ���������� (Activity)
	 * @param clazz - ����� ����������, �� ������� ����� �������������
	 */
	protected void goToActivity(Class<? extends BaseGameActivity> clazz) {
		if(clazz != null) {
			Intent intent = new Intent(this, clazz);
			goToActivity(clazz, intent);
		}
	}

	/**
	 * ������� �� �������� ���������� (Activity)
	 * @param clazz - ����� ����������, �� ������� ����� �������������
	 * @param extraName - ��� �������������� ���������, ������� ����� �������� � ����������
	 * @param extraValue - �������� �������������� ���������, ������� ����� �������� � ����������
	 */
	public void goToActivity(Class<? extends BaseGameActivity> clazz, String extraName, Serializable extraValue) {
		if(clazz != null) {
			Intent intent = new Intent(this, clazz);
			if(extraName != null && extraName.length() > 0 && extraValue != null) {
				intent.putExtra(extraName, extraValue);
			}
			goToActivity(clazz, intent);
		}
	}
	
	private void goToActivity(Class<? extends BaseGameActivity> clazz, Intent intent) {
		if(clazz != null) {
			intent.putExtra(GAME_CONTROLLER_TAG, gameController);
			startActivity(intent);
		}
	}
	
	/**
	 * ����� ���������� (Activity) �� ������� ����� ����������� ������� ��� ������� ������ "�����" (next)
	 * @return ����� ����������
	 */
	protected abstract Class<? extends BaseGameActivity> getDefaultNextActivity();
	/**
	 * ��������, ������� ����� ��������� ��������������� ����� ��������� �� ��������� ���������� 
	 * �� ������� ������ "�����" (next)
	 * @throws Exception
	 */
	protected abstract void doBeforeNext() throws IllegalGameStateException, Exception;
    /** ������������� layout'� ���������� (activity) */
    protected abstract int getContentViewId();
	
	private class NextButtonOnclickListener implements View.OnClickListener {
		@Override
		public void onClick(View view) {
			try {
				doBeforeNext();
				goToActivity(getDefaultNextActivity());
			} catch(IllegalGameStateException iex) {
				Toast.makeText(BaseGameActivity.this, iex.getMessage(), Toast.LENGTH_SHORT).show();
			} catch(Exception ex) {
				Log.e(this.getClass().getName(), ex.getMessage(), ex);
			}
		}
	}
}
