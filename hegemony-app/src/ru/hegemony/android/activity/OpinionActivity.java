package ru.hegemony.android.activity;

import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ru.hegemony.android.activity.popup.SpyAmountPopup;
import ru.hegemony.android.activity.ui.ImageGridAdapter;
import ru.hegemony.android.data.ItemListHolder;
import ru.hegemony.android.data.ResourceType;
import ru.hegemony.android.exception.IllegalGameStateException;
import ru.hegemony.android.helper.ResourceHelper;
import ru.hegemony.xml.data.CountryEntity;
import ru.hegemony.xml.data.ItemEntity;

public class OpinionActivity extends BaseGameActivity {
	public static final String ITEM_LIST_HOLDER = "ItemListHolder";
	/**
	 * ����� ������ ��������� ������ ������, � ������� ����� ����������
	 */
	public static final String CONFIRMED_ITEM_SET = "confirmedItemSet";
	
	private List<ItemEntity> confirmedItems = new ArrayList<ItemEntity>();
    private ImageButton changeGridButton;
    private CountryEntity selectedCountry;
    private boolean optShown;
	
	@Override
	protected Class<? extends BaseGameActivity> getDefaultNextActivity() {
		return SuggestionActivity.class;
	}

	@Override
	protected void doBeforeNext() throws IllegalGameStateException, Exception {
		// do nothing
	}

	@Override
	protected int getContentViewId() {
		return R.layout.activity_opinion;
	}

    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		ItemListHolder holder = (ItemListHolder) getIntent().getSerializableExtra(ITEM_LIST_HOLDER);
		if(holder == null) {
    		Log.w(getClass().toString(), "Unable to find item list holder for opinion. Start SuggestionActivity to set it.");
    		goToActivity(SuggestionActivity.class);
    	}

		selectedCountry = holder.getSelectedCountry();
		
		if(selectedCountry != null) {
            changeGridButton = (ImageButton) findViewById(R.id.change_grid_button);

            Button confirmButton = (Button) findViewById(R.id.confirm_button);
            Button spyButton = (Button) findViewById(R.id.spy_button);

			final GridView dealGrid = (GridView) findViewById(R.id.country_deal);
            final TextView dealCountryName = (TextView) findViewById(R.id.deal_country_name);
            final TextView playerInfText = (TextView) findViewById(R.id.player_inf_text);
            final TextView countryInfText = (TextView) findViewById(R.id.country_inf_text);
            final TextView opinionText = (TextView) findViewById(R.id.opinion_text);

            playerInfText.setText(R.string.inf_empty_text);
            countryInfText.setText(R.string.inf_empty_text);

			dealCountryName.setText(selectedCountry.getName());
			dealCountryName.setCompoundDrawablesWithIntrinsicBounds(
					ResourceHelper.getResourceId(ResourceType.DRAWABLE, selectedCountry.getImage(), this.getResources()), 0, 0, 0);

			try {
				String bisPrefix = getResources().getString(R.string.bisPrefix);
                opinionText.setText(getGameController().getCountryOpinion(selectedCountry, holder.getItemList(), bisPrefix));
			} catch (Exception e) {
				throw new IllegalStateException(e);
			}
			opinionText.setMovementMethod(new ScrollingMovementMethod());
			
			List<ItemEntity> itemSet = getGameController().getItemSetForSendDeal(selectedCountry);
			dealGrid.setAdapter(new ImageGridAdapter<ItemEntity>(this, itemSet));
			dealGrid.setOnItemClickListener(new OnDealItemsClickListener(itemSet, dealGrid, playerInfText, countryInfText, selectedCountry));

            refreshSpyState(false);

            changeGridButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    confirmedItems = new ArrayList<ItemEntity>();
                    List<ItemEntity> itemSet;
                    if(optShown) {
                        itemSet = getGameController().getItemSetForSendDeal(selectedCountry);
                        optShown = false;
                    } else {
                        itemSet = getGameController().getCurrentOptimalItemSet(selectedCountry);
                        optShown = true;
                    }
                    dealGrid.setAdapter(new ImageGridAdapter<ItemEntity>(OpinionActivity.this, itemSet));
                    dealGrid.setOnItemClickListener(new OnDealItemsClickListener(itemSet, dealGrid, playerInfText, countryInfText, selectedCountry));
                }
            });
            spyButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    SpyAmountPopup.getInstance().show(selectedCountry, OpinionActivity.this);
                }
            });
			confirmButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					ItemListHolder holder = new ItemListHolder();
					holder.setSelectedCountry(selectedCountry);
					holder.setItemList(confirmedItems);
					goToActivity(SuggestionActivity.class, CONFIRMED_ITEM_SET, holder);
				}
			});
		}
	}

    public void refreshSpyState(boolean showToast) {
        final CountryEntity initiatorCountry = getGameController().getLevel().getHumanPlayer().getCountry();
        int toastRes;
        if(getGameController().spySuccess(initiatorCountry, selectedCountry)) {
            changeGridButton.setEnabled(true);
            toastRes = R.string.spy_success;
        } else {
            changeGridButton.setEnabled(false);
            toastRes = R.string.spy_fail;
        }
        if(showToast) {
            Toast.makeText(this, toastRes, Toast.LENGTH_LONG).show();
        }
    }
	
	private class OnDealItemsClickListener implements OnItemClickListener {
		private List<ItemEntity> itemSet;
		private GridView dealGrid;
		private TextView playerInfText;
		private TextView countryInfText;
		private CountryEntity selectedCountry;
		public OnDealItemsClickListener(List<ItemEntity> itemSet, GridView dealGrid, TextView playerInfText, TextView countryInfText, CountryEntity selectedCountry) {
			this.itemSet = itemSet;
			this.dealGrid = dealGrid;
			this.playerInfText = playerInfText;
			this.countryInfText = countryInfText;
			this.selectedCountry = selectedCountry;
		}
		@Override
		public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
			Object[] itemArray = this.itemSet.toArray();
			if (itemArray.length > position) {
				ItemEntity selectedItem = ((ItemEntity) (itemArray[position]));
				if(confirmedItems.contains(selectedItem)) {
					confirmedItems.remove(selectedItem);
				} else{
					confirmedItems.add(selectedItem);
				}
				this.dealGrid.setAdapter(new ImageGridAdapter<ItemEntity>(OpinionActivity.this, this.itemSet, confirmedItems));
				this.playerInfText.setText(R.string.inf_empty_text);
                this.playerInfText.setTextColor(getResources().getColor(R.color.default_text_color));
				this.countryInfText.setText(R.string.inf_empty_text);
                this.countryInfText.setTextColor(getResources().getColor(R.color.default_text_color));
				if(confirmedItems.size() > 0) {
					CountryEntity playerCountry = getGameController().getLevel().getHumanPlayer().getCountry();
					fillTextView(playerInfText, playerCountry, R.string.player_inf_text);
					fillTextView(countryInfText, this.selectedCountry, R.string.country_inf_text);
				}
			}
		}
		private void fillTextView(TextView view, CountryEntity country, int startTextId) {
			double avrResDelta = getGameController().getItemSetEffect(country, confirmedItems);
	        view.setText(getResources().getString(startTextId) + " " + Double.toString(avrResDelta));
	        if(avrResDelta >= 0.0) {
	        	view.setTextColor(getResources().getColor(R.color.deal_confirmed_color));
	        } else {
	        	view.setTextColor(getResources().getColor(R.color.deal_canceled_color));
	        }
		}
	}
}
