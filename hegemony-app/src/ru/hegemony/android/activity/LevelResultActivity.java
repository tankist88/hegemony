package ru.hegemony.android.activity;

import java.io.File;
import java.io.FileOutputStream;

import ru.hegemony.android.exception.IllegalGameStateException;
import ru.hegemony.common.helper.DataHelper;
import ru.hegemony.xml.data.CountryEntity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class LevelResultActivity extends BaseGameActivity {
	public static final String CACHE_DIR = "/Android/data/ru.hegemony.android.activity/cache/";
	public static final String CACHE_FILE = "playersState.xml";

	@Override
	protected int getContentViewId() {
		return R.layout.activity_level_result;
	}
	
	@Override
	protected Class<? extends BaseGameActivity> getDefaultNextActivity() {
		return SettingsActivity.class;
	}

	@Override
	protected void doBeforeNext() throws IllegalGameStateException, Exception {
		// do nothing
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		TextView titleText = getTitleTextView();
		TextView levelStatText = (TextView) findViewById(R.id.level_res_text);
		
		CountryEntity winner = getGameController().getWinner();
		if(winner != null) {
			titleText.setText(getResources().getString(R.string.victory_title_text));
			levelStatText.setText(
					getResources().getString(R.string.has_win_text)
					+ " "
					+ "\"" + winner.getName() + "\""
					+ " \n\n"
					+ getResources().getString(R.string.bonus_text)
					+ ": "
					+ DataHelper.round(getGameController().getBonusForWinner(), 3));
		} else {
			titleText.setText(getResources().getString(R.string.lost_title_text));
			levelStatText.setText(getResources().getString(R.string.do_not_have_win_text));
		}
		writeCache(getGameController().getPlayersState());
	}
	
	private void writeCache(String cacheData) {
		String sdState = android.os.Environment.getExternalStorageState();
		if (sdState.equals(android.os.Environment.MEDIA_MOUNTED)) {
			File sdDir = android.os.Environment.getExternalStorageDirectory();
			File fileDir = new File(sdDir.getAbsolutePath(), CACHE_DIR);
			if (!fileDir.exists())
				fileDir.mkdirs();
			try {
				File fileToWrite = new File(fileDir, CACHE_FILE);
				FileOutputStream f = new FileOutputStream(fileToWrite);
				f.write(cacheData.getBytes());
				f.flush();
				f.close();
			} catch (Exception e) {
				Log.w(getClass().toString(), "Unable to write cache. " + e.getMessage());
			}
		}
	}
}
