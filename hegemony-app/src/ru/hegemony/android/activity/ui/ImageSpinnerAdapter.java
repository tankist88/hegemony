package ru.hegemony.android.activity.ui;

import java.util.List;

import ru.hegemony.android.activity.R;
import ru.hegemony.android.data.ResourceType;
import ru.hegemony.android.helper.ResourceHelper;
import ru.hegemony.xml.data.ImageEntity;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ImageSpinnerAdapter<T extends ImageEntity> extends ArrayAdapter<T> {
	/** ������ ��� �������� ����������� ������ */
	private static final int SPINNER_ROW_LAYOUT = R.layout.row_spinner_image;
	
	/** ������ ������ �������� ��� ����������� ������ */
	private List<T> data;
	/** ������ �� ������� ���������� ��� ��������� ID �������� ��� ������� �������� ������ */
	private Resources res;
	/** Inflater ��� ��������� ������ �� ������ ��� ������� � ����������� ������� (�������� � �����) */
	private LayoutInflater inflater;

	/**
	 * �������� �������� ��� ����������� ����������� ������ � ��������� � �������
	 * @param activity - ����������, ��� ����� ��������� ���������� ������
	 * @param objects - ������ ������ ��������� ����������� ������
	 */
	public ImageSpinnerAdapter(Activity activity, List<T> objects) {
		super(activity, SPINNER_ROW_LAYOUT, objects);
		this.data = objects;
		this.res = activity.getResources();
		this.inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		return getCustomView(position, parent);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return getCustomView(position, parent);
	}

	// ����� ���������� ��� ������ ������ � ���������� ������
	public View getCustomView(int position, ViewGroup parent) {
		View row = inflater.inflate(SPINNER_ROW_LAYOUT, parent, false);

		ImageEntity currentPositionValue = data.get(position);

		ImageView image = (ImageView) row.findViewById(R.id.image);
		TextView name = (TextView) row.findViewById(R.id.item_name);

		name.setText(currentPositionValue.getName());
		image.setImageResource(ResourceHelper.getResourceId(ResourceType.DRAWABLE, currentPositionValue.getImage(), res));
		
		return row;
	}
}
