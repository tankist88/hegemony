package ru.hegemony.android.activity.ui;

import java.util.List;

import ru.hegemony.android.activity.R;
import ru.hegemony.android.data.ResourceType;
import ru.hegemony.android.helper.ResourceHelper;
import ru.hegemony.logic.data.TurnStatusForCountry;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ResultListAdapter extends ArrayAdapter<TurnStatusForCountry> {
	/** ������ ��� �������� ������ ����������� */
	private static final int RESULT_ROW_LAYOUT = R.layout.row_list_result;
	
	/** �������� ������ */
	private List<TurnStatusForCountry> data;
	/** Inflater ��� ��������� ������ �� ������ ��� ������� � ����������� ������� */
	private LayoutInflater inflater;
	/** ������ �� ������� ���������� */
	private Resources res;

	public ResultListAdapter(Context context, List<TurnStatusForCountry> objects) {
		super(context, RESULT_ROW_LAYOUT, objects);
		this.data = objects;
		this.res = context.getResources();
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		return getCustomView(position, parent);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return getCustomView(position, parent);
	}

	// ����� ���������� ��� ������ ������ � ������
	public View getCustomView(int position, ViewGroup parent) {
		View row = inflater.inflate(RESULT_ROW_LAYOUT, parent, false);

		TurnStatusForCountry currentValue = data.get(position);

		ImageView image = (ImageView) row.findViewById(R.id.country_image);
		TextView countryName = (TextView) row.findViewById(R.id.country_name);
		TextView dealResult = (TextView) row.findViewById(R.id.deal_result);
		
		image.setImageResource(ResourceHelper.getResourceId(ResourceType.DRAWABLE, currentValue.getCountry().getImage(), res));
		countryName.setText(currentValue.getCountry().getName());
		
		if(currentValue.isConfirmed()) {
			dealResult.setTextColor(res.getColor(R.color.deal_confirmed_color));
			dealResult.setText(res.getString(R.string.deal_confirm));
		} else {
			dealResult.setTextColor(res.getColor(R.color.deal_canceled_color));
			dealResult.setText(res.getString(R.string.deal_cancel));
		}
		
		return row;
	}
}
