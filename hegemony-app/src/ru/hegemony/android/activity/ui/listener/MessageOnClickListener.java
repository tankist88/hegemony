package ru.hegemony.android.activity.ui.listener;

import java.util.Map;

import ru.hegemony.android.activity.BaseGameActivity;
import ru.hegemony.android.activity.R;
import ru.hegemony.android.activity.popup.DetailPopup;
import ru.hegemony.android.activity.popup.data.MsgData;
import ru.hegemony.android.activity.popup.data.PopupType;
import ru.hegemony.android.activity.ui.DealListAdapter;
import ru.hegemony.xml.data.CountryEntity;
import android.view.View;
import android.view.View.OnClickListener;

public class MessageOnClickListener implements OnClickListener {
	private CountryEntity selectedCountry;
	private CountryEntity playerCountry;
	private BaseGameActivity activity;
	private DealListAdapter adapter;
	private boolean isUnionBreak;
	private Map<Integer, Boolean> msgReadMap;
	
	public MessageOnClickListener(BaseGameActivity activity, DealListAdapter adapter, CountryEntity selectedCountry, CountryEntity playerCountry, boolean isUnionBreak, Map<Integer, Boolean> msgReadMap) {
		this.playerCountry = playerCountry;
		this.selectedCountry = selectedCountry;
		this.activity = activity;
		this.adapter = adapter;
		this.isUnionBreak = isUnionBreak;
		this.msgReadMap = msgReadMap;
	}
	@Override
	public void onClick(View v) {
		msgReadMap.put(selectedCountry.getCode(), true);
		String title = activity.getResources().getString(R.string.deal_msg_popup_title);
		MsgData msgData = new MsgData(activity, title, adapter, this.selectedCountry.getCode(), this.playerCountry.getCode(), isUnionBreak);
		DetailPopup.getInstance(PopupType.UNION_SUGGEST, msgData).initPopup();
	}
}
