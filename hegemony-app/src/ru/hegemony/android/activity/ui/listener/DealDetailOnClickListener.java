package ru.hegemony.android.activity.ui.listener;

import java.util.List;
import java.util.Map;

import ru.hegemony.android.activity.BaseGameActivity;
import ru.hegemony.android.activity.R;
import ru.hegemony.android.activity.popup.DetailPopup;
import ru.hegemony.android.activity.popup.data.DetailData;
import ru.hegemony.android.activity.popup.data.PopupType;
import ru.hegemony.android.activity.ui.DealListAdapter;
import ru.hegemony.xml.data.ItemEntity;
import android.annotation.SuppressLint;
import android.view.View;
import android.view.View.OnClickListener;

public class DealDetailOnClickListener implements OnClickListener {
	private List<ItemEntity> itemSet;
	private BaseGameActivity activity;
	private DealListAdapter adapter;
	@SuppressLint("UseSparseArrays")
	private Map<Integer, Boolean> playerAnswers;
	
	public DealDetailOnClickListener(BaseGameActivity activity, DealListAdapter adapter, List<ItemEntity> itemSet, Map<Integer, Boolean> playerAnswers) {
		this.itemSet = itemSet;
		this.activity = activity;
		this.adapter = adapter;
		this.playerAnswers = playerAnswers;
	}
	@Override
	public void onClick(View v) {
		String title = activity.getResources().getString(R.string.deal_detail_popup_title);
		DetailData dd = new DetailData(activity, title, adapter, this.itemSet, playerAnswers);
		DetailPopup.getInstance(PopupType.DETAIL_INFO, dd).initPopup();
	}		
}
