package ru.hegemony.android.activity.ui.listener;

import ru.hegemony.android.activity.BaseGameActivity;
import ru.hegemony.android.activity.R;
import ru.hegemony.android.activity.popup.DetailPopup;
import ru.hegemony.android.activity.popup.data.MsgData;
import ru.hegemony.android.activity.popup.data.PopupType;
import ru.hegemony.android.activity.ui.DealListAdapter;
import ru.hegemony.logic.data.UnionDealData;
import ru.hegemony.xml.data.CountryEntity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.View;
import android.view.View.OnClickListener;

public class UnionOnClickListener implements OnClickListener {
	private CountryEntity selectedCountry;
	private CountryEntity playerCountry;
	private BaseGameActivity activity;
	private DealListAdapter adapter;
	
	public UnionOnClickListener(BaseGameActivity activity, DealListAdapter adapter, CountryEntity selectedCountry, CountryEntity playerCountry) {
		this.playerCountry = playerCountry;
		this.selectedCountry = selectedCountry;
		this.activity = activity;
		this.adapter = adapter;
	}
	@Override
	public void onClick(View v) {
		final boolean countriesInUnion = activity.getGameController().countryInUnion(this.playerCountry, this.selectedCountry);
		
		String question = null;
		if(countriesInUnion) {
			question = activity.getResources().getString(R.string.union_question_text2);
		} else {
			question = activity.getResources().getString(R.string.union_question_text1);
		}
		question += " \"" + this.selectedCountry.getName() + "\"?";
		
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setMessage(question)
				.setCancelable(false)
				.setPositiveButton(activity.getResources().getString(R.string.yes),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								boolean isUnionBreak = false;
								if(countriesInUnion) {
									isUnionBreak = true;
									activity.getGameController().forceBreakTheUnion(
											new UnionDealData(
												playerCountry.getCode(),
												selectedCountry.getCode(),  
												activity.getGameController().getLevel().getPlayers()));
								} else {
									isUnionBreak = false;
								}
								String title = activity.getResources().getString(R.string.deal_union_popup_title);
								MsgData msgData = new MsgData(activity, title, adapter, selectedCountry.getCode(), playerCountry.getCode(), isUnionBreak);
								DetailPopup.getInstance(PopupType.UNION_ANSWER,msgData).initPopup();
							}
						})
				.setNegativeButton(activity.getResources().getString(R.string.no),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});
		AlertDialog alert = builder.create();
		alert.show();
	}
}
