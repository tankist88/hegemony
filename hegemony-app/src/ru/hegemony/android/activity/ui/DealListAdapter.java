package ru.hegemony.android.activity.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ru.hegemony.android.activity.BaseGameActivity;
import ru.hegemony.android.activity.R;
import ru.hegemony.android.activity.ui.listener.DealDetailOnClickListener;
import ru.hegemony.android.activity.ui.listener.MessageOnClickListener;
import ru.hegemony.android.activity.ui.listener.UnionOnClickListener;
import ru.hegemony.android.data.ResourceType;
import ru.hegemony.android.helper.ResourceHelper;
import ru.hegemony.common.random.UnionColorHelper;
import ru.hegemony.logic.action.GameController;
import ru.hegemony.logic.data.UnionDealData;
import ru.hegemony.logic.data.UnionInfo;
import ru.hegemony.logic.helper.ItemHelper;
import ru.hegemony.xml.data.CountryEntity;
import ru.hegemony.xml.data.ItemEntity;
import ru.hegemony.xml.data.PlayerEntity;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class DealListAdapter extends ArrayAdapter<CountryEntity> {
	/** ������ ��� �������� ������ ����������� */
	private static final int DEAL_ROW_LAYOUT = R.layout.row_list_deal;
	
	/** ������ ������ �����, �� ����������� ������, �� ������� ������ �����*/
	private List<CountryEntity> countryListWithoutPlayer;
	/** ������ �� ������� ���������� ��� ��������� ID �������� � �. �. ��� ������� �������� ������ */
	private Resources res;
	/** Inflater ��� ��������� ������ �� ������ ��� ������� � ����������� ������� */
	private LayoutInflater inflater;
	/** ������������ activity */
	private BaseGameActivity activity;
	// ��������� Map, �.�. ������ � ���������� � GameController
	@SuppressLint("UseSparseArrays")
	private Map<Integer, Boolean> playerAnswers;
	@SuppressLint("UseSparseArrays")
	private Map<Integer, Boolean> msgReadMap;
	private Map<CountryEntity, CachedItemSetData> itemSetMap;
	private Map<CountryEntity, Boolean> breakUnionMap;

	/**
	 * �������� �������� ��� ����������� ������ ����������� �����. 
	 * ����� ������ ���� ����������� � ����� �������������, ���� ���
	 * @param activity - ����������, ��� ����� ��������� ������
	 */
	@SuppressLint("UseSparseArrays")
	public DealListAdapter(BaseGameActivity activity) {
		super(activity, DEAL_ROW_LAYOUT, getCountries(activity.getGameController()));
		this.activity = activity;
		this.countryListWithoutPlayer = getCountries(activity.getGameController());
		this.res = activity.getResources();
		this.inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.playerAnswers = new HashMap<Integer, Boolean>();
		this.msgReadMap = new HashMap<Integer, Boolean>();
		this.itemSetMap = new HashMap<CountryEntity, CachedItemSetData>();
		this.breakUnionMap = new HashMap<CountryEntity, Boolean>();
		initMaps();
	}
	
	private static List<CountryEntity> getCountries(GameController gameController) {
		List<CountryEntity> countries = new ArrayList<CountryEntity>();
		int playerCountryCode = gameController.getLevel().getHumanPlayer().getCountry().getCode();
		for(PlayerEntity player : gameController.getLevel().getPlayers()) {
			if(player.getCountry().getCode() != playerCountryCode) {
				countries.add(player.getCountry());
			}
		}
		return countries;
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		return getCustomView(position, parent);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return getCustomView(position, parent);
	}

	// ����� ���������� ��� ������ ������ � ������
	public View getCustomView(int position, ViewGroup parent) {
		View row = this.inflater.inflate(DEAL_ROW_LAYOUT, parent, false);

		final ImageView image = (ImageView) row.findViewById(R.id.country_image);
		final ImageView dealConfirmImage = (ImageView) row.findViewById(R.id.confirm_status_img);
		final GridView dealGrid = (GridView) row.findViewById(R.id.country_deal);
		final ImageButton dealDetailButton = (ImageButton) row.findViewById(R.id.deal_detail_button);
		final ImageButton msgButton = (ImageButton) row.findViewById(R.id.msg_button);
		final ImageButton unionButton = (ImageButton) row.findViewById(R.id.union_button);
		final TextView countryName = (TextView) row.findViewById(R.id.deal_country_name);
		final TextView sugValueText = (TextView) row.findViewById(R.id.sug_value_deal);
		final ImageView unionImage = (ImageView) row.findViewById(R.id.union_logo_image);
		
		CountryEntity selectedCountry = this.countryListWithoutPlayer.get(position);
		CountryEntity playerCountry = this.activity.getGameController().getLevel().getHumanPlayer().getCountry();
		
		unionImage.setVisibility(View.GONE);
		List<UnionInfo> unionList = this.activity.getGameController().getUnionInfo();
		int unionCount = 0;
		for(UnionInfo union : unionList) {
			if(union.getCountryList().contains(selectedCountry) && !union.isWithoutUnion()) {
				unionImage.setVisibility(View.VISIBLE);
				unionImage.setImageDrawable(new ColorDrawable(UnionColorHelper.getColorForUnion(unionCount)));
				unionCount++;
			}
		}
		
		boolean isUnionBreak = this.breakUnionMap.get(selectedCountry);
		CachedItemSetData cachedItemSet = this.itemSetMap.get(selectedCountry);
		
		List<ItemEntity> itemSet = cachedItemSet.getItemList();
		image.setImageResource(ResourceHelper.getResourceId(ResourceType.DRAWABLE, selectedCountry.getImage(), this.res));
		dealGrid.setAdapter(new ImageGridAdapter<ItemEntity>(row.getContext(), itemSet));
		countryName.setText(selectedCountry.getName());
		
        double avrResDelta = cachedItemSet.getPlayerEffect();
        sugValueText.setText(Double.toString(avrResDelta));
        if(avrResDelta >= 0.0) {
        	sugValueText.setTextColor(row.getResources().getColor(R.color.deal_confirmed_color));
        } else {
        	sugValueText.setTextColor(row.getResources().getColor(R.color.deal_canceled_color));
        }
        fillSuggestionStates(dealConfirmImage, itemSet);
        fillUnionStates(playerCountry, selectedCountry, countryName, unionButton);
        fillMsgStates(msgButton, selectedCountry, playerCountry, isUnionBreak);
        
        dealDetailButton.setOnClickListener(new DealDetailOnClickListener(this.activity, this, itemSet, this.playerAnswers));
        msgButton.setOnClickListener(new MessageOnClickListener(this.activity, this, selectedCountry, playerCountry, isUnionBreak, this.msgReadMap));
        unionButton.setOnClickListener(new UnionOnClickListener(this.activity, this, selectedCountry, playerCountry));
        
		return row;
	}
	
	private void initMaps() {
		for(CountryEntity selectedCountry : this.countryListWithoutPlayer) {
			initMapsForSelected(selectedCountry);
		}
	}
	
	private void initMapsForSelected(CountryEntity selectedCountry) {
		CountryEntity playerCountry = this.activity.getGameController().getLevel().getHumanPlayer().getCountry();
		Boolean isUnionBreak = this.breakUnionMap.get(selectedCountry);
		if(isUnionBreak == null) {
			isUnionBreak = this.activity.getGameController().breakTheUnionIfNeed(getUnionDealData(selectedCountry, playerCountry));
			this.breakUnionMap.put(selectedCountry, isUnionBreak);
		}
		CachedItemSetData cachedItemSet = this.itemSetMap.get(selectedCountry);
		if(cachedItemSet == null) {
			List<ItemEntity> itemSet = this.activity.getGameController().getItemSetForSendDeal(selectedCountry);
			cachedItemSet = new CachedItemSetData();
			cachedItemSet.setItemList(itemSet);
			cachedItemSet.setPlayerEffect(this.activity.getGameController().getItemSetEffect(playerCountry, itemSet));
			this.itemSetMap.put(selectedCountry, cachedItemSet);
		}
		Boolean answer = this.playerAnswers.get(ItemHelper.getItemSetHashCode(cachedItemSet.getItemList()));
		if(answer == null) {
			this.playerAnswers.put(ItemHelper.getItemSetHashCode(cachedItemSet.getItemList()), false);
		}
	}
	
	private void fillMsgStates(ImageButton msgButton, CountryEntity selectedCountry, CountryEntity playerCountry, boolean isUnionBreak) {
		Boolean readFlag = this.msgReadMap.get(selectedCountry.getCode());
		if(haveMessages(selectedCountry, playerCountry, isUnionBreak) && (readFlag == null || !readFlag)) {
        	msgButton.setImageResource(R.drawable.have_msg);
        } else {
        	msgButton.setImageResource(R.drawable.msg);
        }
	}
	private void fillSuggestionStates(final ImageView dealConfirmImage, List<ItemEntity> itemSet) {
		Boolean answer = this.playerAnswers.get(ItemHelper.getItemSetHashCode(itemSet));
		if(answer != null && answer) {
			// ����������� �������
			dealConfirmImage.setImageResource(R.drawable.check);
		} else {
			// ����������� ���������
			dealConfirmImage.setImageResource(R.drawable.uncheck);
		}
	}
	private void fillUnionStates(CountryEntity playerCountry, CountryEntity selectedCountry, TextView countryName, ImageButton unionButton) {
		boolean countriesInUnion = this.activity.getGameController().countryInUnion(playerCountry, selectedCountry);
		if(countriesInUnion) {
			countryName.setText("(C) " + selectedCountry.getName());
			unionButton.setImageResource(R.drawable.union_cancel);
		} else {
			countryName.setText(selectedCountry.getName());
			unionButton.setImageResource(R.drawable.union);
		}
	}
	private boolean haveMessages(CountryEntity selectedCountry, CountryEntity playerCountry, boolean isUnionBreak) {
		return isUnionPosible(selectedCountry, playerCountry) || isUnionBreak;
	}
	private UnionDealData getUnionDealData(CountryEntity selectedCountry, CountryEntity playerCountry) {
		List<PlayerEntity> players = this.activity.getGameController().getLevel().getPlayers();
		return new UnionDealData(selectedCountry.getCode(), playerCountry.getCode(), players);
	}
	private boolean isUnionPosible(CountryEntity selectedCountry, CountryEntity playerCountry) {
		return this.activity.getGameController().checkSuggestUnion(getUnionDealData(selectedCountry, playerCountry));
	}
	public List<CountryEntity> getCountryListWithoutPlayer() {
		return this.countryListWithoutPlayer;
	}
	public Map<Integer, Boolean> getPlayerAnswers() {
		return this.playerAnswers;
	}
	private class CachedItemSetData {
		private List<ItemEntity> itemList;
		private double playerEffect;
		public List<ItemEntity> getItemList() {
			return itemList;
		}
		public void setItemList(List<ItemEntity> itemList) {
			this.itemList = itemList;
		}
		public double getPlayerEffect() {
			return playerEffect;
		}
		public void setPlayerEffect(double playerEffect) {
			this.playerEffect = playerEffect;
		}
	}
}
