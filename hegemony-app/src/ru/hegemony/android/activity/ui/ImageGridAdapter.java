package ru.hegemony.android.activity.ui;

import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import ru.hegemony.android.activity.R;
import ru.hegemony.android.activity.ui.view.GridImageView;
import ru.hegemony.android.data.ResourceType;
import ru.hegemony.android.helper.ResourceHelper;
import ru.hegemony.xml.data.ImageEntity;

public class ImageGridAdapter<T extends ImageEntity> extends BaseAdapter {
	/** ���������� ������� ����� while */
	private static final int WATCH_DOG_COUNT = 10;
	/** ���������� ������� � ������� */
	private static final int GRID_NUM_COLS = 5;
	
    private Context context;
    private List<T> imageList;
    private List<T> highlightList;
    private Resources resources;

    public ImageGridAdapter(Context context, Set<T> imageSet) {
        this.context = context;
        this.imageList = new ArrayList<T>();
        for(T image : imageSet) {
        	this.imageList.add(image);
        }
        this.resources = context.getResources();
        this.highlightList = null;
    }
    
    public ImageGridAdapter(Context context, List<T> imageList, List<T> highlightList) {
        this.context = context;
        this.imageList = imageList;
        this.resources = context.getResources();
        this.highlightList = highlightList;
    }
    
    public ImageGridAdapter(Context context, List<T> imageList) {
        this.context = context;
        this.imageList = imageList;
        this.resources = context.getResources();
        this.highlightList = null;
    }

    @Override
    public int getCount() {
    	int size = imageList.size();
    	if(size == 0) {
    		size++;
    	}
    	if(size % GRID_NUM_COLS != 0) {
    		int count = 0;
    		while(size % GRID_NUM_COLS != 0 && count < WATCH_DOG_COUNT) {
    			size++;
    		}
    	}
        return size;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        GridImageView imageView;
        
        if (convertView == null) {
            imageView = new GridImageView(context);
            imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        } else {
            imageView = (GridImageView) convertView;
        }

        Object[] imageArray = imageList.toArray();
        if(imageArray.length > position) {
	        String imageResName = ((ImageEntity)(imageArray[position])).getImage();
	        int imageResId = ResourceHelper.getResourceId(ResourceType.DRAWABLE, imageResName, resources);
	        boolean good = false;
	        if(highlightList != null) {
		        for(T e : highlightList) {
		        	if(e.getImage().equals(imageResName)) {
		        		good = true;
		        		break;
		        	}
		        }
	        }
	        if(good) {
	        	imageView.setBackgroundResource(R.drawable.grid_select_bg_shape);
	        } else {
	        	imageView.setBackgroundResource(R.drawable.grid_bg_shape);
	        }
	        imageView.setImageResource(imageResId);
        } else {
        	imageView.setBackgroundResource(R.drawable.grid_bg_shape);
        }
        
        return imageView;
    }
}
