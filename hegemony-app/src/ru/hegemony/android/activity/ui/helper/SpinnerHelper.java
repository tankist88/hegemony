package ru.hegemony.android.activity.ui.helper;

import java.lang.reflect.Array;
import java.util.List;

import ru.hegemony.android.activity.BaseGameActivity;
import ru.hegemony.android.activity.R;
import ru.hegemony.android.activity.ui.ImageSpinnerAdapter;
import ru.hegemony.android.activity.ui.ItemActionSpinnerAdapter;
import ru.hegemony.xml.data.ImageEntity;
import ru.hegemony.xml.data.ItemEntity;
import android.app.Activity;
import android.widget.ArrayAdapter;

public class SpinnerHelper {
	@SuppressWarnings("unchecked")
	public static <T> ArrayAdapter<T> getSpinnerElements(Activity activity, Class<T> clazz, List<T> list) {
		return getSpinnerElements(activity, clazz, list.toArray((T[]) Array.newInstance(clazz, list.size())));
	}
	
	public static <T> ArrayAdapter<T> getSpinnerElements(Activity activity, Class<T> clazz, T[] array) {
		ArrayAdapter<T> arrayAdapter = new ArrayAdapter<T>(
				activity,
				R.layout.row_spinner_text, 
				array);
		return arrayAdapter;
	}
	
	public static <T extends ImageEntity> ImageSpinnerAdapter<T> getImageSpinnerElements(Activity activity, List<T> list) {
		return new ImageSpinnerAdapter<T>(activity, list);
	}
	
	public static ItemActionSpinnerAdapter getItemActionSpinnerAdapter(BaseGameActivity activity, ItemEntity item) {
		return new ItemActionSpinnerAdapter(activity, item);
	}
}
