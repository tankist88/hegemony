package ru.hegemony.android.activity.ui;

import java.util.Arrays;
import java.util.List;

import ru.hegemony.android.activity.BaseGameActivity;
import ru.hegemony.android.activity.R;
import ru.hegemony.logic.helper.ItemHelper;
import ru.hegemony.xml.data.CountryEntity;
import ru.hegemony.xml.data.ItemActionEntity;
import ru.hegemony.xml.data.ItemEntity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ItemActionSpinnerAdapter extends ArrayAdapter<ItemActionEntity> {
	/** ������ ��� �������� ����������� ������ */
	private static final int SPINNER_ROW_LAYOUT = R.layout.row_spinner_item_action;
	/** ������ ��� �������� ����������� ������ */
	private static final int DEFAULT_SPINNER_ROW_LAYOUT = R.layout.row_spinner_text;
	
	/** ������ ������ �������� ��� ����������� ������ */
	private List<ItemActionEntity> data;
	/** ������� */
	private ItemEntity item;
	/** Inflater ��� ��������� ������ �� ������ ��� ������� � ����������� ������� (�������� � �����) */
	private LayoutInflater inflater;
	/** ������������ ���������� */
	private BaseGameActivity activity;

	/**
	 * �������� �������� ��� ����������� ����������� ������ � ��������� � �������
	 * @param activity - ����������, ��� ����� ��������� ���������� ������
	 * @param item - �������, ��� �������� ������������ ��������
	 */
	public ItemActionSpinnerAdapter(BaseGameActivity activity, ItemEntity item) {
		super(activity, DEFAULT_SPINNER_ROW_LAYOUT, item.getActionList());
		setDropDownViewResource(SPINNER_ROW_LAYOUT);
		this.activity = activity;
		this.item = ItemHelper.createNewInstance(item);
		this.data = item.getActionList();
		this.inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		View row = inflater.inflate(SPINNER_ROW_LAYOUT, parent, false);

		ItemActionEntity currentPositionValue = data.get(position);
		
		TextView name = (TextView) row.findViewById(R.id.action_name);
		TextView value = (TextView) row.findViewById(R.id.action_value);

		name.setText(currentPositionValue.getName());
		
		CountryEntity playerCountry = activity.getGameController().getLevel().getHumanPlayer().getCountry();
		ItemEntity currentItem = ItemHelper.createNewInstance(item);
		currentItem.setSa(currentPositionValue.getCode());
		
		double avrResDelta = activity.getGameController().getItemSetEffect(playerCountry, Arrays.asList(currentItem));
		
		value.setText(Double.toString(avrResDelta));
        if(avrResDelta >= 0.0) {
        	value.setTextColor(activity.getResources().getColor(R.color.deal_confirmed_color));
        } else {
        	value.setTextColor(activity.getResources().getColor(R.color.deal_canceled_color));
        }
		
		return row;
	}
}
