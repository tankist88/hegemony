package ru.hegemony.android.activity.ui;

import java.util.List;

import ru.hegemony.android.activity.R;
import ru.hegemony.android.data.ResourceType;
import ru.hegemony.android.helper.ResourceHelper;
import ru.hegemony.common.data.MainResources;
import ru.hegemony.common.helper.DataHelper;
import ru.hegemony.xml.data.ResourcesEntity;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ResourceListAdapter extends ArrayAdapter<ResourcesEntity> {
	/** ������ ��� �������� ������ ����������� */
	private static final int RESULT_ROW_LAYOUT = R.layout.row_list_resources;
	
	/** �������� ������ */
	private List<ResourcesEntity> data;
	/** Inflater ��� ��������� ������ �� ������ ��� ������� � ����������� ������� */
	private LayoutInflater inflater;
    private Resources resources;

	public ResourceListAdapter(Context context, List<ResourcesEntity> objects) {
		super(context, RESULT_ROW_LAYOUT, objects);
		this.data = objects;
        this.resources = context.getResources();
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		return getCustomView(position, parent);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return getCustomView(position, parent);
	}

	// ����� ���������� ��� ������ ������ � ������
	public View getCustomView(int position, ViewGroup parent) {
		View row = inflater.inflate(RESULT_ROW_LAYOUT, parent, false);

		ResourcesEntity res = data.get(position);

		ImageView resImg = (ImageView) row.findViewById(R.id.res_img);
		TextView resValue = (TextView) row.findViewById(R.id.res_value_text);

        resImg.setImageResource(
                ResourceHelper.getResourceId(
                        ResourceType.DRAWABLE,
                        MainResources.getByCode(res.getCode()).getImage(),
                        resources)
        );
		resValue.setText(Double.toString(DataHelper.round(res.getValue(), 1)));
		
		return row;
	}
}
