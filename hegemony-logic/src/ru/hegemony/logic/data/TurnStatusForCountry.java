package ru.hegemony.logic.data;

import java.io.Serializable;

import ru.hegemony.xml.data.CountryEntity;

/**
 * ����� ������ �� �����������.
 */
public class TurnStatusForCountry implements Serializable {
	private static final long serialVersionUID = 8442857261527925255L;
	
	/** ���������� ������ */
	private CountryEntity country;
	/** ����� ������ ���������, ������� ������������� ������ */
	private int itemSetNum;
	/** ����������� �� ������ � ������������, true - ����������� */
	private boolean confirmed;
	
	public CountryEntity getCountry() {
		return country;
	}
	public void setCountry(CountryEntity country) {
		this.country = country;
	}
	public boolean isConfirmed() {
		return confirmed;
	}
	public void setConfirmed(boolean confirmed) {
		this.confirmed = confirmed;
	}
	public int getItemSetNum() {
		return itemSetNum;
	}
	public void setItemSetNum(int itemSetNum) {
		this.itemSetNum = itemSetNum;
	}
}
