package ru.hegemony.logic.data;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ru.hegemony.xml.data.CountryEntity;
import ru.hegemony.xml.data.PlayerEntity;

public class UnionDealData {
	private CountryEntity initiator;
	private CountryEntity reciever;
	private Set<CountryEntity> initiatorUnion;
	private Set<CountryEntity> recieverUnion;
	
	public UnionDealData() {
	}
	
	public UnionDealData(int initiatorCode, int recieverCode, List<PlayerEntity> players) {
		this.initiator = getCountryFromPlayers(initiatorCode, players);
		this.reciever = getCountryFromPlayers(recieverCode, players);
		this.initiatorUnion = getCountrySet(initiatorCode, initiator.getUnionCountryCodeList(), players);
		this.recieverUnion = getCountrySet(recieverCode, reciever.getUnionCountryCodeList(), players);
	}
	
	private CountryEntity getCountryFromPlayers(int code, List<PlayerEntity> players) {
		for(PlayerEntity player : players) {
			if(code == player.getCountry().getCode()) {
				return player.getCountry();
			}
		}
		return null;
	}
	
	private Set<CountryEntity> getCountrySet(int hostCode, List<Integer> codes, List<PlayerEntity> players) {
		Set<CountryEntity> result = new HashSet<CountryEntity>();
		for(PlayerEntity player : players) {
			if(player.getCountry().getCode() == hostCode) {
				result.add(player.getCountry());
			}
		}
		for(Integer cc : codes) {
			for(PlayerEntity player : players) {
				if(player.getCountry().getCode() == cc.intValue()) {
					result.add(player.getCountry());
				}
			}
		}
		return result;
	}
	
	public Set<CountryEntity> getInitiatorUnion() {
		return initiatorUnion;
	}
	public void setInitiatorUnion(Set<CountryEntity> initiatorUnion) {
		this.initiatorUnion = initiatorUnion;
	}
	public Set<CountryEntity> getRecieverUnion() {
		return recieverUnion;
	}
	public void setRecieverUnion(Set<CountryEntity> recieverUnion) {
		this.recieverUnion = recieverUnion;
	}
	public CountryEntity getInitiator() {
		return initiator;
	}
	public void setInitiator(CountryEntity initiator) {
		this.initiator = initiator;
	}
	public CountryEntity getReciever() {
		return reciever;
	}
	public void setReciever(CountryEntity reciever) {
		this.reciever = reciever;
	}
}
