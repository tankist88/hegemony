package ru.hegemony.logic.data;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ru.hegemony.common.helper.DataHelper;
import ru.hegemony.logic.helper.CountryHelper;
import ru.hegemony.logic.helper.ItemHelper;
import ru.hegemony.xml.data.CountryEntity;
import ru.hegemony.xml.data.ItemEntity;
import ru.hegemony.xml.data.ItemValueType;
import ru.hegemony.xml.data.LevelType;
import ru.hegemony.xml.data.PatternType;
import ru.hegemony.xml.data.PlayerEntity;
import ru.hegemony.xml.data.ResourceDeltaEntity;
import ru.hegemony.xml.data.ResourcesEntity;
import ru.hegemony.xml.data.ValueCode;

public class Level implements Serializable {
	private static final long serialVersionUID = -8441355884880948675L;
	
	private final static DecimalFormat itemCodeFormat = new DecimalFormat("00");
	private final static DecimalFormat actionCodeFormat = new DecimalFormat("00");
	
	/** ���������� �� ������ */
	private LevelType headerInfo;
	/** ������ */
	private List<PlayerEntity> players;
	/** ��������� ������� ��������� */
	private LevelDifficulty difficulty;
	/** ������� ������ */
	private PatternType opinionPatterns;
	/** ����� �������� ���� */
	private int currentTurn;
	/** ����� �������� ������ */
	private int round;
	
	public Level() {
		this.currentTurn = 0;
		this.round = 0;
	}
	
	public LevelType getHeaderInfo() {
		return headerInfo;
	}
	public void setHeaderInfo(LevelType headerInfo) {
		this.headerInfo = headerInfo;
	}
	public LevelDifficulty getDifficulty() {
		return difficulty;
	}
	public void setDifficulty(LevelDifficulty difficulty) {
		this.difficulty = difficulty;
	}
	public int getCurrentTurn() {
		return currentTurn;
	}
	public void setCurrentTurn(int currentTurn) {
		this.currentTurn = currentTurn;
	}
	public void increaseTurn() {
		this.currentTurn = this.currentTurn + 1;
	}
	public void increaseRound() {
		this.round = this.round + 1;
	}
	public List<PlayerEntity> getPlayers() {
		return players;
	}
	public void setPlayers(List<PlayerEntity> players) {
		this.players = players;
	}
	public int getRound() {
		return round;
	}
	public void setRound(int round) {
		this.round = round;
	}
	public PatternType getOpinionPatterns() {
		return opinionPatterns;
	}
	public void setOpinionPatterns(PatternType opinionPatterns) {
		this.opinionPatterns = opinionPatterns;
	}
	
	public CountryEntity getCountryByCode(int code) {
		return CountryHelper.getCountryByCode(code, players);
	}
	public PlayerEntity getHumanPlayer() {
		if(players != null) {
			for(PlayerEntity player : players) {
				if(!player.isArtIntSign()) {
					return player;
				}
			}
		}
		return null;
	}
	public String getItemSetStringCode(List<ItemEntity> itemSet) {
		String itemSetCode = "";
		for(ItemEntity item : itemSet) {
			itemSetCode += itemCodeFormat.format(item.getCode()) + actionCodeFormat.format(item.getSa());
		}
		return itemSetCode;
	}
	public List<ItemEntity> getItemSetFromString(String itemsString) {
		List<ItemEntity> result = new ArrayList<ItemEntity>();
		int size = itemsString.length()/4;
		for(int i = 0; i < size; i++) {
			String itemChar = itemsString.substring(i * 4, (i * 4) + 4);
			int itemCode = Integer.parseInt(itemChar.substring(0, 2));
			int actionCode = Integer.parseInt(itemChar.substring(2, 4));
			ItemEntity selectedItem = null;
			for(ItemEntity item : getHeaderInfo().getItemList()) {
				if(item.getCode() == itemCode) {
					selectedItem = ItemHelper.createNewInstance(item);
					break;
				}
			}
			if(selectedItem == null) {
				throw new IllegalStateException("Item code not found in item dictionary");
			}
			selectedItem.setSa(actionCode);
			result.add(selectedItem);
		}
		return result;
	}
	public double getItemSetEffect(String itemsString, CountryEntity country) {
		return getItemSetEffect(getItemSetFromString(itemsString), country);
	}
	public double getItemSetEffect(List<ItemEntity> itemSet, CountryEntity country) {
		return DataHelper.round(CountryHelper.getResDeltaKoef(country, createResourceDeltaList(getItemInfluenceMap(), itemSet, country)), 3);
	}
	public List<ResourceDeltaEntity> getItemSetEffectList(String itemsString, CountryEntity country) {
		return getItemSetEffectList(getItemSetFromString(itemsString), country);
	}
	public List<ResourceDeltaEntity> getItemSetEffectList(List<ItemEntity> itemSet, CountryEntity country) {
		return createResourceDeltaList(getItemInfluenceMap(), itemSet, country);
	}
	private Map<ValueCode, Double> getItemInfluenceMap() {
		Map<ValueCode, Double> result = new HashMap<ValueCode, Double>();
		for(ItemValueType vt : getHeaderInfo().getItemsEffectList()) {
			result.put(vt.getValueCode(), vt.getValue());
		}
		return result;
	}
	private List<Double> getItemSetDeltaByRes(
			ResourcesEntity res, 
			List<ItemEntity> itemSet,
			Map<ValueCode, Double> itemInfluenceMap, 
			CountryEntity country) 
	{
		List<Double> deltaList = new ArrayList<Double>();
		for(ItemEntity item : itemSet) {
			ValueCode key = new ValueCode();
			key.setActionCode(item.getSa());
			key.setCountryCode(country.getCode());
			key.setResourceCode(res.getCode());
			key.setItemCode(item.getCode());
			Double value = itemInfluenceMap.get(key);
			deltaList.add(value);
		}
		return deltaList;
	}
	private List<ResourceDeltaEntity> createResourceDeltaList(
			Map<ValueCode, Double> itemInfluenceMap, 
			List<ItemEntity> itemSet, 
			CountryEntity country) 
	{
		List<ResourceDeltaEntity> resDeltaList = new ArrayList<ResourceDeltaEntity>();
		for(ResourcesEntity res : country.getResourceList()) {
			ResourceDeltaEntity delta = new ResourceDeltaEntity();
			delta.setResourceCode(res.getCode());
			List<Double> deltaList = getItemSetDeltaByRes(res, itemSet, itemInfluenceMap, country);
			double mean = getMean(deltaList);
			double roundedMean = DataHelper.round(mean, 3);
			delta.setDelta(roundedMean);
			resDeltaList.add(delta);
		}
		return resDeltaList;
	}
	private static double getMean(List<Double> values) {
		double sum = 0.0;
		for(Double value : values) {
			sum += value.doubleValue();
		}
		return sum/values.size();
	}
}
