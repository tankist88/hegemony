package ru.hegemony.logic.data;

import java.io.Serializable;

public class LevelInitData implements Serializable {
	private static final long serialVersionUID = -5115109428623531615L;
	
	private DataSources levelDataSources;
	private String levelFilename;
	private int playerCountryId;
	private LevelDifficulty difficulty;
	private String playerName;
	
	public LevelInitData(String levelFilename, DataSources levelDataSources, int playerCountryId, LevelDifficulty difficulty, String playerName) {
		String[] filenamePart = levelFilename.split("\\.");
		String filenameWithOutExt = filenamePart[0].toLowerCase();
		this.levelFilename = filenameWithOutExt;
		this.levelDataSources = levelDataSources;
		this.playerCountryId = playerCountryId;
		this.difficulty = difficulty;
		this.playerName = playerName;
	}
	public DataSources getLevelDataSources() {
		return levelDataSources;
	}
	public void setLevelDataSources(DataSources levelDataSources) {
		this.levelDataSources = levelDataSources;
	}
	public int getPlayerCountryId() {
		return playerCountryId;
	}
	public void setPlayerCountryId(int playerCountryId) {
		this.playerCountryId = playerCountryId;
	}
	public LevelDifficulty getDifficulty() {
		return difficulty;
	}
	public void setDifficulty(LevelDifficulty difficulty) {
		this.difficulty = difficulty;
	}
	public String getPlayerName() {
		return playerName;
	}
	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}
	public String getLevelFilename() {
		return levelFilename;
	}
}
