package ru.hegemony.logic.data;

import java.util.Set;

import ru.hegemony.xml.data.CountryEntity;

public class UnionInfo {
	private Set<CountryEntity> countryList;
	private boolean withoutUnion;
	
	public UnionInfo(Set<CountryEntity> countryList, boolean withoutUnion) {
		super();
		this.countryList = countryList;
		this.withoutUnion = withoutUnion;
	}
	public Set<CountryEntity> getCountryList() {
		return countryList;
	}
	public void setCountryList(Set<CountryEntity> countryList) {
		this.countryList = countryList;
	}
	public boolean isWithoutUnion() {
		return withoutUnion;
	}
	public void setWithoutUnion(boolean withoutUnion) {
		this.withoutUnion = withoutUnion;
	}
}
