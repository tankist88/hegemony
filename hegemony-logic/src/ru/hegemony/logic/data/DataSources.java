package ru.hegemony.logic.data;

import java.io.InputStream;
import java.io.Serializable;

public class DataSources implements Serializable {
	private static final long serialVersionUID = -3250643695702084034L;
	
	private InputStream levelDs;
	private InputStream playersStatDs;
	private InputStream opinionPatternsDs;
	
	public InputStream getLevelDs() {
		return levelDs;
	}
	public void setLevelDs(InputStream levelDs) {
		this.levelDs = levelDs;
	}
	public InputStream getPlayersStatDs() {
		return playersStatDs;
	}
	public void setPlayersStatDs(InputStream playersStatDs) {
		this.playersStatDs = playersStatDs;
	}
	public InputStream getOpinionPatternsDs() {
		return opinionPatternsDs;
	}
	public void setOpinionPatternsDs(InputStream opinionPatternsDs) {
		this.opinionPatternsDs = opinionPatternsDs;
	}
}
