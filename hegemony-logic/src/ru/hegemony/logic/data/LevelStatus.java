package ru.hegemony.logic.data;

import java.io.Serializable;

/**
 * ������������, ���������� ������� ����������� ������ <br>
 * <li><b>LEVEL_CONTINUES</b> - ���� �� ������� ������ ������������</li>
 * <li><b>LEVEL_END_WIN</b> - ���� �� ������� ������ ����������� - ���� ����������</li>
 * <li><b>LEVEL_END_LOST</b> - ���� �� ������� ������ ����������� - ����������� ���</li>
 */
public enum LevelStatus implements Serializable {
	/** ���� �� ������� ������ ������������ */
	LEVEL_CONTINUES(1, "������� ������������"),
	/** ���� �� ������� ������ ����������� - ���� ���������� */
	LEVEL_END_WIN(2, "������� ����������, ����� �������"),
	/** ���� �� ������� ������ ����������� - ����������� ��� */
	LEVEL_END_LOST(3, "������� ����������, ����� ��������");
	
	private final int status;
	private final String description;
	
	private LevelStatus(int status, String desc) {
		this.status = status;
		this.description = desc;
	}

	public int getStatus() {
		return status;
	}

	public String getDescription() {
		return description;
	}
}
