package ru.hegemony.logic.data;

import java.io.Serializable;

public enum LevelDifficulty implements Serializable {
	LOW(0, "������"),
	NORMAL(1, "����������"),
	HARD(2, "�������");
	
	private final int code;
	private final String description;
	
	private LevelDifficulty(int code, String description) {
		this.code = code;
		this.description = description;
	}

	public int getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}
	
	@Override
	public String toString() {
		return description;
	}
}
