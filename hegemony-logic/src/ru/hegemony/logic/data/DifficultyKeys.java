package ru.hegemony.logic.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import ru.hegemony.xml.data.CountryEntity;
import ru.hegemony.xml.data.TurnEntity;

public class DifficultyKeys implements Serializable {
	private static final long serialVersionUID = 858565023564320817L;
	
	private static final int KEY_USED_FOR_TURN = -100;
	
	private static Map<Integer, DifficultyKeys> instances = new ConcurrentHashMap<Integer, DifficultyKeys>();
	private static Map<String, Integer> outKeys = new ConcurrentHashMap<String, Integer>();
	
	private List<String> lowKeysList;
	private List<String> normalKeysList;
	private List<String> hardKeysList;
	
	private LevelDifficulty difficulty;
	
	private int playersCount;

	public static DifficultyKeys getInstance(TurnEntity turnList, LevelDifficulty difficulty, int playersCount) {
		if(instances.get(turnList.getCountryCode()) == null) {
			synchronized(instances) {				
				instances.put(turnList.getCountryCode(), new DifficultyKeys(turnList, difficulty, playersCount));
			}
		}
		return instances.get(turnList.getCountryCode());
	}
	/**
	 * ���������� ������, ���� ���� ��������� �������������
	 * ����� ������� NULL
	 */
	public static DifficultyKeys getInstance(CountryEntity country) {
		return instances.get(country.getCode());
	}
	public static void clearCache() {
		instances = new ConcurrentHashMap<Integer, DifficultyKeys>();
	}
	private DifficultyKeys(TurnEntity turnList, LevelDifficulty difficulty, int playersCount) {
		this.difficulty = difficulty;
		this.playersCount = playersCount;
		if(difficulty.equals(LevelDifficulty.LOW)) {
			this.lowKeysList = new ArrayList<String>();
		}
		if(difficulty.equals(LevelDifficulty.NORMAL)) {
			this.normalKeysList = new ArrayList<String>();
		}
		if(difficulty.equals(LevelDifficulty.HARD)) {
			this.hardKeysList = new ArrayList<String>();
		}
		int partSize = turnList.getGkList().size()/LevelDifficulty.values().length;
		if(partSize > 0) {
			int lowBorder = (turnList.getGkList().size() - 1) - partSize;
			int normalBorder = (turnList.getGkList().size() - 1) - (partSize * 2);
			// ��������� ���� � �������� ������� � ����� �� ��� ����� � ����������� �� ������������� ������� ��� ������� ������
			for(int i = turnList.getGkList().size()-1; i >= 0; i--) {
				if(i > lowBorder) {
					if(difficulty.equals(LevelDifficulty.LOW)) {
						this.lowKeysList.add(turnList.getGkList().get(i));
					}
				} else if(i > normalBorder) {
					if(difficulty.equals(LevelDifficulty.NORMAL)) {
						this.normalKeysList.add(turnList.getGkList().get(i));
					}
				} else {
					if(difficulty.equals(LevelDifficulty.HARD)) {
						this.hardKeysList.add(turnList.getGkList().get(i));
					}
				}
			}
			if(this.normalKeysList != null && !this.normalKeysList.isEmpty()) {
				Collections.reverse(this.normalKeysList);
			}
			if(this.hardKeysList != null && !this.hardKeysList.isEmpty()) {
				Collections.reverse(this.hardKeysList);
			}
			if(this.lowKeysList != null && !this.lowKeysList.isEmpty()) {
				Collections.reverse(this.lowKeysList);
			}
		} else {
			throw new IllegalStateException("Can't fill difficulty lists. Part size less or equal zero");
		}
	}
	public String getCurrentOptCode(int turnNum) {
		if(difficulty.equals(LevelDifficulty.LOW)) {
			return getKeyFromList(lowKeysList, turnNum);
		} else if(difficulty.equals(LevelDifficulty.NORMAL)) {
			return getKeyFromList(normalKeysList, turnNum);
		} else if(difficulty.equals(LevelDifficulty.HARD)) {
			return getKeyFromList(hardKeysList, turnNum);
		} else {
			throw new IllegalArgumentException("Unknown difficulty");
		}
	}
	private String getKeyFromList(List<String> keyList, int turnNum) {
		for(String key : keyList) {
			if(!keyIsOut(key, turnNum)) {
				registerKey(key, turnNum);
				return key;
			}
		}
		return keyList.get(keyList.size() - 1);
	}
	/**
	 * �������� �� ����� �� ���� ������ ��������� �� ����
	 * @param key - ���� (�����) ������ ���������
	 * @return true - ���� ����� �� ����
	 */
	private boolean keyIsOut(String key, int turnNum) {
		Integer count = getOutKeysMap().get(key);
		if(count != null && (count.intValue() == KEY_USED_FOR_TURN || turnNum - count.intValue() >= playersCount)) {
			return true;
		} else {
			return false;
		}
	}
	private void registerKey(String key, int turnNum) {
		Integer count = getOutKeysMap().get(key);
		if(count == null) {
			getOutKeysMap().put(key, turnNum);
		}
	}
	public static void putToOutKeys(String key) {
//		getOutKeysMap().put(key, KEY_USED_FOR_TURN);
	}
	private static Map<String, Integer> getOutKeysMap() {
		if(outKeys == null) {
			synchronized(outKeys) {
				outKeys = new ConcurrentHashMap<String, Integer>();
			}
		}
		return outKeys;
	}
}
