package ru.hegemony.logic.helper;

import java.util.Random;

public class MathHelper {
    public static boolean nextBernoulli(double p) {
        double q = 1.0 - p;
        Random rnd = new Random(System.nanoTime());
        double uniformValue = rnd.nextDouble();
        return (uniformValue < q) ? false : true;
    }
}
