package ru.hegemony.logic.helper;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import org.jibx.runtime.BindingDirectory;
import org.jibx.runtime.IBindingFactory;
import org.jibx.runtime.IMarshallingContext;
import org.jibx.runtime.IUnmarshallingContext;
import org.jibx.runtime.JiBXException;

import ru.hegemony.logic.data.DataSources;
import ru.hegemony.logic.data.Level;
import ru.hegemony.xml.data.CountryEntity;
import ru.hegemony.xml.data.LevelList;
import ru.hegemony.xml.data.LevelListType;
import ru.hegemony.xml.data.LevelType;
import ru.hegemony.xml.data.PatternType;
import ru.hegemony.xml.data.PlayerEntity;
import ru.hegemony.xml.data.PlayersType;

public class LevelHelper {
	public static void fillLevelEntity(final int humanPlayerCountryCode, final DataSources dataSources, Level levelEntity) throws Exception {
		fillLevelEntity(null, humanPlayerCountryCode, dataSources, levelEntity);
	}
	
	public static void fillLevelEntity(final String playerName, final int humanPlayerCountryCode, final DataSources dataSources, Level level) throws Exception {
		level.setHeaderInfo(getXmlObject(LevelType.class, dataSources.getLevelDs()));
		level.setOpinionPatterns(getXmlObject(PatternType.class, dataSources.getOpinionPatternsDs()));
		
		List<PlayerEntity> players = new ArrayList<PlayerEntity>();
		List<PlayerEntity> playersFromCache = new ArrayList<PlayerEntity>();
		
		try {
			if(dataSources.getPlayersStatDs() != null) {
				playersFromCache = getXmlObject(PlayersType.class, dataSources.getPlayersStatDs()).getPlayerList();
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		}
		
		PlayerEntity humanPlayer = null;
		
		int playersCount = 2;
		for(CountryEntity country : level.getHeaderInfo().getCountryList()) {
			PlayerEntity player = null;
			PlayerEntity cachedPlayer = getPlayerFromListByCountry(country, playersFromCache);
			if(cachedPlayer == null) {
				player = new PlayerEntity();
				player.setCountry(country);
			} else {
				player = cachedPlayer;
			}
			if(country.getCode() == humanPlayerCountryCode) {
				player.setArtIntSign(false);
				if(playerName != null && playerName.length() > 0) {
					player.setName(playerName);
				} else {
					player.setName("Player1");
				}
				humanPlayer = player;
			} else {
				player.setArtIntSign(true);
				player.setName("Player" + playersCount);
				players.add(player);
				playersCount++;
			}
		}
		if(humanPlayer != null) {
			players.add(0, humanPlayer);
		}
		
		level.setPlayers(players);
	}
	
	private static PlayerEntity getPlayerFromListByCountry(CountryEntity country, List<PlayerEntity> playersList) {
		for(PlayerEntity player : playersList) {
			if(player.getCountry().getCode() == country.getCode()) {
				return player;
			}
		}
		return null;
	}
	
	public static String createPlayersState(List<PlayerEntity> playersList) {
		String result = null;
		try {
			PlayersType pt = new PlayersType();
			pt.setPlayerList(playersList);
			IBindingFactory bfact = BindingDirectory.getFactory(PlayersType.class);
			IMarshallingContext mctx = bfact.createMarshallingContext();
			mctx.setIndent(2);
			StringWriter out = new StringWriter();
			mctx.marshalDocument(pt, "UTF-8", null, out);
			result = out.toString();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
	
	public static List<PlayerEntity> getPlayersStat(InputStream is) throws JiBXException, IOException {
		return getXmlObject(PlayersType.class, is).getPlayerList();
	}
	
	public static List<LevelList> getLevelList(InputStream is) throws JiBXException, IOException {
		return getXmlObject(LevelListType.class, is).getLevelList();
	}
	
	public static LevelType getLevel(InputStream is) throws JiBXException, IOException {
		return getXmlObject(LevelType.class, is);
	}
	
	@SuppressWarnings("unchecked")
	private static <T> T getXmlObject(Class<T> clazz, InputStream is) throws JiBXException, IOException {
		IBindingFactory bfact = BindingDirectory.getFactory(clazz);
		IUnmarshallingContext uctx = bfact.createUnmarshallingContext();
		T obj = (T) uctx.unmarshalDocument(is, null);
		is.close();
		return obj;
	}
}
