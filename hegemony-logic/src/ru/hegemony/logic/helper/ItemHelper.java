package ru.hegemony.logic.helper;

import java.util.ArrayList;
import java.util.List;

import ru.hegemony.xml.data.ItemActionEntity;
import ru.hegemony.xml.data.ItemEntity;
import ru.hegemony.xml.data.ItemPropertyEntity;

public class ItemHelper {
	/**
	 * ������ ������� ��� ��������. ������ ���������� ����� ����� �� ��������, ��� � �����
	 * ru.hegemony.common.editor.helper.ItemHelper.getItemHashCode(ItemEntity item)
	 * @see ru.hegemony.common.editor.helper.ItemHelper
	 * @param item - �������
	 * @return - ������ ��������
	 */
	public static int getItemHashCode(ItemEntity item) {
		final int prime = 31;
		int result = 1;
		result = prime * result + item.getCode();
		result = prime * result + ((item.getImage() == null) ? 0 : item.getImage().hashCode());
		result = prime * result + ((item.getName() == null) ? 0 : item.getName().hashCode());
		result = prime * result + item.getSa();
		return result;
	}
	
	/**
	 * ������ ������� ��� ������ ���������
	 * @see ru.hegemony.common.editor.gen.GenerateItemSetsTask
	 * @param itemList - ����� ���������
	 * @return ������ ������ ���������
	 */
	public static int getItemSetHashCode(List<ItemEntity> itemList) {
		int itemSetHashCode = 0;
		for(ItemEntity item : itemList) {
			itemSetHashCode += ItemHelper.getItemHashCode(item);
		}
		return itemSetHashCode;
	}
	
	/**
	 * ��������� ������������ ���� �� ���� ������� �� ������ ������ � ������
	 * @param oneItemList - ������ ��������� �1
	 * @param twoItemList - ������ ��������� �2
	 * @return true - ���� � ������ oneItemList ���������� ������ ���� ������� �� ������ twoItemList
	 */
	public static boolean itemListContainsAtLeastOneElementFrom(List<ItemEntity> oneItemList, List<ItemEntity> twoItemList){
		for(ItemEntity item : twoItemList) {
			if(oneItemList.contains(item)) {
				return true;
			}
		}
		return false;
	}
	
	public static ItemActionEntity getItemActionByCode(int code, List<ItemActionEntity> actionList) {
		for(ItemActionEntity action : actionList) {
			if(action.getCode() == code) {
				return action;
			}
		}
		return null;
	}
	
	public static ItemEntity createNewInstance(ItemEntity item) {
		if(item == null) {
			return null;
		}
		ItemEntity res = new ItemEntity();
		res.setActionList(createActionList(item.getActionList()));
		res.setCode(item.getCode());
		res.setImage(item.getImage());
		res.setName(item.getName());
		res.setPropertyList(createPropertyList(item.getPropertyList()));
		res.setSa(item.getSa());
		return res;
	}
	
	private static ItemActionEntity createItemActionInstance(ItemActionEntity act) {
		if(act == null) {
			return null;
		}
		ItemActionEntity res = new ItemActionEntity();
		res.setCode(act.getCode());
		res.setName(act.getName());
		return res;
	}
	
	private static ItemPropertyEntity createItemPropertyInstance(ItemPropertyEntity prop) {
		if(prop == null) {
			return null;
		}
		ItemPropertyEntity res = new ItemPropertyEntity();
		res.setCode(prop.getCode());
		res.setName(prop.getName());
		res.setValue(prop.getValue());
		return res;
	}
	
	private static List<ItemActionEntity> createActionList(List<ItemActionEntity> actList) {
		if(actList == null) {
			return null;
		}
		List<ItemActionEntity> resList = new ArrayList<ItemActionEntity>();
		for(ItemActionEntity act : actList) {
			resList.add(createItemActionInstance(act));
		}
		return resList;
	}
	
	private static List<ItemPropertyEntity> createPropertyList(List<ItemPropertyEntity> propList) {
		if(propList == null) {
			return null;
		}
		List<ItemPropertyEntity> resList = new ArrayList<ItemPropertyEntity>();
		for(ItemPropertyEntity prop : propList) {
			resList.add(createItemPropertyInstance(prop));
		}
		return resList;
	}
}
