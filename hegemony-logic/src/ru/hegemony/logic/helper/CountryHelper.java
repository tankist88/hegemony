package ru.hegemony.logic.helper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import ru.hegemony.common.data.MainResources;
import ru.hegemony.common.helper.ItemSetCompareHelper;
import ru.hegemony.logic.data.Level;
import ru.hegemony.xml.data.CountryEntity;
import ru.hegemony.xml.data.KeyValueBool;
import ru.hegemony.xml.data.KeyValueDbl;
import ru.hegemony.xml.data.PlayerEntity;
import ru.hegemony.xml.data.ResourceDeltaEntity;
import ru.hegemony.xml.data.ResourcesEntity;

public class CountryHelper {
	public final static double ZERO = 0.001;
	//-----------------------------------------------------------------
	public final static double ZERO_RESOURCE = 0.01;
	//-----------------------------------------------------------------
	/**
	 * ������� ����� ������������ ������� ��������� � �����������, ������ ������� ����� ��������� ��������� ��������
	 * �� ���������
	 * � ��������� %
	 */
	private final static double DEFAULT_CONFIRM_ZERO_PERCENT = 1.0;
	/**
	 * ������� ����� ������������ ������� ��������� � �����������, ������ ������� ����� ��������� ��������� ��������
	 * ��� ������ �����
	 * � ��������� %
	 */
	private final static double UNION_CONFIRM_ZERO_PERCENT = 10.0;
	//-----------------------------------------------------------------
	// ��������� ������� ��� �������� �������������, �.�. ��� ������ ������
	// ������� ��������������� � �������� �� 0 �� 1, �� ���������,
	// ��������� � �������� ������ �� � ���������, � � �������������� ���������
	/**
	 * ����� �� ������� ������� "�������" ����� ���, ��� ���������� � ���������� �����������
	 * ��� �������� ������� � �������� ������ ���������
	 */
	private final static double INFLUENCE_DIVIDER = 5;
	/**
	 * ��� ���������� ��������� ������� �����
	 */
	private final static double RELATIVE_INFLUENCE_ADD = 0.05;
	/**
	 * ������������ �������� ��������� �������
	 */
	private final static double MAX_RELATIVE_INFLUENCE = 10.0;
	/**
	 * ��� ���������� ��������� ������� �����
	 */
	private final static double RELATIVE_INFLUENCE_SUB = 0.01;
	/**
	 * ����� ��������� ������� (��������) ����� �������� ������ ����� ����� ����������
	 */
	private final static double RELATIVE_INFLUENCE_FOR_UNION = 0.15;
	//-----------------------------------------------------------------
	
	/**
	 * ������ ���������� ������������ ����������� �� delta �������� ��������
	 * @param country - ������ ��� ������� ���� ������
	 * @param itemSetList - delta �������� ������
	 * @return ��������� �����������
	 */
	public static double getResDeltaKoef(CountryEntity country, final List<ResourceDeltaEntity> itemSetList) {
		Map<Integer, Double> resWeightMap = new HashMap<Integer, Double>();
		double resWeightSum = 0.0;
		for(ResourcesEntity res : country.getResourceList()) {
			resWeightMap.put(res.getCode(), res.getWeight());
			resWeightSum += res.getWeight();
		}
		
		if(Math.abs(resWeightSum - 1) > ZERO) {
			throw new IllegalStateException("������ ������� resWeightSum! ����� resWeightSum (" + resWeightSum + ") �� ����� 1.0");
		}
		
		return getKoef(resWeightMap, itemSetList);
	}
	
	private static double getKoef(Map<Integer, Double> resWeightMap, final List<ResourceDeltaEntity> itemSetList) {
		double koef = 0.0;
		for(ResourceDeltaEntity resDelta : itemSetList) {
			koef += resDelta.getDelta() * resWeightMap.get(resDelta.getResourceCode());
		}
		return koef;
	}
	
	public static boolean dealBetterThenOpt(CountryEntity hostCountry, CountryEntity initiatorCountry, double optK, double dealK, List<PlayerEntity> players) {
		boolean currentDealBetter = false;
		double dealSub = 0.0;
		double influence = 0.0;
		double resDelta = getResourceByCode(MainResources.INFLUENCE.getCode(), initiatorCountry).getValue();
		if(resDelta > 0.0) {
			influence = resDelta / INFLUENCE_DIVIDER;
		}
		influence += getRelativeInfluenceValue(hostCountry, initiatorCountry);
		dealSub = (new ItemSetCompareHelper(hostCountry.getUnionDelta(), optK)).getSub(dealK + influence);
		if(countryInUnion(hostCountry, initiatorCountry)) {
			dealSub = (new ItemSetCompareHelper(hostCountry.getUnionDelta(), optK)).getSub(dealK + influence);
		} else {
			dealSub = (new ItemSetCompareHelper(DEFAULT_CONFIRM_ZERO_PERCENT, optK)).getSub(dealK + influence);
		}
		currentDealBetter = MathHelper.nextBernoulli(getSuccessProbability(dealSub));
		updateRelativeInfluenceValue(hostCountry, initiatorCountry, currentDealBetter);
		return currentDealBetter;
	}
	
	private static double getSuccessProbability(double x) {
		if(x >= 0.0) {
			return 1 - (Math.pow(Math.E, -7.0 * Math.abs(x)) / 4);
		} else {
			return Math.pow(Math.E, -7.0 * Math.abs(x)) / 4;
		}
	}
	
	public static ResourcesEntity getResourceByCode(int code, CountryEntity country) {
		for(ResourcesEntity cRes : country.getResourceList()) {
			if(cRes.getCode() == code) {
				return cRes;
			}
		}
		return null;
	}
	
	public static boolean countryInUnion(CountryEntity hostCountry, CountryEntity initiatorCountry) {
		for(Integer cCode : hostCountry.getUnionCountryCodeList()) {
			if(cCode.intValue() == initiatorCountry.getCode()) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * ������������ ������ ��� ��������� ��������. ������ ������������ ��� ��������� �������� ����� ������ �� MAP.
	 */
	public static Map<Integer, Double> getRelativeInfluenceMap(CountryEntity country) {
		Map<Integer, Double> map = new HashMap<Integer, Double>();
		for(KeyValueDbl keyValue : country.getRelativeInfluenceMapList()) {
			map.put(keyValue.getKey(), keyValue.getValue());
		}
		return map;
	}
	
	public static void putRelativeInfluenceMap(CountryEntity country, Integer key, Double value) {
		boolean valueSetted = false;
		for(KeyValueDbl keyValue : country.getRelativeInfluenceMapList()) {
			if(keyValue.getKey() == key.intValue()) {
				keyValue.setValue(value);
				valueSetted = true;
			}
		}
		if(!valueSetted) {
			KeyValueDbl keyValue = new KeyValueDbl();
			keyValue.setKey(key);
			keyValue.setValue(value);
			country.getRelativeInfluenceMapList().add(keyValue);
		}
	}
	
	/**
	 * ������������ ������ ��� ��������� ��������. ������ ������������ ��� ��������� �������� ����� ������ �� MAP.
	 */
	public static Map<Integer, Boolean> getPlayerAnswers(PlayerEntity player) {
		Map<Integer, Boolean> map = new HashMap<Integer, Boolean>();
		for(KeyValueBool keyValue : player.getPlayerAnswerList()) {
			map.put(keyValue.getKey(), keyValue.isValue());
		}
		return map;
	}
	
	public static void putPlayerAnswers(PlayerEntity player, Integer key, Boolean value) {
		boolean valueSetted = false;
		for(KeyValueBool keyValue : player.getPlayerAnswerList()) {
			if(keyValue.getKey() == key.intValue()) {
				keyValue.setValue(value);
				valueSetted = true;
			}
		}
		if(!valueSetted) {
			KeyValueBool keyValue = new KeyValueBool();
			keyValue.setKey(key);
			keyValue.setValue(value);
			player.getPlayerAnswerList().add(keyValue);
		}
	}
	
	private static double getRelativeInfluenceValue(CountryEntity hostCountry, CountryEntity initiatorCountry) {
		// �������� ������� ������ ���������� ����������� �� ������� ������, ������� ����������� ��������������
		Double iRelInf = getRelativeInfluenceMap(initiatorCountry).get(hostCountry.getCode());
		if (iRelInf != null && iRelInf.doubleValue() > 0.0) {
			return iRelInf.doubleValue();
		} else {
			return 0.0;
		}
	}
	
	public static void updateRelativeInfluenceValue(CountryEntity hostCountry, CountryEntity initiatorCountry, boolean currentDealBetter) {
		// �������� ������� ������, ������� �������������� �����������, �� ������ ���������� �����������
		Double hRelInf = getRelativeInfluenceMap(hostCountry).get(initiatorCountry.getCode());
		// ���� ������� ������ ����������� � ������������ ������ ����������, �� � ������� �� ���������� ������,
		// ���� ������� ������ �������� �������, �� � ������� �� ������ ���������� �����������
		if(currentDealBetter && hRelInf != null) {
			hRelInf = new Double(hRelInf.doubleValue() + RELATIVE_INFLUENCE_ADD);
		} else if(currentDealBetter && hRelInf == null) {
			hRelInf = new Double(RELATIVE_INFLUENCE_ADD);
		} else if(hRelInf != null) {
			hRelInf = new Double(hRelInf.doubleValue() - RELATIVE_INFLUENCE_SUB);
		}
		if(hRelInf != null && hRelInf.doubleValue() > 0.0 && hRelInf.doubleValue() <= MAX_RELATIVE_INFLUENCE) {
			putRelativeInfluenceMap(hostCountry, initiatorCountry.getCode(), hRelInf);
		} else if(hRelInf != null && hRelInf.doubleValue() > 0.0 && hRelInf.doubleValue() > MAX_RELATIVE_INFLUENCE) {
			putRelativeInfluenceMap(hostCountry, initiatorCountry.getCode(), MAX_RELATIVE_INFLUENCE);
		} else {
			putRelativeInfluenceMap(hostCountry, initiatorCountry.getCode(), new Double(0.0));
		}
	}
	
	public static CountryEntity getCountryByCode(int code, List<PlayerEntity> players) {
		for(PlayerEntity player : players) {
			if(player.getCountry().getCode() == code) {
				return player.getCountry();
			}
		}
		return null;
	}
	
	public static boolean testRelationshipBetweenUnions(Set<CountryEntity> in, Set<CountryEntity> rec, boolean isGood) {
		boolean relative = false;
		for(CountryEntity inCountry : in) {
			for(CountryEntity recCountry : rec) {
				Double val = CountryHelper.getRelativeInfluenceMap(inCountry).get(recCountry.getCode());
				if(val != null) {
					if(isGood) {
						relative = val.doubleValue() >= CountryHelper.RELATIVE_INFLUENCE_FOR_UNION;
						if(!relative) {
							return false;
						}
					} else {
						relative = val.doubleValue() < CountryHelper.RELATIVE_INFLUENCE_FOR_UNION;
						if(relative) {
							return true;
						}
					}
				}
			}
		}
		return relative;
	}
	
	public static void applyUnion(Set<CountryEntity> in, Set<CountryEntity> rec, boolean addUnion) {
		for(CountryEntity inCountry : in) {
			for(CountryEntity recCountry : rec) {
				if(addUnion) {
					inCountry.getUnionCountryCodeList().add(Integer.valueOf(recCountry.getCode()));
					inCountry.setUnionDelta(CountryHelper.UNION_CONFIRM_ZERO_PERCENT);
				} else {
					inCountry.getUnionCountryCodeList().remove(Integer.valueOf(recCountry.getCode()));
					inCountry.setUnionDelta(CountryHelper.DEFAULT_CONFIRM_ZERO_PERCENT);
				}
			}
		}
	}
	
	public static void initCountries(Level level) {
		for(CountryEntity country : level.getHeaderInfo().getCountryList()) {
			country.setUnionDelta(CountryHelper.DEFAULT_CONFIRM_ZERO_PERCENT);
		}
		for(PlayerEntity player : level.getPlayers()) {
			player.getCountry().setUnionDelta(CountryHelper.DEFAULT_CONFIRM_ZERO_PERCENT);
		}
	}
	
	public static boolean isOneDealBetterThenTwo(double one, double two) {
		return (new ItemSetCompareHelper(DEFAULT_CONFIRM_ZERO_PERCENT, two)).compareWith(one);
	}
	
	public static boolean isOneDealBetterThenTwoForUnion(double one, double two) {
		return (new ItemSetCompareHelper(UNION_CONFIRM_ZERO_PERCENT, two)).compareWith(one);
	}
	
	public static boolean isOneDealBetterThenTwoForUnion(double one, double two, CountryEntity country) {
		return (new ItemSetCompareHelper(country.getUnionDelta(), two)).compareWith(one);
	}
}
