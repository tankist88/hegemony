package ru.hegemony.logic.helper;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import ru.hegemony.xml.data.ItemEntity;
import ru.hegemony.xml.data.OpinionTemplateEntity;
import ru.hegemony.xml.data.PatternType;

public class OpinionHelper {
	/** ���������� ������� ����� ������������ */
	private static final double NUMBER_OF_SCALE_POINTS = 20.0;
	
	/** ����� ������������ ��������� ������ � ������ ��������� */
	private static Map<Integer, Double> scaleMap = null;

	/** 
	 * ������������� ����� ������������
	 * @return ����� ������������ ��������� ������ � ������ ���������
	 */
	private static Map<Integer, Double> getScaleMap(double max, double min) {
		Map<Integer, Double> resultMap = new HashMap<Integer, Double>();
		double range = Math.abs((max - min) / NUMBER_OF_SCALE_POINTS);
		double value = min + range;
		for(int i = 1; i <= NUMBER_OF_SCALE_POINTS; i++) {
			resultMap.put(i, value);
			value += range;
		}
		return resultMap;
	}
	
	public static int getScalePoint(double resDelta, double max, double min) {
		Iterator<Integer> scaleIter = scaleMap.keySet().iterator();
		if(resDelta >= max) {
			return (int) NUMBER_OF_SCALE_POINTS;
		} else if(resDelta <= min) {
			return 1;
		}
		while(scaleIter.hasNext()) {
			Integer key = scaleIter.next();
			Double scalePoint = scaleMap.get(key);
			if(resDelta < scalePoint.doubleValue()) {
				return key.intValue();
			}
		}
		return -1;
	}
	
	/**
	 * ������ ������� ������ �� XML ����� � ��������� ��� ������� ������
	 * @param itemCount - ���������� ��������� � ������
	 * @return ����������� ������� ������ textPatternFor...Item
	 */
	private static Map<Integer, List<String>> getTextPatternMap(int itemCount, PatternType patternType) {
		Map<Integer, List<String>> textPatternMap = new HashMap<Integer, List<String>>();
		switch(itemCount) {
			case 1: {
				// ���� ������� � �����������
				for(OpinionTemplateEntity template : patternType.getForOneItemList()) {
					textPatternMap.put(template.getGrade(), template.getPatternList());
				}
			} break;
			case 2: {
				// ��� �������� � �����������
				for(OpinionTemplateEntity template : patternType.getForTwoItemList()) {
					textPatternMap.put(template.getGrade(), template.getPatternList());
				}
			} break;
			case 3: {
				// ��� �������� � �����������
				for(OpinionTemplateEntity template : patternType.getForThreeItemList()) {
					textPatternMap.put(template.getGrade(), template.getPatternList());
				}
			} break;
			case 4: {
				// ������ �������� � �����������
				for(OpinionTemplateEntity template : patternType.getForFourItemList()) {
					textPatternMap.put(template.getGrade(), template.getPatternList());
				}
			} break;
			case 5: {
				// ���� ��������� � �����������
				for(OpinionTemplateEntity template : patternType.getForFiveItemList()) {
					textPatternMap.put(template.getGrade(), template.getPatternList());
				}
			} break;
			default: throw new IllegalStateException("Support only 5 or less then 5 item count");
		}
		return textPatternMap;
	}
	
	/**
	 * ���������� ����� ������ ������ � ������ ���������
	 * @param resDelta - ��������� �������
	 * @param itemSet - ����� ���������
	 * @param patternType - ���������� �������� ������
	 * @return �����, ���������� ������ ������ �� ������ ������ ���������
	 */
	public static String getSimpleOpinionText(double resDelta, List<ItemEntity> itemSet, PatternType patternType, double max, double min) {
		String text = null;
		
		if(scaleMap == null) {
			scaleMap = getScaleMap(max, min);
		}
		
		int aggrGrade = getScalePoint(resDelta, max, min);
		int itemSetSize = itemSet.size();
		
		List<String> patternList = null;
		switch(itemSetSize) {
			case 1: {
				// ���� ������� � �����������
				patternList = getTextPatternMap(1, patternType).get(aggrGrade);
			} break;
			case 2: {
				// ��� �������� � �����������
				patternList = getTextPatternMap(2, patternType).get(aggrGrade);
			} break;
			case 3: {
				// ��� �������� � �����������
				patternList = getTextPatternMap(3, patternType).get(aggrGrade);
			} break;
			case 4: {
				// ������ �������� � �����������
				patternList = getTextPatternMap(4, patternType).get(aggrGrade);
			} break;
			case 5: {
				// ���� ��������� � �����������
				patternList = getTextPatternMap(5, patternType).get(aggrGrade);
			} break;
			default: throw new IllegalStateException("Support only 5 or less then 5 item count");
		}
		
		if(patternList != null && patternList.size() > 0) {
			int index = 0;
			if(patternList.size() - 1 > 0) {
				index = new Random(System.currentTimeMillis()).nextInt(patternList.size() - 1);
			}
			text = patternList.get(index);
			for(int i = 0; i < itemSet.size(); i++) {
				ItemEntity item = itemSet.get(i);
				// a - ���������� �� �������� ��� ������� ��������� �����������
				// i - ���������� �� �������� �������� �������� �����������
				// � ������ ����� ����� ���� ������� <bis>, �� �������� 
				// ���������� �������� � �������� ��� ���������� ���������� �����������,
				// ������� ���������� �� ����� ���������� ���������
				text = text.replaceAll("<a" + (i + 1) + ">", ItemHelper.getItemActionByCode(item.getSa(), item.getActionList()).getName().toLowerCase());
				text = text.replaceAll("<i" + (i + 1) + ">", item.getName().toLowerCase());
			}
		} else {
			System.err.println("�� ������� ��������� ���������� �������� ������. PatternList is null or is empty. ResDelta=" + resDelta + ". AggrGrade=" + aggrGrade);
		}
		
		return text;
	}
}
