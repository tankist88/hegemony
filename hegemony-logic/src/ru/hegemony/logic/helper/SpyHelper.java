package ru.hegemony.logic.helper;

public class SpyHelper {
    public static double getSpySuccessProbability(double sumFromSpy) {
        if(sumFromSpy > 0.0) {
            return 1 - Math.pow(Math.E, -0.005 * sumFromSpy);
        } else {
            return 0.0;
        }
    }
}
