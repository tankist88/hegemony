package ru.hegemony.logic.action;

import java.util.List;

import ru.hegemony.logic.data.LevelDifficulty;
import ru.hegemony.logic.data.LevelStatus;
import ru.hegemony.logic.data.TurnStatusForCountry;
import ru.hegemony.logic.data.UnionDealData;
import ru.hegemony.logic.data.UnionInfo;
import ru.hegemony.xml.data.CountryEntity;
import ru.hegemony.xml.data.ItemEntity;

public interface LevelController extends Controller {
	/**
	 * ������� ����������� ���� ������� - ����������
	 * @param initiatorCountry - ������ ���������
	 * @param itemSet - ����� ���������, ������� ������ ����������
	 * @return ������, �� �������� ����� ������ ����� ������ ������� �����������, � ����� ��������
	 */
	public List<TurnStatusForCountry> sendDealToAll(CountryEntity initiatorCountry, List<ItemEntity> itemSet) throws Exception;
	/**
	 * ��������� �������� ������� ����������� ������
	 * @return - ������� ������ ������
	 */
	public LevelStatus getLevelStatus();
	/**
	 * ��������� ������������� ����� �����
	 */
	public int getRealMaxTurns();
	/**
	 * ��������� ����������
	 */
	public CountryEntity getWinner();
	/**
	 * �������� ���������� ������
	 */
	public CountryController getCountryController();
	/**
	 * �������� ������ ������ �������� ��� ����������
	 */
	public double getBonusForWinner();
	/**
	 * ���������� ������� ��������� �� ������� ������
	 * @param difficulty - ������� ���������
	 */
	public void setLevelDifficulty(LevelDifficulty difficulty);
	/**
	 * �������� ���������� �������� ����
	 */
	public List<TurnStatusForCountry> getCurrentTurnResults();
	/**
	 * ���������� ���� ������
	 * @param unionDeal - ������, ����������� ���� � �� ��������
	 * @param playerAnswer - true, ���� ����� ���������� � ������
	 * @return true - ����������� � ����� �������
	 */
	public boolean suggestUnion(UnionDealData unionDeal, boolean playerAnswer);
	/**
	 * ��������� ����� �� ����� ���������� ���� ������
	 * @param unionDeal - ������, ����������� ���� � �� ��������
	 * @return true - ������ ����� ������� ����, false - ���� �� ����� ������
	 */
	public boolean checkSuggestUnion(UnionDealData unionDeal);
	/**
	 * ��������� ����� �� ��������� ���� � �����-������ �������
	 * @param unionDeal - ������, ����������� ���� � �� ��������
	 */
	public boolean breakTheUnionIfNeed(UnionDealData unionDeal);
	/**
	 * ������������� ��������� ���� � ��������� �������
	 * @param unionDeal - ������, ����������� ���� � �� ��������
	 */
	public void forceBreakTheUnion(UnionDealData unionDeal);
	/**
	 * �������� ������ � ������ �� ������ �������� ����
	 * @return ������ � ������ ����������� ������
	 */
	public List<UnionInfo> getUnionInfo();
	/**
	 * �������� �������� �� ������ ����������
	 * @param country1 - ������ ������ 
	 * @param country2 - ������ ������
	 * @return true - ������ ��������
	 */
	public boolean countryInUnion(CountryEntity country1, CountryEntity country2);
}
