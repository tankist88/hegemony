package ru.hegemony.logic.action;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import ru.hegemony.common.data.MainResources;
import ru.hegemony.logic.data.DifficultyKeys;
import ru.hegemony.logic.data.Level;
import ru.hegemony.logic.helper.CountryHelper;
import ru.hegemony.logic.helper.ItemHelper;
import ru.hegemony.logic.helper.MathHelper;
import ru.hegemony.logic.helper.OpinionHelper;
import ru.hegemony.logic.helper.SpyHelper;
import ru.hegemony.logic.strategy.Strategy;
import ru.hegemony.logic.strategy.StrategyFactory;
import ru.hegemony.logic.strategy.StrategyType;
import ru.hegemony.xml.data.CountryEntity;
import ru.hegemony.xml.data.ItemEntity;
import ru.hegemony.xml.data.PlayerEntity;
import ru.hegemony.xml.data.ResourceDeltaEntity;
import ru.hegemony.xml.data.ResourcesEntity;
import ru.hegemony.xml.data.TurnEntity;

public class CountryControllerImpl implements CountryController {
	private static final long serialVersionUID = 1673511423644268414L;
	
	private Level level;
	private Set<ItemEntity> selectedItems;
	private volatile Map<CountryEntity, List<ItemEntity>> countryTurnMap;
    private volatile  Map<CountryEntity, Set<Integer>> spyMap;
	
	protected CountryControllerImpl(Level level) {
		this.level = level;
		this.countryTurnMap = new ConcurrentHashMap<CountryEntity, List<ItemEntity>>();
        this.spyMap = new ConcurrentHashMap<CountryEntity, Set<Integer>>();
	}

	@Override
	public boolean confirmDeal(PlayerEntity player, List<ItemEntity> itemSet, CountryEntity initiatorCountry) throws Exception {
		Boolean answer = CountryHelper.getPlayerAnswers(player).get(ItemHelper.getItemSetHashCode(itemSet));
		CountryEntity country = player.getCountry();
		if(answer != null) {
			CountryHelper.updateRelativeInfluenceValue(country, initiatorCountry, answer);
			return answer;
		}
		
		DifficultyKeys dk = DifficultyKeys.getInstance(country);
		List<ResourceDeltaEntity> optDelta = level.getItemSetEffectList(dk.getCurrentOptCode(level.getCurrentTurn()), country);
		List<ResourceDeltaEntity> dealDelta = level.getItemSetEffectList(itemSet, country);
		List<ResourceDeltaEntity> currentDelta = level.getItemSetEffectList(getItemSetForSendDeal(country), country);
		
		double optK = CountryHelper.getResDeltaKoef(country, optDelta);
		double dealK = CountryHelper.getResDeltaKoef(country, dealDelta);
		double curK = CountryHelper.getResDeltaKoef(country, currentDelta);
		
		return 	CountryHelper.dealBetterThenOpt(country, initiatorCountry, optK, dealK, level.getPlayers())
				|| CountryHelper.dealBetterThenOpt(country, initiatorCountry, curK, dealK, level.getPlayers());
	}
	
	@Override
	public List<ItemEntity> getItemSetForSendDeal(CountryEntity country) {
		List<ItemEntity> result = countryTurnMap.get(country);
		if(result == null) {
			Strategy strategy = StrategyFactory.getInstance(StrategyType.DEFAULT);
			String key = strategy.getTurnKey(country, level);
			if(key != null) {
				result = level.getItemSetFromString(key);
			} else {
				result = level.getItemSetFromString(StrategyFactory.getInstance().getTurnKey(country, level));
			}
			synchronized (countryTurnMap) {
				countryTurnMap.put(country, result);
			}
		}
		return result;
	}
	
	@Override
	public void setResourcesFromDelta(CountryEntity country, List<ItemEntity> itemSet) throws Exception {
		final List<ResourceDeltaEntity> resourcesDelta = level.getItemSetEffectList(itemSet, country);
		for(ResourceDeltaEntity delta : resourcesDelta) {
			for(ResourcesEntity res : country.getResourceList()) {
				if(res.getCode() == delta.getResourceCode()) {
					double cVal = res.getValue();
					cVal += delta.getDelta();
					if(cVal > CountryHelper.ZERO_RESOURCE) {
						res.setValue(cVal);
					} else {
						res.setValue(CountryHelper.ZERO_RESOURCE);
					}
				}
			}
		}
	}
	
	@Override
	public List<ItemEntity> getWorstItemSetForCountry(CountryEntity country) {
		return level.getItemSetFromString(country.getWorstItemSet());
	}
	
	@Override
	public Level getLevel() {
		return level;
	}
	
	@Override
	public String getCountryOpinion(CountryEntity country, List<ItemEntity> itemSet, String bisPrefix) throws Exception {
		// �������� ������ �� �������� ������ ���������.
		// � ������ ������ ����� ���� ������������������ ��������, ������� ����� �������� �� �������� 
		// ��������� � �������� �� ���������� ���������� �� ������� ������ ��� ������ ������ ���������.
		List<ItemEntity> bestItemSet = getItemSetForSendDeal(country);
		
		double currResDelta = level.getItemSetEffect(itemSet, country);
		double bestResDelta = level.getItemSetEffect(bestItemSet, country);
		
		double resDelta = CountryHelper.isOneDealBetterThenTwo(currResDelta, bestResDelta) ? 1.0 : currResDelta;
		
		List<TurnEntity> turnList = level.getHeaderInfo().getCountriesTurnList();
		TurnEntity countryTurns = null;
		for(TurnEntity turn : turnList) {
			if(turn.getCountryCode() == country.getCode()) {
				countryTurns = turn;
			}
		}
		
		double max = level.getItemSetEffect(countryTurns.getGkList().get(0), country);
		double min = level.getItemSetEffect(country.getWorstItemSet(), country);
		
		String currentOpinion = OpinionHelper.getSimpleOpinionText(
				resDelta, 
				itemSet, 
				level.getOpinionPatterns(), 
				max, 
				min);
		
		String bestOpinionPart = "";
		for(int i = 0; i < bestItemSet.size(); i++) {
			ItemEntity item = bestItemSet.get(i);
			bestOpinionPart += ItemHelper.getItemActionByCode(item.getSa(), item.getActionList()).getName().toLowerCase() + " " + item.getName().toLowerCase();
			if(i < bestItemSet.size() - 1) {
				bestOpinionPart += ", ";
			}
		}
		if(bisPrefix != null && ItemHelper.itemListContainsAtLeastOneElementFrom(itemSet, bestItemSet)) {
			return currentOpinion.replaceAll("::bis::", bisPrefix + " " + bestOpinionPart);
		} else {
			return currentOpinion.replaceAll("::bis::", bestOpinionPart);
		}
	}
	
	public Set<ItemEntity> getSelectedItems() {
		return selectedItems;
	}
	public void setSelectedItems(Set<ItemEntity> selectedItems) {
		this.selectedItems = selectedItems;
	}

	@Override
	public void applyBonus(CountryEntity country, double bonus) {
		for(ResourcesEntity res : country.getResourceList()) {
			double cVal = res.getValue() + bonus;
			if(cVal > CountryHelper.ZERO_RESOURCE) {
				res.setValue(cVal);
			} else {
				res.setValue(CountryHelper.ZERO_RESOURCE);
			}
		}
	}

	@Override
	public void setResourceByCode(CountryEntity country, int resourceCode, double newVal) {
		for(ResourcesEntity cRes : country.getResourceList()) {
			if(cRes.getCode() == resourceCode) {
				if(newVal > CountryHelper.ZERO_RESOURCE) {
					cRes.setValue(newVal);
				} else {
					cRes.setValue(CountryHelper.ZERO_RESOURCE);
				}
			}
		}
	}

    @Override
    public boolean spySuccess(CountryEntity initiatorCountry, CountryEntity destCountry) {
        return spySuccess(initiatorCountry, destCountry, Double.NEGATIVE_INFINITY);
    }

    @Override
    public boolean spySuccess(CountryEntity initiatorCountry, CountryEntity destCountry, double creditVal) {
        Set<Integer> spyCountryCodes = spyMap.get(initiatorCountry);
        if(spyCountryCodes != null) {
            for(Integer code : spyCountryCodes) {
                if(code.intValue() == destCountry.getCode()) {
                    return true;
                }
            }
        }
        if(creditVal > 0.0) {
            double oldCreditValue = CountryHelper.getResourceByCode(MainResources.CREDITS.getCode(), initiatorCountry).getValue();
            double newCreditValue = oldCreditValue - creditVal;
            if(newCreditValue < 0.0) {
                return false;
            }
            setResourceByCode(initiatorCountry, MainResources.CREDITS.getCode(), newCreditValue);
            boolean spySuccess = MathHelper.nextBernoulli(SpyHelper.getSpySuccessProbability(creditVal));
            if(spySuccess) {
                if(spyCountryCodes == null) {
                    spyCountryCodes = new HashSet<Integer>();
                }
                spyCountryCodes.add(destCountry.getCode());
                spyMap.put(initiatorCountry, spyCountryCodes);
            }
            return spySuccess;
        } else {
            return false;
        }
    }

    protected void clearTurnMap() {
		this.countryTurnMap = new ConcurrentHashMap<CountryEntity, List<ItemEntity>>();
        this.spyMap = new ConcurrentHashMap<CountryEntity, Set<Integer>>();
	}
}
