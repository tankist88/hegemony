package ru.hegemony.logic.action;

import java.io.InputStream;
import java.util.List;
import java.util.Set;

import ru.hegemony.logic.data.LevelInitData;
import ru.hegemony.logic.data.LevelStatus;
import ru.hegemony.logic.data.TurnStatusForCountry;
import ru.hegemony.logic.data.UnionDealData;
import ru.hegemony.logic.data.UnionInfo;
import ru.hegemony.xml.data.CountryEntity;
import ru.hegemony.xml.data.ItemEntity;
import ru.hegemony.xml.data.PlayerEntity;

/**
 * ��������� ����������� �������������� ������ ���� � ������� � ������������� (����������� �������������� � �. �.)
 * @author Ustinov A. V.
 */
public interface GameController extends Controller {
	/**
	 * ������������� ������
	 * @param initData - ������ ��� ������������� ������
	 * @see LevelInitData
	 */
	public void initLevel(LevelInitData initData) throws Exception;
	/**
	 * ���������� ����� ����� ��������� ����� ������ ���� ������� �� ������� ����.
	 * ����� ������ ��������� ��, ���������������� �� �������� "����������", �������
	 * ������� ���� ����� �������� ������ ��������� ��� ������
	 * @param country - ������ ��� ������� ����� �������� ��� 
	 * @return - ����������� ����� ��������� ��� �������� ����
	 */
	public List<ItemEntity> getItemSetForSendDeal(CountryEntity country);
	/**
	 * ��������� ������ ���������� ��� NULL ���� ���������� ��� ���.
	 */
	public CountryEntity getWinner();
	/**
	 * �������� ������ ������ �������� ��� ����������
	 */
	public double getBonusForWinner();
	/**
	 * �������� ������ ������ � ������ ��������� (�����������)
	 * @param country - ������ ��� ������� �������� ������
	 * @param itemSet - ����� ��������� �� �������� �������� ������
	 * @param bisPrefix - �������, ������� ����� ������������ ����� ������������� ������� ��� ������ ������ ���������,
	 * ���� ������ ���� �� ��������� ������� ������ ���������� � ������� ������
	 * @return ������ ������ � ������ ���������
	 */
	public String getCountryOpinion(CountryEntity country, List<ItemEntity> itemSet, String bisPrefix) throws Exception;
	/**
	 * �������� ��������� ������� ��������.
	 * ���������� ����� �����������, � ������� ������ setSelectedItems, ���������.
	 * ����� ���������� ��� ��������� ���������, ��������� ������� ���������. 
	 * @return - ��������� (Set) ��������� ���������
	 */
	public Set<ItemEntity> getSelectedItems();
	/**
	 * ���������� ��������� (Set) ��������� ������� ���������.
	 * ����� ���������� ��� ���������� ���������, ��������� ������� ���������.
	 * @param selectedItems - ��������� (Set) ���������
	 */
	public void setSelectedItems(Set<ItemEntity> selectedItems);
	/**
	 * ���������� ����� ������ �� ����������� (����� ���������)
	 * @param itemSetHashCode - ��� ��� ������ ���������
	 * @param answer - ��������/����� ������
	 */
	public void setHumanPlayerAnswer(int itemSetHashCode, Boolean answer);
	/**
	 * �������� ����� �������� ����
	 * @return ����� �������� ����
	 */
	public int getCurrentTurn();
	/**
	 * ��������� �������� ������� ����������� ������
	 * @see LevelStatus
	 * @return ������� ������ ������
	 */
	public LevelStatus getLevelStatus();
	/**
	 * ������� ����������� ���� ������� - ����������
	 * @param initiatorCountry - ������ ���������
	 * @param itemSet - ����� ���������, ������� ������ ����������
	 * @return ������, �� �������� ����� ������ ����� ������ ������� �����������, � ����� ��������
	 */
	public List<TurnStatusForCountry> sendDealToAll(CountryEntity initiatorCountry, List<ItemEntity> itemSet) throws Exception;
	/**
	 * ��������� ������������� ����� �����.
	 * ������������ ����� ����� ����� ���������� ��������� ��������� ������� ���������.
	 */
	public int getRealMaxTurns();
    /**
     * ���������� ������ �� ������ ������ ����� ��������� �� ������� �������� ������
     * @param initiatorCountry - ������ ��������� (�������� ������)
     * @param destCountry - ������ �� ������� �������
     * @param creditVal - ����� �� ������� ��� ����� �����, ��� ������, ��� ����� ����������������� ������ ����� ������
     * @return true - ������ ������� �������� ����������
     */
    public boolean spySuccess(CountryEntity initiatorCountry, CountryEntity destCountry, double creditVal);
    /**
     * ���������� ������ �� ������ ������ ����� ��������� �� ������� �������� ������
     * @param initiatorCountry - ������ ��������� (�������� ������)
     * @param destCountry - ������ �� ������� �������
     * @return true - ������ ������� �������� ����������
     */
    public boolean spySuccess(CountryEntity initiatorCountry, CountryEntity destCountry);
	/**
	 * �������� ����������� ��� �������� ���� ����� ���������.
	 * ��� ����������� ���������� ����� ���������, �� ������� ������ ������ �����������
	 * �� ������� ����.
	 * @param country - ������ ��� ������� ��������� �������� ����������� ����� ���������
	 * @return ����������� ��� �������� ���� ����� ��������� ��� ��������� ������
	 */
	public List<ItemEntity> getCurrentOptimalItemSet(CountryEntity country);
	/**
	 * �������� xml � ��������� ����� ��� ���������� � ����.
	 * ���������� ��� ���������� ������ ��� ���������� ����������� ������.
	 * @return xml � ��������� ����� ��� ���������� � ����
	 */
	public String getPlayersState();
	/**
	 * �������� ������ ������ �� ����
	 * @return ������ ������ �� ����
	 */
	public PlayerEntity getHumanPlayerFromCache(InputStream in) throws Exception;
	/**
	 * �������� ������ �������� �� ����
	 * @return ������ �������� �� ����
	 */
	public PlayerEntity getHegemonFromCache(InputStream in) throws Exception;
	/**
	 * ���������� ���� ������
	 * @param unionDeal - ������, ����������� ���� � �� ��������
	 * @param playerAnswer - true, ���� ����� ���������� � ������
	 * @return true - ����������� � ����� �������
	 */
	public boolean suggestUnion(UnionDealData unionDeal, boolean playerAnswer);
	/**
	 * ��������� ����� �� ����� ���������� ���� ������
	 * @param unionDeal - ������, ����������� ���� � �� ��������
	 * @return true - ������ ����� ������� ����, false - ���� �� ����� ������
	 */
	public boolean checkSuggestUnion(UnionDealData unionDeal);
	/**
	 * ��������� ����� �� ��������� ���� � �����-������ �������
	 * @param unionDeal - ������, ����������� ���� � �� ��������
	 */
	public boolean breakTheUnionIfNeed(UnionDealData unionDeal);
	/**
	 * ������������� ��������� ���� � ��������� �������
	 * @param unionDeal - ������, ����������� ���� � �� ��������
	 */
	public void forceBreakTheUnion(UnionDealData unionDeal);
	/**
	 * �������� ������ � ������ �� ������ �������� ����
	 * @return ������ � ������ ����������� ������
	 */
	public List<UnionInfo> getUnionInfo();
	/**
	 * �������� �������� �� ������ ����������
	 * @param country1 - ������ ������ 
	 * @param country2 - ������ ������
	 * @return true - ������ ��������
	 */
	public boolean countryInUnion(CountryEntity country1, CountryEntity country2);
	/**
	 * �������� ����� �� ������ ��������� ��� ������
	 * @param country - ������
	 * @param itemSet - ����� ���������
	 * @return ������ �� ������ ���������
	 */
	public double getItemSetEffect(CountryEntity country, List<ItemEntity> itemSet);
}
