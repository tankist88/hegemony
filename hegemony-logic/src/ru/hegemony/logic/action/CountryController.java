package ru.hegemony.logic.action;

import java.util.List;
import java.util.Set;

import ru.hegemony.xml.data.CountryEntity;
import ru.hegemony.xml.data.ItemEntity;
import ru.hegemony.xml.data.PlayerEntity;

public interface CountryController extends Controller {
	/**
	 * ���������� ����������� �� �����������
	 * @param player - �����, ��� �������� ������������ ����������� �� ����������� ��� ���
	 * @param itemSet - ����� ���������
	 * @param initiatorCountry - �����, ������� ���������� ����� ���������
	 * @return true - ����������� �������, false - �� �������
	 * @throws Exception
	 */
	public boolean confirmDeal(PlayerEntity player, List<ItemEntity> itemSet, CountryEntity initiatorCountry) throws Exception;
	/**
	 * ���������� ����� ����� ��������� ����� ������ ���� ������� �� ������� ����.
	 * ���������� ����������� ����� ��������� ��� ���� �����. 
	 * @param country - ������ ��� ������� ����� �������� ��� 
	 * @return - ����������� ����� ��������� ��� �������� ����
	 */
	public List<ItemEntity> getItemSetForSendDeal(CountryEntity country);
	/**
	 * ��������� ��������� �������� ��� ������
	 * @param country - ������ ��� ������� ����������� ��������� ��������
	 * @param itemSet - ����� ���������
	 * @throws Exception
	 */
	public void setResourcesFromDelta(CountryEntity country, List<ItemEntity> itemSet) throws Exception;
	/**
	 * �������� ��������� ����� ��������� ��� ������
	 * @param country - ������
	 */
	public List<ItemEntity> getWorstItemSetForCountry(final CountryEntity country);
	/**
	 * �������� ������ ������ � ���������
	 * @param country - ������, ��� ������� �������� ������
	 * @param itemSet - ����� ��������, �� �������� �������� ������
	 * @param bisPrefix - �������, ������� ����� ������������ ����� ������������� ������� ��� ������ ������ ���������,
	 * ���� ������ ���� �� ��������� ������� ������ ���������� � ������� ������
	 * @return ������ ������ � ������ ���������
	 */
	public String getCountryOpinion(CountryEntity country, List<ItemEntity> itemSet, String bisPrefix) throws Exception;
	/**
	 * �������� ��������� ������� ��������
	 * @return - ��������� (Set) ��������� ���������
	 */
	public Set<ItemEntity> getSelectedItems();
	/**
	 * ���������� ��������� (Set) ��������� ������� ���������
	 * @param selectedItems - ��������� (Set) ���������
	 */
	public void setSelectedItems(Set<ItemEntity> selectedItems);
	/**
	 * ��������� ���������� ����� � �������� ������
	 * @param country - ������ ��� �������� ������� ����������� �����
	 * @param bonus - �������� ������
	 */
	public void applyBonus(CountryEntity country, double bonus);
	/**
	 * ���������� �������� ����������� ������� ��� ������
	 * @param country - ������
	 * @param resourceCode - ��� �������
	 * @param newVal - �������� �������
	 */
	public void setResourceByCode(CountryEntity country, int resourceCode, double newVal);
    /**
     * ���������� ������ �� ������ ������ ����� ��������� �� ������� �������� ������
     * @param initiatorCountry - ������ ��������� (�������� ������)
     * @param destCountry - ������ �� ������� �������
     * @param creditVal - ����� �� ������� ��� ����� �����, ��� ������, ��� ����� ����������������� ������ ����� ������
     * @return true - ������ ������� �������� ����������
     */
    public boolean spySuccess(CountryEntity initiatorCountry, CountryEntity destCountry, double creditVal);
    /**
     * ���������� ������ �� ������ ������ ����� ��������� �� ������� �������� ������
     * @param initiatorCountry - ������ ��������� (�������� ������)
     * @param destCountry - ������ �� ������� �������
     * @return true - ������ ������� �������� ����������
     */
    public boolean spySuccess(CountryEntity initiatorCountry, CountryEntity destCountry);
}
