package ru.hegemony.logic.action;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;
import ru.hegemony.logic.data.DifficultyKeys;
import ru.hegemony.logic.data.Level;
import ru.hegemony.logic.data.LevelDifficulty;
import ru.hegemony.logic.data.LevelStatus;
import ru.hegemony.logic.data.TurnStatusForCountry;
import ru.hegemony.logic.data.UnionDealData;
import ru.hegemony.logic.data.UnionInfo;
import ru.hegemony.logic.helper.CountryHelper;
import ru.hegemony.xml.data.CountryEntity;
import ru.hegemony.xml.data.ItemEntity;
import ru.hegemony.xml.data.PlayerEntity;

public class LevelControllerImpl implements LevelController {
	private static final long serialVersionUID = 7216376193981054444L;
	
	private static final int ROUND_COUNT = 1;
	
	private Level level;
	private int realMaxTurns;
	private int playersDoTurn;
	private double bonus;
	private List<TurnStatusForCountry> currentTurnResults;
	private CountryControllerImpl countryController;
	private LevelStatus levelStatus;
	private CountryEntity winner;

	protected LevelControllerImpl(Level level) {
		this.level = level;
		this.realMaxTurns = level.getHeaderInfo().getEventDesc().getTurnCount() * ROUND_COUNT;
		this.countryController = new CountryControllerImpl(level);
		this.playersDoTurn = 0;
		this.winner = null;
	}
	
	private void endTurn(boolean dealConfirmed, List<ItemEntity> itemSet) throws Exception {
		if(winner != null && levelStatus != null && !levelStatus.equals(LevelStatus.LEVEL_CONTINUES)) {
			// ���������� ��� �����������
			return;
		}
		if(dealConfirmed) {
			levelStatus = LevelStatus.LEVEL_END_WIN;
			// ���������� ���������� ������ ��������� ��� ���� �����
			for(PlayerEntity player : level.getPlayers()) {
				countryController.setResourcesFromDelta(player.getCountry(), itemSet);
			}
			String expr = level.getHeaderInfo().getEventDesc().getBonusFormula();
			expr = expr.toLowerCase().replaceAll("\\[t\\]", Integer.toString(level.getCurrentTurn() + 1));
			Expression e = new ExpressionBuilder(expr).build();
			bonus = e.evaluate();
			countryController.applyBonus(winner, bonus);
		} else if(level.getCurrentTurn() >= (realMaxTurns - 1)) {
			levelStatus = LevelStatus.LEVEL_END_LOST;
			// ���������� ���������� �� ������� ��������� ��� ���� �����
			List<ItemEntity> worstItemSet = level.getItemSetFromString(level.getHeaderInfo().getEventDesc().getWorstItemSet());
			for(PlayerEntity player : level.getPlayers()) {
				countryController.setResourcesFromDelta(player.getCountry(), worstItemSet);
			}
		} else {
			if(playersDoTurn == level.getPlayers().size() - 1) {
				countryController.clearTurnMap();
				level.increaseTurn();
				playersDoTurn = 0;
				int turnCount = level.getHeaderInfo().getEventDesc().getTurnCount();
				if(level.getCurrentTurn() > 0 && level.getCurrentTurn() % turnCount == 0) {
					level.increaseRound();
				}
			} else {
				playersDoTurn++;
			}
			levelStatus = LevelStatus.LEVEL_CONTINUES;
		}
	}

	@Override
	public List<TurnStatusForCountry> sendDealToAll(CountryEntity initiatorCountry, List<ItemEntity> itemSet) throws Exception {
		List<TurnStatusForCountry> result = new ArrayList<TurnStatusForCountry>();		
		boolean dealConfirmed = false;
		int confirmCount = 0;
		for(PlayerEntity player : level.getPlayers()) {
			if(player.getCountry().getCode() != initiatorCountry.getCode()) {
				TurnStatusForCountry cTurnStatus = new TurnStatusForCountry();
				cTurnStatus.setCountry(player.getCountry());
				if(countryController.confirmDeal(player, itemSet, initiatorCountry)) {
					confirmCount++;
					cTurnStatus.setConfirmed(true);
				} else {
					cTurnStatus.setConfirmed(false);
				}
				result.add(cTurnStatus);
			}
		}
		
		if(confirmCount == level.getPlayers().size() - 1) {
			winner = initiatorCountry;
			dealConfirmed = true;
		}
		
		DifficultyKeys.putToOutKeys(level.getItemSetStringCode(itemSet));
		
		endTurn(dealConfirmed, itemSet);
		
		currentTurnResults = result;
		
		return result;
	}

	@Override
	public LevelStatus getLevelStatus() {
		return levelStatus;
	}

	@Override
	public int getRealMaxTurns() {
		return realMaxTurns;
	}

	@Override
	public CountryEntity getWinner() {
		return winner;
	}

	@Override
	public Level getLevel() {
		return level;
	}

	@Override
	public CountryController getCountryController() {
		return countryController;
	}

	@Override
	public double getBonusForWinner() {
		return bonus;
	}

	@Override
	public void setLevelDifficulty(LevelDifficulty difficulty) {
		this.level.setDifficulty(difficulty);
	}

	@Override
	public List<TurnStatusForCountry> getCurrentTurnResults() {
		return currentTurnResults;
	}

	@Override
	public boolean suggestUnion(UnionDealData unionDeal, boolean playerAnswer) {
		if(checkSuggestUnion(unionDeal) || playerAnswer) {
			CountryHelper.applyUnion(unionDeal.getInitiatorUnion(), unionDeal.getRecieverUnion(), true);
			CountryHelper.applyUnion(unionDeal.getRecieverUnion(), unionDeal.getInitiatorUnion(), true);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean breakTheUnionIfNeed(UnionDealData unionDeal) {
		boolean goodRelative1 = CountryHelper.testRelationshipBetweenUnions(unionDeal.getInitiatorUnion(), unionDeal.getRecieverUnion(), false);
		boolean goodRelative2 = CountryHelper.testRelationshipBetweenUnions(unionDeal.getRecieverUnion(), unionDeal.getInitiatorUnion(), false);
		boolean haveUnion = CountryHelper.countryInUnion(unionDeal.getInitiator(), unionDeal.getReciever());
		if(haveUnion && (goodRelative1 || goodRelative2)) {
			forceBreakTheUnion(unionDeal);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void forceBreakTheUnion(UnionDealData unionDeal) {
		CountryHelper.applyUnion(unionDeal.getInitiatorUnion(), unionDeal.getRecieverUnion(), false);
		CountryHelper.applyUnion(unionDeal.getRecieverUnion(), unionDeal.getInitiatorUnion(), false);
	}

	@Override
	public List<UnionInfo> getUnionInfo() {
		List<List<Integer>> tmpList = new ArrayList<List<Integer>>();
		for(int i = 0; i < getLevel().getPlayers().size(); i++) {
			List<Integer> countryUnionList = new ArrayList<Integer>();
			PlayerEntity player = getLevel().getPlayers().get(i);
			countryUnionList.add(player.getCountry().getCode());
			for(Integer cCode : player.getCountry().getUnionCountryCodeList()) {
				if(!isInListList(cCode, tmpList)) {
					countryUnionList.add(cCode);
				}
			}
			tmpList.add(countryUnionList);
		}
		List<UnionInfo> result = new ArrayList<UnionInfo>();
		Set<CountryEntity> withoutUnionSet = new HashSet<CountryEntity>();
		for(List<Integer> list : tmpList) {
			Set<CountryEntity> unionSet = new HashSet<CountryEntity>();
			for(Integer val : list) {
				CountryEntity country = CountryHelper.getCountryByCode(val, getLevel().getPlayers());
				if(list.size() == 1 && country.getUnionCountryCodeList().isEmpty()) { 
					withoutUnionSet.add(country);
				} else if(list.size() > 1 && !country.getUnionCountryCodeList().isEmpty()) {
					unionSet.add(country);
				}
			}
			if(!unionSet.isEmpty()) {
				result.add(new UnionInfo(unionSet, false));
			}
		}
		result.add(new UnionInfo(withoutUnionSet, true));
		return result;
	}
	
	private boolean isInListList(Integer value, List<List<Integer>> list) {
		for(List<Integer> plist : list) {
			for(Integer listVal : plist) {
				if(listVal.equals(value)) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public boolean checkSuggestUnion(UnionDealData unionDeal) {
		boolean goodRelative1 = CountryHelper.testRelationshipBetweenUnions(unionDeal.getInitiatorUnion(), unionDeal.getRecieverUnion(), true);
		boolean goodRelative2 = CountryHelper.testRelationshipBetweenUnions(unionDeal.getRecieverUnion(), unionDeal.getInitiatorUnion(), true);
		boolean haveUnion = CountryHelper.countryInUnion(unionDeal.getInitiator(), unionDeal.getReciever());
		// ����������� �� ����, ���� ��� ��� ����� ����� �������� � ����
		// ����� �������� ������� ���������
		if(!haveUnion && goodRelative1 && goodRelative2) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean countryInUnion(CountryEntity country1, CountryEntity country2) {
		return CountryHelper.countryInUnion(country1, country2);
	}
}
