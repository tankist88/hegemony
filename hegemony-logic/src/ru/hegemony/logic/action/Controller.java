package ru.hegemony.logic.action;

import java.io.Serializable;

import ru.hegemony.logic.data.Level;

public interface Controller extends Serializable {
	public Level getLevel();
}
