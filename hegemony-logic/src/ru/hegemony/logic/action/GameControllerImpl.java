package ru.hegemony.logic.action;

import java.io.InputStream;
import java.util.List;
import java.util.Set;

import ru.hegemony.common.data.MainResources;
import ru.hegemony.logic.data.DifficultyKeys;
import ru.hegemony.logic.data.Level;
import ru.hegemony.logic.data.LevelInitData;
import ru.hegemony.logic.data.LevelStatus;
import ru.hegemony.logic.data.TurnStatusForCountry;
import ru.hegemony.logic.data.UnionDealData;
import ru.hegemony.logic.data.UnionInfo;
import ru.hegemony.logic.helper.CountryHelper;
import ru.hegemony.logic.helper.LevelHelper;
import ru.hegemony.logic.helper.MathHelper;
import ru.hegemony.logic.helper.SpyHelper;
import ru.hegemony.xml.data.CountryEntity;
import ru.hegemony.xml.data.ItemEntity;
import ru.hegemony.xml.data.PlayerEntity;
import ru.hegemony.xml.data.ResourcesEntity;
import ru.hegemony.xml.data.TurnEntity;

public class GameControllerImpl implements GameController {
	private static final long serialVersionUID = 4313679489486262906L;
	
	private Level currentLevel;
	private LevelController levelController;

	public GameControllerImpl() {
		this.currentLevel = null;
		this.levelController = null;
	}
	
	@Override
	public Level getLevel() {
		return currentLevel;
	}

	@Override
	public void initLevel(LevelInitData initData) throws Exception {
		currentLevel = new Level();
		LevelHelper.fillLevelEntity(
				initData.getPlayerName(), 
				initData.getPlayerCountryId(), 
				initData.getLevelDataSources(), 
				currentLevel
			);		
		currentLevel.setDifficulty(initData.getDifficulty());
		levelController = new LevelControllerImpl(currentLevel);
		CountryHelper.initCountries(currentLevel);
		DifficultyKeys.clearCache();
		for(TurnEntity turnList : currentLevel.getHeaderInfo().getCountriesTurnList()) {
			DifficultyKeys.getInstance(turnList, currentLevel.getDifficulty(), currentLevel.getPlayers().size());
		}
	}

	@Override
	public List<ItemEntity> getItemSetForSendDeal(CountryEntity country) {
		return levelController.getCountryController().getItemSetForSendDeal(country);
	}

	@Override
	public CountryEntity getWinner() {
		return levelController.getWinner();
	}

	@Override
	public double getBonusForWinner() {
		return levelController.getBonusForWinner();
	}

	@Override
	public String getCountryOpinion(CountryEntity country, List<ItemEntity> itemSet, String bisPrefix) throws Exception {
		return levelController.getCountryController().getCountryOpinion(country, itemSet, bisPrefix);
	}

	@Override
	public Set<ItemEntity> getSelectedItems() {
		return levelController.getCountryController().getSelectedItems();
	}

	@Override
	public int getCurrentTurn() {
		return levelController.getLevel().getCurrentTurn();
	}

	@Override
	public LevelStatus getLevelStatus() {
		return levelController.getLevelStatus();
	}

	@Override
	public List<TurnStatusForCountry> sendDealToAll(CountryEntity initiatorCountry, List<ItemEntity> itemSet) throws Exception {
		return levelController.sendDealToAll(initiatorCountry, itemSet);
	}

	@Override
	public int getRealMaxTurns() {
		return levelController.getRealMaxTurns();
	}

    @Override
	public void setSelectedItems(Set<ItemEntity> selectedItems) {
		levelController.getCountryController().setSelectedItems(selectedItems);
	}

    @Override
    public boolean spySuccess(CountryEntity initiatorCountry, CountryEntity destCountry, double creditVal) {
        return levelController.getCountryController().spySuccess(initiatorCountry, destCountry, creditVal);
    }

    @Override
    public boolean spySuccess(CountryEntity initiatorCountry, CountryEntity destCountry) {
        return levelController.getCountryController().spySuccess(initiatorCountry, destCountry);
    }

	@Override
	public List<ItemEntity> getCurrentOptimalItemSet(CountryEntity country) {
		TurnEntity turnEntity = null;
		for(TurnEntity t : getLevel().getHeaderInfo().getCountriesTurnList()) {
			if(t.getCountryCode() == country.getCode()) {
				turnEntity = t;
				break;
			}
		}
		DifficultyKeys dk = DifficultyKeys.getInstance(turnEntity, getLevel().getDifficulty(), getLevel().getPlayers().size());
		String bestKey = dk.getCurrentOptCode(getLevel().getCurrentTurn());
		return getLevel().getItemSetFromString(bestKey);
	}

	@Override
	public String getPlayersState() {
		return LevelHelper.createPlayersState(getLevel().getPlayers());
	}

	@Override
	public PlayerEntity getHumanPlayerFromCache(InputStream in) throws Exception {
		List<PlayerEntity> pList = LevelHelper.getPlayersStat(in);
		for(PlayerEntity p : pList) {
			if(!p.isArtIntSign()) {
				return p;
			}
		}
		return null;
	}
	
	@Override
	public PlayerEntity getHegemonFromCache(InputStream in) throws Exception {
		List<PlayerEntity> pList = LevelHelper.getPlayersStat(in);
		double infVal = 0.0;
		int index = 0;
		for(int i = 0; i < pList.size(); i++) {
			PlayerEntity p = pList.get(i);
			ResourcesEntity res = CountryHelper.getResourceByCode(MainResources.INFLUENCE.getCode(), p.getCountry());
			if(p.isArtIntSign() && res.getValue() > infVal) {
				infVal = res.getValue();
				index = i;
			}
		}
		return pList.get(index);
	}

	@Override
	public boolean suggestUnion(UnionDealData unionDeal, boolean playerAnswer) {
		return levelController.suggestUnion(unionDeal, playerAnswer);
	}

	@Override
	public boolean breakTheUnionIfNeed(UnionDealData unionDeal) {
		return levelController.breakTheUnionIfNeed(unionDeal);
	}

	@Override
	public void forceBreakTheUnion(UnionDealData unionDeal) {
		levelController.forceBreakTheUnion(unionDeal);
	}

	@Override
	public List<UnionInfo> getUnionInfo() {
		return levelController.getUnionInfo();
	}

	@Override
	public boolean checkSuggestUnion(UnionDealData unionDeal) {
		return levelController.checkSuggestUnion(unionDeal);
	}

	@Override
	public boolean countryInUnion(CountryEntity country1, CountryEntity country2) {
		return levelController.countryInUnion(country1, country2);
	}

	@Override
	public void setHumanPlayerAnswer(int itemSetHashCode, Boolean answer) {
		PlayerEntity player = getLevel().getHumanPlayer();
		CountryHelper.putPlayerAnswers(player, itemSetHashCode, answer);
	}

	@Override
	public double getItemSetEffect(CountryEntity country, List<ItemEntity> itemSet) {
		return getLevel().getItemSetEffect(itemSet, country);
	}
}
