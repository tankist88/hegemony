package ru.hegemony.logic.strategy;

import java.util.List;

import ru.hegemony.logic.data.Level;
import ru.hegemony.xml.data.CountryEntity;

public class DefaultStrategy extends AbstractStrategy implements Strategy {
	protected DefaultStrategy() {
	}
	@Override
	public String getTurnKey(CountryEntity country, Level level) {
		List<String> optKeys = getOptKeys(country, level.getHeaderInfo().getCountriesTurnList());
		int currentTurn = level.getCurrentTurn();
		int index = optKeys.size() - (((((int)(currentTurn/optKeys.size())) + 1) * optKeys.size()) - currentTurn);
		if(index > optKeys.size() - 1) {
			return optKeys.get(optKeys.size() - 1);
		} else {
			return optKeys.get(index);
		}
	}
	@Override
	public boolean turnKeyMayBeNull() {
		return false;
	}
}
