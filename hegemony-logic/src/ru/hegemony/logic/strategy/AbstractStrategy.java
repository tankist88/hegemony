package ru.hegemony.logic.strategy;

import java.util.List;

import ru.hegemony.logic.data.DifficultyKeys;
import ru.hegemony.logic.data.LevelDifficulty;
import ru.hegemony.xml.data.CountryEntity;
import ru.hegemony.xml.data.TurnEntity;

public abstract class AbstractStrategy {
	protected List<String> getOptKeys(CountryEntity country, List<TurnEntity> turnList) {
		List<String> optKeys = null;
		for(TurnEntity turns : turnList) {
			if(turns.getCountryCode() == country.getCode()) {
				optKeys = turns.getGkList();
				break;
			}
		}
		return optKeys;
	}
	
	protected String getOwnCurrentKey(CountryEntity country, List<TurnEntity> turnsList, LevelDifficulty diff, int turnNum) {
		DifficultyKeys ownDk = DifficultyKeys.getInstance(getTurnEntity(country, turnsList), diff, turnsList.size());
		return ownDk.getCurrentOptCode(turnNum);
	}
	
	protected TurnEntity getTurnEntity(CountryEntity country, List<TurnEntity> turnsList) {
		return getTurnEntity(country.getCode(), turnsList);
	}
	protected TurnEntity getTurnEntity(int code, List<TurnEntity> turnsList) {
		for(TurnEntity t : turnsList) {
			if(t.getCountryCode() == code) {
				return t;
			}
		}
		return null;
	}
}
