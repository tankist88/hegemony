package ru.hegemony.logic.strategy;

import ru.hegemony.logic.data.Level;
import ru.hegemony.xml.data.CountryEntity;

public interface Strategy {
	/**
	 * �������� ��� ������ ��������� ��� ���������� ����
	 * @param country - ������ ������� ������ ���
	 * @param level - ���������� �� ������
	 * @return ����� ������ ���������
	 */
	public String getTurnKey(CountryEntity country, Level level);
	/**
	 * ����� �� ��������� �� ������� ��������� ���
	 * @return true - ����� ��������� ���������� ���� ����� ������� NULL
	 */
	public boolean turnKeyMayBeNull();
}
