package ru.hegemony.logic.strategy;

import java.util.List;

import ru.hegemony.logic.data.DifficultyKeys;
import ru.hegemony.logic.data.Level;
import ru.hegemony.logic.data.LevelDifficulty;
import ru.hegemony.logic.helper.CountryHelper;
import ru.hegemony.xml.data.CountryEntity;
import ru.hegemony.xml.data.PlayerEntity;
import ru.hegemony.xml.data.TurnEntity;

public class UnionStrategy extends AbstractStrategy implements Strategy {
	protected UnionStrategy() {
	}
	@Override
	public String getTurnKey(CountryEntity country, Level level) {
		List<TurnEntity> turnList = level.getHeaderInfo().getCountriesTurnList();
		List<String> optKeys = getOptKeys(country, turnList);
		LevelDifficulty diff = level.getDifficulty();
		List<PlayerEntity> players = level.getPlayers();
		int currentTurn = level.getCurrentTurn();
		String ownCurrentKey = getOwnCurrentKey(country, turnList, diff, currentTurn);
		double ownVal = level.getItemSetEffect(ownCurrentKey, country);
		if(!country.getUnionCountryCodeList().isEmpty()) {
			// ���� ��������, ����� ������ �� ��������
			for(String gk : optKeys) {
				double gkVal = level.getItemSetEffect(gk, country);
				int unionApprovedCount = 0;
				for(Integer code : country.getUnionCountryCodeList()) {
					DifficultyKeys dk = DifficultyKeys.getInstance(getTurnEntity(code, turnList), diff, players.size());
					String ugk = dk.getCurrentOptCode(currentTurn);
					List<String> unionCountryOptKeys = null;
					for(TurnEntity turns : turnList) {
						if(turns.getCountryCode() == code.intValue()) {
							unionCountryOptKeys = turns.getGkList();
							break;
						}
					}
					String cgk = null;
					for(String t : unionCountryOptKeys) {
						if(t.equals(gk)) {
							cgk = t;
							break;
						}
					}
					if(cgk != null) {
						double cgkVal = level.getItemSetEffect(cgk, level.getCountryByCode(code));
						double ugkVal = level.getItemSetEffect(ugk, level.getCountryByCode(code));
						if(	CountryHelper.isOneDealBetterThenTwoForUnion(cgkVal, ugkVal, country)
							&& CountryHelper.isOneDealBetterThenTwoForUnion(gkVal, ownVal, country)) {
							unionApprovedCount++;
						}
					}
				}
				if(unionApprovedCount >= country.getUnionCountryCodeList().size()) {
					// ��� ������ ����� �������� �����������
					return gk;
				}
			}
		}
		return null;
	}
	@Override
	public boolean turnKeyMayBeNull() {
		return true;
	}
}
