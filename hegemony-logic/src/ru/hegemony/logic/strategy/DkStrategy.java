package ru.hegemony.logic.strategy;

import ru.hegemony.logic.data.Level;
import ru.hegemony.logic.data.LevelDifficulty;
import ru.hegemony.xml.data.CountryEntity;

public class DkStrategy extends AbstractStrategy implements Strategy {
	@Override
	public String getTurnKey(CountryEntity country, Level level) {
		LevelDifficulty diff = level.getDifficulty();
		int currentTurn = level.getCurrentTurn();
		return getOwnCurrentKey(country, level.getHeaderInfo().getCountriesTurnList(), diff, currentTurn);
	}

	@Override
	public boolean turnKeyMayBeNull() {
		return false;
	}
}
