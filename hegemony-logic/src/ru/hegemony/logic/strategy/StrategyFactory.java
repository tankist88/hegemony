package ru.hegemony.logic.strategy;

public abstract class StrategyFactory {
	public static Strategy getInstance(StrategyType type) {
		if(type != null && type.equals(StrategyType.COMMON)) {
			return new CommonStrategy();
		} else if(type != null && type.equals(StrategyType.UNION)) {
			return new UnionStrategy();
		} else if(type != null && type.equals(StrategyType.DK)) {
			return new DkStrategy();
		} else {
			return new DefaultStrategy();
		}
	}
	public static Strategy getInstance() {
		return getInstance(null);
	}
}
