package ru.hegemony.logic.strategy;

import java.util.List;

import ru.hegemony.logic.data.DifficultyKeys;
import ru.hegemony.logic.data.Level;
import ru.hegemony.logic.data.LevelDifficulty;
import ru.hegemony.logic.helper.CountryHelper;
import ru.hegemony.xml.data.CountryEntity;
import ru.hegemony.xml.data.PlayerEntity;
import ru.hegemony.xml.data.TurnEntity;

public class CommonStrategy extends AbstractStrategy implements Strategy {
	protected CommonStrategy() {
	}
	@Override
	public String getTurnKey(CountryEntity country, Level level) {
		List<TurnEntity> turnList = level.getHeaderInfo().getCountriesTurnList();
		List<String> optKeys = getOptKeys(country, turnList);
		LevelDifficulty diff = level.getDifficulty();
		List<PlayerEntity> players = level.getPlayers();
		int currentTurn = level.getCurrentTurn();
		String ownCurrentKey = getOwnCurrentKey(country, turnList, diff, currentTurn);
		double ownVal = level.getItemSetEffect(ownCurrentKey, country);
		// � ������ optKeys ����� �������� ������
		for(String gk : optKeys) {
			int countryApprovedCount = 0;
			for(PlayerEntity player : players) {
				if(player.getCountry().getCode() == country.getCode()) {
					continue;
				}
				double gkVal = level.getItemSetEffect(gk, country);
				if(CountryHelper.isOneDealBetterThenTwoForUnion(gkVal, ownVal)) {
					continue;
				}
				DifficultyKeys countryDk = DifficultyKeys.getInstance(getTurnEntity(player.getCountry(), turnList), diff, players.size());
				String countryCurrentKey = countryDk.getCurrentOptCode(currentTurn);
				List<String> countryOptKeys = null;
				for(TurnEntity turns : turnList) {
					if(turns.getCountryCode() == player.getCountry().getCode()) {
						countryOptKeys = turns.getGkList();
						break;
					}
				}
				String countryKey = null;
				for(String t : countryOptKeys) {
					if(t.equals(gk)) {
						countryKey = t;
						break;
					}
				}
				if(countryKey != null) {
					double countryVal = level.getItemSetEffect(countryKey, player.getCountry());
					double countryCurrentVal = level.getItemSetEffect(countryCurrentKey, player.getCountry());
					if(CountryHelper.countryInUnion(country, player.getCountry())) {
						if(	CountryHelper.isOneDealBetterThenTwoForUnion(countryVal, countryCurrentVal, country)
							&& CountryHelper.isOneDealBetterThenTwoForUnion(gkVal, ownVal, country)) {
							countryApprovedCount++;
						}
						
					} else {	
						if(	CountryHelper.isOneDealBetterThenTwo(countryVal, countryCurrentVal)
							&& CountryHelper.isOneDealBetterThenTwo(gkVal, ownVal)) {
							countryApprovedCount++;
						}
					}
				}
			}
			if(countryApprovedCount == players.size() - 1) {
				return gk;
			}
		}
		return null;
	}
	@Override
	public boolean turnKeyMayBeNull() {
		return true;
	}
}
