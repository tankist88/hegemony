package ru.hegemony.logic.main;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.DecimalFormat;
import java.util.List;

import ru.hegemony.common.data.MainResources;
import ru.hegemony.common.helper.DataHelper;
import ru.hegemony.logic.action.GameController;
import ru.hegemony.logic.action.GameControllerImpl;
import ru.hegemony.logic.data.DataSources;
import ru.hegemony.logic.data.Level;
import ru.hegemony.logic.data.LevelDifficulty;
import ru.hegemony.logic.data.LevelInitData;
import ru.hegemony.logic.data.LevelStatus;
import ru.hegemony.logic.data.TurnStatusForCountry;
import ru.hegemony.logic.data.UnionDealData;
import ru.hegemony.logic.data.UnionInfo;
import ru.hegemony.logic.helper.CountryHelper;
import ru.hegemony.logic.helper.ItemHelper;
import ru.hegemony.logic.helper.LevelHelper;
import ru.hegemony.xml.data.CountryEntity;
import ru.hegemony.xml.data.ItemEntity;
import ru.hegemony.xml.data.LevelList;
import ru.hegemony.xml.data.PlayerEntity;
import ru.hegemony.xml.data.ResourcesEntity;

/**
 * ����� ��� ������������ ������� ������. ��������� �� �����, �� ������� �������� �����, ���������� �������
 */
public class ConsoleMain {
	public static void main(String args[]) throws FileNotFoundException, Exception {
		List<LevelList> levelList = LevelHelper.getLevelList(new FileInputStream("res/raw/level_list.xml"));
		DataSources dataSources = new DataSources();
		dataSources.setLevelDs(new FileInputStream("res/raw/" + levelList.get(0).getFilename()));
		dataSources.setOpinionPatternsDs(new FileInputStream("res/raw/opinion_patterns.xml"));
//		dataSources.setPlayersStatDs(new FileInputStream("res/data/playersState.xml"));
		
		LevelInitData initData = new LevelInitData(levelList.get(0).getFilename(), dataSources, 1, LevelDifficulty.HARD, null);
		GameController gameController = new GameControllerImpl();
		gameController.initLevel(initData);
		
		System.out.println("�������: " + gameController.getLevel().getHeaderInfo().getEventDesc().getTitle());
		
		printResources(gameController.getLevel());
		
		boolean gameEnd = false;
		for(int i = 0; i < gameController.getRealMaxTurns(); i++) {
			System.out.println("---------------------------------");
			System.out.println("��� �" + (gameController.getCurrentTurn() + 1) + ". �����: " + gameController.getLevel().getRound());
			System.out.println("---------------------------------");
			for(PlayerEntity currentPlayer : gameController.getLevel().getPlayers()) {
				System.out.println("---------------------------------");
				
				List<ItemEntity> itemSet = gameController.getItemSetForSendDeal(currentPlayer.getCountry());
				
				System.out.println(
							"������������ ������: " + currentPlayer.getCountry().getName() 
							+ ". ��� ������: " + ((currentPlayer.getName() != null) ? currentPlayer.getName() : "<�����>")
							+ ". �������: " + CountryHelper.getResourceByCode(MainResources.INFLUENCE.getCode(), currentPlayer.getCountry()).getValue());
				System.out.println("������ �����������:");
				printItemSet(itemSet);
				
				for(PlayerEntity player : gameController.getLevel().getPlayers()) {
					System.out.println(player.getCountry().getName() + ". ������: " + gameController.getCountryOpinion(player.getCountry(), itemSet, "������"));
				}
				
				List<TurnStatusForCountry> sendResList = gameController.sendDealToAll(currentPlayer.getCountry(), itemSet);
				for(TurnStatusForCountry res : sendResList) {
					String resStr = null;
					if(res.isConfirmed()) {
						resStr = "����������� �������!";
					} else {
						resStr = "��������!";
					}
					Double d = CountryHelper.getRelativeInfluenceMap(res.getCountry()).get(currentPlayer.getCountry().getCode());
					String strVal = null;
					if(d != null) {
						strVal = new DecimalFormat("0.00").format(DataHelper.round(d.doubleValue(), 2));
					} else {
						strVal = "null";
					}
					System.out.println("������� �� ���������� �����������: " + strVal + " \t\t" + res.getCountry().getName() + " - " + resStr);
				}
				
				if(endGame(gameController)) {
					gameEnd = true;
					break;
				}
				for(PlayerEntity player : gameController.getLevel().getPlayers()) {
					if(player.getCountry().getCode() != currentPlayer.getCountry().getCode()) {
						if(gameController.breakTheUnionIfNeed(new UnionDealData(currentPlayer.getCountry().getCode(), player.getCountry().getCode(), gameController.getLevel().getPlayers()))) {
							System.out.println("������ " + currentPlayer.getCountry().getName() + " ��������� ���� �� ������� " + player.getCountry().getName());
						}
					}
				}
				for(PlayerEntity player : gameController.getLevel().getPlayers()) {
					if(player.getCountry().getCode() != currentPlayer.getCountry().getCode()) {
						if(gameController.suggestUnion(new UnionDealData(currentPlayer.getCountry().getCode(), player.getCountry().getCode(), gameController.getLevel().getPlayers()), false)) {
							System.out.println("������ " + currentPlayer.getCountry().getName() + " ��������� ���� �� ������� " + player.getCountry().getName());
						}
					}
				}
			}
			printUnionList(gameController);
			if(gameEnd) {
				break;
			}
		}
		System.out.println("����� � �������� ����������: " + gameController.getBonusForWinner());
		printResources(gameController.getLevel());
		System.out.println("*************************************************************");
	}
	
	private static boolean endGame(GameController controller) {
		System.out.println();
		if(controller.getLevelStatus() == LevelStatus.LEVEL_END_WIN) {
			System.out.println("*************************************************************");
			System.out.println("* ������ " + controller.getWinner().getName() + " ���������!");
			System.out.println("*************************************************************");
			return true;
		} else if(controller.getLevelStatus() == LevelStatus.LEVEL_END_LOST) {
			System.out.println("*************************************************************");
			System.out.println("* GAME OVER!");
			System.out.println("*************************************************************");
			return true;
		} else {
			return false;
		}
	}
	
	private static void printItemSet(List<ItemEntity> itemSet) throws Exception {
		for(ItemEntity item : itemSet) {
			System.out.println("\t�������: " + item.getName() + ". ��������: " + ItemHelper.getItemActionByCode(item.getSa(), item.getActionList()).getName());
		}
	}
	
	private static void printResources(Level levelEntity) {
		double sum = 0.0;
		for(PlayerEntity player : levelEntity.getPlayers()) {
			System.out.println("---------------------------------");
			System.out.println("������� ������: " + player.getCountry().getName());
			for(ResourcesEntity res : player.getCountry().getResourceList()) {
				System.out.println("\t��������: " + res.getName() + ". ��������: " + res.getValue());
				sum += res.getValue();
			}
		}
		System.out.println();
		System.out.println("�����: " + DataHelper.round(sum, 3));
	}
	
	private static void printUnionList(GameController gameController) {
		List<UnionInfo> unionInfoList = gameController.getUnionInfo();
		for(UnionInfo union : unionInfoList) {
			if(union.isWithoutUnion()) {
				System.out.println("---------------------------");
				System.out.println("| ��������");
				System.out.println("---------------------------");
			} else{
				System.out.println("---------------------------");
				System.out.println("| ����");
				System.out.println("---------------------------");
			}
			for(CountryEntity country : union.getCountryList()) {
				System.out.println("������: " + country.getName());
			}
			System.out.println("---------------------------");
		}
	}
}
