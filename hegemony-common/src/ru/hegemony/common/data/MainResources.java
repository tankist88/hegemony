package ru.hegemony.common.data;

import java.io.Serializable;

/**
 * ������������ ��������� ��������. ��� ������� ����������� � ������ ������.
 * ����� ���� �������������� �������, ������� ����� �������������� ��� �� �������������� � ������.
 * �������������� ������� ����� ��������� �� XML.
 */
public enum MainResources implements Serializable {
	/** ������� */
	INFLUENCE(1, "res_inf", "�������"),
	/** ������� */
	CREDITS(2, "res_credits", "�������");
	
	private final int code;
    private final String image;
	private final String description;
	
	private MainResources(int code, String image, String description) {
		this.code = code;
        this.image = image;
		this.description = description;
	}

	public int getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

    public String getImage() {
        return image;
    }
	
	public static String[] getStringArray() {
		MainResources[] resArr = values();
		String[] strArr = new String[resArr.length];
		for(int i = 0; i < strArr.length; i++) {
			strArr[i] = resArr[i].getDescription();
		}
		return strArr;
	}

    public static MainResources getByCode(int code) {
        for(MainResources res : values()) {
            if(res.getCode() == code) {
                return res;
            }
        }
        return null;
    }
	
	@Override
	public String toString() {
		return description;
	}
}
