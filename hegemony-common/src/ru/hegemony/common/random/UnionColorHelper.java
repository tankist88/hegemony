package ru.hegemony.common.random;

import java.util.Random;

public class UnionColorHelper {
	private static final int COLORS[] = {
		0xFFFF0000,
		0xFF4B09FF,
		0xFF249512,
		0xFFEBEB0C,
		0xFF04FDBB,
		0xFFB76507
	};
	
	public static int getColorForUnion(int num) {
		if(num < COLORS.length) {
			return COLORS[num];
		} else {
			return new Random(System.currentTimeMillis()).nextInt(0xFFFFFFFF - 1);
		}
	}
}
