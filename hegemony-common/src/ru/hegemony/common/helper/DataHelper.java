package ru.hegemony.common.helper;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class DataHelper {
	public static double round(final double val, final int digitCount) {
		double pow10PlusOne = Math.pow(10, digitCount + 1);
		
		int tmpValPlusOne = (int)(val * pow10PlusOne);
		
		String strVal = Integer.toString(tmpValPlusOne);
		
		String lastDigitStr = (new Character(strVal.charAt(strVal.length()-1))).toString();
		
		double pow10 = Math.pow(10, digitCount);
		int tmpVal = (int)(val * pow10);
		
		int lastDigit = Integer.parseInt(lastDigitStr);
		if(lastDigit >= 5) {
			tmpVal++;
		}
		
		return tmpVal/pow10;
	}
	
	private static int factorial(int input) {
		int x, fact = 1;
		for (x = input; x > 1; x--) {
			fact *= x;
		}
		return fact;
	}
	
	private static int getCombinasionCount(int n, int k) {
		return (int) ((double) factorial(n) / (double) ( (double) factorial(n - k) * (double) (factorial(k))));
	}
	
	public static int calcCombinasionCount(int itemCount, int setLength) {
		int result = 0;
		for(int i = 1; i <= itemCount; i++) {
			if(i < setLength) {
				result += getCombinasionCount(itemCount, i);
			} else {
				break;
			}
		}
		return result + getCombinasionCount(itemCount, setLength);
	}
	
	public static boolean itemListEquals(Collection<?> listOne, Collection<?> listTwo) {
		return listTwo.containsAll(listOne) && listOne.size() == listTwo.size();
	}
	
	public static Map<String, InputStream> readZip(final String filename) throws IOException {
		Map<String, InputStream> zipMap = new HashMap<String, InputStream>();
		ZipFile zipFile = new ZipFile(filename);
		Enumeration<? extends ZipEntry> entries = zipFile.entries();
		while (entries.hasMoreElements()) {
			ZipEntry entry = entries.nextElement();
			InputStream stream = zipFile.getInputStream(entry);
			zipMap.put(entry.getName(), stream);
		}
		return zipMap;
	}
	
	public static Map<String, InputStream> readZip(final InputStream is) throws IOException {
		Map<String, InputStream> zipMap = new HashMap<String, InputStream>();
		ZipInputStream zis = new ZipInputStream(is);
		byte[] buffer = new byte[4096];
		ZipEntry entry = null;
		while ((entry = zis.getNextEntry()) != null) {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			int len = 0;
			while ((len = zis.read(buffer)) > 0) {
				bos.write(buffer, 0, len);
			}
			zipMap.put(entry.getName(), new ByteArrayInputStream(bos.toByteArray()));
			bos.close();
		}
		zis.close();
		is.close();
		return zipMap;
	}
	
	public static void putToZip(final Map<String, InputStream> zipMap, final String path) throws IOException {
		FileOutputStream fos = new FileOutputStream(path);
		ZipOutputStream zos = new ZipOutputStream(fos);		
		for(Entry<String, InputStream> entry : zipMap.entrySet()) {
			putEntryToZip(entry.getKey(), zos, entry.getValue());
			entry.getValue().close();
		}
		zos.close();
		fos.close();
	}
	
	public static InputStream putToZip(final Map<String, InputStream> zipMap) throws IOException {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ZipOutputStream zos = new ZipOutputStream(bos);		
		for(Entry<String, InputStream> entry : zipMap.entrySet()) {
			putEntryToZip(entry.getKey(), zos, entry.getValue());
			entry.getValue().close();
		}
		zos.close();
		ByteArrayInputStream result = new ByteArrayInputStream(bos.toByteArray());
		bos.close();
		return result;
	}
	
	private static void putEntryToZip(final String entryName, final ZipOutputStream zos, final InputStream in) throws IOException {
		ZipEntry ze = new ZipEntry(entryName);
		zos.putNextEntry(ze);
		byte[] buffer = new byte[1024];
		int len;
		while ((len = in.read(buffer)) > 0) {
			zos.write(buffer, 0, len);
		}
		zos.closeEntry();
	}
	
	public static boolean refreshCountersWithDuplicate(int[] indexCounters, int setLength, int allowedListSize) {
		boolean needContinue = false;
		int i = setLength - 1;
		while (i >= 0 && indexCounters[i] == allowedListSize - 1) {
			indexCounters[i] = 0;
			i--;
		}
		if (i >= 0) {
			indexCounters[i]++;
			needContinue = true;
		} else {
			needContinue = false;
		}
		return needContinue;
	}
	
	public static boolean refreshCountersWithoutDuplicate(int[] indexCounters, int setLength, int allowedListSize) {
		int i = setLength - 1;
		while (i >= 0 && (indexCounters[i] + setLength - 1 - i + 1 > allowedListSize - 1)) {
			--i;
		}
		if(i < 0) {
			return false;
		}
		indexCounters[i]++;
		for (int j = i + 1; j <= setLength - 1; ++j) {
			indexCounters[j] = indexCounters[j - 1] + 1;
		}
		return true;
	}
	
	public static int[] addToIntArray(final int element, final int index, final int expansionValue, final int[] array) {
		if(index >= array.length) {
			int[] newArray = new int[array.length + expansionValue];
			for(int i = 0; i < array.length; i++) {
				newArray[i] = array[i];
			}
			newArray[index] = element;
			return newArray;
		} else {
			array[index] = element;
			return array;
		}
	}
	
	public static int[] trimToSizeIntArray(final int[] array, int nullValue) {
		int realSize = 0;
		for(int i = 0; i < array.length; i++) {
			if(array[i] == nullValue) {
				break;
			}
			realSize++;
		}
		int[] newArray = new int[realSize];
		for(int i = 0; i < newArray.length; i++) {
			newArray[i] = array[i];
		}
		return newArray;
	}
	
	public static <T> List<T> createListFromSet(Set<T> set) {
		List<T> list = new ArrayList<T>();
		for(T element : set) {
			list.add(element);
		}
		return list;
	}
	
	public static <T> Set<T> createSetFromList(List<T> list) {
		Set<T> set = new HashSet<T>();
		for(T element : list) {
			set.add(element);
		}
		return set;
	}
	
//	public static int[] decompressIntArray(int[] compressed, int size) {
//		IntegerCODEC codec = new Composition(new FastPFOR(), new VariableByte());
//		int[] recovered = new int[size];
//		IntWrapper recoffset = new IntWrapper(0);
//		codec.uncompress(compressed, new IntWrapper(0), compressed.length, recovered, recoffset);
//		return recovered;
//	}
//	
//	public static List<Integer> compressIntValues(List<Integer> orig) {
//		if(orig instanceof ArrayList) {
//			((ArrayList<Integer>) orig).trimToSize();
//		}
//		int[] data = createIntArrayFromList(orig);
//		int[] compressed = new int[orig.size() + 1024];// could need more
//		IntegerCODEC codec = new Composition(new FastPFOR(), new VariableByte());
//		IntWrapper inputoffset = new IntWrapper(0);
//		IntWrapper outputoffset = new IntWrapper(0);
//		codec.compress(data, inputoffset, data.length, compressed, outputoffset);
//		compressed = Arrays.copyOf(compressed, outputoffset.intValue());
//		return createListFromArray(compressed);
//	}
	
	public static int[] createIntArrayFromList(List<Integer> orig) {
		int[] array = new int[orig.size()];
		for(int i = 0; i < array.length; i ++) {
			array[i] = orig.get(i).intValue();
		}
		return array;
	}
	
	public static List<Integer> createListFromArray(int[] array) {
		List<Integer> list = new ArrayList<Integer>();
		for(int i = 0; i < array.length; i++) {
			list.add(array[i]);
		}
		return list;
	}
	
	//	public static void main(String args[]) {
	//	int[] arr = {-1,-1,-1,-1};
	//	int K = 4;
	//	int N = 10;
	//	
	//	int realCount = 0;
	//	boolean needContinur = true;
	//	while(needContinur) {
	//		needContinur = refreshCountersWithoutDuplicate(arr, K, N);
	//		if(!needContinur) {
	//			break;
	//		}
	//		System.out.print("[");
	//		for(int i = 0; i < arr.length; i++) {
	//			if(i < arr.length - 1) {
	//				System.out.print(arr[i] + ", ");
	//			} else {
	//				System.out.print(arr[i]);
	//			}
	//		}
	//		System.out.println("]");
	//		realCount++;
	//	}
	//	
	//	System.out.println("calc count: " + calcCount(N, K) + ", realCount: " + realCount);
	//}
	
	//public static void main(String args[]) {
	//	int[] arr = {0,0,0};
	//	boolean res = true;
	//	while(res) {
	//		res = refreshCounters(arr, 3, 3);
	//		System.out.print("[");
	//		for(int i = 0; i < arr.length; i++) {
	//			if(i < arr.length - 1) {
	//				System.out.print(arr[i] + ", ");
	//			} else {
	//				System.out.print(arr[i]);
	//			}
	//		}
	//		System.out.println("]");
	//	}
	//	System.out.println("res=" + res);
	//}
}
