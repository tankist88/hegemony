package ru.hegemony.common.helper;

public class ItemSetCompareHelper {
	private double itemSetValue;
	private double delta;
	
	public ItemSetCompareHelper(int percent, double itemSetValue) {
		this((double) percent, itemSetValue);
	}
	
	public ItemSetCompareHelper(double percent, double itemSetValue) {
		this.itemSetValue = itemSetValue;
		this.delta = (percent/100.0) * itemSetValue;
	}
	
	public boolean compareWith(double value) {
		return value - this.itemSetValue >= (-1) * Math.abs(this.delta);
	}
	
	public double getSub(double value) {
		return value - this.itemSetValue + this.delta;
	}
}
